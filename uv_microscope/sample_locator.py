'''
Created on Jun 22, 2021

@author: Sriram Sridhar
'''

import numpy as np
from skimage import transform
import os
import math
import json
import datetime


class SampleLocator:
    
    cornerCounter = 0
    #flakeDict = {}
    #chipDict = {}
    dataDict = {}
    microscopeDict = {'survey_microscope', 'xfer_station'}
    
    def setup(self):
        self.add_operation("sample calibration", self.start_calibration)
        self.add_operation("complete calibration", self.complete_calibration)
        self.add_operation("save key point", self.save_key_point)
        self.add_operation("add chip", self.add_chip)
        self.add_operation("add feature", self.add_feature)
        self.add_operation("locate feature", self.locate_feature)
        self.add_operation("List chips", self.list_chips)
        self.add_operation("List flakes", self.list_flakes)
        self.add_operation('test', self.test)

        self.settings.New("Chips", str, initial = '')
        self.settings.New("Features", str, initial = '')
        
        try:
            self.canRotate = self.app.canRotate
        except:
            self.canRotate = False
        
        self.settings.New("Key Point 1 x", float, initial=0, spinbox_decimals=6)
        self.settings.New("Key Point 1 y", float, initial=0, spinbox_decimals=6)
        self.settings.New("Key Point 2 x", float, initial=0, spinbox_decimals=6)
        self.settings.New("Key Point 2 y", float, initial=0, spinbox_decimals=6)
        self.settings.New("Key Point 3 x", float, initial=0, spinbox_decimals=6)
        self.settings.New("Key Point 3 y", float, initial=0, spinbox_decimals=6)
        self.settings.New("initial_angle", float, initial=0, spinbox_decimals=2)
        
        self.calibration_setup()
        
    def calibration_setup(self):        
        #chipsFolder = fr'C:\Users\lab\Documents\foundry_scope\Sample_Locator\chips'
        #if not os.path.exists(chipsFolder):
        #    os.mkdir(chipsFolder)
        #    with open(os.path.join(chipsFolder, "chip_dict.json"), "w") as write_file:
        #        json.dump({}, write_file)
        #        write_file.close()
        #self.update_chip_dict()
          
            
        SampleLocatorFolder = fr'C:\Users\lab\Documents\foundry_scope\Sample_Locator'
        with open(os.path.join(SampleLocatorFolder, "data.json"), "w") as write_file:
            json.dump({}, write_file)
            write_file.close()    
        self.update_data_dict()
    
    def start_calibration(self):
        self.featureDict = {}
        if self.canRotate:
            self.settings['initial_angle'] = self.app.hardware['rotation_motor'].settings['position']
        #self.status = f'Line up crosshairs with {self.corner_number} of sample, then click "save corner"'
        self.status = f'Line up crosshairs with {self.key_point_number} of sample, then type in feature name and click "save key point"'
    
    def update_calib_status(self):
        self.status = f'Line up crosshairs with {self.key_point_number} of sample, then type in feature name and click "save key point"'

    def save_key_point(self):
        if self.settings["Features"] == '':
            self.status = 'Type in a name for the key point, then click "save key point"'
        elif self.settings["Features"] in self.featureDict.keys():
            self.status = 'Key point name already exists on this chip.\nType in a new name for the key point, then click "save key point"'
        else:
            x, y, z = self.get_current_stage_position()
            self.settings[f'{self.key_point_number} x'] = x
            self.settings[f'{self.key_point_number} y'] = y
            self.featureDict.update({self.settings["Features"]: [x, y]})
            self.status = 'Choose next key point to calibrate with or click "complete calibration"'
        
    def complete_calibration(self):
        self.status = 'Calibration complete: Type in chip name and click add chip.\nType in feature name and click add feature or locate feature.'
    
    #def add_chip(self):
    #    #self.update_chip_dict()
    #    if self.settings["Chips"] == '':
    #        self.status = 'Please enter a chip name before saving chip'
    #    elif self.settings["Chips"] in self.chipDict:
    #        self.status = f'{self.settings["Chips"]} already exists.\nType in a flake name and click "Add feature" to establish a feature or click "Locate feature"'
    #    else:
    #        self.append_chip()
    #        
    #        #chipFolder = fr'C:\Users\lab\Documents\foundry_scope\Sample_Locator\chips\{self.settings["Chips"]}'
    #        #if not os.path.exists(chipFolder):
    #        #    os.mkdir(chipFolder)
    #        #    with open(os.path.join(chipFolder, "flake_dict.json"), "w") as write_file:
    #        #        json.dump({}, write_file)
    #        #        write_file.close()
    #        
    #        self.status = f'{self.settings["Chips"]} added to chip dictionary.\nType in a feature name and click "Add feature" to establish a feature or click "Locate feature"' 
                    
    '''def add_flake(self):
        if self.settings["Flakes"] == '':
            self.status = 'Please enter a flake name'
        elif self.settings["Chips"] == '':
            self.status = 'Please enter a chip name'
        else:
            self.append_flake()'''
        
    def locate_flake(self):
        #find original plane using x,y coordinates of corners from different setup
        #translate that plane to current plane using x,y coordinates of corners from current setup
        #locate original flake from different setup on current setup by translating original point to current plane
        self.update_chip_dict()
        self.update_flake_dict()
        if self.settings["Chips"] not in self.chipDict:
            self.status = 'This chip does not exist for the specified microscope, please enter a new chip name'
        elif self.settings["Flakes"] not in self.flakeDict:
            self.status = 'This flake does not exist for the specified chip, please enter a new flake name'
        else:
            origFlakeX, origFlakeY = self.flakeDict[self.settings["Flakes"]]
            origFlakeX, origFlakeY = float(origFlakeX), float(origFlakeY)
            currFlakeX, currFlakeY = self.transform_flake_to_curr()

            if self.canRotate == True:
                currAngle = math.degrees(math.acos(np.array(self.matrix_transform().params)[0][0]))
                angleShift = self.app.hardware['rotation_motor'].settings['position'] - self.settings['initial_angle']
                #for method2
                angleShift = angleShift%360
                if math.fabs(angleShift) > 180:
                    if angleShift < 0:
                        angleShift = 360-math.fabs(angleShift)
                    else:
                        angleShift = -(360-angleShift)
                '''#for method1
                currAngle += angleShift
                angleDif = currAngle%360
                if math.fabs(angleDif) > 180:
                    if angleDif < 0:
                        angleRot = 360-math.fabs(angleDif)
                    else:
                        angleRot = -(360-angleDif)
                else:
                    angleRot = angleDif'''

                #method below changes angle to original chip angle and moves to flake pos in orig coord system
                #self.app.hardware['rotation_motor'].settings['target_position'] = self.app.hardware['rotation_motor'].settings['position']-angleRot
                #self.set_stage_position(origFlakeX, origFlakeY)
                
                #method below changes angle to curr chip angle (if angleShift has occurred) and moves to flake pos in curr coord system
                self.app.hardware['rotation_motor'].settings['target_position'] = self.app.hardware['rotation_motor'].settings['position'] - angleShift
                self.set_stage_position(currFlakeX, currFlakeY)
            else:
                self.set_stage_position(currFlakeX, currFlakeY)
            
            self.status = f'{self.settings["Flakes"]} located'
    
    

    def locate_feature(self):
        self.update_data_dict()
        if self.settings["Chips"] not in self.dataDict.keys():
            self.status = 'This chip does not exist for the specified microscope, please enter a new chip name'
            return
        pointsList = list(self.dataDict.get(self.settings["Chips"]).get('points').keys())
        if self.settings["Features"] not in pointsList:
            self.status = 'This feature does not exist for the specified chip, please enter a new feature name'
            return
        dateList = list(self.dataDict.get(self.settings["Chips"]).get('points').get(self.settings["Features"]).keys())
        dateList.sort()
        origFlakeX, origFlakeY = self.dataDict.get(self.settings["Chips"]).get('points').get(self.settings["Features"]).get(dateList[0]).get('coordinates')
        origFlakeX, origFlakeY = float(origFlakeX), float(origFlakeY)
        currFlakeX, currFlakeY = self.transform_feature_to_curr()
    
        if self.canRotate == True:
                currAngle = math.degrees(math.acos(np.array(self.matrix_transform().params)[0][0]))
                angleShift = self.app.hardware['rotation_motor'].settings['position'] - self.settings['initial_angle']
                #for method2
                angleShift = angleShift%360
                if math.fabs(angleShift) > 180:
                    if angleShift < 0:
                        angleShift = 360-math.fabs(angleShift)
                    else:
                        angleShift = -(360-angleShift)
                '''#for method1
                currAngle += angleShift
                angleDif = currAngle%360
                if math.fabs(angleDif) > 180:
                    if angleDif < 0:
                        angleRot = 360-math.fabs(angleDif)
                    else:
                        angleRot = -(360-angleDif)
                else:
                    angleRot = angleDif'''

                #method below changes angle to original chip angle and moves to flake pos in orig coord system
                #self.app.hardware['rotation_motor'].settings['target_position'] = self.app.hardware['rotation_motor'].settings['position']-angleRot
                #self.set_stage_position(origFlakeX, origFlakeY)
                
                #method below changes angle to curr chip angle (if angleShift has occurred) and moves to flake pos in curr coord system
                self.app.hardware['rotation_motor'].settings['target_position'] = self.app.hardware['rotation_motor'].settings['position'] - angleShift
                self.set_stage_position(currFlakeX, currFlakeY)
        else:
            self.set_stage_position(currFlakeX, currFlakeY)
        self.status = f'{self.settings["Features"]} located'
    
    
    
    
    
    
    #returns matrix transform from original coord system to current coord system
    def matrix_transform(self):
        #origCx1, origCy1, origCx2, origCy2, origCx3, origCy3 = self.chipDict[self.settings["Chips"]]
        key_points = self.dataDict.get(self.settings["Chips"]).get('coordinate_systems').get(self.coord_sys_name).get('key_points')
        points = self.dataDict.get(self.settings["Chips"]).get('points')
        coordDict = {}
        #this implementation considers the earliest measurement of key points as the original coord system (can change so that a specified coord_sys is the 'original')
        for pt in key_points:
            dateList = list(points.get(pt).keys())
            dateList.sort()
            x, y = points.get(pt).get(dateList[0]).get('coordinates')
            coordDict[pt] = [x, y]
        #for now, going to consider corner order based on how the dictionary is ordered (first corner = first feature established)
        coordList = list(coordDict.keys())
        origCx1, origCy1 = coordDict.get(coordList[0])[:2]
        origCx2, origCy2 = coordDict.get(coordList[1])[:2]
        origCx3, origCy3 = coordDict.get(coordList[2])[:2]    
        
        currCx1, currCy1 = self.settings["Key Point 1 x"], self.settings["Key Point 1 y"]
        currCx2, currCy2 = self.settings["Key Point 2 x"], self.settings["Key Point 2 y"]
        currCx3, currCy3 = self.settings["Key Point 3 x"], self.settings["Key Point 3 y"]
        
        chipOrig = np.array([origCx1, origCy1, origCx2, origCy2, origCx3, origCy3]).astype(np.float).reshape((3,2))
        chipCurr = np.array([currCx1, currCy1, currCx2, currCy2, currCx3, currCy3]).astype(np.float).reshape((3,2))
        
        T = transform.estimate_transform('affine', chipOrig, chipCurr)
        return T
        
    
    '''def transform_flake_to_curr(self):
        self.update_flake_dict()
        origFlakeX, origFlakeY = self.flakeDict[self.settings["Flakes"]]
        T = self.matrix_transform()
        origFlakeCoord = np.array([origFlakeX, origFlakeY]).astype(np.float).reshape(1,2)
        currFlakeCoord = T(origFlakeCoord)
        currFlakeX, currFlakeY = currFlakeCoord[0][0], currFlakeCoord[0][1]
        return currFlakeX, currFlakeY
    '''
    
    
    def transform_feature_to_curr(self):
        dateList = list(self.dataDict.get(self.settings["Chips"]).get('points').get(self.settings["Features"]).keys())
        dateList.sort()
        origFlakeX, origFlakeY = self.dataDict.get(self.settings["Chips"]).get('points').get(self.settings["Features"]).get(dateList[0]).get('coordinates')
        T = self.matrix_transform()
        origFlakeCoord = np.array([origFlakeX, origFlakeY]).astype(np.float).reshape(1,2)
        currFlakeCoord = T(origFlakeCoord)
        currFlakeX, currFlakeY = currFlakeCoord[0][0], currFlakeCoord[0][1]
        return currFlakeX, currFlakeY
        
        
    
    def transform_feature_to_orig(self):
        T = self.matrix_transform()
        currFlakeX, currFlakeY, currFlakeZ = self.get_current_stage_position()
        #if user rotates after establishing chip, we must account for the angle shift
        #we use rotate about current pos math to transform current coordinates to coordinates on currently established chip
        if (self.canRotate):
            angleShift = self.app.hardware['rotation_motor'].settings['position'] - self.settings['initial_angle']
            new_pos = self.get_rotated_stage_position(currFlakeX, currFlakeY, currFlakeZ, -angleShift)
            currFlakeX, currFlakeY, currFlakeZ = new_pos[0], new_pos[1], new_pos[2]
        currFlakeCoord = np.array([currFlakeX, currFlakeY, 1]).astype(np.float).reshape(3,1)
        origFlakeCoord = np.linalg.inv(np.array(T.params)) @ currFlakeCoord
        origFlakeX, origFlakeY = origFlakeCoord[0][0], origFlakeCoord[1][0]
        return origFlakeX, origFlakeY
    
    
    def list_chips(self):
        self.update_chip_dict()
        if len(self.chipDict) == 0:
            print('No chips currently saved, please calibrate new chips')
        else:
            print(self.chipDict.keys())
            
    
    def list_flakes(self):
        chipName = self.settings["Chips"]
        
        if chipName not in self.chipDict:
            print('Chip name not found in dictionary')
        else:
            self.update_flake_dict()
            if len(self.flakeDict) == 0:
                print('No flakes recorded for this chip, please locate flakes')
            else:
                print(self.flakeDict.keys())
    
    #save corner info every time not just when chip is established for first time
    def add_chip(self):
        if self.settings["Chips"] == '':
            self.status = 'Please enter a chip name before saving chip'
        #elif self.settings["Chips"] in self.chipDict:
        #    self.status = f'{self.settings["Chips"]} already exists.\nType in a flake name and click "Add feature" to establish a feature or click "Locate feature"'
        else:
            #self.chipFilePath = fr'C:\Users\lab\Documents\foundry_scope\Sample_Locator\chips\chip_dict.json'
            self.dataFilePath = fr'C:\Users\lab\Documents\foundry_scope\Sample_Locator\data.json'
            
            '''#!!!need a way for users to name these key points
            currCx1, currCy1 = self.settings["Corner 1 x"], self.settings["Corner 1 y"]
            currCx2, currCy2 = self.settings["Corner 2 x"], self.settings["Corner 2 y"]
            currCx3, currCy3 = self.settings["Corner 3 x"], self.settings["Corner 3 y"]'''
            
            ##dictionary with corner names,xy as key
            #self.chipDict[self.settings["Chips"]] = [currCx1, currCy1, currCx2, currCy2, currCx3, currCy3]
            # 
            #with open(self.chipFilePath, "w") as write_file:
            #    json.dump(self.chipDict, write_file)
            #    write_file.close()
            
            if self.dataDict.get(self.settings["Chips"]) != None:
                coord_sys_list = list(self.dataDict.get(self.settings["Chips"]).get('coordinate_systems').keys())
                coord_sys_list.sort()
                self.coord_sys_name = 'coord_sys_' + str(int(coord_sys_list[-1][-1:]) + 1)
                self.dataDict.get(self.settings["Chips"]).get('coordinate_systems').update( 
                                                                    {self.coord_sys_name: 
                                                                        {'key_points': list(self.featureDict.keys()),
                                                                         'transforms': None
                                                                        }
                                                                    })
            else:
                self.coord_sys_name = 'coord_sys_1'
                self.dataDict[self.settings["Chips"]] = {'coordinate_systems': 
                                                                {self.coord_sys_name: 
                                                                    {'key_points': list(self.featureDict.keys()),
                                                                     'transforms': None
                                                                    }
                                                                }, 
                                                             'points': {},
                                                             'img': {}
                                                        }
            
            x = datetime.datetime.now()
            date = x.strftime("%x, %X")
            
            points = self.dataDict.get(self.settings["Chips"]).get('points')
            for x in list(self.featureDict.keys()):
                if points.get(x) != None:
                    points.get(x).update({date:
                                            {'coordinates': self.featureDict[x],
                                             'coord_sys': self.coord_sys_name
                                            }
                                        })
                else:
                    points[x] = {date:
                                    {'coordinates': self.featureDict[x],
                                     'coord_sys': self.coord_sys_name
                                    }
                                }
            
            #not working since AffineTransform object is not JSON serializable
            #self.dataDict.get(self.settings["Chips"]).get('coordinate_systems').get(self.coord_sys_name)['transforms'] = self.matrix_transform()
            
            with open(self.dataFilePath, "w") as write_file:
                json.dump(self.dataDict, write_file)
                write_file.close()
            
            self.status = f'{self.settings["Chips"]} added to chip dictionary.\nType in a feature name and click "Add feature" to establish a feature or click "Locate feature"'

        
    #def append_flake(self):
    #    self.update_flake_dict()
    #    if self.settings["Flakes"] in self.flakeDict:
    #        self.status = f'{self.settings["Flakes"]} already exists on this chip, please enter a new flake name'
    #        return
    #    
    #    #transform current flake coords to original coord system and store original system coords
    #    origFlakeX, origFlakeY = self.transform_flake_to_orig()
    #    
    #    self.update_chip_dict()
    #    self.flakeDict[self.settings["Flakes"]] = [origFlakeX, origFlakeY]
    #    
    #    self.flakeFilePath = fr'C:\Users\lab\Documents\foundry_scope\Sample_Locator\chips\{self.settings["Chips"]}\flake_dict.json'
    #    with open(self.flakeFilePath, "w") as write_file:
    #        json.dump(self.flakeDict, write_file)
    #        write_file.close()
    #
    #    self.status = f'{self.settings["Flakes"]} added to flake dictionary'
       
        
        
        
        
    def add_feature(self):    
        pointsList = list(self.dataDict.get(self.settings["Chips"]).get('points').keys())
        if self.settings["Features"] == '':
            self.status = 'Please enter a feature name'
        elif self.settings["Chips"] == '':
            self.status = 'Please enter a chip name'
        elif self.settings["Features"] in pointsList:
            self.status = f'{self.settings["Features"]} already exists on this chip, please enter a new feature name'
        else:
            self.dataFilePath = fr'C:\Users\lab\Documents\foundry_scope\Sample_Locator\data.json'
            x = datetime.datetime.now()
            date = x.strftime("%x, %X")
            #transform current flake coords to original coord system and store original system coords
            origFlakeX, origFlakeY = self.transform_feature_to_orig()
            
            points = self.dataDict.get(self.settings["Chips"]).get('points')
            if points.get(self.settings["Features"]) != None:
                points.get(self.settings["Features"]).update({date: 
                                                                {'coordinates': [origFlakeX, origFlakeY],
                                                                 'coord_sys': self.coord_sys_name
                                                                }
                                                            })
            else:
                points[self.settings["Features"]] = {date: 
                                                        {'coordinates': [origFlakeX, origFlakeY],
                                                         'coord_sys': self.coord_sys_name
                                                        }
                                                    }
                
            with open(self.dataFilePath, "w") as write_file:
                json.dump(self.dataDict, write_file)
                write_file.close()

            self.status = f'{self.settings["Features"]} added to feature dictionary'
            
        
        
        
    def update_data_dict(self):
        self.dataFilePath = fr'C:\Users\lab\Documents\foundry_scope\Sample_Locator\data.json'
        with open(self.dataFilePath, "r") as read_file:
            self.dataDict = json.load(read_file)
            read_file.close()
    
        
    def update_chip_dict(self):
        self.chipFilePath = fr'C:\Users\lab\Documents\foundry_scope\Sample_Locator\chips\chip_dict.json'
        with open(self.chipFilePath, "r") as read_file:
            self.chipDict = json.load(read_file)
            read_file.close()
    
    
    def update_flake_dict(self):
        self.flakeFilePath = fr'C:\Users\lab\Documents\foundry_scope\Sample_Locator\chips\{self.settings["Chips"]}\flake_dict.json'
        with open(self.flakeFilePath, "r") as read_file:
            self.flakeDict = json.load(read_file)
            read_file.close()    
            
    
    def test(self):
        #print(self.settings['initial_angle'])
        #print(math.degrees(math.acos(np.array(self.matrix_transform().params)[0][0])))
        #print(self.app.hardware['rotation_motor'].settings['position'])
        #print(self.matrix_transform().params)
        
        #data = {1:1, 2:2}
        #with open(fr'C:\Users\lab\Documents\foundry_scope\{self.settings["Microscope"]}\data.json', "w") as write_file:
        #    json.dump(data, write_file)
        #    write_file.close()
        #
        #with open(fr'C:\Users\lab\Documents\foundry_scope\{self.settings["Microscope"]}\data.json', "r") as read_file:
        #    data2 = json.load(read_file)
        #    read_file.close()
        # 
        #print(data2)
        
        #self.status = 'TESTING STATUS'
        #T = self.matrix_transform()
        #print(T)
        #origCx1, origCy1, origCx2, origCy2, origCx3, origCy3 = self.chipDict[self.settings["Chips"]]
        #origArr = np.array([origCx1, origCy1, origCx2, origCy2, origCx3, origCy3]).astype(np.float).reshape(3,2)
        #origArr = np.array([origCx1, origCy1]).astype(np.float).reshape(1,2)
        #currCx1, currCy1 = self.settings["Corner 1 x"], self.settings["Corner 1 y"]
        #currArr = np.array([currCx1, currCy1, 1]).astype(np.float).reshape(3,1)
        #print(T(origArr))
        #print()
        
        #self.settings["Chips"] = 'test'
        #print(self.settings.Chips)
        
        self.status = ''
    
    
    
    
    
