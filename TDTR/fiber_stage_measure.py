from ScopeFoundry import Measurement
import numpy as np
import time
import pyqtgraph as pg

class FiberStageMeasure(Measurement):
    
    name = 'fiber_stage'
    
#     def __init__(self, app, name=None, hw_name='elliptec'):
#         self.hw_name = hw_name
#         Measurement.__init__(self, app, name=name)
#         self.hw = self.app.hardware[self.hw_name]
        
    def setup(self):
        self.piezo = self.app.hardware['thorlabs_benchtop_piezo']
        self.lockin = self.app.hardware['TDTR_Lockin']
        
#         self.settings.New('step_size', dtype=float, unit='um', initial = 0.1)

    def setup_figure(self):
        self.ui = self.plot = pg.PlotWidget()
        
      
#     def find_max_old(self, axis, range_to_test = 20, coarse_step = 1, fine_step = 0.1):
#         """finds the position of the fiber stage that gives maximum signal of the lock-in aux input
#         checks within range_to_test of current position"""
#         
#         assert axis in self.piezo.ax_names
#         
#         if not self.app.hardware['TDTR_Lockin'].settings['connected']:
#             print("Please enable lock-in hardware.")
#             return -1
#         #set up lock-in for measurement (if not done)
#         if not hasattr(self.lockin, 'h_dAM'): #if a data acquisition model doesn't exist yet, create one
#             h = self.lockin.create_dataAcquisitionModule()
#         else: #if it does exist, use it
#             h = self.lockin.h_dAM
#         self.lockin.subscribe_dataAcquisitionModule(h) #set the lock-in to report the signals useful for measurement:r, x, y, auxin0, auxin1 (at the moment)
#         
#         start_pos = self.piezo.settings[axis+'_position']
#         min_pos = max(0, start_pos-range_to_test)
#         max_pos = min(20, start_pos+range_to_test)  
#         
#         print(min_pos, max_pos, coarse_step)
#         pos_to_test = np.arange(min_pos, max_pos, coarse_step)
# #         print(pos_to_test)
#         mags = np.zeros_like(pos_to_test)
#         for i, position in enumerate(pos_to_test):
#             if self.interrupt_measurement_called:
#                 break
#             self.piezo.settings[axis+'_target_position'] = position
#             time.sleep(0.2)
#             mags[i] = self.lockin.read_single(0.1, vals_expected = ['auxin1'])[0] #aux ref intensity
#         
# #         print(mags)
#         max_index = np.argmax(mags)
#         coarse_max_pos = pos_to_test[max_index]
#         print(coarse_max_pos)
#         
#         pos_to_test_2 = np.arange(coarse_max_pos - coarse_step, coarse_max_pos + 2*coarse_step, fine_step)
#         mags_2 = np.zeros_like(pos_to_test_2)
#         for i, position in enumerate(pos_to_test_2):
#             if self.interrupt_measurement_called:
#                 break
#             self.piezo.settings[axis+'_target_position'] = position
#             time.sleep(0.01)
#             mags_2[i] = self.lockin.read_single(0.1, vals_expected = ['auxin1'])[0] #aux ref intensity
#         max_index = np.argmax(mags_2)
#         
#         return pos_to_test_2[max_index]
    
    def find_max(self, axis, range_to_test = 20, coarse_step = 0.2, fine_step = 0.05):
        """finds the position of the fiber stage that gives maximum signal of the lock-in aux input
        checks within range_to_test of current position"""
        
        assert axis in self.piezo.ax_names
        
        if not self.app.hardware['TDTR_Lockin'].settings['connected']:
            print("Please enable lock-in hardware.")
            return -1
        if not self.app.hardware['thorlabs_benchtop_piezo'].settings['connected']:
            print("Please enable benchtop piezo hardware.")
            return -1
        #set up lock-in for measurement (if not done)
        if not hasattr(self.lockin, 'h_dAM'): #if a data acquisition model doesn't exist yet, create one
            h = self.lockin.create_dataAcquisitionModule()
        else: #if it does exist, use it
            h = self.lockin.h_dAM
        self.lockin.subscribe_dataAcquisitionModule(h) #set the lock-in to report the signals useful for measurement:r, x, y, auxin0, auxin1 (at the moment)
        
        start_pos = self.piezo.settings[axis+'_position']
        min_pos = max(0, start_pos-range_to_test)
        max_pos = min(20, start_pos+range_to_test)  
        
        print("min, max, step", min_pos, max_pos, coarse_step)
        pos_to_test = np.arange(min_pos, max_pos, coarse_step)
        print(pos_to_test)
        self.mags = np.zeros_like(pos_to_test)
        self.positions = np.zeros_like(pos_to_test)
        for i, position in enumerate(pos_to_test):
#             print(i)
#             if self.interrupt_measurement_called:
#                 break
            self.piezo.settings[axis+'_target_position'] = position
#             print("setting to", position)
#             time.sleep(0.2)
            self.mags[i] =  self.lockin.read_single(0.01, vals_expected = ['auxin1'])[0] #aux ref intensity
            self.piezo.read_position_from_hardware()
            self.positions[i] = self.piezo.settings[axis+'_position']
        
        print(self.mags)
        print(self.positions)
        
        max_index = np.argmax(self.mags)
        coarse_max_pos = self.positions[max_index]
        print(coarse_max_pos)
        
        if coarse_step > 0.2:
        
            min_pos = max(0, coarse_max_pos - coarse_step)
            max_pos = min(20,  coarse_max_pos + 2*coarse_step)
            pos_to_test_2 = np.arange(min_pos, max_pos, fine_step)
            self.mags = np.zeros_like(pos_to_test_2)
            self.positions = np.zeros_like(pos_to_test_2)
            for i, position in enumerate(pos_to_test_2):
                if self.interrupt_measurement_called:
                    break
                self.piezo.settings[axis+'_target_position'] = position
#                time.sleep(0.01)
                self.mags[i] =  self.lockin.read_single(0.01, vals_expected = ['auxin1'])[0] #aux ref intensity
                self.piezo.read_position_from_hardware()
                self.positions[i] = self.piezo.settings[axis+'_position']
            max_index = np.argmax(self.mags)
        
            print(self.mags)
            print(self.positions)
            
         
        return self.positions[max_index]
#         return coarse_max_pos
    
    def set_to_max(self, axis, range_to_test = 20, coarse_step = 0.2, fine_step = 0.05):
        max_loc = self.find_max(axis, range_to_test = range_to_test, coarse_step = coarse_step, fine_step = fine_step)
        self.piezo.settings[axis+'_target_position'] = max_loc
            
    def set_all_to_max(self, range_to_test = 3, repeats = 2, coarse_step = 0.2, fine_step = 0.05):
        for i in range(repeats):
            for axis in self.piezo.ax_names:
                if axis == 'x':
                    print("x")
                    self.set_to_max(axis, range_to_test = 20, coarse_step = 1, fine_step = 0.2)
                else:
                    print("not x")
                    self.set_to_max(axis, range_to_test = range_to_test, coarse_step = coarse_step, fine_step = fine_step)
        
        
    def find_val(self, axis, val_to_find, range_to_test = 20, coarse_step = 1, fine_step = 0.1):
        """finds the position of the fiber stage that gives a specific value of the lock-in aux input
        checks within range_to_test of current position. probably a bad idea to use"""
        
        assert axis in self.piezo.ax_names
        
        if not self.app.hardware['TDTR_Lockin'].settings['connected']:
            print("Please enable lock-in hardware.")
            return -1
        if not self.app.hardware['thorlabs_benchtop_piezo'].settings['connected']:
            print("Please enable benchtop piezo hardware.")
            return -1
        #set up lock-in for measurement (if not done)
        if not hasattr(self.lockin, 'h_dAM'): #if a data acquisition model doesn't exist yet, create one
            h = self.lockin.create_dataAcquisitionModule()
        else: #if it does exist, use it
            h = self.lockin.h_dAM
        self.lockin.subscribe_dataAcquisitionModule(h) #set the lock-in to report the signals useful for measurement:r, x, y, auxin0, auxin1 (at the moment)
        
        start_pos = self.piezo.settings[axis+'_position']
        min_pos = max(0, start_pos-range_to_test)
        max_pos = min(20, start_pos+range_to_test)  
        
        print(min_pos, max_pos, coarse_step)
        pos_to_test = np.arange(min_pos, max_pos, coarse_step)
#         print(pos_to_test)
        self.mags = np.zeros_like(pos_to_test)
        self.positions = np.zeros_like(pos_to_test)
        for i, position in enumerate(pos_to_test):
            if self.interrupt_measurement_called:
                break
            self.piezo.settings[axis+'_target_position'] = position
#             time.sleep(0.2)
            self.mags[i] =  self.lockin.read_single(0.01, vals_expected = ['auxin1'])[0] #aux ref intensity
            self.piezo.read_position_from_hardware()
            self.positions[i] = self.piezo.settings[axis+'_position']
        
#         print(self.mags)
#         print(self.positions)
        distance_from_val = self.mags - np.ones_like(pos_to_test)*val_to_find
        distance_sq = distance_from_val**2
        best_match_index = np.argmin(distance_sq)
        coarse_match_pos = self.positions[best_match_index]
        print(coarse_match_pos)
        
        if coarse_step > 0.2:
        
            min_pos = max(0, coarse_match_pos - coarse_step)
            max_pos = min(20,  coarse_match_pos + 2*coarse_step)
            pos_to_test_2 = np.arange(min_pos, max_pos, fine_step)
            self.mags = np.zeros_like(pos_to_test_2)
            self.positions = np.zeros_like(pos_to_test_2)
            for i, position in enumerate(pos_to_test_2):
                if self.interrupt_measurement_called:
                    break
                self.piezo.settings[axis+'_target_position'] = position
#                time.sleep(0.01)
                self.mags[i] =  self.lockin.read_single(0.01, vals_expected = ['auxin1'])[0] #aux ref intensity
                self.piezo.read_position_from_hardware()
                self.positions[i] = self.piezo.settings[axis+'_position']
            distance_from_val = self.mags - np.ones_like(pos_to_test)*val_to_find
            distance_sq = distance_from_val**2
            best_match_index = np.argmin(distance_sq)
        
#             print(self.mags)
#             print(self.positions)
            
         
        return self.positions[best_match_index]
#         return coarse_max_pos

    def set_to_val(self, axis, val_to_set, range_to_test = 20, coarse_step = 0.2, fine_step = 0.05):
        best_loc = self.find_val(axis, val_to_set, range_to_test = range_to_test, coarse_step = coarse_step, fine_step = fine_step)
        self.piezo.settings[axis+'_target_position'] = best_loc
        
    def run(self):
        self.set_all_to_max()
#         self.set_to_max('x')
        pass
        
    def update_display(self):
#         self.plotline.setData(r_array) #data
        self.plot.clear()
        self.plot.plot(self.positions, self.mags)
#         print("plotting")
                
    