from ScopeFoundry import BaseMicroscopeApp
from ScopeFoundryHW.newport_esp300.esp300_xyz_stage_hw import ESP300XYZStageHW
# from ScopeFoundryHW.newport_esp300.esp300_xyz_control_measure import ESP300XYZStageControlMeasure
from TDTR.delay_stage_measure import DelayStageMeasure
from TDTR.tdtr_lockin_hw import TDTR_LockinHW
from ScopeFoundryHW.thorcam.thorcam_hw import ThorCamHW
from ScopeFoundryHW.linkam_thermal_stage.linkam_temperature_controller import LinkamControllerHC
from ScopeFoundryHW.thorlabs_elliptec.elliptec_hw import ThorlabsElliptecSingleHW
from ScopeFoundryHW.thorcam.thorcam_capture import ThorCamCaptureMeasure
from TDTR.lockin_oscope_measure import LockinOscopeMeasure
from TDTR.time_sweep_measure import TimeSweepMeasure
from ScopeFoundryHW.shutter_servo_arduino.shutter_servo_arduino_hc import ShutterServoHW
from ScopeFoundry.helper_funcs import sibling_path, load_qt_ui_file
from TDTR.freq_sweep_measure import FrequencySweepMeasure
from TDTR.elliptec_ND_measure import ElliptecNDMeasure
from TDTR.fiber_stage_measure import FiberStageMeasure
from TDTR.auto_align_test import AutoAlign
from TDTR.pump_probe_align_dashboard import PumpProbeAlignDashboard
from TDTR.autoalign_quad import AutoalignQuad
from ScopeFoundryHW.thorlabs_kinesis.thorlabs_benchtop_piezo_hw import ThorlabsBenchtopPiezoHW

class TDTRMicroscopeApp(BaseMicroscopeApp):

    name = 'tdtr_microscope'
    
    def setup(self):
        
        self.add_quickbar(load_qt_ui_file(sibling_path(__file__, 'tdtr_quick_access.ui')))
        
        ## Hardware
        print("Adding Hardware Components")
        
        lockin = self.add_hardware(TDTR_LockinHW(self))
        stage_hw = self.add_hardware(ESP300XYZStageHW(self, ax_names='__z'))
        beamblock = self.add_hardware(ShutterServoHW(self))
        fiber_stage_hw = self.add_hardware(ThorlabsBenchtopPiezoHW(self))
        self.add_hardware(ThorCamHW(self))
        self.add_hardware(LinkamControllerHC(self))
        elliptec = self.add_hardware(ThorlabsElliptecSingleHW(self))
        
        from ScopeFoundryHW.nidaqmx.ni_ai_hw import NI_AI_HW
        quad_detector = self.add_hardware(NI_AI_HW(self, name = "quad_detectors",
                                         channels=[("ai4", "bias_neg"), 
                                                   ("ai0", "bias_pos"),
                                                   ("ai2", "det1_sum"), 
                                                   ("ai1", 'det1_X'),
                                                   ("ai5", 'det1_Y')]))
        quad_detector.settings['input_mode'] = "RSE"
        quad_detector.settings['ni_device'] = 'Dev1'      
        
        self.hardware['shutter_servo'].settings['port'] = 'COM4'
        self.hardware['elliptec'].settings['port'] = 'COM3'
        
        ## Measurements
        #self.add_measurement(ESP300XYZStageControlMeasure(self))
        delay_stage = self.add_measurement(DelayStageMeasure(self))
        
        self.add_measurement(ThorCamCaptureMeasure(self))
        
        self.add_measurement(LockinOscopeMeasure(self))
        
        fiber_stage = self.add_measurement(FiberStageMeasure(self))
        
        time_sweep = self.add_measurement(TimeSweepMeasure(self))
        
        self.add_measurement(FrequencySweepMeasure(self))
        
        elliptec_ND = self.add_measurement(ElliptecNDMeasure(self))
        
        self.add_measurement(AutoAlign(self))
        
        self.add_measurement(PumpProbeAlignDashboard(self))
        
        ## Quickbar connections
        
        Q = self.quickbar
        
        #Stage
        def move_to_time(time):
            delay_stage.settings['target_delay_time'] = time
        def move_to_pos(position):
            stage_hw.settings['z_target_position'] = position
        
        delay_stage.settings.target_delay_time.connect_to_widget(Q.target_td_doubleSpinBox)
        delay_stage.settings.current_delay_time.connect_to_widget(Q.curr_td_doubleSpinBox) 
        delay_stage.settings.offset.connect_to_widget(Q.offset_doubleSpinBox)
        Q.goto_min_pushButton.clicked.connect(lambda: move_to_pos(250))
        Q.goto_neg10_pushButton.clicked.connect(lambda: move_to_time(-10))
        Q.goto_pos10_pushButton.clicked.connect(lambda: move_to_time(10))
        Q.goto_max_pushButton.clicked.connect(lambda: move_to_pos(0))
        
        #Lock-in
        lockin.settings.input_range.connect_to_widget(Q.in_range_doubleSpinBox)
        lockin.settings.time_constant.connect_to_widget(Q.tc_doubleSpinBox)
        lockin.settings.filter_order.connect_to_widget(Q.filter_order_doubleSpinBox)
        lockin.settings.range_util.connect_to_widget(Q.range_use_progressBar)
        
        #Fiber stage
#         Q.optimize_pushButton.clicked.connect(lambda: fiber_stage.set_all_to_max())
        Q.optimize_pushButton.clicked.connect(fiber_stage.run)
        fiber_stage_hw.settings.x_position.connect_to_widget(Q.x_pos_doubleSpinBox)
        fiber_stage_hw.settings.y_position.connect_to_widget(Q.y_pos_doubleSpinBox)
        fiber_stage_hw.settings.z_position.connect_to_widget(Q.z_pos_doubleSpinBox)
        fiber_stage_hw.settings.x_target_position.connect_to_widget(Q.x_target_doubleSpinBox)
        fiber_stage_hw.settings.y_target_position.connect_to_widget(Q.y_target_doubleSpinBox)
        fiber_stage_hw.settings.z_target_position.connect_to_widget(Q.z_target_doubleSpinBox)
        
        #Elliptec stage
        elliptec.settings.position.connect_to_widget(Q.pos_doubleSpinBox)
        Q.balance_coarse_pushButton.clicked.connect(elliptec_ND.move_coarse)
        Q.balance_fine_pushButton.clicked.connect(elliptec_ND.move_fine)
        lockin.settings.mag_compare.connect_to_widget(Q.rel_mag_horizontalSlider)
        
        #Beamblock
        beamblock.shutter_open.connect_to_widget(Q.beamblock_checkBox)
        
        # Picomotors
        from ScopeFoundryHW.picomotor.picomotor_hw import PicomotorHW
        self.add_hardware(PicomotorHW(self))#, name='picomotor'))
        
        
        from ScopeFoundryHW.picomotor.picomotor_control_panel import PicoMotorControlPanel
        self.add_measurement(PicoMotorControlPanel(self, hw=['picomotor']))
        
        
        
        self.add_measurement(AutoalignQuad(self))
        
        
        #attempt at lock-in poll lockout
        self.lockin_polling = False
        
        self.settings_load_ini("tdtr_defaults.ini")
        
        
if __name__ == '__main__':
    import sys
    app = TDTRMicroscopeApp(sys.argv)
    sys.exit(app.exec_())
