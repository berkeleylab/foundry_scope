from ScopeFoundry import Measurement
import time
import pyqtgraph as pg
import numpy as np
from ScopeFoundry import  h5_io 
# from ScopeFoundry.helper_funcs import load_qt_ui_file, sibling_path

class FrequencySweepMeasure(Measurement):
    
    name = 'frequency_sweep'
    
    def setup(self):
        
        self.settings.New('start_freq', dtype=float, si=True, initial='0.1e6', unit='Hz')
        self.settings.New('stop_freq', dtype=float, si=True, initial='10e6', unit='Hz')
        self.settings.New('data_points', dtype=int, initial='50')
        self.settings.New('logarithmic', dtype=bool, initial=True) #False for linear
        self.settings.New('settling_threshold', dtype=float, initial='1e-4', unit='%', si=True) 
            # filter setting--will wait long enough to reach this remaining proportion of an incoming step function
        self.settings.New('averaging_tc_count', dtype=int, initial='15')
            # min averaging time (in time constants), 0 for none, [5, 15, 50] recommended for [low, med, high] precision
        
        #include delay time in ui
    
    def setup_figure(self):
        
        self.ui = self.plot = pg.PlotWidget()
#         self.plotline = self.plot.plot()
    
    def run(self):
        lockin = self.app.hardware['TDTR_Lockin']
                
        sweep = lockin.create_sweepModule()
        
        #set settings
        sweep.set([("sweep/start", self.settings['start_freq']),
                   ("sweep/stop", self.settings['stop_freq']),
                   ("sweep/samplecount", self.settings['data_points']),
                   ("sweep/xmapping", self.settings['logarithmic']),
                   ("sweep/settling/inaccuracy", self.settings['settling_threshold']),
                   ("sweep/averaging/tc", self.settings['averaging_tc_count']),
                   ('sweep/bandwidthcontrol', 2), #auto bandwidth, I think this is the default
                   ('sweep/maxbandwidth', 1.0000000) #max bandwidth 5 Hz, which is equivalent to tc near 20 ms
                   ])
         
        
        first_timestamp = np.nan
        self.to_measure = ['x','y']

        self.H = h5_io.h5_base_file(app=self.app, measurement=self)
        self.M = h5_io.h5_create_measurement_group(self, self.H)
        
        self.M['delay_time'] = self.app.measurements['delay_stage'].settings['current_delay_time']
#         self.M.create_dataset('frequency', (self.settings['data_points'],), dtype=float)
#         for value in self.to_measure:
#             self.M.create_dataset(value, (self.settings['data_points'],), dtype=float)
        data_array_h5 = self.M.create_dataset('data_array', (len(self.to_measure)+1,self.settings['data_points']), dtype=float)
        
        def store_data(data):
            for ii, value in enumerate(data.keys()):
                data_array_h5[ii] = data[value]
                data_array_h5.attrs['row_'+str(ii)] = value #could be more elegant

        #measure (and save) background phase!
        #also offsets

        sweep.execute()
        
        try:
            while not self.interrupt_measurement_called:
                tempdata, first_timestamp, progress, finished, success =  lockin.read_sweep(first_timestamp, signals = self.to_measure)#, verbose = True)
                self.set_progress(100*progress)
                if success:
                    self.data = tempdata
                    store_data(self.data)
                time.sleep(0.1)
                if finished: break
                #could use first_timestamp and time() to add a timeout if desired
            #one more time in case it wasn't quite done
            tempdata, first_timestamp, _,  _, success =  lockin.read_sweep(first_timestamp, signals = self.to_measure)
            if success:
                self.data = tempdata
                store_data(self.data)
        finally:
            #save_data
            self.H.close()
            
        sweep.finish()
        sweep.unsubscribe('*')
    
    def update_display(self):
#         self.plotline.setData(r_array) #data
        self.plot.clear()
        for val in self.to_measure:
            self.plot.plot(self.data['frequency'], self.data[val])
        