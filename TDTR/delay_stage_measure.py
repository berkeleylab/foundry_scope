from ScopeFoundry import Measurement

class DelayStageMeasure(Measurement):
    
    name = 'delay_stage'
    
#     def __init__(self, app, name=None, hw_name='esp300_xyz_stage'):
#         self.hw_name = hw_name
#         Measurement.__init__(self, app, name=name)
#         self.hw = self.app.hardware[self.hw_name]
        
    def setup(self):
        self.settings.New('offset', dtype=float, initial='224.43', unit='mm')
        
        def pos_to_time(stage_pos, offset):
            light_speed = 0.299792 #mm/ps
            bounces = 4. #double pass delay stage: out, back, out, back
            return -(stage_pos - offset)/light_speed*bounces 
        
        def time_to_pos(delay_time, pos_and_offset):
            _, offset = pos_and_offset
            light_speed = 0.299792 #mm/ps
            bounces = 4. #double pass delay stage
            return -delay_time*light_speed/bounces + offset
        
        curr_td = self.settings.New('current_delay_time', dtype=float, ro=True, unit='ps')
        target_td = self.settings.New('target_delay_time', dtype=float, unit='ps')
        
        stage = self.app.hardware['esp300_xyz_stage']
        curr_td.connect_lq_math(lqs=[stage.settings.z_position, self.settings.offset], func=pos_to_time) #ro so no reverse func needed
        target_td.connect_lq_math(lqs=[stage.settings.z_target_position, self.settings.offset], func=pos_to_time, reverse_func=time_to_pos)
        
    
    def setup_figure(self):
        pass
        
    def run(self):
        pass        