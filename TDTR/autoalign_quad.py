from ScopeFoundry import Measurement
import numpy as np

class AutoalignQuad(Measurement):
    
    name = 'autoalign_quad'
    
        
    def setup(self):
        self.picomotor = self.app.hardware['picomotor']
        self.quad_detectors = self.app.hardware['quad_detectors']
        
    def autoalign(self, tol = .005):     #tolerance for det1_X and det1_Y
        import time
        verbose = True
        
        start_det1_sum = self.quad_detectors.settings['det1_sum']
        start_det1_X = self.quad_detectors.settings['det1_X']/self.quad_detectors.settings['det1_sum']
        start_det1_Y = self.quad_detectors.settings['det1_Y']/self.quad_detectors.settings['det1_sum']
        
        if start_det1_sum<0.001:
            print("Not enough signal to align")
            return -1
        if start_det1_sum>3.49: 
            print("Warning: signal likely saturated")
            
        if abs(start_det1_X) < tol and abs(start_det1_Y) < tol:
            print("Already aligned within tolerance")
            return np.sqrt(start_det1_X**2 + start_det1_Y**2)
        
        start_slope = 500 #units to move picomotor per det1_X or det1_Y
        
        if verbose: print("Attempting alignment")
        
        if verbose: 
            print("Initial picomotor positions:")
            print("axis 1: " + str(self.picomotor.settings['axis_1_position']) + ", axis 2: " + str(self.picomotor.settings['axis_2_position']))

        slope_X = -start_slope #setup specific: increasing axis 3 pos makes det1_X more positive
        slope_Y = start_slope  #setup specific: increasing axis 4 pos makes det1_Y more negative
        
        first_step_X = int(start_det1_X * slope_X)
        first_step_Y = int(start_det1_Y * slope_Y)
        if verbose: print("First steps: axis 1 " + str(first_step_X) + ", axis 2 " + str(first_step_Y))
        
#         if self.picomotor.settings['axis_1_target'] != self.picomotor.settings['axis_1_position'] or self.picomotor.settings['axis_2_target'] != self.picomotor.settings['axis_2_position']:
#             print("Picomotor target and position do not match")
#             return -1 
        
        if verbose: print("Moving")
#         self.picomotor.settings['axis_1_target'] = self.picomotor.settings['axis_1_position'] + first_step_X
#         self.picomotor.settings['axis_2_target'] = self.picomotor.settings['axis_2_position'] + first_step_Y
        
        self.picomotor.settings['axis_1_target'] = self.picomotor.settings['axis_1_target'] + first_step_X
        self.picomotor.settings['axis_2_target'] = self.picomotor.settings['axis_2_target'] + first_step_Y
        
        
#         while (self.picomotor.settings['axis_1_target'] != self.picomotor.settings['axis_1_position'] or self.picomotor.settings['axis_2_target'] != self.picomotor.settings['axis_2_position']) and not self.interrupt_measurement_called:
        time.sleep(min(0.5, 0.3/100*max(abs(first_step_X), abs(first_step_Y))))
        
#         new_det1_X = self.quad_detectors.settings['det1_X']
#         new_det1_Y = self.quad_detectors.settings['det1_Y'] 
#         new_slope_X = first_step_X / (new_det1_X - start_det1_X)
#         new_slope_Y = first_step_Y / (new_det1_Y - start_det1_Y)
         
        
        tries = 1
#         tried_small_step = False

        old_det1_X = start_det1_X
        old_det1_Y = start_det1_Y
        new_slope_X = slope_X
        new_slope_Y = slope_Y
        
        while tries < 5 and not self.interrupt_measurement_called:        
            new_det1_X = self.quad_detectors.settings['det1_X']/self.quad_detectors.settings['det1_sum']
            new_det1_Y = self.quad_detectors.settings['det1_Y']/self.quad_detectors.settings['det1_sum']
            
#             if abs(new_det1_X - old_det1_X)>0.01:
#                 new_slope_X = -first_step_X / (new_det1_X - old_det1_X) #super noise intolerant, fix in future
#                 if abs(new_slope_X) > 1000: new_slope_X = 1000*np.sign(new_slope_X)
# #             else: new_slope_X = slope_X
#             if abs(new_det1_Y - old_det1_Y)>0.01:
#                 new_slope_Y = -first_step_Y / (new_det1_Y - old_det1_Y)
#                 if abs(new_slope_Y) > 1000: new_slope_Y = 1000*np.sign(new_slope_Y)
# #             else: new_slope_Y = slope_Y
#             if verbose: 
#                 print("New det1_X, Y: ", new_det1_X, new_det1_Y)
#                 print("New slopes: ", new_slope_X, new_slope_Y)
            
            
            if abs(start_det1_X) < tol and abs(start_det1_Y) < tol:
                return np.sqrt(start_det1_X**2 + start_det1_Y**2) #success
            
            step_X = -int(new_det1_X * new_slope_X)
            step_Y = int(new_det1_Y * new_slope_Y)
            if verbose: print("Steps: axis 3 " + str(step_X) + ", axis 4 " + str(step_Y))
            
            if verbose: print("Moving")
#             self.picomotor.settings['axis_1_target'] = self.picomotor.settings['axis_1_position'] + step_X
#             self.picomotor.settings['axis_2_target'] = self.picomotor.settings['axis_2_position'] + step_Y
            
            self.picomotor.settings['axis_1_target'] = self.picomotor.settings['axis_1_target'] + step_X
            self.picomotor.settings['axis_2_target'] = self.picomotor.settings['axis_2_target'] + step_Y
            
            
#             while (self.picomotor.settings['axis_1_target'] != self.picomotor.settings['axis_1_position'] or self.picomotor.settings['axis_2_target'] != self.picomotor.settings['axis_2_position']) and not self.interrupt_measurement_called:
            time.sleep(min(0.5, 0.3/100*max(abs(step_X), abs(step_Y))))
                
            old_det1_X = new_det1_X
            old_det1_Y = new_det1_Y
            
            tries = tries + 1

            
        #if code gets here, optimization didn't work
        print("Failed to balance within tol after 5 tries.")
        return -1

    
    def setup_figure(self):
        pass
        
    def run(self):
        self.autoalign()