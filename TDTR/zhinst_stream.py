from ScopeFoundry.measurement import Measurement
import pyqtgraph as pg

class ZhinstStreamMeasure(Measurement):
    
    name = 'zhinst_stream'
    
    
    def setup(self):
        
        pass
    
    def setup_figure(self):
        
        self.ui = self.graph_layout = pg.GraphicsLayoutWidget()
        
        self.scope_plot = self.graph_layout.addPlot()
        
        
        self.scope_plotline = self.scope_plot.plot()
        
        
    def run(self):
        pass
        