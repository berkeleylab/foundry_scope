from ScopeFoundry import Measurement
import time
import pyqtgraph as pg
import numpy as np
from ScopeFoundry.helper_funcs import load_qt_ui_file, sibling_path

class LockinOscopeMeasure(Measurement):
    
    name = 'lockin_oscope'
    
    def setup(self):
        
        self.settings.New('show_aux', dtype=bool, initial=True)
        self.settings.New('signal_select', dtype=str, choices=[('Demodulated R','demod_R'),
                                                           ('Raw lock-in input','raw_input')],
                                                           initial='demod_R')
    
    def setup_figure(self):
        
        self.ui = load_qt_ui_file(sibling_path(__file__, "lockin_oscope.ui"))
        
        self.graph_layout = pg.GraphicsLayoutWidget()
        
        self.ui.plot_groupBox.layout().addWidget(self.graph_layout)
        
        
        # UI widget connections
        self.settings.activation.connect_to_widget(self.ui.run_checkBox)
        self.settings.show_aux.connect_to_widget(self.ui.show_aux_checkBox)
        self.settings.signal_select.connect_to_widget(self.ui.signal_comboBox)  
        
    
    def run(self):
        verbose = False
        
        self.first_run_graphics = True
        lockin = self.app.hardware['TDTR_Lockin']
        
        
        def start_or_create_h(signal_select):
            verbose = False
            if not hasattr(lockin, 'daq'):
                print("Is lockin connected?")
                return -1
            if signal_select == 'demod_R': #want dataAcquisitionModule
                if not hasattr(lockin, 'h_dAM'):
                    if verbose: print("No dataAcquitisionModule found, creating.")
                    h = lockin.create_dataAcquisitionModule()
                else:
                    if verbose: print("dataAcquisitionModule found")
                    h = lockin.h_dAM
                lockin.subscribe_dataAcquisitionModule(h)
                lockin.oscope_using_dAM = True
                if verbose: print("dataAcquisitionModule subscribed")
            else: #want scopeModule
#                 if not hasattr(lockin, 'h_scope'):
#                     if verbose: print("No scopeModule found, creating.")
                h = lockin.create_scopeModule() #could set this up to allow fft and pass through desired mode
#                 else: 
#                     h = lockin.h_scope
                lockin.subscribe_scopeModule(h)
                if verbose: print("scopeModule subscribed")
            return h
        
        h = start_or_create_h(self.settings['signal_select'])
        if h == -1:
            return
        previous_signal_select = self.settings['signal_select']
        
        h.execute()
        
#         self.x_array = np.zeros(0,dtype=float)
#         self.y_array = np.zeros(0,dtype=float)
        self.r_array = np.zeros(0,dtype=float)
        self.x_array = np.zeros(0,dtype=float)
        self.y_array = np.zeros(0,dtype=float)
        self.raw_array =  np.zeros(0,dtype=float)
        self.aux0_array = np.zeros(0,dtype=float)
        self.aux1_array = np.zeros(0,dtype=float)

        while not self.interrupt_measurement_called:
            
            if previous_signal_select != self.settings['signal_select']: #selected signal has changed
                if previous_signal_select == 'demod_R':
                    lockin.oscope_using_dAM = False
                    lockin.finish_dataAcquisitionModule(h)
                    self.r_array = np.zeros(0,dtype=float)
                    self.x_array = np.zeros(0,dtype=float)
                    self.y_array = np.zeros(0,dtype=float)
                    self.aux0_array = np.zeros(0,dtype=float)
                    self.aux1_array = np.zeros(0,dtype=float)
                    print('dataAcquisitionModule asked to stop.')
                if previous_signal_select == 'raw_input':
                    lockin.finish_scopeModule(h)
                    self.raw_array =  np.zeros(0,dtype=float)
                    print('scopeModule stopped.')
                h = start_or_create_h(self.settings['signal_select'])
                h.execute()
        
#             if '/dev1021/demods/0/sample.x' in data:
#                 x = data['/dev1021/demods/0/sample.x'][0]['value'][0]
# #                 print(x.shape)
#                 self.x_array = np.concatenate([self.x_array, x])
# #                 print(self.x_array)
# 
#             if '/dev1021/demods/0/sample.y' in data:
#                 y = data['/dev1021/demods/0/sample.y'][0]['value'][0]
# #                 print(x.shape)
#                 self.y_array = np.concatenate([self.y_array, y])
# #                 print(self.x_array)

            if verbose: print("Reading")
            self.data = data = (h.read(True)) #True returns a flat dict
#             print(data.keys())

            lockin.settings['range_util'] = lockin.get_range_util() #would like to make this update happen in lock-in hardware whether oscope is running or not
            
            if '/dev1021/demods/0/sample.r' in data:
                if '/dev1021/demods/0/sample.auxin0' not in data:
                    print("aux 0 not recorded")
                r_flat = []
                for burst in data['/dev1021/demods/0/sample.r']:
                    for row in burst['value']:
                        r_flat = np.concatenate([r_flat,row])
                #r = data['/dev1021/demods/0/sample.r'][0]['value'][0]
                if verbose: print(data['/dev1021/demods/0/sample.r'][0]['header']['flags'])
                lockin.settings['demod_mag'] = np.mean(r_flat) 
#                 print(r.shape)
                self.r_array = np.concatenate([self.r_array, r_flat])
#                 print(self.r_array)
                if verbose: print("r_array updated, last entry ", self.r_array[-1])

            if '/dev1021/demods/0/sample.x' in data:
                x_flat = []
                for burst in data['/dev1021/demods/0/sample.x']:
                    for row in burst['value']:
                        x_flat = np.concatenate([x_flat,row])
                self.x_array = np.concatenate([self.x_array, x_flat])
                if verbose: print("x_array updated, last entry ", self.x_array[-1])
                
            if '/dev1021/demods/0/sample.y' in data:
                y_flat = []
                for burst in data['/dev1021/demods/0/sample.y']:
                    for row in burst['value']:
                        y_flat = np.concatenate([y_flat,row])
                self.y_array = np.concatenate([self.y_array, y_flat])
                if verbose: print("y_array updated, last entry ", self.y_array[-1])

            if '/dev1021/scopes/0/wave' in data:
                if '/error' in data:
                    if data['/error'][0]:
                        print("Error in reading scope data.")
                if verbose: print(data['/dev1021/scopes/0/wave'][0][0]['flags'])
                raw = data['/dev1021/scopes/0/wave'][0][0]['wave'][0]
                
                scaling = -1
                for i in range(len(data['/dev1021/scopes/0/wave'][0][0]['channelenable'])):
                    if data['/dev1021/scopes/0/wave'][0][0]['channelenable'][i]: #channel is enabled
                        if scaling == -1: #scaling not set
                            scaling = data['/dev1021/scopes/0/wave'][0][0]['channelscaling'][i]
                            if scaling != 1:
                                print("Channel scaling not 1; raw magnitude likely inaccurate.")
                        else:
                            print('Warning: multiple channel scalings found.')     
                               
                self.raw_array = np.concatenate([self.raw_array, raw])
                if verbose: print("raw_array updated, last entry ", self.raw_array[-1])
                if len(self.raw_array) > 10000:
                    self.raw_array = self.raw_array[-9999:]

            if '/dev1021/demods/0/sample.auxin0' in data:
#                 aux0 = data['/dev1021/demods/0/sample.auxin0'][0]['value'][0]
                aux0_flat = []
                for burst in data['/dev1021/demods/0/sample.auxin0']:
                    for row in burst['value']:
                        aux0_flat = np.concatenate([aux0_flat,row])
                self.aux0_array = np.concatenate([self.aux0_array, aux0_flat])
                lockin.settings['sig_aux_mag'] = np.mean(aux0_flat)
#                 print(self.aux0_array)

            if '/dev1021/demods/0/sample.auxin1' in data:
#                 aux1 = data['/dev1021/demods/0/sample.auxin1'][0]['value'][0]
                aux1_flat = []
                for burst in data['/dev1021/demods/0/sample.auxin1']:
                    for row in burst['value']:
                        aux1_flat = np.concatenate([aux1_flat,row])
                self.aux1_array = np.concatenate([self.aux1_array, aux1_flat])
                lockin.settings['ref_aux_mag'] = np.mean(aux1_flat)
#                 print(self.aux1_array)

                

            previous_signal_select = self.settings['signal_select']
            
            if verbose: print('sleeping')
            time.sleep(0.2)
            
            
        if self.settings['signal_select'] == 'demod_R':
            lockin.oscope_using_dAM = False
            lockin.finish_dataAcquisitionModule(h)
            print('dataAcquisitionModule asked to stop.')
        else:
            lockin.finish_scopeModule(h)
            print('scopeModule stopped.')
                
        
    
    def update_display(self):

        if self.first_run_graphics:
            self.graph_layout.clear()
            self.plot = self.graph_layout.addPlot(0,0)
            self.plot_phase = self.graph_layout.addPlot(1,0)
            self.plot_aux = self.graph_layout.addPlot(2,0)
            aux_legend = self.plot_aux.addLegend()
            
            self.plotline = self.plot.plot()
            self.plotline_phase = self.plot_phase.plot()
            self.plotline_aux0 = self.plot_aux.plot(pen = 'y')
            self.plotline_aux1 = self.plot_aux.plot(pen = 'g')
            aux_legend.addItem(self.plotline_aux0, 'signal')
            aux_legend.addItem(self.plotline_aux1, 'reference')
            
            self.plot_aux.setXLink(self.plot)                                                                     
        self.first_run_graphics = False
        
        sparcity = 10 #should clean this up/make a setting how much data to plot
        #can also use /{dev}/n/scopes/0/time to request less data (values 0 to 7, span = 2^val * 10us, sample rate = 210 MSamples/2^val)
        raw_array_toplot = self.raw_array[0::sparcity]
        
        #crude scrolling
        if len(self.r_array) > 9000:
            r_array = self.r_array[-8800:-1]
        else: r_array = self.r_array
        if len(self.x_array) > 9000:
            x_array = self.x_array[-8800:-1]
        else: x_array = self.x_array
        if len(self.y_array) > 9000:
            y_array = self.y_array[-8800:-1]
        else: y_array = self.y_array
        if len(self.aux0_array) > 9000:
            aux0_array = self.aux0_array[-8800:-1]
        else: aux0_array = self.aux0_array
        if len(self.aux1_array) > 9000:
            aux1_array = self.aux1_array[-8800:-1]
        else: aux1_array = self.aux1_array
        #scrolling for raw_array coming from its update function; not trying to save all of its data because there's a lot
        
        phase_array = np.arctan2(-x_array, y_array)
        
        if self.settings['signal_select'] == 'demod_R':
            self.plotline.setData(r_array)
            self.plotline_phase.setData(phase_array)
        else:
            self.plotline.setData(raw_array_toplot)
        
        if self.settings['show_aux']:
            self.plotline_aux0.setData(aux0_array)
            self.plotline_aux1.setData(aux1_array)
#         else:
#             self.plotline_aux0.setData([0])
#             self.plotline_aux1.setData([0])

        show_aux = self.settings['show_aux'] and self.settings['signal_select']=='demod_R'
        self.plot_aux.setVisible(show_aux)
        
        self.plot_phase.setVisible(self.settings['signal_select']=='demod_R')
#         print("updating graphics")
        