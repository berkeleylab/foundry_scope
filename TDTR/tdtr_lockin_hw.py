from ScopeFoundry import HardwareComponent
import time
import numpy as np
# from astropy.utils.xml.iterparser import _convert_to_fd_or_read_function

class TDTR_LockinHW(HardwareComponent):
    
    name = 'TDTR_Lockin'
    
    def setup(self):
        
        self.settings.New('dev_id', dtype=str, initial='dev1021')
        self.settings.New('mod_freq', dtype=float, initial=3e6, unit='Hz', si=True)
        self.settings.New('output_enable', dtype=bool, initial=False)
        self.settings.New('filter_order', dtype=int, initial=3)
        self.settings.New('time_constant', dtype=float, initial=0.03, unit='s', si=True)
        self.settings.New('input_range', dtype=float, initial=0.5, unit='V', si=True)
        self.settings.New('x_offset', dtype=float, initial=0, unit='V', si=True, ro=True) #offsets calculated at start of measurement
        self.settings.New('y_offset', dtype=float, initial=0, unit='V', si=True, ro=True)
        self.settings.New('demod_mag', dtype=float, initial=0, unit = 'V', si=True, ro=True) #magnitude of demodulated signal (at mod freq)
        self.settings.New('range_util', dtype=float, initial=0, ro=True, unit='%', si=True) #setting si=True to make it appear ro
        
        self.settings.New('x_uncorrected', dtype=float, ro=True, unit='V', si=True) #x at mod freq, no offsets applied/phase correction not guaranteed
        self.settings.New('y_uncorrected', dtype=float, ro=True, unit='V', si=True) #y at mod freq
        self.settings.New('sig_aux_mag', dtype=float, ro=True, unit='V', si=True) #magnitude of signal from aux photodetector; input from signal detector
        self.settings.New('ref_aux_mag', dtype=float, ro=True, unit='V', si=True) #magnitude of signal from aux photodetector; input from ref detector
        mag_comp = self.settings.New('mag_compare', dtype=float, ro=True, si=True)
    
        def mag_comp_func(sig_mag, ref_aux_mag):
            sig = abs(sig_mag) #should be positive values when photodetector on
            ref = abs(ref_aux_mag)
            if sig + ref==0:
                return 0.                
            pct_diff = 2.*(sig-ref)/(sig+ref)*100. 
            if pct_diff>50: 
                return 50.*2e10
            elif pct_diff<-50:
                return -50.*2e10
            return pct_diff*2e10 #okay apparently values range from -1e12 to 1e12
        mag_comp.connect_lq_math(lqs=[self.settings.sig_aux_mag,self.settings.ref_aux_mag], func=mag_comp_func)
        
        
#         range_util = self.settings.New('range_util', dtype=float, initial=0, ro=True, unit='%')
         
        #redefining range_util to be updated directly based on usage of ADC range so this is outdated
#         S = self.settings
#         def range_util_func(in_range, signal_mag):
#             return 100 * signal_mag/in_range
#         range_util.connect_lq_math(lqs=[S.input_range,S.signal_mag], func=range_util_func)
        
        #self.settings.New('R', dtype=float, ro=True, si=True)
        #self.settings.New('Theta', dtype=float, ro=True, si=True)
        

    
    def connect(self):
        import zhinst.ziPython, zhinst.utils
        
        device_id = self.settings['dev_id']
        
        # The API level supported by this example. Note, the HF2 data server
        # only supports API Level 1.
        apilevel = 1 
        # Call a zhinst utility function that returns:
        # - an API session `daq` in order to communicate with devices via the data server.
        # - the device ID string that specifies the device branch in the server's node hierarchy.
        # - the device's discovery properties.
        err_msg = "This example only supports HF2 Instruments."
        (daq, device, props) = zhinst.utils.create_api_session(device_id, apilevel, required_devtype='HF2',
                                                               required_err_msg=err_msg)
        zhinst.utils.api_server_version_check(daq)
        
        self.daq = daq
        self.device = device
        
        # Enable the API's log.
        daq.setDebugLevel(3) #0 is most verbose; trace:0, info:1, debug:2, warning:3, error:4, fatal:5, status:6
        
        zhinst.utils.api_server_version_check(daq)
        
        # Create a base configuration: Disable all available outputs, awgs, demods, scopes,...
        zhinst.utils.disable_everything(daq, device)        
        
        
        self.S = S = dict(
            dev=device_id,
            out_chan = 0,
            out_demod = 6,
            out_freq = self.settings['mod_freq'],
            out_range = 1.0, # V 
            out_amplitude = 0.5,
            out_offset = 0.5,
            out_enable = self.settings['output_enable']
        )
        
        
        ### Setup Signal Generation
        
        daq.set([('/{dev}/oscs/{out_chan}/freq'.format(**S), S['out_freq']),  #set frequency to 2.5 MHz
                ('/{dev}/sigouts/{out_chan}/range'.format(**S), S['out_range']), #set output range to 1 V
                #set output amplitude for demodulator 7 (zero indexed here but not on panel) to 0.5 V
                ('/{dev}/sigouts/{out_chan}/amplitudes/{out_demod}'.format(**S), S['out_amplitude']),
                ('/{dev}/sigouts/{out_chan}/enables/{out_demod}'.format(**S), True), #enable demodulator 7 output
                ('/{dev}/sigouts/{out_chan}/offset'.format(**S), S['out_offset']), #set overall output offset to 0.5 V
                ('/{dev}/sigouts/{out_chan}/on'.format(**S), S['out_enable'])]) #turn on output
        
                                                   
        S.update(dict(
            in_chan = 0, #signal input 1
            demod_chan = 0, #demodulator input 1 (of 6)
            in_range = self.settings['input_range'],
            in_AC_coupling = True, #input AC coupling on
            in_imp_50 = True, #input impedence 50 ohm (alternative is "high") 
#             in_AC_coupling = False, #input AC coupling on
#             in_imp_50 = False, #input impedence 50 ohm (alternative is "high") 
            in_differential =  False, #input is absolute, not difference between + and - ports
            in_osc = 0, #use oscillator 1 for lock-in input
            input_signal = 0, #input is coming to "Input 1" on the front panel
            filter_order = self.settings['filter_order'],
            time_constant = self.settings['time_constant'], #time constant for signal filter [s]. 30 ms gives ~3.6 Hz 3 db BW
            data_trans_rate = 100 #Hz, arbitrary. Should be at least 7-10x filter bw to be high enough to not miss data, low enough to not saturate connection
            ))
        
        daq.set([
            ('/{dev}/sigins/{in_chan}/range'.format(**S), S['in_range']), #input range: 1.5 V #the photodetector can definitely exceed this
            ('/{dev}/sigins/{in_chan}/ac'.format(**S), S['in_AC_coupling']), #input AC coupling on
            ('/{dev}/sigins/{in_chan}/imp50'.format(**S), S['in_imp_50']), #input 50 ohm impedence on (alternative is "high" impedance)
            ('/{dev}/sigins/{in_chan}/diff'.format(**S), False), #differential input mode off
            ('/{dev}/demods/{demod_chan}/oscselect'.format(**S), S['in_osc']), #select oscillator 1 (zero indexing) for demodulator 1
            ('/{dev}/demods/{demod_chan}/harmonic'.format(**S), 1), #use the first harmonic of oscillator 1
            ('/{dev}/demods/{demod_chan}/adcselect'.format(**S), S['input_signal']), #for demod 1 use input signal 1 (zero indexing for both)
            ('/{dev}/demods/{demod_chan}/order'.format(**S), S['filter_order']), #second order low-pass filter
            ('/{dev}/demods/{demod_chan}/timeconstant'.format(**S), S['time_constant']), #30 ms time constant, ~3.6 Hz 3 db BW
            ('/{dev}/demods/{demod_chan}/rate'.format(**S), S['data_trans_rate']), #data transfer rate 100 Hz #100 arbitrary, should be fine for 30 ms TC but possibly should change if TC decreased
            ('/{dev}/demods/{demod_chan}/enable'.format(**S), True)]) #enable the demodulator
            
            
#                         enabled = self.settings.get_lq(axis_name + "_enabled")
#             enabled.connect_to_hardware(
#                 read_func = lambda a=axis_num: E.read_enabled(a),
#                 write_func = lambda enabled, a=axis_num: E.write_enabled(a, enabled))
#             enabled.read_from_hardware()
            
            
        def update_mod_freq(new_freq):
            self.daq.set([('/{dev}/oscs/{out_chan}/freq'.format(**self.S), new_freq)])
        def read_mod_freq():
            return self.daq.getDouble('/{dev}/oscs/{out_chan}/freq'.format(**self.S))
        
        def update_output_enable(new_enable):
            self.daq.set([('/{dev}/sigouts/{out_chan}/on'.format(**self.S), new_enable)])
        def read_output_enable():
            return self.daq.getInt('/{dev}/sigouts/{out_chan}/on'.format(**self.S))

        def update_time_constant(new_tc):
            self.daq.set([('/{dev}/demods/{demod_chan}/timeconstant'.format(**self.S), new_tc)])
            if hasattr(self, 'h_dAM'): # a dataAcquisitionModule exists
                dAM_running = self.h_dAM.getDouble("dataAcquisitionModule/enable")
                S = self.dAM_settings
                S['burst_duration'] = new_tc  
                S['num_cols'] = int(np.ceil(S['module_sampling_rate']*S['burst_duration']))
#                 S['num_bursts'] = int(np.ceil(S['total_duration']/S['burst_duration']))
#                 self.h_dAM.set("dataAcquisitionModule/count", S['num_bursts'])
                self.h_dAM.set("dataAcquisitionModule/duration", S['burst_duration']) 
                self.h_dAM.set("dataAcquisitionModule/grid/cols", S['num_cols'])
                if dAM_running:
                    self.h_dAM.set("dataAcquisitionModule/enable", True) #if it was running, make sure it still is
        def read_time_constant():
            return self.daq.getDouble('/{dev}/demods/{demod_chan}/timeconstant'.format(**self.S))
        
        def update_filter_order(new_order):
            self.daq.set([('/{dev}/demods/{demod_chan}/order'.format(**self.S), new_order)])
            if hasattr(self, 'sweep'): #a sweeper module exists
                self.sweep.set([('sweep/order', new_order)]) #not entirely convinced this will work
        def read_filter_order():
            return self.daq.getInt('/{dev}/demods/{demod_chan}/order'.format(**self.S)) #could make this check that the sweeper module agrees
        
        def update_input_range(new_range):
            if new_range > 0: #should make this a lq limit
                self.daq.set([('/{dev}/sigins/{in_chan}/range'.format(**self.S), new_range)])
                if hasattr(self, 'h_scope'):
                    self.h_scope.set('scopeModule/externalscaling', new_range)
                    print("Scope scaling updated.")
        def read_input_range():
            return self.daq.getDouble('/{dev}/sigins/{in_chan}/range'.format(**self.S))
            
            
        self.settings.mod_freq.connect_to_hardware(
            read_func=read_mod_freq,
            write_func=update_mod_freq)
        self.settings.output_enable.connect_to_hardware(
            read_func=read_output_enable,
            write_func=update_output_enable)
        self.settings.time_constant.connect_to_hardware(
            read_func=read_time_constant,
            write_func=update_time_constant)
        self.settings.filter_order.connect_to_hardware(
            read_func=read_filter_order,
            write_func=update_filter_order)
        self.settings.input_range.connect_to_hardware(
            read_func=read_input_range,
            write_func=update_input_range)

        
            
    def get_range_util(self):
        #Read max and min values from ADC in last 100 ms. Values reported between -8192 and 8192 (I think)
        adc_max = self.daq.getDouble('/{dev}/status/adc{in_chan}max'.format(**self.S))
        adc_min = self.daq.getDouble('/{dev}/status/adc{in_chan}min'.format(**self.S))
        util = np.max([np.abs(adc_max), np.abs(adc_min)])/128.*100#/8192.*100
#         self.settings['range_util'] = util
        return util
        
    def create_dataAcquisitionModule(self):
#         demod_path = '/{dev}/demods/{demod_chan}/sample'.format(**self.S)
#         signal_paths = []
#         signal_paths.append(demod_path + '.x')  # The demodulator X output.
#         signal_paths.append(demod_path + '.y')  # The demodulator Y output.
#         signal_paths.append(demod_path + '.R')  # The demodulator R output.
#         signal_paths.append(demod_path + '.auxin0')  # Auxin1: raw photodetector input magnitude
#         signal_paths.append(demod_path + '.auxin1')  # Auxin2: raw photodetector input magnitude

        # Defined the total time we would like to record data for and its sampling rate.
        # total_duration: Time in seconds: This examples stores all the acquired data in the `data` dict - remove this
        # continuous storing in read_data_update_plot before increasing the size of total_duration!
        S = self.dAM_settings = dict()
#         S['total_duration'] = max(1, self.settings['time_constant']) #second #was 2
        S['module_sampling_rate'] = 1000  # Number of points/second #was 3000
        S['burst_duration'] = self.settings['time_constant']  # Time in seconds for each data burst/segment. #was 0.02, now tying to tc
        S['num_cols'] = np.ceil(S['module_sampling_rate']*S['burst_duration']) #dropped int()
        S['num_bursts'] = 1 #np.ceil(S['total_duration']/S['burst_duration']) #ditto
        S['type'] = 0 #not a triggered input
        S['grid_mode'] = 1 #see definitions below; linearly interpreted values
        S['endless'] = True #run continuously
        
        # Create an instance of the Data Acquisition Module.
        self.h_dAM = h = self.daq.dataAcquisitionModule()

        
        # Configure the Data Acquisition Module.
        
        #set the device that will be used for the trigger - this parameter must be set
        h.set("dataAcquisitionModule/device", self.S['dev']) 
        
        #Specify continuous acquisition (type=0). other values refer to triggered inputs
        h.set("dataAcquisitionModule/type", S['type']) 
        
        # 'dataAcquisitionModule/grid/mode' - Specify the interpolation method of
        #   the returned data samples.
        # 1 = Nearest. If the interval between samples on the grid does not match
        #     the interval between samples sent from the device exactly, the nearest
        #     sample (in time) is taken.
        # 2 = Linear interpolation. If the interval between samples on the grid does
        #     not match the interval between samples sent from the device exactly,
        #     linear interpolation is performed between the two neighbouring
        #     samples.
        # 4 = Exact. The subscribed signal with the highest sampling rate (as sent
        #     from the device) defines the interval between samples on the DAQ
        #     Module's grid. If multiple signals are subscribed, these are
        #     interpolated onto the grid (defined by the signal with the highest
        #     rate, "highest_rate"). In this mode, dataAcquisitionModule/duration is
        #     read-only and is defined as num_cols/highest_rate.
        # Using mode = 4 causes confusing timing problems
        h.set("dataAcquisitionModule/grid/mode", S['grid_mode']) 
        
        h.set("dataAcquisitionModule/endless", S['endless'])
        
        # 'dataAcquisitionModule/count' - Specify the number of bursts of data the
        #   module should return (if dataAcquisitionModule/endless=0). The
        #   total duration of data returned by the module will be
        #   dataAcquisitionModule/count*dataAcquisitionModule/duration.
        h.set("dataAcquisitionModule/count", S['num_bursts'])
        
        # 'dataAcquisitionModule/duration' - Burst duration in seconds.
        #   If the data is interpolated linearly or using nearest neighbout, specify
        #   the duration of each burst of data that is returned by the DAQ Module.
        h.set("dataAcquisitionModule/duration", S['burst_duration']) 
        
        # 'dataAcquisitionModule/grid/cols' - The number of points within each duration.
        #   This parameter specifies the number of points to return within each
        #   burst (dataAcquisitionModule/duration seconds worth of data) that is
        #   returned by the DAQ Module.
        h.set("dataAcquisitionModule/grid/cols", S['num_cols'])
        
#         for signal_path in signal_paths:
#             print("Subscribing to", signal_path)
#             h.subscribe(signal_path)
#             #data[signal_path] = []

        h.set("dataAcquisitionModule/flags",4) #an attempt to make it throw exceptions if it detects sample loss
        
        #test:
#         h.set("dataAcquisitionModule/grid/overwrite", True) #setting this to true makes each call to read() return only one data grid (discards measurements between reads)

        self.oscope_using_dAM = False
        self.time_sweep_using_dAM = False
            
        return h
    
    def subscribe_dataAcquisitionModule(self, h):
        demod_path = '/{dev}/demods/{demod_chan}/sample'.format(**self.S)
        signal_paths = []
        signal_paths.append(demod_path + '.x')  # The demodulator X output.
        signal_paths.append(demod_path + '.y')  # The demodulator Y output.
        signal_paths.append(demod_path + '.r')  # The demodulator R output.
        signal_paths.append(demod_path + '.auxin0')  # Auxin1: raw photodetector input magnitude
        signal_paths.append(demod_path + '.auxin1')  # Auxin2: raw photodetector input magnitude
        
        for signal_path in signal_paths:
            print("Subscribing to", signal_path)
            h.subscribe(signal_path)
            #data[signal_path] = []
            
        return h
    
    def finish_dataAcquisitionModule(self, h, force = False):
        if force or (self.oscope_using_dAM==False and self.time_sweep_using_dAM==False):
#             h.set("dataAcquisitionModule/enable",0)
            h.finish()
            h.unsubscribe('*')
        else: 
            print("Waiting for time sweep or oscope to stop before finishing dataAcquisitionModule.")
        
    
    def create_scopeModule(self, mode = 'time_domain'):
        #for more detail on implementation including fft function, see C:\Users\lab\Anaconda3\envs\scopeFoundry\Lib\site-packages\zhinst\examples\hf2\example_scope.py
        self.h_scope = h_scope = self.daq.scopeModule()
        
#         scope_path = '/{dev}/scopes/{in_chan}/wave'.format(**self.S)
#         h_scope.subscribe(scope_path)
        
        self.daq.setInt('/dev1021/scopes/{in_chan}/channel'.format(**self.S), self.S['in_chan']) #set input channel for scope
        self.daq.setInt('/dev1021/scopes/{in_chan}/enable'.format(**self.S), True)
        
        if mode == 'time_domain': #1 is time domain, 3 is freq, 0 is "pass-through" (segments not assembled), 2 does not exist
            mode_num = 1
        elif mode == 'freq_domain':
            mode_num = 3
        else:
            print("Invalid mode selected (choose 'time_domain' or 'freq_domain'. Assuming time domain.")
            mode_num = 1 
        h_scope.set("scopeModule/mode", mode_num)
        #not currently implementing fft-related options
        
#         ext_scale = self.daq.getDouble('/{dev}/sigins/{in_chan}/range'.format(**self.S))
        #print(ext_scale)
        #print(h_scope.get('scopeModule/externalscaling'))
        h_scope.set('scopeModule/externalscaling', self.settings['input_range'])
        
        h_scope.set("scopeModule/averager/weight", 1) #no averaging
        
        num_records = 1 #theoretically how much data is returned but I haven't really figured it out
        h_scope.set("scopeModule/historylength",num_records)
        
        return h_scope
    
    def subscribe_scopeModule(self, h_scope):
        scope_path = '/{dev}/scopes/{in_chan}/wave'.format(**self.S)
        h_scope.subscribe(scope_path)
        return h_scope
    
    def finish_scopeModule(self, h_scope):
        self.daq.setInt('/{dev}/scopes/{in_chan}/enable'.format(**self.S), 0) 
        h_scope.finish()
        h_scope.unsubscribe('*')

    def read_single_deprecated(self, int_time): #causes crashes when run simultaneously with other attempts to read from lockin
        """Returns the mean values of (x-x_offset), (y-y_offset), and the aux inputs, averaged over int_time. """
        verbose = False
        
        #disable lockin oscope to try not to crash things
        self.app.measurements['lockin_oscope'].interrupt_measurement_called=True
        
        #test: lock out (doesn't currently work)
        if self.app.lockin_polling == True:
            print("Already reading; waiting")
            i = 0
            while self.app.lockin_polling and i < 10:
                time.sleep(0.1)
                i = i+1
            if i == 10:
                print("Gave up on waiting")
        else:
            self.app.lockin_polling = True
            if verbose: print("lock enabled")
        #started to also write a pause for when the scope or daq modules are running but didn't finish 
#         if hasattr(self, 'daq'):
#             print("Checking for modules to pause")
#             if hasattr(self.daq, 'scopeModule'):
#                 if self.daq.getInt('/{dev}/scopes/{in_chan}/enable'.format(**self.S)):
#                     print("pausing h_scope")
#                     self.h_scope.finish()
#                     if hasattr(self.daq, 'dataAcquisitionModule'):
#                         print("pausing dataAcqMod")
#                         self.daq.dataAquisitionModule.finish()
        
        # Unsubscribe any streaming data.
        if verbose: print("unsubscribing")
        self.daq.unsubscribe('*')

        # Wait for the demodulator filter to settle.
        if verbose: print("waiting for 10*tc")
        time.sleep(10*self.settings['time_constant'])

        # Perform a global synchronisation between the device and the data server:
        # Ensure that 1. the settings have taken effect on the device before issuing
        # the poll() command and 2. clear the API's data buffers. Note: the sync()
        # must be issued after waiting for the demodulator filter to settle above.
        if verbose: print("syncing")
        self.daq.sync()

        # Subscribe to the demodulator's sample node path.
        if verbose: print("subscribing")
        demod_path = '/{dev}/demods/{demod_chan}/sample'.format(**self.S)
        self.daq.subscribe(demod_path)
        
        # Poll the subscribed data from the data server. Poll will block and record
        # for poll_length seconds
        poll_length = int_time  # [s]
        poll_timeout = 500  # [ms]
        poll_flags = 0
        poll_return_flat_dict = True
        if verbose: print("polling")
        read_data = self.daq.poll(poll_length, poll_timeout, poll_flags, poll_return_flat_dict)
        
        # Unsubscribe from all paths.
        if verbose: print("unsubscribing")
        self.daq.unsubscribe('*')
        
        #test: unlock
        self.app.lockin_polling = False
        if verbose: print("unlocking")
        
        # Check the dictionary returned is non-empty
        assert read_data, "poll() returned an empty data dictionary, did you subscribe to any paths?"
        
        # The data returned is a dictionary of dictionaries that reflects the node's path.
        # Note, the data could be empty if no data had arrived, e.g., if the demods
        # were disabled or had demodulator rate 0.
        assert demod_path in read_data, "The data dictionary returned by poll has no key `%s`." % demod_path
        
        # Access the demodulator sample using the node's path.
        sample = read_data[demod_path]
        
        # Let's check how many seconds of demodulator data were returned by poll.
        # First, get the sampling rate of the device's ADCs, the device clockbase...
        clockbase = float(self.daq.getInt('/%s/clockbase' % self.S['dev']))
        # ... and use it to convert sample timestamp ticks to seconds:
        dt_seconds = (sample['timestamp'][-1] - sample['timestamp'][0])/clockbase
#         print("poll() returned {:.3f} seconds of demodulator data.".format(dt_seconds))
        tol_percent = 50
        dt_seconds_expected = poll_length #could be longer if more happened between subscribe() (or sync?) and poll()
#         assert (dt_seconds - dt_seconds_expected)/dt_seconds_expected*100 < tol_percent, \
#             "Duration of demod data returned by poll() (%.3f s) differs " % dt_seconds + \
#             "from the expected duration (%.3f s) by more than %0.2f %%." % \
#             (dt_seconds_expected, tol_percent)
        ##can probably replace this with checking flags for data loss?
        
        self.settings['x_uncorrected'] = np.mean(sample['x'])
        self.settings['y_uncorrected'] = np.mean(sample['y'])
        
        x = sample['x'] - self.settings['x_offset']
        y = sample['y'] - self.settings['y_offset']
        
        x_mean = np.mean(x)
        y_mean = np.mean(y)
#         
#         print("mean of raw x, x offset, x_mean")
#         print(np.mean(sample['x']), self.settings['x_offset'], x_mean)
        
        aux0_mean = np.mean(sample['auxin0'])
        aux1_mean = np.mean(sample['auxin1'])
        
        self.settings['sig_aux_mag'] = aux0_mean
        self.settings['ref_aux_mag'] = aux1_mean

        return (x_mean, y_mean, aux0_mean, aux1_mean) 
    
    def read_single(self, int_time, vals_expected = ['x','y','auxin0', 'auxin1']): #attempting to rewrite in a way that plays more nicely with other reads by using DAQ module instead of poll()
        """Returns the mean values of (x-x_offset), (y-y_offset), and the aux inputs, averaged over int_time. """
        verbose = False
        
        #Check that the dataAcquisitionModule exists, quit if not
        if not hasattr(self, 'h_dAM'):
            print("Please initialize dAM before calling read_single().")
            return -1
        
        h = self.h_dAM
        
#         self.lock_in_use = True
        already_enabled = h.getDouble("dataAcquisitionModule/enable")
        if not already_enabled:
            print("enabling dAM")
            self.subscribe_dataAcquisitionModule(h)
            h.execute()
            
        #estimating int_time by reading multiple times
        bursts_to_read = int(np.ceil(int_time/self.dAM_settings['burst_duration']))
        reading_time = bursts_to_read*self.dAM_settings['burst_duration']
        
        # Wait for the demodulator filter to settle. 
        if verbose: print("waiting for 10*tc plus read duration")
        time.sleep(10*self.settings['time_constant'] + reading_time)
        
        start_time = time.time()
        if verbose: print("Reading")
        data={}
        timeout = 1 + 5*int_time
        while ('/dev1021/demods/0/sample.'+vals_expected[0]) not in data and ('/dev1021/demods/0/sample.'+vals_expected[-1]) not in data and time.time()-start_time<timeout:
            data = (h.read(True))
#             if '/dev1021/demods/0/sample.x' not in data:
#                 print(data)
            #data is a dictionary of subscribed values; for each value there is a list of recorded bursts, each of which is represented by a dictionary
        if time.time()-start_time>timeout:
            print ("reading from lock-in probably timed out")
        vals_read = {}
        if verbose: print('Read from lock-in in ' + str(time.time()-start_time) +' sec')
#         self.lock_in_use = False
        
        if len(data['/dev1021/demods/0/sample.'+vals_expected[0]])<bursts_to_read:
            print("Fewer bursts recorded than requested. Averaging over a shorter time") #shouldn't come up, but worth handling in case
            bursts_to_read = len(data['/dev1021/demods/0/sample.'+vals_expected[0]]) #maximum number of available values
        
        #if x and y were measured, note that and update relevant logged quantities
        if '/dev1021/demods/0/sample.x' in data:
            raw_x = []
            for burst in data['/dev1021/demods/0/sample.x'][-bursts_to_read:]:
                raw_x.append(burst['value']) #burst['value'] gives an array of num_bursts (currently 1) by (sampling rate*burst duration) values
            x = self.settings['x_uncorrected'] = np.mean(raw_x) #average of the recorded values for the most recent burst
#             print(np.shape(data['/dev1021/demods/0/sample.x']))
#             print(data['/dev1021/demods/0/sample.x'][0]['header'])
#             print(data['/dev1021/demods/0/sample.x'][1]['header'])
#             print(np.shape(data['/dev1021/demods/0/sample.x'][0]['value'][0]))
            x_corr = x - self.settings['x_offset'] 
            vals_read['x'] = x_corr
            if verbose: print('x flags', data['/dev1021/demods/0/sample.x'][-1]['header']['flags'])
        else:
            x_corr = None
        if '/dev1021/demods/0/sample.y' in data:
            raw_y = []
            for burst in data['/dev1021/demods/0/sample.y'][-bursts_to_read:]:
                raw_y.append(burst['value']) #burst['value'] gives an array of num_bursts (currently 1) by (sampling rate*burst duration) values
            y = self.settings['y_uncorrected'] = np.mean(raw_y) #average of the recorded values for the most recent burst
            y_corr = y - self.settings['y_offset']
            vals_read['y'] = y_corr
            if verbose: print('y flags', data['/dev1021/demods/0/sample.y'][-1]['header']['flags'])
        else: 
            y_corr = None
        
        #if auxin0 and auxin1 were measured, note and update logged quantities #for now, not averaging over multiple bursts for these
        if '/dev1021/demods/0/sample.auxin0' in data: 
            sig_aux_mag = self.settings['sig_aux_mag'] = np.mean(data['/dev1021/demods/0/sample.auxin0'][-1]['value'][0])
            vals_read['auxin0'] = sig_aux_mag 
            if verbose: print('auxin0 flags', data['/dev1021/demods/0/sample.auxin0'][-1]['header']['flags'])
        if '/dev1021/demods/0/sample.auxin1' in data:
            ref_aux_mag = self.settings['ref_aux_mag'] = np.mean(data['/dev1021/demods/0/sample.auxin1'][-1]['value'][0])
            vals_read['auxin1'] = ref_aux_mag
            if verbose: print('auxin1 flags', data['/dev1021/demods/0/sample.auxin1'][-1]['header']['flags'])
        
        if vals_read == {}:
            return -1
        output = []
        for val in vals_expected:
            if val not in vals_read.keys():
                print("Warning: " + val + " not read. Are the appropriate signals subscribed?")
            else:
                output.append(vals_read[val])
        
#         #reset dataAcqMod settings
#         h.set("dataAcquisitionModule/endless", dAM_endless)
#         h.set("dataAcquisitionModule/count", dAM_num_bursts)
#         h.set("dataAcquisitionModule/duration", dAM_burst_duration) 
#         h.set("dataAcquisitionModule/grid/cols", dAM_num_cols)
#         h.execute()
        return output
    
    
#    ## I am fairly sure this is never and should not be used (in principle, it should work but in practice is likely to confuse the lock-in)
#     def read_raw_single(self, int_time):
#         """Returns the mean value of the input signal, averaged over (approx) int_time [s]. """
#         #disable lockin oscope to try not to crash things
#         self.app.measurements['lockin_oscope'].interrupt_measurement_called=True
#         
#         self.daq.unsubscribe('*')
#         
#         self.daq.setInt('/{dev}/scopes/{in_chan}/enable'.format(**self.S),1) #enables the scope channel
#         self.daq.sync()
#         
#         scope_path = '/{dev}/scopes/0/wave'.format(**self.S)
#         self.daq.subscribe(scope_path)
#         
#         poll_length = int_time
#         poll_timeout = 500  # [ms]
#         poll_flags = 0
#         poll_return_flat_dict = True
#         data = self.daq.poll(poll_length, poll_timeout, poll_flags, poll_return_flat_dict)
#         
#         self.daq.setInt('/{dev}/scopes/{in_chan}/enable'.format(**self.S),0) #disables the scope channel
#         self.daq.unsubscribe('*')
#         
#         # Check the dictionary returned is non-empty
#         assert data, "poll() returned an empty data dictionary, did you subscribe to any paths?"
#     
#         assert scope_path in data, "The data dictionary returned by poll has no key `%s`." % scope_path
# 
#         # Access the demodulator sample using the node's path.
#         sample = data[scope_path]
#         
#         scaling = self.settings['input_range']
#         
#         raw_mag = np.mean(sample['wave'])*scaling/2**15 
#         #scaling by 2**15 seems to give good values, but should confirm
#               
#         return raw_mag
        

    def set_phase(self, phase):
        self.daq.set([('{dev}/demods/{demod_chan}/phaseshift'.format(**self.S), phase)])
    
    def create_sweepModule(self):
        loop_count = 1 #number of scans to complete
        endless = False #False to execute loop_count times, True to run indefinitely
        bandwidth_control = 2 #0 = manual, 1 = fixed, 2 = auto. 2 default
        max_bandwidth = 1.25e6 #maximum bandwidth used in auto bandwidth mode, 1.25 MHz default
        omega_suppression = 40 #damping (dB) for omega and 2-omega components. 40 is default
        history_length = 10 #upper limit for number of captures stored in module. example used 100
        
        self.sweep = sweep = self.daq.sweep()
        sweep.set("sweep/device", self.S['dev']) 
        sweep.set([
            ('sweep/loopcount', loop_count),
            ('sweep/endless', endless),
            ('sweep/bandwidthcontrol', bandwidth_control),
            ('sweep/maxbandwidth', max_bandwidth),
            ('sweep/omegasuppression', omega_suppression),
            ('sweep/historylength', history_length),
            ('sweep/order', self.settings['filter_order']),
            ("sweep/gridnode", 'oscs/{out_chan}/freq'.format(**self.S))]) #set parameter to sweep: modulation freq

        demod_path = '/{dev}/demods/{demod_chan}/sample'.format(**self.S)
        print("Subscribing to", demod_path)
        sweep.subscribe(demod_path)
        
        return sweep
        
    def read_sweep(self, first_timestamp, signals = ['x','y','r'], verbose = False):
        """Reads data recorded so far by sweeper module. Returns 'data', 
        dictionary with keys 'freq' and matching elements of input 'signals'; 
        timestamp of run start; progress; whether run is finished; whether measurement succeeded"""
        demod_path= '/{dev}/demods/{demod_chan}/sample'.format(**self.S)
        sweep = self.sweep
        
        data = dict() #there's probably a tidier way to define this
        data['frequency'] = []
        for signal in signals:
            data[signal] = [] 
        
        success = False #no data has been read
        
        data_read = sweep.read(True) #read what data has been acquired to this point. True returns a flat dict

        if '/remainingtime' in data_read.keys() and demod_path in data_read.keys():
            unpack = data_read[demod_path][0][0]
            desired_keys = signals + ['frequency', 'timestamp']
            if np.all([key in unpack for key in desired_keys]):
                success = True #all expected values returned
                
        finished = sweep.finished()
        progress = sweep.progress()[0]
        if not success:
            return data, first_timestamp, progress, finished, success #don't try to report
        
#         if sweep.progress()[0] < 0.02: #if no progress, nothing to report and data_read will likely be incomplete
#             finished = False
#             return data, first_timestamp, finished
        
        time_left = data_read['/remainingtime'][0]
        
        if verbose:
            print("Progress: {0:.2f}%".format(100*progress))
            print("Time remining: {0:.2f} seconds".format(time_left))
            if finished:
                print("Finished.")
        
        if np.any(np.isnan(first_timestamp)):
            #set first_timestamp to the first timestamp obtained
            first_timestamp = data_read[demod_path][0][0]['timestamp']            
    
        data['frequency'] = data_read[demod_path][0][0]['frequency']
        for signal in signals:
            if signal in data_read[demod_path][0][0].keys(): #desired signal was recorded
                data[signal] = data_read[demod_path][0][0][signal]
        
        return data, first_timestamp, progress, finished, success
    
    def threaded_update(self):
#         pos = self.settings.get_lq(axis_name + "_position") #copied in as example
#             pos.read_from_hardware()
#         mod_freq = self.settings.get_lq('mod_freq')
#         mod_freq.read_from_hardware()

        self.settings.get_lq('mod_freq').read_from_hardware()
        self.settings.get_lq('output_enable').read_from_hardware()
        self.settings.get_lq('filter_order').read_from_hardware()
        self.settings.get_lq('time_constant').read_from_hardware()
        self.settings.get_lq('input_range').read_from_hardware()
        
#         self.settings['range_util'] = self.get_range_util() ##nope, still crashes if this is called
#         
        time.sleep(1)
#         pass
                

    def disconnect(self):
        
        self.settings.disconnect_all_from_hardware()
        
        if hasattr(self, 'daq'):
            import zhinst.utils
            if hasattr(self, 'sweep'):
                self.sweep.unsubscribe('*')
                del self.sweep
            self.daq.unsubscribe('*')
            zhinst.utils.disable_everything(self.daq, self.device)
            self.daq.disconnect()
            if hasattr(self, 'h_scope'):
                del self.h_scope
            if hasattr(self, 'h_dAM'):
#                 self.finish_dataAcquisitionModule(self.h_dAM)#, force = True) #this should be fine but has started giving errors
                del self.h_dAM
            