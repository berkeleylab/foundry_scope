from ScopeFoundry import Measurement
import pyqtgraph as pg
import time

class PumpProbeAlignDashboard(Measurement):
    
    name = 'pump_probe_align_dashboard'
    
    def setup(self):
        
        pass
    
    def setup_figure(self):
        
        self.ui = self.graph_layout = pg.GraphicsLayoutWidget()
        
        self.D = dict()
        
        for i in [1]:# [1,2,3]:    
            
            d = self.D[i] = dict()
            
            d['sum_plot'] = self.graph_layout.addPlot()
            d['xy_plot' ] = self.graph_layout.addPlot()

            self.graph_layout.nextRow()
    
    def pre_run(self):

        for i, d in self.D.items():
            d['sum_plot'].clear()
            d['xy_plot' ].clear()

            d['xy_plot'].showGrid(x=True, y=True)
            d['xy_plot'].setAspectLocked(True, 1)

            
            d['xy_point'] = d['xy_plot'].plot(pen=None, symbol='o')
            
            

            # Cross Hairs
            infy =  pg.InfiniteLine(movable=False, angle=90, pen='r')
            infy.setPos([0,0])
            infx =  pg.InfiniteLine(movable=False, angle=0, pen='r')
            infx.setPos([0,0])

            d['xy_plot'].addItem(infx)
            d['xy_plot'].addItem(infy)
            
            # Box at max voltages
            x, y = 2,2
            d['xy_plot'].plot([-x,+x,+x,-x,-x],
                              [-y,-y,+y,+y,-y], pen='r')
            
            
            d['sum_plot'].showGrid(x=True, y=True)

            infx =  pg.InfiniteLine(movable=False, angle=0, pen='r')
            infx.setPos([0,0])
            d['sum_plot'].addItem(infx)
            infx =  pg.InfiniteLine(movable=False, angle=0, pen='r')
            infx.setPos([2,2])
            d['sum_plot'].addItem(infx)
            
            # Sum Data plotline            
            d['sum_plotline'] = d['sum_plot'].plot(pen='g', fill=-0.3)

            
            

    def run(self):
        
        while not self.interrupt_measurement_called:
            time.sleep(0.1)
            
    def update_display(self):
        hw = self.app.hardware.quad_detectors
        for i, d in self.D.items():
            
            x = hw.settings["det{}_X".format(i)]
            y = hw.settings["det{}_Y".format(i)]
            sum_ = hw.settings["det{}_sum".format(i)]
            
            d['xy_point'].setData([x/sum_],[y/sum_])

            d['sum_plotline'].setData(*hw.get_history_by_name(
                "det{}_sum".format(i), past_time=2.0, time_from_now=True))
            