from ScopeFoundry import Measurement
import time
import pyqtgraph as pg
import numpy as np
from ScopeFoundry import  h5_io 
from ScopeFoundry.helper_funcs import load_qt_ui_file, sibling_path

class TimeSweepMeasure(Measurement):
    
    name = 'time_sweep'
    
    def setup(self):
        
        self.settings.New('objective', dtype=int, choices=[('2x',2),
                                                           ('5x',5),
                                                            ('10x',10),
                                                            ('20x',20)], initial=10)
        self.settings.New('notes', dtype=str, initial='')
        self.settings.New('current_delay_time', dtype=float, initial='-20', unit='ps', ro = True)   
        self.settings.New('autophase', dtype = bool, initial=True)
        self.settings.New('phase_correction', dtype=float, initial='0', unit = 'degree') #only read at start of measurement
        
        self.settings.New('start_time', dtype=float, initial='-20', unit='ps')
        self.settings.New('step_time_low', dtype=float, initial='1', unit='ps') #make small time steps at low delay time
        self.settings.New('switch_time', dtype=float, initial='80', unit='ps')
        self.settings.New('step_time_high', dtype=float, initial='20', unit='ps') #make larger steps at higher delay time
        self.settings.New('end_time', dtype=float, initial='2000', unit='ps')
        
        self.settings.New('lockin_read_time', dtype=float, initial=0.1, unit='s')
       
        self.settings.New('plot_ratio', dtype=bool, initial=False)
        self.settings.New('show_probes', dtype=bool, initial=True)
        
        self.settings.New('reoptimize_interval', dtype=float, initial='80', unit='ps') #how often to rebalance ref and signal (and eventually fiber stage)
        self.settings.New('ND_balance_tol', dtype=float, initial='0.5', unit='%')
       
#         self.settings.New('offset', dtype=float, initial='223.3', unit='mm')  #delay stage location corresponding to 0 delay time
    #replaced by delay stage measurement lq
        
        #moving to be lockin hw properties
#         self.settings.New('signal_mag', dtype=float, ro=True, unit='V', si=True)
#         self.settings.New('ref_mag', dtype=float, ro=True, unit='V', si=True)
#         mag_comp = self.settings.New('mag_compare', dtype=float, ro=True)
#     
#         def mag_comp_func(sig_mag, ref_mag):
#             sig = abs(sig_mag) #should be positive values when photodetector on
#             ref = abs(ref_mag)
#             if sig + ref==0:
#                 return 0.                
#             pct_diff = 2.*(sig-ref)/(sig+ref)*100. 
#             if pct_diff>50: 
#                 return 50.*2e10
#             elif pct_diff<-50:
#                 return -50.*2e10
#             return pct_diff*2e10 #okay apparently values range from -1e12 to 1e12
#         mag_comp.connect_lq_math(lqs=[self.settings.signal_mag,self.settings.ref_mag], func=mag_comp_func)
        
    
    
    def setup_figure(self):
        
#         self.ui = self.plot = pg.PlotWidget()
# 
#         self.plotline = self.plot.plot()
        ui = self.ui = load_qt_ui_file(sibling_path(__file__, "time_sweep.ui"))
        self.graph_layout = pg.GraphicsLayoutWidget()
        self.ui.plot_groupBox.layout().addWidget(self.graph_layout)
        
        S = self.settings
        lockin = self.app.hardware['TDTR_Lockin']
        beamblock = self.app.hardware['shutter_servo']
        
        # UI widget connections
        S.activation.connect_to_widget(ui.run_checkBox)
        self.app.settings.sample.connect_to_widget(ui.sample_lineEdit)
        S.objective.connect_to_widget(ui.objective_comboBox)
        S.progress.connect_to_widget(ui.progressBar)
        S.current_delay_time.connect_to_widget(ui.curr_td_doubleSpinBox)
        lockin.settings.mod_freq.connect_to_widget(ui.mod_freq_doubleSpinBox)
        S.notes.connect_to_widget(ui.notes_lineEdit)
        S.phase_correction.connect_to_widget(ui.phase_doubleSpinBox)
        S.autophase.connect_to_widget(ui.autophase_checkBox)
        beamblock.shutter_open.connect_to_widget(ui.beamblock_checkBox)
        
        S.start_time.connect_to_widget(ui.start_time_doubleSpinBox)
        S.step_time_low.connect_to_widget(ui.step_time_low_doubleSpinBox)
        S.switch_time.connect_to_widget(ui.switch_time_doubleSpinBox)
        S.step_time_high.connect_to_widget(ui.step_time_high_doubleSpinBox)
        S.end_time.connect_to_widget(ui.end_time_doubleSpinBox)
        
        lockin.settings.sig_aux_mag.connect_to_widget(ui.signal_doubleSpinBox)
        lockin.settings.ref_aux_mag.connect_to_widget(ui.ref_doubleSpinBox)
        lockin.settings.mag_compare.connect_to_widget(ui.balance_horizontalSlider)
        
        lockin.settings.input_range.connect_to_widget(ui.in_range_doubleSpinBox)
        lockin.settings.x_offset.connect_to_widget(ui.x_offset_doubleSpinBox)
        lockin.settings.y_offset.connect_to_widget(ui.y_offset_doubleSpinBox)
        lockin.settings.time_constant.connect_to_widget(ui.tc_doubleSpinBox)
        lockin.settings.filter_order.connect_to_widget(ui.filter_order_doubleSpinBox)
        S.lockin_read_time.connect_to_widget(ui.averaging_doubleSpinBox)
        lockin.settings.range_util.connect_to_widget(ui.range_use_progressBar)
        
        S.plot_ratio.connect_to_widget(ui.plot_ratio_checkBox)
        S.show_probes.connect_to_widget(ui.show_probe_checkBox)
                                           
        
    def run(self):
        self.first_run_graphics = True
        
        lockin = self.app.hardware['TDTR_Lockin']
        stage = self.app.hardware['esp300_xyz_stage']
        beamblock = self.app.hardware['shutter_servo']
        delay_stage = self.app.measurements['delay_stage']
        elliptec = self.app.measurements['elliptec_ND']
        fiber_stage = self.app.measurements['fiber_stage']
        
        def calc_stage_pos(delay_time):
            light_speed = 0.299792 #mm/ps
            bounces = 4. #double pass delay stage
#             print(delay_time*light_speed/bounces)
            return -delay_time*light_speed/bounces + delay_stage.settings['offset']    
        
        def calc_delay_time(stage_pos):
            light_speed = 0.299792 #mm/ps
            bounces = 4. #double pass delay stage: out, back, out, back
            return -(stage_pos - delay_stage.settings['offset'])/light_speed*bounces  
        
        def set_stage_pos(new_stage_pos): #tells stage to move and waits for it to get there
            stage.settings['z_target_position'] = new_stage_pos
            time.sleep(0.010)             
            # wait till motion complete
            is_moving = stage.settings.z_is_moving 
            is_moving.read_from_hardware()
            while is_moving.value:
                time.sleep(0.020)
                is_moving.read_from_hardware()
                if self.interrupt_measurement_called:
                    break
            time.sleep(0.020)    
            
        def elliptec_coarse_and_fine(fine_tol = 0.01):
            #coarse balance of signal and reference (test)
            coarse_balance = elliptec.move_coarse()
            if coarse_balance == -1:
                print("Warning: Failed to coarsely balance signal and reference intensities.")
                return -1
            else: 
                #fine balance
                fine_balance = elliptec.move_fine(fine_tol)
                time.sleep(0.5)#temp, probably overkill
                if fine_balance == -1:
                    print("Warning: Failed to finely balance signal and reference intensities.")
            return fine_balance
                
        #disable changing of settings that cannot be altered during run
#         self.settings.start_time.ro=True #doesn't affect anything I see?
#         self.ui.start_time_doubleSpinBox.readOnly = True #also nothing

        # create data array 
        low_delay_times = np.arange(self.settings['start_time'], self.settings['switch_time'], self.settings['step_time_low'])
        high_delay_times = np.arange(self.settings['switch_time'], self.settings['end_time'], self.settings['step_time_high'])
        delay_times = np.concatenate([low_delay_times, high_delay_times])
#         self.t_array = np.zeros(0,dtype=float)
#         self.x_array = np.zeros(0,dtype=float)
#         self.y_array = np.zeros(0,dtype=float)
        self.t_array = []
        self.x_array = []
        self.y_array = []
        self.detector_sig_array = []
        self.detector_ref_array = []
        
        #set up lock-in for measurement (if not done)
        if not hasattr(lockin, 'h_dAM'): #if a data acquisition model doesn't exist yet, create one
            h = lockin.create_dataAcquisitionModule()
        else: #if it does exist, use it
            h = lockin.h_dAM
        lockin.subscribe_dataAcquisitionModule(h) #set the lock-in to report the signals useful for measurement:r, x, y, auxin0, auxin1 (at the moment)
        lockin.time_sweep_using_dAM = True
        h.execute()
        
        lockin.settings['output_enable'] = True #make sure modulation signal is being sent to EOM
        set_stage_pos(calc_stage_pos(self.settings['start_time']))
        
        elliptec_coarse_and_fine(self.settings['ND_balance_tol']/100.)
        optimized_at_td = self.settings['start_time'] #record delay time at which these were balanced
        balance_fails = 0 #number of times balancing doesn't work during measurement
        balance_stubborness = 10 #not meant to be the permanent solution for this--how many times to try balancing even if it doesn't work
        
#         fiber_stage.set_to_max('y')
#         fiber_stage.set_to_max('z')
        fiber_stage.set_all_to_max()
        init_aux_val = lockin.read_single(0.01, vals_expected = ['auxin1'])[0]
#         time.sleep(10)

        elliptec_coarse_and_fine(self.settings['ND_balance_tol']/100.) #redo elliptec balance
                     
        #set phase to a known value, zero offset
        lockin.set_phase(self.settings['phase_correction'])
        print("phase", self.settings['phase_correction'])
        lockin.settings['x_offset'] = 0 #forget previous values
        lockin.settings['y_offset'] = 0
        time.sleep(0.100)
            
        #measure new lock-in offsets
        set_stage_pos(calc_stage_pos(self.settings['start_time']))
        beamblock.settings['shutter_open'] = False #block the pump beam
        time.sleep(1) #wait for that to happen (probably doesn't take this long)
        x_offset, y_offset, _, _ = lockin.read_single(0.8)
        print("new offsets, unrot",x_offset, y_offset)
        beamblock.settings['shutter_open'] = True #unblock the pump beam
        time.sleep(1) #wait for that to happen
        #store offset values (will be applied whenever lockin.read_single() is called)
        lockin.settings['x_offset'] = x_offset
        lockin.settings['y_offset'] = y_offset
        
        lockin.settings['range_util'] = lockin.get_range_util() #check whether lock-in range seems reasonable
        
        if self.settings['autophase']:
            #adjust phase to make y const across zero delay
            #measure output at -10 ps
            set_stage_pos(calc_stage_pos(-10))
            x_neg, y_neg, _, _ = lockin.read_single(0.5)
            #and at +10 ps
            set_stage_pos(calc_stage_pos(+10)) #well making that the right value probably helps
            x_pos, y_pos, _, _ = lockin.read_single(0.5)
            #figure out angle to rotate
            x_diff = x_pos - x_neg
            y_diff = y_pos - y_neg
            phase_corr = np.arctan2(y_diff, x_diff)*180/np.pi #phase to rotate by
            print("x jump, y jump, phase:",x_diff, y_diff, phase_corr)
            #adjust phase correction based on that
            new_phase = self.settings['phase_correction'] + phase_corr #plus or minus??
            new_phase = (new_phase + 180.) %360. - 180. #I think this makes it stay in [-180, 180)
            #adjust the offsets for the new angle
            phase_diff = new_phase - self.settings['phase_correction'] #might not be phase_corr because of limited domain
            lockin.settings['x_offset'] = x_offset*np.cos(phase_diff*np.pi/180.) + y_offset*np.sin(phase_diff*np.pi/180.)
            lockin.settings['y_offset'] = y_offset*np.cos(phase_diff*np.pi/180.) - x_offset*np.sin(phase_diff*np.pi/180.)
            #set new phase
            self.settings['phase_correction'] = new_phase
            lockin.set_phase(self.settings['phase_correction'])
        
#         #for testing: check that offsets still reasonable
#         set_stage_pos(calc_stage_pos(self.settings['start_time']))
#         beamblock.settings['shutter_open'] = False #block the pump beam
#         time.sleep(1) #wait for that to happen (probably doesn't take this long)
#         print("phase", self.settings['phase_correction'], "phase diff", phase_diff)
#         x, y, _, _ = lockin.read_single(0.5)
#         print("these values should be 0:", x, y)
#         print("as fractions:", x/lockin.settings['x_offset'], y/lockin.settings['y_offset'])
# #         print("if shouldn't rotate, these should be 0:", x+lockin.settings['x_offset']-x_offset, y+lockin.settings['y_offset']-y_offset)
#         beamblock.settings['shutter_open'] = True #unblock the pump beam
#         time.sleep(1) #wait for that to happen
        
    
        self.H = h5_io.h5_base_file(app=self.app, measurement=self)
        self.M = h5_io.h5_create_measurement_group(self, self.H)
        
        self.M['delay_times'] = delay_times
        
        t_array_h5 = self.M.create_dataset('t_array', (len(delay_times),), dtype=float)
        x_array_h5 = self.M.create_dataset('x_array', (len(delay_times),), dtype=float)
        y_array_h5 = self.M.create_dataset('y_array', (len(delay_times),), dtype=float)
        detector_sig_array_h5 = self.M.create_dataset('detector_sig_array', (len(delay_times),), dtype=float)
        detector_ref_array_h5 = self.M.create_dataset('detector_ref_array', (len(delay_times),), dtype=float)
        
        try:
            for ii, td in enumerate(delay_times):
                self.set_progress(100*ii/len(delay_times))
                if self.interrupt_measurement_called:
                    break
                
                new_stage_pos = calc_stage_pos(td)
                if new_stage_pos < 0 or new_stage_pos > 250:
                    print("Delay time out of range.")
                    break
                
                set_stage_pos(new_stage_pos)
                
                curr_pos = stage.settings.z_position.read_from_hardware()
                time.sleep(0.010) #does reading from hardware take time?
                delay_time = calc_delay_time(curr_pos) #should be ~the same as td
                self.settings['current_delay_time'] = delay_time
                
                
                #don't move elliptec during measurement; it changes the phase 
#                 if (delay_time - optimized_at_td > self.settings['reoptimize_interval']) and balance_fails < balance_stubborness:
#                     fine_balance = elliptec.move_fine(self.settings['ND_balance_tol']/100.)
#                     if fine_balance == -1: #that wasn't enough
#                         print("Fine balancing failed, trying coarse and fine together.")
#                         if elliptec_coarse_and_fine(self.settings['ND_balance_tol']/100.) != -1: #try again, see if it works
#                             optimized_at_td = delay_time #worked, record that it worked at this td
#                         else: balance_fails = balance_fails + 1 #didn't work, count that so it doesn't try infinitely
#                     else: 
# #                         print("waiting for nd stage")
# #                         time.sleep(0.3)#probably overkill #was 0.5
#                         optimized_at_td = delay_time #worked, record that it worked at this td
                
                if (delay_time - optimized_at_td > self.settings['reoptimize_interval']):
                    orig_ref = lockin.read_single(0.01, vals_expected = ['auxin1'])[0] #get current magnitude of reference sig
                    fiber_stage.set_to_max('y', range_to_test = 1, coarse_step = 0.1)
                    fiber_stage.set_to_max('z', range_to_test = 1, coarse_step = 0.1)
                    new_ref = lockin.read_single(0.01, vals_expected = ['auxin1'])[0] 
                    if new_ref < 0.95*orig_ref: #maximization didn't go well
                        fiber_stage.set_to_max('y', range_to_test = 2, coarse_step = 0.1)
                        fiber_stage.set_to_max('z', range_to_test = 2, coarse_step = 0.1) #try again
#                     fiber_stage.set_to_val('y', init_aux_val, range_to_test = 2, coarse_step = 0.1)
                    
#                     time.sleep(10)
                    optimized_at_td = delay_time #worked, record that it worked at this td
                
                stubborness = 5
                i=0
                lockin_read = -1
                while lockin_read == -1 and i < stubborness:    
                    lockin_read = lockin.read_single(self.settings['lockin_read_time'])
                    if len(lockin_read) == 4:
                        x, y, detector_sig, detector_ref = lockin_read 
                    else:
                        print(lockin_read)
                    i=i+1
                    if i>1: print("trying again to read")
                
                lockin.settings['range_util'] = lockin.get_range_util() #I think this is fast enough to just do, should confirm
            
                #add to data arrays
#                 lockin.settings['sig_aux_mag'] = detector_sig #moving these updates to be part of read_single()
#                 lockin.settings['ref_aux_mag'] = detector_ref
                self.t_array.append(delay_time)
                t_array_h5[ii] = delay_time
                self.x_array.append(x)
                x_array_h5[ii] = x
                self.y_array.append(y)
                y_array_h5[ii] = y
                self.detector_sig_array.append(detector_sig)
                detector_sig_array_h5[ii] = detector_sig
                self.detector_ref_array.append(detector_ref)
                detector_ref_array_h5[ii] = detector_ref
                
            stage.settings['z_target_position'] = calc_stage_pos(self.settings['start_time'])
                
        
        finally:
            #save_data
            lockin.time_sweep_using_dAM = False
            lockin.finish_dataAcquisitionModule(h)
            self.H.close()
    
    def update_display(self):
#         self.plotline.setData(self.x_array)
#         self.plot.plot(self.t_array, self.x_array)
#         self.plot.plot(self.t_array, self.y_array)
        
        if self.first_run_graphics:
            self.graph_layout.clear()
            self.plot = self.graph_layout.addPlot(0,0)
            self.plot_aux = self.graph_layout.addPlot(1,0)
            
            self.plotline_x = self.plot.plot(pen = 'b')
            self.plotline_y = self.plot.plot(pen = 'r')
            self.plotline_ratio = self.plot.plot()
            legend = self.plot.addLegend()
            legend.addItem(self.plotline_x, 'x')
            legend.addItem(self.plotline_y, 'y')
            legend.addItem(self.plotline_ratio, 'ratio')
            
            self.plotline_sig = self.plot_aux.plot(pen = 'y')
            self.plotline_ref = self.plot_aux.plot(pen = 'g')
            aux_legend = self.plot_aux.addLegend()
            aux_legend.addItem(self.plotline_sig, 'signal')
            aux_legend.addItem(self.plotline_ref, 'reference')
            
            self.plot_aux.setXLink(self.plot)                                                                     
        self.first_run_graphics = False
        
        if self.settings['plot_ratio']:
            x = np.array(self.x_array)
            y = np.array(self.y_array)
            self.plotline_ratio.setData(self.t_array, -x/y)
        else:
            self.plotline_x.setData(self.t_array, self.x_array)
            self.plotline_y.setData(self.t_array, self.y_array)
        self.plotline_ratio.setVisible(self.settings['plot_ratio'])
        self.plotline_x.setVisible(not self.settings['plot_ratio'])
        self.plotline_y.setVisible(not self.settings['plot_ratio'])
            
        
        if self.settings['show_probes']:
            self.plotline_sig.setData(self.t_array,self.detector_sig_array)
            self.plotline_ref.setData(self.t_array,self.detector_ref_array)

        self.plot_aux.setVisible(self.settings['show_probes'])
