from ScopeFoundry import Measurement
import time
# import pyqtgraph as pg
import numpy as np

class AutoAlign(Measurement):
    
    name = 'auto_align'
    
    def setup(self):
        
#         self.settings.New('one_mirror', dtype=bool, initial=True)
#         self.settings.New('set_to_max', dtype=bool, initial=True)
        pass
    
    def setup_figure(self):
        pass
                                           
        
    def run(self):
        
        lockin = self.app.hardware['TDTR_Lockin']
        picomotor = self.app.hardware['picomotor']
        
        
        #set up lock-in for measurement (if not done)
        if not hasattr(lockin, 'h_dAM'): #if a data acquisition model doesn't exist yet, create one
            h = lockin.create_dataAcquisitionModule()
        else: #if it does exist, use it
            h = lockin.h_dAM
        lockin.subscribe_dataAcquisitionModule(h) #set the lock-in to report the signals useful for measurement:r, x, y, auxin0, auxin1 (at the moment)
        lockin.auto_align_using_dAM = True ###############make this matter###########
        h.execute()
        
        lockin.settings['output_enable'] = True #make sure modulation signal is being sent to EOM        
        lockin.settings['range_util'] = lockin.get_range_util() #check whether lock-in range seems reasonable
        
#         #for now
#         if not self.settings['one_mirror']:
#             print("Only one mirror optimization currently supported.")
#             self.settings['one_mirror'] = True
            
        def move_axis(axis, position, timeout = 0.5):
            target = axis+'_target'
            pos = axis + '_position'
            picomotor.settings[target] = position
            time.sleep(0.05)
            start_wait = time.time() 
            while picomotor.settings[pos] != position and time.time() - start_wait < timeout:
#                 picomotor.settings.get_lq(pos).read_from_hardware()
#                 print(picomotor.settings[pos])
                time.sleep(0.05)
        def read_lockin_mag(stubborness = 5):
            i=0
            lockin_read = -1
            while lockin_read == -1 and i < stubborness:    
                lockin_read = lockin.read_single(0.2)
                if len(lockin_read) == 4:
                    x, y, _, _ = lockin_read 
                else:
                    print(lockin_read)
                i=i+1
                if i>1: print("trying again to read")
            if lockin_read == -1:
                return -1
            else:
                return np.sqrt(x**2+y**2)
        
        picomotor.settings.get_lq('axis_3_position').read_from_hardware()
        picomotor.settings.get_lq('axis_4_position').read_from_hardware()
        current_y = picomotor.settings['axis_3_position'] # 3 and 4 are mirror closer to sample
        current_x = picomotor.settings['axis_4_position']
#         picomotor.settings.axis_3_target.update_value(current_x) #should do nothing, but I don't trust the read value
#         picomotor.settings.axis_4_target.update_value(current_x)
        move_axis('axis_4', current_x)
#         time.sleep(0.2)
        move_axis('axis_3', current_y)
#         time.sleep(0.2)
        #deliberately not checking whether the reported position is correct because I trust the reporting less than the motion
        #oh probably manually reading from hardware would work
        
        x_step = 10
        y_step = 10
        steps = 0
        
        while (abs(x_step) > 5 or abs(y_step) > 5) and steps < 10: 
            if self.interrupt_measurement_called:
                break
            initial_mag = read_lockin_mag()
            #step on axis 3 and remeasure
            new_y = current_y + y_step
            move_axis('axis_3', new_y)
            mag_y = read_lockin_mag()
            #same on axis 4
            new_x = current_x + x_step
            move_axis('axis_4', new_x)
            mag_x = read_lockin_mag()
            change_y_pct = (mag_y - initial_mag)/initial_mag
            change_x_pct = (mag_x - initial_mag)/initial_mag 
            print("fractional changes", change_y_pct, change_x_pct)
            grad_y = change_y_pct/y_step
            grad_x = change_x_pct/x_step
            current_y = new_y
            current_x = new_x 
            new_x_step = max(min(grad_x*2000, 50), -50)
            new_y_step = max(min(grad_y*2000, 50), -50)
            x_step = max(min(new_x_step, 2*abs(x_step)), -2*abs(x_step))
            y_step = max(min(new_y_step, 2*abs(y_step)), -2*abs(y_step))
            print("new step size", y_step, x_step)
            steps = steps+1        
        
        
        
        #old version 3
#         print("Ready to start measurement sweep in x")
#         
#         picomotor.settings['axis_4_velocity'] = 2000 #in case it stayed slow
#         x_min = current_x - 100
#         x_max = current_x + 100
#         move_axis('axis_4', x_min - 100)
# #         picomotor.settings.axis_3_target.update_value(x_min - 1000) #start by overshooting to hopefully minimize backlash effects
#         move_axis('axis_4', x_min)
# #         picomotor.settings.axis_3_target.update_value(x_min)
#         picomotor.settings['axis_4_velocity'] = 100 #move slowly to give lock-in time to read
#         time.sleep(0.1)
#         # read continuously from lock-in while moving
#         r_array = np.zeros(0,dtype=float)
#         print("measuring")
#         while not self.interrupt_measurement_called and picomotor.settings['axis_4_position'] < x_max-5: #arbitrarily adding a little slop
#             data = (h.read(True))
#             if '/dev1021/demods/0/sample.r' in data:
#                 r_flat = []
#                 for burst in data['/dev1021/demods/0/sample.r']:
#                     for row in burst['value']:
#                         r_flat = np.concatenate([r_flat,row])
#                 r_array = np.concatenate([r_array, r_flat])
#             move_axis('axis_4', x_max)
# #             picomotor.settings.axis_4_target.update_value(x_max)
#             picomotor.settings.get_lq('axis_4_position').read_from_hardware()
#         max_mag_4 = max(r_array) #find max value read during motion
#         print("max found", max_mag_4)
#         print(r_array)
#         
#         print("returning to start")
#         picomotor.settings['axis_4_velocity'] = 2000 #back to more typical speed
#         move_axis('axis_4', x_min - 100)
#         move_axis('axis_4', x_min)
# #         picomotor.settings.axis_4_target.update_value(x_min - 100) #back to the beginning
# #         picomotor.settings.axis_4_target.update_value(x_min)
#         
#         print("seeking max value")
#         x_target = x_min
#         curr_mag = 0
#         tol = 0.1 #a starting guess
#         step = 10
#         while x_target <= x_max and curr_mag < max_mag_4*(1-tol):
#             move_axis('axis_4', x_target)
#             time.sleep(0.2)
#             #read from lockin 
#             #using read_single isn't working right, so acting like I want the continuous version
#             start_time = time.time()
#             r_array = []
#             while not self.interrupt_measurement_called and time.time()- start_time <0.1:
#                 data = (h.read(True))
#                 if '/dev1021/demods/0/sample.r' in data:
#                     r_flat = []
#                     for burst in data['/dev1021/demods/0/sample.r']:
#                         for row in burst['value']:
#                             r_flat = np.concatenate([r_flat,row])
#                     r_array = np.concatenate([r_array, r_flat])
#             if len(r_array) > 1:
#                 curr_mag = np.mean(r_array)
#                 print(curr_mag/max_mag_4)
#                 x_target = x_target + step
#         if x_target >= x_max - step:
#             print("Warning: failed to reach max intensity, resetting to original")
#             move_axis('axis_4', current_x)
# #             picomotor.settings.axis_4_target.update_value(current_x)
#         
#         #read from lock-in one more time for testing
#         stubborness = 5
#         i=0
#         lockin_read = -1
#         while lockin_read == -1 and i < stubborness:    
#             lockin_read = lockin.read_single(0.08)
#             if len(lockin_read) == 4:
#                 x, y, _, _ = lockin_read #should really fix this to allow directly reading r
#             else:
#                 print(lockin_read)
#             i=i+1
#             if i>1: print("trying again to read")
#         curr_mag = np.sqrt(x**2+y**2)
#         print("new mag", curr_mag, "fraction of max", curr_mag/max_mag_4)
#         
#         #now y
#         picomotor.settings['axis_3_velocity'] = 2000 #in case it stayed slow
#         y_min = current_y - 100
#         y_max = current_y + 100
#         move_axis('axis_3', y_min - 20)
# #         picomotor.settings.axis_3_target.update_value(x_min - 1000) #start by overshooting to hopefully minimize backlash effects
#         move_axis('axis_3', y_min)
# #         picomotor.settings.axis_3_target.update_value(x_min)
#         picomotor.settings['axis_3_velocity'] = 100 #move slowly to give lock-in time to read
#         time.sleep(0.1)
#         # read continuously from lock-in while moving
#         r_array = np.zeros(0,dtype=float)
#         print("measuring")
#         while not self.interrupt_measurement_called and picomotor.settings['axis_3_position'] < y_max-5: #arbitrarily adding a little slop
#             data = (h.read(True))
#             if '/dev1021/demods/0/sample.r' in data:
#                 r_flat = []
#                 for burst in data['/dev1021/demods/0/sample.r']:
#                     for row in burst['value']:
#                         r_flat = np.concatenate([r_flat,row])
#                 r_array = np.concatenate([r_array, r_flat])
#             move_axis('axis_3', y_max)
# #             picomotor.settings.axis_4_target.update_value(x_max)
#             picomotor.settings.get_lq('axis_3_position').read_from_hardware()
#         max_mag_3 = max(r_array) #find max value read during motion
#         print("max found", max_mag_3)
#         print(r_array)
#         
#         print("returning to start")
#         picomotor.settings['axis_3_velocity'] = 2000 #back to more typical speed
#         move_axis('axis_3', y_min - 50)
#         move_axis('axis_3', y_min)
# #         picomotor.settings.axis_4_target.update_value(x_min - 100) #back to the beginning
# #         picomotor.settings.axis_4_target.update_value(x_min)
#         
#         print("seeking max value")
#         y_target = y_min
#         curr_mag = 0
#         tol = 0.1 #a starting guess, might not be a good choice
#         step = 10
#         while y_target <= y_max and curr_mag < max_mag_3*(1-tol):
#             move_axis('axis_3', y_target)
#             time.sleep(0.2)
#             #read from lockin 
#             #using read_single isn't working right, so acting like I want the continuous version
#             start_time = time.time()
#             r_array = []
#             while not self.interrupt_measurement_called and time.time()- start_time <0.1:
#                 data = (h.read(True))
#                 if '/dev1021/demods/0/sample.r' in data:
#                     r_flat = []
#                     for burst in data['/dev1021/demods/0/sample.r']:
#                         for row in burst['value']:
#                             r_flat = np.concatenate([r_flat,row])
#                     r_array = np.concatenate([r_array, r_flat])
#             if len(r_array) > 1:
#                 curr_mag = np.mean(r_array)
#                 print(curr_mag/max_mag_3)
#                 y_target = y_target + step
#         if y_target >= y_max - step:
#             print("Warning: failed to reach max intensity, resetting to original")
#             move_axis('axis_3', current_x)
# #             picomotor.settings.axis_4_target.update_value(current_x)
#         
#         #read from lock-in one more time for testing
#         stubborness = 5
#         i=0
#         lockin_read = -1
#         while lockin_read == -1 and i < stubborness:    
#             lockin_read = lockin.read_single(0.08)
#             if len(lockin_read) == 4:
#                 x, y, _, _ = lockin_read #should really fix this to allow directly reading r
#             else:
#                 print(lockin_read)
#             i=i+1
#             if i>1: print("trying again to read")
#         curr_mag = np.sqrt(x**2+y**2)
#         print("new mag", curr_mag, "fraction of max", curr_mag/max_mag_3)
        
        #old version 2
#         x_to_test = np.arange(-100,100,25) + current_x
#         y_to_test = np.arange(-100,100,25) + current_y
#         mags_x = []
#         mags_y = []
#         
#         
#         repetitions = 1
#         for rep in range(repetitions):
#             for x in x_to_test:
#                 picomotor.settings.axis_3_target.update_value(x)
#                 time.sleep(0.2)
#                 #read from lockin
#                 stubborness = 5
#                 i=0
#                 lockin_read = -1
#                 while lockin_read == -1 and i < stubborness:    
#                     lockin_read = lockin.read_single(0.08)
#                     if len(lockin_read) == 4:
#                         x, y, _, _ = lockin_read 
#                     else:
#                         print(lockin_read)
#                     i=i+1
#                     if i>1: print("trying again to read")
#                 mag = np.sqrt(x**2+y**2)
#                 mags_x.append(mag)
#             print("x", mags_x) 
#             if self.settings['set_to_max']:
#                 #find max of magnitudes, set corresponding position
#                 max_x_i = mags_x.index(max(mags_x))
#                 print(mags_x[max_x_i])
#                 print(x_to_test[max_x_i])
#                 picomotor.settings.axis_3_target.update_value(x_to_test[max_x_i])
#                 time.sleep(0.2)
#             
#             test = True
#             if test:
#                 #read from lockin
#                 stubborness = 5
#                 i=0
#                 lockin_read = -1
#                 while lockin_read == -1 and i < stubborness:    
#                     lockin_read = lockin.read_single(0.08)
#                     if len(lockin_read) == 4:
#                         x, y, _, _ = lockin_read 
#                     else:
#                         print(lockin_read)
#                     i=i+1
#                     if i>1: print("trying again to read")
#                 mag = np.sqrt(x**2+y**2)
#                 print(mag)
                
            
                
        
#         #set positions to test--around current, presumably?
#         #if one mirror, test only the positions of that one mirror
#         if self.settings['one_mirror']:
#             x_to_test = np.arange(-200, 200, 25)
#             y_to_test = np.arange(-200, 300, 25)
#             pos_x, pos_y = np.meshgrid(x_to_test, y_to_test)
#             
#         magnitudes = np.zeros_like(pos_x)
#         
#         
# #         for ii in range(len(pos_x)):
#         for jj in range(len(pos_x[0])):
#                 ii=0
#                 self.set_progress(100*ii/len(pos_x))
# #             for jj in range(len(pos_x[0])):
#                 
#                 if self.interrupt_measurement_called:
#                     break
#                 
#                 #set mirror position to x[ii,jj], y[ii,jj]
#                 picomotor.settings.axis_3_target.update_value(pos_x[ii,jj])
#                 time.sleep(1)
#                 picomotor.settings.axis_4_target.update_value(pos_y[ii,jj])
#                 
# #                 i = 0
# #                 while picomotor.settings['axis_1_position'] != pos_x[ii,jj] and i<10 and not self.interrupt_measurement_called:
# # #                     picomotor.settings.axis_1_target.update_value(pos_x[ii,jj])
# #                     time.sleep(0.05)
# #                     print(picomotor.settings['axis_1_position'], pos_x[ii,jj], i)
# #                     i = i+1
# #                 if i>=10: print("Picomotor position 1 failed to settle.")
# #                 i = 0
# #                 while picomotor.settings['axis_2_position'] != pos_y[ii,jj] and i<10 and not self.interrupt_measurement_called:
# # #                     picomotor.settings.axis_2_target.update_value(pos_y[ii,jj])
# #                     time.sleep(0.05)
# #                     print(picomotor.settings['axis_2_position'], pos_y[ii,jj], i)
# #                     i = i+1
# #                 if i>=10: print("Picomotor position 2 failed to settle.")
# #                 
#                 #read from lockin
#                 stubborness = 5
#                 i=0
#                 lockin_read = -1
#                 while lockin_read == -1 and i < stubborness:    
#                     lockin_read = lockin.read_single(0.08)
#                     if len(lockin_read) == 4:
#                         x, y, _, _ = lockin_read 
#                     else:
#                         print(lockin_read)
#                     i=i+1
#                     if i>1: print("trying again to read")
#                 
#                 mag = np.sqrt(x**2+y**2)
#                 magnitudes[ii,jj] = mag
#         print(magnitudes)
#                 
#         if self.settings['set_to_max']:
#             max_mag_loc = magnitudes.index(max(magnitudes))
#             print(magnitudes[max_mag_loc])
#             print(pos_x[max_mag_loc])
#             print(pos_y[max_mag_loc])
# #             find max of magnitudes, set corresponding position
                
    
    def update_display(self):
        pass
#         self.plotline.setData(self.x_array)
#         self.plot.plot(self.t_array, self.x_array)
#         self.plot.plot(self.t_array, self.y_array)
        
#         if self.first_run_graphics:
#             self.graph_layout.clear()
#             self.plot = self.graph_layout.addPlot(0,0)
#             
#             self.plotline_x = self.plot.plot(pen = 'b')
#             self.plotline_y = self.plot.plot(pen = 'r')
#             self.plotline_ratio = self.plot.plot()
#             legend = self.plot.addLegend()
#             legend.addItem(self.plotline_x, 'x')
#             legend.addItem(self.plotline_y, 'y')
#             legend.addItem(self.plotline_ratio, 'ratio')
#             
#             self.plotline_sig = self.plot_aux.plot(pen = 'y')
#             self.plotline_ref = self.plot_aux.plot(pen = 'g')
#             aux_legend = self.plot_aux.addLegend()
#             aux_legend.addItem(self.plotline_sig, 'signal')
#             aux_legend.addItem(self.plotline_ref, 'reference')
#             
#             self.plot_aux.setXLink(self.plot)                                                                     
#         self.first_run_graphics = False
#         
#         if self.settings['plot_ratio']:
#             x = np.array(self.x_array)
#             y = np.array(self.y_array)
#             self.plotline_ratio.setData(self.t_array, -x/y)
#         else:
#             self.plotline_x.setData(self.t_array, self.x_array)
#             self.plotline_y.setData(self.t_array, self.y_array)
#         self.plotline_ratio.setVisible(self.settings['plot_ratio'])
#         self.plotline_x.setVisible(not self.settings['plot_ratio'])
#         self.plotline_y.setVisible(not self.settings['plot_ratio'])
#             
#         
#         if self.settings['show_probes']:
#             self.plotline_sig.setData(self.t_array,self.detector_sig_array)
#             self.plotline_ref.setData(self.t_array,self.detector_ref_array)
# 
#         self.plot_aux.setVisible(self.settings['show_probes'])
