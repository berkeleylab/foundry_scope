from ScopeFoundry.base_app import BaseMicroscopeApp
from TDTR.pump_probe_align_dashboard import PumpProbeAlignDashboard
#import logging

#logging.basicConfig(level=logging.DEBUG)

class PumpProbeAlignApp(BaseMicroscopeApp):
    
    name = 'pump_probe_align_app'
    
    def setup(self):

        from ScopeFoundryHW.nidaqmx.ni_ai_hw import NI_AI_HW
        hw = self.add_hardware(NI_AI_HW(self, name = "quad_detectors",
                                         channels=[("ai4", "bias_neg"), 
                                                   ("ai0", "bias_pos"),
                                                   ("ai2", "det1_sum"), 
                                                   ("ai1", 'det1_X'),
                                                   ("ai5", 'det1_Y')]))                                        
        hw.settings['input_mode'] = "RSE"
        hw.settings['ni_device'] = 'Dev1'
        
        from ScopeFoundryHW.nidaqmx.ni_ai_stripchart_measure import NI_AI_StripChartMeasure
        #self.add_measurement(NI_AI_StripChartMeasure(self))
        
        self.add_measurement(PumpProbeAlignDashboard(self))
        
        
        
if __name__ == '__main__':
    import sys
    app = PumpProbeAlignApp(sys.argv)
    sys.exit(app.exec_())