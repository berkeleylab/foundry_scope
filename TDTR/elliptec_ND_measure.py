from ScopeFoundry import Measurement
import numpy as np

class ElliptecNDMeasure(Measurement):
    
    name = 'elliptec_ND'
    
#     def __init__(self, app, name=None, hw_name='elliptec'):
#         self.hw_name = hw_name
#         Measurement.__init__(self, app, name=name)
#         self.hw = self.app.hardware[self.hw_name]
        
    def setup(self):
        self.elliptec = self.app.hardware['elliptec']
        self.lockin = self.app.hardware['TDTR_Lockin']
        
        self.useful_range_min = 14 #mm 
        self.useful_range_max = 45
        self.max_power_pos = 15 #mm
        self.bypassed_max = 2 #mm (for stage positions less than this, the ND filter is fully out of the beam path
        
        #coeffs based on calibration data in Sarah notes/lin_nd_pos_vs_volt.txt, should make updateable
        #assuming a logarithmic behavior for the ND filter, which should be true
        self.ND_predict_coeff_0 = 1.176104986033395
        self.ND_predict_coeff_1 = -0.10445508857877778
        
        self.min_step = 0.07 #mm, ~the smallest step I can get the stage to take consistently

    def predict_intens_frac(self, pos):
        """gives the expected voltage for the reference signal (fraction of that with no ND filter) as a function of position"""
        if pos <= self.bypassed_max: 
            return 1
        elif self.useful_range_min <= pos <= self.useful_range_max:
            return np.exp(self.ND_predict_coeff_0 + self.ND_predict_coeff_1*pos)
        else:
            return 0 
        
    def predict_rel_intensity(self, curr_pos, new_pos):
        if curr_pos <= self.bypassed_max or self.useful_range_min < curr_pos < self.useful_range_max:
            return self.predict_intens_frac(new_pos)/self.predict_intens_frac(curr_pos)
        else:
            return -1 #current intensity probably 0, so fraction not defined
            
    def est_pos_for_intensity(self, intens_frac):
        if intens_frac < self.predict_intens_frac(self.useful_range_max):
            print("Requested attenuation probably more than max this ND filter provides")
            return self.useful_range_max
        elif intens_frac > self.predict_intens_frac(self.useful_range_min):
            print("Requested attenuation probably less than min this ND filter provides")
            return self.useful_range_min
        else: #fraction should be achievable 
            return (np.log(intens_frac) - self.ND_predict_coeff_0)/self.ND_predict_coeff_1
        
    def move_coarse(self):     
        verbose = True
        
        if not self.app.hardware['TDTR_Lockin'].settings['connected']:
            print("Please enable lock-in hardware.")
            return -1
        #set up lock-in for measurement (if not done)
        if not hasattr(self.lockin, 'h_dAM'): #if a data acquisition model doesn't exist yet, create one
            h = self.lockin.create_dataAcquisitionModule()
        else: #if it does exist, use it
            h = self.lockin.h_dAM
        self.lockin.subscribe_dataAcquisitionModule(h) #set the lock-in to report the signals useful for measurement:r, x, y, auxin0, auxin1 (at the moment)
        
#         if self.app.measurements['lockin_oscope'].settings['progress'] != 0:
#             print("Unable to automatically balance while oscope is running. Please stop it and try again.")
#             #obviously this isn't ideal. Could possibly have a setup that reads from the dataAcquisitionModule if it's running, but idk what to do if scopeModule is running.
#             #also I don't really know why it crashes if I do both so there's that
#             return -1
        
        print("Attempting coarse balancing of signal and reference.")
        if not self.useful_range_min < self.elliptec.settings['position'] < self.useful_range_max: #beam not passing through ND filter
            if verbose: print("moving stage into range")
            self.elliptec.settings['position'] = self.max_power_pos #move such that beam is passing through filter; arbitrarily using point of min attenuation
         
        if verbose: print("Reading from lock-in")   
#         _, _, curr_signal_mag, curr_ref_mag = self.lockin.read_single(0.1) #read for 100 ms
        curr_signal_mag, curr_ref_mag = self.lockin.read_single(0.1, vals_expected = ['auxin0', 'auxin1']) #read for 100 ms
        if verbose: print("current sig, ref", curr_signal_mag, curr_ref_mag)
            
        if curr_signal_mag < 0.1:
            print('Insufficient signal to match to.')
            return -1 #failed
        elif curr_signal_mag > 9.9:
            print("Warning: signal intensity nearly saturated")
            if curr_ref_mag > 9.9:
                return curr_signal_mag/curr_ref_mag #signals are close to matched, so we're done here
                
        if curr_ref_mag < 0.1:
            self.elliptec.settings['position'] = self.max_power_pos #minimize degree of attenuation
#             _, _, curr_signal_mag, curr_ref_mag = self.lockin.read_single(0.1) #read for 100 ms
            curr_signal_mag, curr_ref_mag = self.lockin.read_single(0.1, vals_expected = ['auxin0', 'auxin1']) #read for 100 ms
            if curr_ref_mag < 0.1:
                print("Minimal reference intensity available. Check alignment.")
                return -1 #failed
            elif curr_ref_mag < curr_signal_mag:
                print("Insufficient reference intensity available to match signal.")
                return -1 #failed
            #if moving to max_power_pos gave enough signal to try to balance, do so
        elif curr_ref_mag > 9.9: #ref mag nearly saturated but signal mag not (or we would have caught it above)
            if verbose: print("reference nearly saturated, increasing attentuation")
            i=0
            while curr_ref_mag > 9.9 and i<3: #try to reduce ref mag
                if self.elliptec.settings['position'] < self.useful_range_max - 5: #increase attenuation--could improve this to be a more intelligent move than +5
                    self.elliptec.settings['position'] = self.elliptec.settings['position'] + 5 
                curr_signal_mag, curr_ref_mag = self.lockin.read_single(0.1, vals_expected = ['auxin0', 'auxin1']) #read for 100 ms
#                 _, _, curr_signal_mag, curr_ref_mag = self.lockin.read_single(0.1) #read for 100 ms
                i=i+1
            if curr_ref_mag > 9.9: #still nearly saturated (I doubt this will happen)
                self.elliptec.settings['position'] = self.useful_range_max - 0.5 #last attempt: go to max attenuation
#                 _, _, curr_signal_mag, curr_ref_mag = self.lockin.read_single(0.1) #read for 100 ms
                curr_signal_mag, curr_ref_mag = self.lockin.read_single(0.1, vals_expected = ['auxin0', 'auxin1']) #read for 100 ms
                if curr_ref_mag > 9.9: 
                    print("Reference intensity nearly saturated even at maximum attenuation, please reduce.")
                    return -1 #can't balance if it's staying saturated
            #if intensity now reasonable, continue to trying to balance
                
        #try to adjust ND filter to make reference match signal
        curr_atten_est = self.predict_intens_frac(self.elliptec.settings['position'])
        if verbose: print("curr_atten_est",curr_atten_est)
        if curr_atten_est == 0:
            print("Stage somehow at an invalid position. Probable error in code.") #trying to avoid confusing myself with div by 0 errors 
            return -1
        assumed_full_mag = curr_ref_mag/curr_atten_est #as a reference point, estimating what the signal intensity would be without the ND filter
        desired_intens_frac = curr_signal_mag/assumed_full_mag
        est_new_pos = self.est_pos_for_intensity(desired_intens_frac)
        if verbose: print("assumed full mag, desired intens_frac, est_new_pos", assumed_full_mag, desired_intens_frac, est_new_pos)
        self.elliptec.settings['position'] = est_new_pos
            
#         _, _, new_signal_mag, new_ref_mag = self.lockin.read_single(0.1) #read for 100 ms
        new_signal_mag, new_ref_mag = self.lockin.read_single(0.1, vals_expected = ['auxin0', 'auxin1']) #read for 100 ms
        return new_signal_mag/new_ref_mag
    
    def move_fine(self, tol = .005):     #tolerance for fractional difference between ref and signal
        import time
        verbose = False
        
        if not self.app.hardware['TDTR_Lockin'].settings['connected']:
            print("Please enable lock-in hardware.")
            return -1
        #set up lock-in for measurement (if not done)
        if not hasattr(self.lockin, 'h_dAM'): #if a data acquisition model doesn't exist yet, create one
            h = self.lockin.create_dataAcquisitionModule()
        else: #if it does exist, use it
            h = self.lockin.h_dAM
        self.lockin.subscribe_dataAcquisitionModule(h) #set the lock-in to report the signals useful for measurement:r, x, y, auxin0, auxin1 (at the moment)
#         
#         if self.app.measurements['lockin_oscope'].settings['progress'] != 0:
#             print("Unable to automatically balance while oscope is running. Please stop it and try again.")
#             #obviously this isn't ideal. Could possibly have a setup that reads from the dataAcquisitionModule if it's running, but idk what to do if scopeModule is running.
#             #also I don't really know why it crashes if I do both so there's that
#             return -1
        
        if verbose: print("Reading from lock-in")   
        start_signal_mag, start_ref_mag = self.lockin.read_single(0.1, vals_expected = ['auxin0', 'auxin1']) #read for 100 ms
#         _, _, start_signal_mag, start_ref_mag = self.lockin.read_single(0.1) #read for 100 ms
        
        if start_signal_mag < 0.1 or start_ref_mag < 0.1:
            print("Not enough signal to balance.")
            return -1
        
        if start_signal_mag > 9.9 or start_ref_mag > 9.9:
            print("Warning: signal or reference intensity near saturation.")
        
        if (not self.useful_range_min-.1 < self.elliptec.settings['position'] < self.useful_range_max+.1) or (not 0.8 < start_signal_mag/start_ref_mag < 1.2):
            print("Stage out of range or signals too unbalanced. Please use move_coarse first.")
            return -1
        
        sig_ref_diff = np.abs(start_ref_mag-start_signal_mag)/start_signal_mag
        if verbose: print("Percent difference between sig and ref:", sig_ref_diff*100, " tol:", tol*100)
        
        if sig_ref_diff < tol: 
            if verbose: print("Signals already balanced within tolerance.") #nothing to do here
            return start_signal_mag/start_ref_mag #could be consistent about fractional difference or ratio rather than both
            
#         print("Attempting fine balancing of signal and reference.")
        
        start_pos = self.elliptec.settings['position']
        if verbose: print("Starting position: ", start_pos)
        est_dIdx = self.ND_predict_coeff_1
        desired_dI = start_signal_mag - start_ref_mag #I think that's the right sign?
        start_dx = desired_dI/est_dIdx
        if verbose: print("dIdx, dI, estdx:", est_dIdx, desired_dI, start_dx)
        
        if verbose: print("Moving")
        self.elliptec.settings['position'] = self.elliptec.settings['position'] + start_dx
        time.sleep(0.2)
        
        tries = 1
        tried_small_step = False
        
        while tries < 5:        
            new_signal_mag, new_ref_mag = self.lockin.read_single(0.1, vals_expected = ['auxin0', 'auxin1']) #read for 100 ms
#             _, _, new_signal_mag, new_ref_mag = self.lockin.read_single(0.1) #read for 100 ms
            
            if verbose: print("New signal and reference mags: ", new_signal_mag, new_ref_mag)
            
            if np.abs(new_ref_mag-new_signal_mag)/new_signal_mag < tol:
                time.sleep(0.3)
                return new_signal_mag/new_ref_mag #success
            
            if np.abs((new_signal_mag - start_signal_mag)/start_signal_mag) > 0.1: 
                print("Signal magnitude changed too much to allow balancing.")
                return -1
        
            actual_dI = new_ref_mag - start_ref_mag
            new_desired_dI = new_signal_mag - new_ref_mag
            new_pos = self.elliptec.settings['position']
            actual_dx = new_pos-start_pos
            if verbose: print("Actual dx: ", actual_dx)
            
#             if new_desired_dI > desired_dI: #not what should happen:
#                 print("That got worse, check signs in code")
            
            new_est_dIdx = actual_dI/actual_dx  #super noise intolerant for small dx, should improve
            new_dx = new_desired_dI/new_est_dIdx
            if np.abs(new_dx) < self.min_step:
                if tried_small_step: #worth asking for a very small step once, but after that not much happens
                    print("Adjusted within stage capability.")
                    time.sleep(0.3)
                    return new_signal_mag/new_ref_mag #a form of success I hope
                else:
                    tried_small_step = True
            if verbose: print("New dx: ", new_dx)
            
            start_pos = new_pos            
            if verbose: print("Moving")
            self.elliptec.settings['position'] = self.elliptec.settings['position'] + new_dx
            time.sleep(0.2)
            
            start_signal_mag = new_signal_mag
            start_ref_mag = new_ref_mag
#             start_dx = new_dx #could make these one var and just overwrite value; leaving this for now for readability
            
#             print("try", tries)
            tries = tries + 1
            
#             if self.interrupt_measurement_called: #this always interrupts--I think because the function is called without the measurement running? ie I'm not using scopeFoundry correctly
#                 print("interrupted")
#                 break
            
        #if code gets here, optimization didn't work
        print("Failed to balance within tol after 5 tries.")
        return -1

    
    def setup_figure(self):
        pass
        
    def run(self):
        pass
        
    
                
    