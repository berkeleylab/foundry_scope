from ScopeFoundry import Measurement
import pyqtgraph as pg
import pyqtgraph.dockarea as dockarea

class AugerSyncScanView(Measurement):
    
    name = "auger_scan_view"
    
    def setup(self):
        
        pass
    
    def setup_figure(self):
        
        self.ui = self.dockarea = dockarea.DockArea()
        
        self.imview = pg.ImageView()
        self.imview.getView().invertY(False) # lower left origin
        self.image_dock = self.dockarea.addDock(name='Image', widget=self.imview)
        self.graph_layout = pg.GraphicsLayoutWidget()
        self.spec_dock = self.dockarea.addDock(name='Spec Plot', widget=self.graph_layout)
        
        