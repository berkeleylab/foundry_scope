from ScopeFoundry import Measurement
from ScopeFoundryHW.sync_raster_daq import SyncRasterScan
import time
import numpy as np
from scipy import interpolate

#from Auger.auger_spectrum import AugerSpectrum

import configparser
from datetime import datetime
import os
from ScopeFoundry.helper_funcs import str2bool
import json
import pyqtgraph

from qtpy import QtWidgets

'''
    Todo list
    Finish output scan callback low level **DONE**
    track per-frame offset due to rt drift correction or operator action in HDF **DONE**
    display per-frame offset
    track span and timestamp
        get display working in real distance units
    Should CTR be dropped since not used? 
    Implement shutdown of EHT, mult after acq  **DONE** ('eht_off_at_end' option, mult always turns off)
    Implement multi-peak hyperspectral acq, HDF, viewer...
        config-parser ini to save peak data [name, 
    Add area-integrated spectrum to RT display
    Save/load SEM params, requires manual gun align info
    
    Modify remcon32_hw so kV is only updated from HW if EHT is on (otherwise 
        saved value set to zero. kV control should remember target while EHT is off
'''

class AugerSyncRasterScan(SyncRasterScan):
    
    name = "auger_sync_raster_scan"
    
    def setup(self):
        SyncRasterScan.setup(self)
        self.display_update_period = 0.1
        
        self.disp_chan_choices +=  ['auger{}'.format(i) for i in range(10)] + ['sum_auger']
        self.settings.display_chan.change_choice_list(tuple(self.disp_chan_choices))
        
        self.settings.New('ke_start', dtype=float, initial=30,unit = 'V',vmin=5,vmax = 2200)
        self.settings.New('ke_end',   dtype=float, initial=600,unit = 'V',vmin=1,vmax = 2200)
        self.settings.New('ke_delta', dtype=float, initial=0.5,unit = 'V',vmin=0.02979,vmax = 2200,si=True)
        self.settings.New('pass_energy', dtype=float, initial=50,unit = 'V', vmin=5,vmax=500)
        self.settings.New('crr_ratio', dtype=float, initial=5, vmin=1.5,vmax=20)
        self.settings.New('CAE_mode', dtype=bool, initial=False)      

        for lq_name in ['ke_start', 'ke_end', 'ke_delta', 'CAE_mode']:
            self.settings.get_lq(lq_name).add_listener(self.compute_ke)
        
        self.settings.New('eht_off_at_end', dtype=bool, initial=False)
        
        
        self.settings.New('No_dispersion', dtype=bool, initial=False)
        self.settings.New('Chan_sum', dtype=bool, initial=True)
        
        self.settings.New('auto_focus', dtype=bool, initial=False)
        self.settings.New('frames_before_focus', dtype=int, initial=5, vmin=1)

        #self.ui.centralwidget.layout().addWidget(self.settings.New_UI(), 0,0)
        #self.testui = self.settings.New_UI()
        #self.testui.show()
        
        self.settings_scrollarea = QtWidgets.QScrollArea()
        self.settings_scrollarea.setWidget(self.settings.New_UI())
        self.ui.details_groupBox.layout().addWidget(self.settings_scrollarea)
        
    def compute_ke(self):
        '''
        calculate ke[i] for each channel over spectral region based on dispersion and
        analyzer parameters
        
        channel zero is center channel and auger analyzer requested energy
        '''
        self.analyzer_hw = self.app.hardware['auger_electron_analyzer']

        CAE_mode = self.settings['CAE_mode']
        ke_start = self.settings['ke_start']
        low_ke = min( self.analyzer_hw.analyzer.get_chan_ke(ke_start,CAE_mode))
        ke_end = self.settings['ke_end']
        high_ke = max( self.analyzer_hw.analyzer.get_chan_ke(ke_end,CAE_mode))
        ke_delta = self.settings['ke_delta']
        self.npoints = int((high_ke-low_ke)/ke_delta)+1
        self.ke = np.zeros((7,self.npoints))
        self.ke[0,:] = np.linspace(low_ke,high_ke,num=self.npoints)
        self.ke_interp = self.ke[0,:].copy()
        
        for i in range(self.npoints):
            ke0 = self.ke[0,i]
            self.ke[:,i] = self.analyzer_hw.analyzer.get_chan_ke(ke0,CAE_mode)
        self.settings['n_frames'] = self.npoints
        
        # Global Spectrum array in (7xframes) in counts/sec
        self.global_spectrum_chan = np.zeros_like(self.ke, dtype=float)
        self.global_spectrum = np.zeros_like(self.ke_interp, dtype=float)
        
        return self.ke


    def on_start_of_run(self):
        # Hardware
        self.auger_fpga_hw = self.app.hardware['auger_fpga']
        self.auger_fpga_hw.settings['trigger_mode'] = 'off'
        time.sleep(0.01)
        self.auger_fpga_hw.flush_fifo()

        self.auger_fpga_hw.settings['trigger_mode'] = 'pxi'
        
        self.analyzer_hw = self.app.hardware['auger_electron_analyzer']
        
        self.analyzer_hw.settings['multiplier'] = True
        self.analyzer_hw.settings['CAE_mode'] = self.settings['CAE_mode']
        self.analyzer_hw.settings['KE'] = self.settings['ke_start']
        self.analyzer_hw.settings['pass_energy'] = self.settings['pass_energy']
        self.analyzer_hw.settings['crr_ratio'] = self.settings['crr_ratio']
        
        
        time.sleep(3.0) #let electronics settle, multiplier ramp up finish

        # set up KE array and global spectrum array
        self.compute_ke()
        
        
        
    def pre_scan_setup(self):
        # Data Arrays
        self.auger_queue = []
        self.auger_i = 0
        self.auger_total_i = 0
        self.auger_chan_pixels = np.zeros((self.Npixels, 10), dtype=np.uint32)
        self.auger_chan_map = np.zeros( self.scan_shape + (10,), dtype=np.uint32)
        if self.settings['save_h5']:
            self.auger_chan_map_h5 = self.create_h5_framed_dataset("auger_chan_map", self.auger_chan_map)            
            self.h5_m['ke'] = self.ke
            self.h5_m['ke_interp'] = self.ke_interp
            self.global_spectrum_h5 = self.h5_m.create_dataset('global_spectrum', self.global_spectrum.shape, dtype=float)
            self.global_spectrum_chan_h5 = self.h5_m.create_dataset('global_spectrum_chan', self.global_spectrum_chan.shape, dtype=float)
            
        # figure?
        
        self.app.hardware['sem_remcon'].read_from_hardware()
        
    def handle_new_data(self):
        """ Called during measurement thread wait loop"""
        SyncRasterScan.handle_new_data(self)
        
        new_auger_data = self.auger_fpga_hw.read_fifo()
        self.auger_queue.append(new_auger_data)
        
        #ring_buf_index_array = (i + np.arange(n, dtype=int)) % self.Npixels
        #self.auger_chan_pixels[ring_buf_index_array] = new_auger_data
        
        while len(self.auger_queue) > 0:
            # grab the next available data chunk
            #print('new_adc_data_queue' + "[===] "*len(self.new_adc_data_queue))            
            new_data = self.auger_queue.pop(0)
            i = self.auger_i
            n = new_data.shape[0]

            # grab only up to end of frame, put the rest back in the queue
            if i + n > self.Npixels:
                split_index = (self.Npixels - i )
                print("split", i, n, split_index, self.Npixels)
                self.auger_queue.append(new_data[split_index: ])
                new_data = new_data[0:split_index]
                n = new_data.shape[0]
            
            # copy data to image shaped map
            self.auger_chan_pixels[i:i+n,:] = new_data
            x = self.scan_index_array[i:i+n,:].T
            self.auger_chan_map[x[0], x[1], x[2],:] = new_data
            
            # new frame
            if self.auger_i == 0:
                frame_num = (self.auger_total_i // self.Npixels) - 1
                
                # Calculate global spectrum
                self.global_spectrum_chan[:,frame_num] = self.auger_chan_map[:,:,:,0:7].mean(axis=(0,1,2))
                self.global_spectrum_chan[:,frame_num] /=  self.settings['pixel_time']
                self.global_spectrum[:] = 0
                for i in range(0,7):
                    x = self.ke[i,:]
                    y = self.global_spectrum_chan[i,:]
                    ff = interpolate.interp1d(x,y,bounds_error=False,fill_value='extrapolate')
                    self.global_spectrum += ff(self.ke_interp)

                
                if self.settings['save_h5']:
                    self.global_spectrum_h5[:] = self.global_spectrum[:]
                    self.global_spectrum_chan_h5[:, frame_num] = self.global_spectrum_chan[:, frame_num]

                    # Write Auger channel map to H5
                    self.extend_h5_framed_dataset(self.auger_chan_map_h5, frame_num)
                    self.auger_chan_map_h5[frame_num,:,:,:,:] = self.auger_chan_map
                    
                    

            if self.interrupt_measurement_called:
                break
        
            self.auger_i += n
            self.auger_total_i += n 
            self.auger_i %= self.Npixels

        print(new_auger_data.shape)

    
    def post_scan_cleanup(self):
        self.auger_fpga_hw.settings['trigger_mode'] = 'off'
        self.analyzer_hw.settings['multiplier'] = False
        self.analyzer_hw.settings['KE'] = self.ke[0,0]
        if self.settings['eht_off_at_end']:
            self.app.hardware['sem_remcon'].settings['eht_on'] = False
            
    def get_display_pixels(self):
        #SyncRasterScan.get_display_pixels(self)
        #self.display_pixels = self.auger_chan_pixels[:,0:8].sum(axis=1)
        #self.display_pixels[0] = 0
        #self.display_pixels = self._pixels[:,0]
        
        #DISPLAY_CHAN = 0
        #self.display_pixels = self.adc_pixels[:,DISPLAY_CHAN]
        #self.display_pixels[0] = 0
        
        chan = self.settings['display_chan']
        if 'auger' in chan:
            if chan == 'sum_auger':
                self.display_pixels = self.auger_chan_pixels[:,0:7].sum(axis=1)
            else:
                self.display_pixels = self.auger_chan_pixels[:,int(chan[-1])]
        else:
            return SyncRasterScan.get_display_pixels(self)


    
    def on_new_frame(self, frame_i):
        SyncRasterScan.on_new_frame(self, frame_i)
        
        self.analyzer_hw.settings['KE'] = self.ke[0,frame_i]
        print("New Frame: ")
    
    def on_end_frame(self, frame_i):
        SyncRasterScan.on_end_frame(self, frame_i)
        
        # Need to figure out how to pause scanning for auto-focus
        
#         # Auto-focus -- Run auto-focus routine here
#         if np.mod(frame_i+1, self.settings['frames_before_focus']) == 0:
#             wd_cur = self.app.hardware['sem_remcon'].settings['WD'] # Gives value in mm
#             # Take images at working distances over a +- 50 um range with 5 um precision (20 images)
#             wd_range = np.arange(wd_cur-0.050,wd_cur+0.050,0.005)
#             # Need to perform the scans...
    


from qtpy import QtWidgets, QtGui
import pyqtgraph as pg

class MultiSpecAugerScan(Measurement):
    
    name = 'multi_spec_auger_scan'
    
    def setup(self):
        
        self.settings.New('multispec_setting_names', dtype=str)
        self.settings.New('element_db_file', dtype='file', initial='auger_element_db.ini')
        self.settings.New('element_to_add', dtype=str, initial='Ag_default', choices=['Ag_default'])
        self.settings.New('eht_off_after_multispec', dtype=bool)
        #self.settings.New('scan_params', dtype=str)
        
        self.auger_scan = self.app.measurements['auger_sync_raster_scan']
        
        self.add_operation('add_element', self.load_from_db)
        
        self.settings.element_db_file.add_listener(self.on_new_element_db)
        
    def setup_figure(self):
        
        self.ui = QtWidgets.QWidget()
        self.ui.setLayout(QtWidgets.QVBoxLayout())

        self.ui.layout().addWidget(self.settings.New_UI(), stretch=0)
        
        pb = self.add_el_pushButton = QtWidgets.QPushButton('Add Element')
        self.ui.layout().addWidget(pb)
        pb.clicked.connect(self.load_from_db)

        pb = self.load_state_pushButton = QtWidgets.QPushButton('Load Scans...')
        self.ui.layout().addWidget(pb)
        pb.clicked.connect(self.file_browse_json)
        
        
        
        self.ptree = pg.parametertree.ParameterTree()
        self.ui.layout().addWidget(self.ptree, stretch=1)
        
        params = [
            #{'name': 'Ag', 'type':'int', 'children':[]}
            ]
        
        self.p = pg.parametertree.Parameter.create(name='params', type='group', children=params)
        self.ptree.setParameters(self.p, showTop=False)
        
    
    def on_new_element_db(self):
        self.config = configparser.RawConfigParser()
        self.config.optionxform = lambda option: option
        self.config.read(self.settings['element_db_file'])
        self.settings.element_to_add.change_choice_list(list(self.config.keys()))
    
    def run(self):
        scans = self.p.children()        
        scans.sort(key=lambda s: s.value())
        
        self.p.clearChildren()
        self.p.addChildren(scans)
        
        time0 = time.time()
        
        fname = self.app.generate_data_path(measurement=self, ext='ini', t=time0)    
        self.app.settings_save_ini(fname)
        
        fname = self.app.generate_data_path(measurement=self, ext='json', t=time0)
        import json
        with open(fname, 'w') as f:
            json.dump(self.p.saveState(), f, indent=4)
        
        self.analyzer_hw = self.app.hardware['auger_electron_analyzer']
        
        interrupted = False
        
        #names_to_read = self.settings['multispec_setting_names'].split(',')
        #names_to_read = [name.strip() for name in names_to_read]
        for i, scan in enumerate(scans):
            #scan.set('str')
            #scan.setValue("Running...")
            if self.interrupt_measurement_called:
                self.auger_scan.interrupt()
                interrupted = True
                break
            self.set_progress(100.*(i+0.5) /len(scans))
            for key, (val, children) in scan.getValues().items():
                # Update measurement settings
                if key in ['ke_start', 'ke_end', 'ke_delta', 'pass_energy', 'crr_ratio', 'CAE_mode', 'adc_oversample']:
                    self.auger_scan.settings[key] = val
                # Update analyzer hardware settings
                elif key =='analyzer_quad_X1':
                    self.analyzer_hw.settings['quad_X1'] = val
                elif key =='analyzer_quad_Y1':
                    self.analyzer_hw.settings['quad_Y1'] = val

            # Start the measurement
            print('Start measurement scan', scan.name())
            time.sleep(1.0)
#             self.auger_scan.start()
#  
#             # Wait for measurement to finish before running next measurement
#             while self.auger_scan.is_measuring():
#                 if self.interrupt_measurement_called:
#                     self.auger_scan.interrupt()
#                     interrupted = True
#                     break
#                 #self.auger_scan.update_display()
#                 time.sleep(1.0)
#             time.sleep(5.0)

            self.start_nested_measure_and_wait(measure=self.auger_scan)#, start_time=5)
            print(scan.name(), "done  -->", self.auger_scan.h5_filename)
            if not("filename" in scan.names):
                scan.addChild({"name":"filename", 'ro':True, 'type': 'str', 'removable':True })
            scan.child("filename").setValue(self.auger_scan.h5_filename)
            #scan.setType('int')
            #scan.setValue(i)

        
        if not interrupted and self.settings['eht_off_after_multispec']:
            self.app.hardware['sem_remcon'].settings['eht_on'] = False
        
        fname = self.app.generate_data_path(measurement=self, ext='json', t=time0)
        import json
        with open(fname, 'w') as f:
            json.dump(self.p.saveState(), f, indent=4)

        
    def update_display(self):
        #self.auger_scan.update_display()
        pass
#         params = [
#             {'name': 'Ag', 'type':'int', 'renamable': True, 'children':[ {'name': 'ke_start', 'type': 'float'}]},
#             ]
#         params = [
#             {'name': 'Ag', 'type':'int', 'children':[]}
#             ]
#         self.p = pg.parametertree.Parameter.create(name='params', type='group', children=params)
#         self.ptree.setParameters(self.p, showTop=False)
        
    def load_from_db(self, name=None):
        if not name:
            name = self.settings['element_to_add']
        print("load_from_db", name)
        self.config = configparser.RawConfigParser()
        self.config.optionxform = lambda option: option
        self.config.read(self.settings['element_db_file'])

        g = self.p.addChild({'name': name, 'type':'int', 'removable':True, 'renamable':True}, autoIncrementName=True)


        for key in self.config[name]:
            val = self.config[name][key]
            if key == 'CAE_mode':
                type = 'bool'
                val = str2bool(val)
            else:
                type = 'float'
                val = float(val)
            g.addChild({'name':key, 'type':type, 'value':val, 'removable':True})
            
    def load_from_json(self, fname):
        import json 
        with open(fname, 'r') as f:
            params = json.load(f)


        self.p.restoreState(params)

        
        print("load_from_json",params)
        
    def file_browse_json(self):
        fname, _ = QtWidgets.QFileDialog.getOpenFileName(None, "Settings (*.json)")
        print("file_browse_json",repr(fname))
        if fname:
            self.load_from_json(fname)

    