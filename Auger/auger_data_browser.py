'''
Created on Mar 2, 2022

@author: lab
'''

from ScopeFoundry.data_browser import DataBrowser
import logging
import sys
import FoundryDataBrowser.viewers as viewers
import traceback

# views are loaded in order of more generic to more specific.
## ie the last loaded views are checked first for compatibility

app = DataBrowser(sys.argv)


def _on_load_err(err):
    text = "".join(traceback.format_exc(limit=None, chain=True))
    print("Failed to load viewer with error:", err, file=sys.stderr)
    print(text, file=sys.stderr)


from FoundryDataBrowser.viewers.h5_tree import H5TreeView, H5TreeSearchView
app.load_view(H5TreeView(app))
app.load_view(H5TreeSearchView(app))

try:
    from FoundryDataBrowser.viewers.h5_tree import H5TreeView, H5TreeSearchView
    app.load_view(H5TreeView(app))
    app.load_view(H5TreeSearchView(app))
except Exception as err:    _on_load_err(err)


try:
    from PIL import Image
    from FoundryDataBrowser.viewers.images import PILImageView
    app.load_view(PILImageView(app))
except ImportError:
    logging.warning("missing PIL library")    
except Exception as err:    _on_load_err(err)    

try:
    from FoundryDataBrowser.viewers.sync_raster_scan_h5 import SyncRasterScanH5
    app.load_view(SyncRasterScanH5(app))
except Exception as err:    _on_load_err(err)

try:
    from FoundryDataBrowser.viewers.auger_spec_map import AugerSpecMapView
    app.load_view(AugerSpecMapView(app))
except Exception as err:    _on_load_err(err)

try:
    from FoundryDataBrowser.viewers.auger_sync_raster_scan_h5 import AugerSyncRasterScanH5View
    app.load_view(AugerSyncRasterScanH5View(app))
except Exception as err:    _on_load_err(err)

try:
    from FoundryDataBrowser.viewers.auger_spectrum_h5 import AugerSpectrumH5
    app.load_view(AugerSpectrumH5(app))
except Exception as err:    _on_load_err(err)




app.exec_()
