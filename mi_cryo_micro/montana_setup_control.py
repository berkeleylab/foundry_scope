from ScopeFoundry import Measurement
from ScopeFoundry.helper_funcs import load_qt_ui_file, sibling_path, replace_widget_in_layout
import pyqtgraph as pg
import time

class MontanaSetupControlPanel(Measurement):

    name='montanta_setup_control_panel'


    def setup(self):
        self.ui=load_qt_ui_file(sibling_path(__file__, 'montana_setup_control.ui'))
        self.hw=self.app.hardware
        self.meas=self.app.measurements

        #Connect the Hardware Input widgets of the UI
        self.hw['TiSa_flip'].settings.named_position.connect_to_widget(self.ui.Flipmirror_TiSa)
        self.ui.Flipmirror_TiSa.currentTextChanged.connect(self.change_TiSa_color)
        self.change_TiSa_color(self.ui.Flipmirror_TiSa.currentText())

        self.hw['532nm_flip'].settings.named_position.connect_to_widget(self.ui.Flipmirror_532nm)
        self.ui.Flipmirror_532nm.currentTextChanged.connect(self.change_532nm_color)
        self.change_532nm_color(self.ui.Flipmirror_532nm.currentText())

        self.hw['beam_block'].settings.named_position.connect_to_widget(self.ui.Flip_BeamBlock)
        self.ui.Flip_BeamBlock.currentTextChanged.connect(self.change_BeamBlock_color)
        self.change_BeamBlock_color(self.ui.Flip_BeamBlock.currentText())

        self.hw['rot_filter_exc'].settings.target_position.connect_to_widget(self.ui.Rotation_filter_excitation)
        self.hw['fix_ND'].settings.named_position.connect_to_widget(self.ui.ND_filterwheel_fixed)
        self.hw['var_ND1'].settings.target_position.connect_to_widget(self.ui.ND_filterwheel_var1)
        self.hw['var_ND2'].settings.target_position.connect_to_widget(self.ui.ND_filterwheel_var2)
        self.hw['power_meter_flip'].settings.named_position.connect_to_widget(self.ui.Flipmirror_Powermeter)
        self.hw['output_select'].settings.named_position.connect_to_widget(self.ui.Output_select)
        self.hw['beamsplitter_select'].settings.named_position.connect_to_widget(self.ui.BeamSplitter_select)
        self.hw['wire_coupling_select'].settings.named_position.connect_to_widget(self.ui.WireCoupling_select)
        self.hw['rot_filter_det'].settings.target_position.connect_to_widget(self.ui.Rotation_filter_detection)
        self.hw['filter_wheel_det'].settings.named_position.connect_to_widget(self.ui.filterwheel_det)
        self.hw['rot_filter_det_flip'].settings.named_position.connect_to_widget(self.ui.filterwheel_det_flip)
        self.hw['detection_select'].settings.named_position.connect_to_widget(self.ui.Detection_select)
        self.hw['andor_spec'].settings.grating_id.connect_to_widget(self.ui.Grating_select)

        self.hw['andor_spec'].settings.output_flipper.connect_to_widget(self.ui.Spectro_out_select)
        self.ui.Spectro_out_select.currentTextChanged.connect(self.change_Spectro_Out_color)
        self.change_Spectro_Out_color(self.ui.Spectro_out_select.currentText())

        self.hw['andor_spec'].settings.input_flipper.connect_to_widget(self.ui.Spectro_in_select)
        self.ui.Spectro_in_select.currentTextChanged.connect(self.change_Spectro_In_color)
        self.change_Spectro_In_color(self.ui.Spectro_in_select.currentText())

        self.hw['lens_fourier100'].settings.named_position.connect_to_widget(self.ui.Fliplens_Fourier_f100)
        self.hw['lens_fourier300'].settings.named_position.connect_to_widget(self.ui.Fliplens_Fourier_f300)
        self.hw['lens_fourier400'].settings.named_position.connect_to_widget(self.ui.Fliplens_Fourier_f400)
        self.hw['thorlabs_powermeter'].settings.wavelength.connect_to_widget(self.ui.Powermeter_wavelength)
        self.hw['thorlabs_powermeter'].settings.power.connect_bidir_to_widget(self.ui.Powermeter_power)
        #self.ui.Powermeter_power_read = replace_widget_in_layout(self.ui.doubleSpinBox,
        #                                                               pg.widgets.SpinBox.SpinBox())
        #self.hw['thorlabs_powermeter'].settings.power.connect_bidir_to_widget(self.ui.Powermeter_power_read)
        self.ui.start_Powermeter.clicked.connect(self.meas['powermeter_optimizer'].start)
        self.ui.stop_Powermeter.clicked.connect(self.meas['powermeter_optimizer'].interrupt)
        self.ui.zero_Powermeter.clicked.connect(self.hw['thorlabs_powermeter'].run_zero)




        #Connect the "connected status" checkboxes of the UI
        self.hw['TiSa_flip'].settings.connected.connect_to_widget(self.ui.checkBox_TiSa_In)

        self.hw['532nm_flip'].settings.connected.connect_to_widget(self.ui.checkBox_532nm_In)

        self.hw['beam_block'].settings.connected.connect_to_widget(self.ui.checkBox_Block)
        
        self.hw['rot_filter_exc'].settings.connected.connect_to_widget(self.ui.checkBox_Fil_exc)

        self.hw['fix_ND'].settings.connected.connect_to_widget(self.ui.checkBox_fix_ND)

        self.hw['var_ND1'].settings.connected.connect_to_widget(self.ui.checkBox_var_ND1)

        self.hw['var_ND2'].settings.connected.connect_to_widget(self.ui.checkBox_var_ND2)

        self.hw['power_meter_flip'].settings.connected.connect_to_widget(self.ui.checkBox_PwrMtr)

        self.hw['output_select'].settings.connected.connect_to_widget(self.ui.checkBox_Output)

        self.hw['beamsplitter_select'].settings.connected.connect_to_widget(self.ui.checkBox_Beamspl)

        self.hw['wire_coupling_select'].settings.connected.connect_to_widget(self.ui.checkBox_Wire_out)

        self.hw['rot_filter_det'].settings.connected.connect_to_widget(self.ui.checkBox_Fil_det)

        self.hw['rot_filter_det_flip'].settings.connected.connect_to_widget(self.ui.checkBox_Fil_det)
        
        self.hw['detection_select'].settings.connected.connect_to_widget(self.ui.checkBox_Filterwheel_det_pos)



        self.hw['andor_spec'].settings.connected.connect_to_widget(self.ui.checkBox_Spectro)

        self.hw['lens_fourier100'].settings.connected.connect_to_widget(self.ui.checkBox_Four_100)

        self.hw['lens_fourier300'].settings.connected.connect_to_widget(self.ui.checkBox_Four_300)

        self.hw['lens_fourier400'].settings.connected.connect_to_widget(self.ui.checkBox_Four_400)

        self.hw['andor_ccd'].settings.connected.connect_to_widget(self.ui.checkBox_CCD)

        self.hw['apd_counter'].settings.connected.connect_to_widget(self.ui.checkBox_APD_spec)

        self.hw['flircam'].settings.connected.connect_to_widget(self.ui.checkBox_Flircam)

        self.hw['dynamixel_servos'].settings.connected.connect_to_widget(self.ui.checkBox_Servos)

        self.hw['thorlabs_powermeter'].settings.connected.connect_to_widget(self.ui.checkBox_Powermeter)

        self.hw['filter_wheel_det'].settings.connected.connect_to_widget(self.ui.checkBox_Filterwheel_det)

        self.hw['apd_stage'].settings.connected.connect_to_widget(self.ui.checkBox_APD_stage)

        self.hw['Nanopositioner_stage'].settings.connected.connect_to_widget(self.ui.checkBox_NanoPos)

        self.hw['Piezo_xy_stage'].settings.connected.connect_to_widget(self.ui.checkBox_PiezoPos)
        
        self.hw['picoharp'].settings.connected.connect_to_widget(self.ui.checkBox_PicoHarp)

        self.hw['andor3_camera'].settings.connected.connect_to_widget(self.ui.checkBox_CMOS)

        


        #connect the "connect all hardware" button
        self.ui.Button_connectALLHardware.clicked.connect(self.connectHardwareClicked)

        



        
    def connectHardwareClicked(self): 
        #self.ui.checkBox_TiSa_In.toggle().
        self.hw['TiSa_flip'].settings['connected']=True
        self.hw['532nm_flip'].settings['connected']=True
        self.hw['beam_block'].settings['connected']=True
        self.hw['rot_filter_exc'].settings['connected']=True
        self.hw['fix_ND'].settings['connected']=True
        self.hw['var_ND1'].settings['connected']=True
        self.hw['var_ND2'].settings['connected']=True
        self.hw['power_meter_flip'].settings['connected']=True
        self.hw['output_select'].settings['connected']=True
        self.hw['beamsplitter_select'].settings['connected']=True
        self.hw['wire_coupling_select'].settings['connected']=True
        self.hw['rot_filter_det'].settings['connected']=True
        self.hw['rot_filter_det_flip'].settings['connected']=True
        self.hw['detection_select'].settings['connected']=True
        self.hw['andor_spec'].settings['connected']=True
        self.hw['lens_fourier100'].settings['connected']=True
        self.hw['lens_fourier300'].settings['connected']=True
        self.hw['lens_fourier400'].settings['connected']=True
        self.hw['andor_ccd'].settings['connected']=True
        self.hw['apd_counter'].settings['connected']=True
        self.hw['flircam'].settings['connected']=True
        self.hw['dynamixel_servos'].settings['connected']=True
        self.hw['thorlabs_powermeter'].settings['connected']=True
        self.hw['filter_wheel_det'].settings['connected']=True
        self.hw['apd_stage'].settings['connected']=True
        self.hw['Nanopositioner_stage'].settings['connected']=True
        self.hw['Piezo_xy_stage'].settings['connected']=True
        self.hw['picoharp'].settings['connected']=True
        self.hw['andor3_camera'].settings['connected']=True


    def change_TiSa_color(self,value):
        if value=="open":
            self.ui.Flipmirror_TiSa.setStyleSheet("QComboBox{background-color: rgb(255,255,255); font: 75 8pt MS Shell Dlg 2; border-style: solid; border-width:1px; border-color:black;} QComboBox:editable {background: qlineargradient(spread:pad, x1:0, y1:0, x2:0, y2:1, stop:0.33 rgba(170, 40, 40, 255), stop:0.5 rgba(170, 40, 40, 255), stop:0.51 rgba(255, 255, 255, 255), stop:1 rgba(255,255,255, 255));}")  
        elif value=="TiSa":
            self.ui.Flipmirror_TiSa.setStyleSheet("QComboBox{background-color: rgb(170, 40, 40); font: 75 8pt MS Shell Dlg 2; border-style: solid; border-width:1px; border-color:black;} QComboBox:editable {background: qlineargradient(spread:pad, x1:0, y1:0, x2:0, y2:1, stop:0.33 rgba(170, 40, 40, 255), stop:0.5 rgba(170, 40, 40, 255), stop:0.51 rgba(255, 255, 255, 255), stop:1 rgba(255,255,255, 255));}")  

    def change_532nm_color(self,value):
        if value=="open":
            self.ui.Flipmirror_532nm.setStyleSheet("QComboBox{background-color: rgb(255,255,255); font: 75 8pt MS Shell Dlg 2; border-style: solid; border-width:1px; border-color:black;} QComboBox:editable {background: qlineargradient(spread:pad, x1:0, y1:0, x2:0, y2:1, stop:0.33 rgba(40,170, 40, 255), stop:0.5 rgba(40, 170, 40, 255), stop:0.51 rgba(255, 255, 255, 255), stop:1 rgba(255,255, 255, 255));}")  
        elif value=="532nm":
            self.ui.Flipmirror_532nm.setStyleSheet("QComboBox{background-color: rgb(40, 170, 40); font: 75 8pt MS Shell Dlg 2; border-style: solid; border-width:1px; border-color:black;} QComboBox:editable {background: qlineargradient(spread:pad, x1:0, y1:0, x2:0, y2:1, stop:0.33 rgba(40,170, 40, 255), stop:0.5 rgba(40, 170, 40, 255), stop:0.51 rgba(255, 255, 255, 255), stop:1 rgba(255,255, 255, 255));}") 

    def change_BeamBlock_color(self,value):
        if value=="open":
            self.ui.Flip_BeamBlock.setStyleSheet("QComboBox{background-color: rgb(255,255,255); font: 75 8pt MS Shell Dlg 2; border-style: solid; border-width:1px; border-color:black;} QComboBox:editable {background: qlineargradient(spread:pad, x1:0, y1:0, x2:0, y2:1, stop:0.33 rgba(120, 120, 120, 255), stop:0.5 rgba(120, 120, 120, 255), stop:0.51 rgba(255, 255, 255, 255), stop:1 rgba(255,255,255, 255));}")  
        elif value=="block_path":
            self.ui.Flip_BeamBlock.setStyleSheet("QComboBox{background-color: rgb(120,120,120); font: 75 8pt MS Shell Dlg 2; border-style: solid; border-width:1px; border-color:black;} QComboBox:editable {background: qlineargradient(spread:pad, x1:0, y1:0, x2:0, y2:1, stop:0.33 rgba(120, 120, 120, 255), stop:0.5 rgba(120, 120, 120, 255), stop:0.51 rgba(255, 255, 255, 255), stop:1 rgba(255,255,255, 255));}")  

    def change_Spectro_In_color(self,value):
        if value=="direct":
            self.ui.Spectro_in_select.setStyleSheet("QComboBox{background-color: rgb(107, 177, 240, 255); font: 75 8pt MS Shell Dlg 2; border-style: solid; border-width:1px; border-color:black;} QComboBox:editable {background: qlineargradient(spread:pad, x1:0, y1:0, x2:0, y2:1, stop:0.33 rgba(107, 177, 240, 255), stop:0.5 rgba(107, 177, 240, 255), stop:0.51 rgba(255,165, 0, 255), stop:1 rgba(255,165, 0, 255));}")  
        elif value=="side":
            self.ui.Spectro_in_select.setStyleSheet("QComboBox{background-color: rgb(255,165, 0, 255); font: 75 8pt MS Shell Dlg 2; border-style: solid; border-width:1px; border-color:black;} QComboBox:editable {background: qlineargradient(spread:pad, x1:0, y1:0, x2:0, y2:1, stop:0.33 rgba(107, 177, 240, 255), stop:0.5 rgba(107, 177, 240, 255), stop:0.51 rgba(255,165, 0, 255), stop:1 rgba(255,165, 0, 255));}")  

    def change_Spectro_Out_color(self,value):
        if value=="direct":
            self.ui.Spectro_out_select.setStyleSheet("QComboBox{background-color: rgb(107, 177, 240, 255); font: 75 8pt MS Shell Dlg 2; border-style: solid; border-width:1px; border-color:black;} QComboBox:editable {background: qlineargradient(spread:pad, x1:0, y1:0, x2:0, y2:1, stop:0.33 rgba(107, 177, 240, 255), stop:0.5 rgba(107, 177, 240, 255), stop:0.51 rgba(255,165, 0, 255), stop:1 rgba(255,165, 0, 255));}")  
        elif value=="side":
            self.ui.Spectro_out_select.setStyleSheet("QComboBox{background-color: rgb(255,165, 0, 255); font: 75 8pt MS Shell Dlg 2; border-style: solid; border-width:1px; border-color:black;} QComboBox:editable {background: qlineargradient(spread:pad, x1:0, y1:0, x2:0, y2:1, stop:0.33 rgba(107, 177, 240, 255), stop:0.5 rgba(107, 177, 240, 255), stop:0.51 rgba(255,165, 0, 255), stop:1 rgba(255,165, 0, 255));}")  


