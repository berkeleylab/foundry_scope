import time
from ScopeFoundryHW.attocube_anc300.anc300_slow_scan import  AttocubeANC300_2DSlowScan
import numpy as np


class PiezoHyperspecScan(AttocubeANC300_2DSlowScan):

    name = 'piezo_hyperspec'

    def __init__(self, app, stage_name):
        AttocubeANC300_2DSlowScan.__init__(self, app, hw_name=stage_name)

    def scan_specific_setup(self):
        pass
        #Hardware                  
        #self.stage = self.app.hardware['asi_stage']
        self.spec  = self.app.measurements['andor_ccd_readout_montana']

    def pre_scan_setup(self):
        self.settings.save_h5.change_readonly(True)
        if 'bg_subtract' in self.spec.settings:
            self.spec.settings['bg_subtract'] = False
        self.spec.settings['continuous'] = False
        self.spec.settings['save_h5'] = False
        time.sleep(0.01)
        self.stage.other_observer = True
    
    def collect_pixel(self, pixel_num, k, j, i):
        #if self.settings['debug']: print("collect_pixel", pixel_num, k,j,i)
        self.spec.interrupt_measurement_called = self.interrupt_measurement_called
        # self.start_nested_measure_and_wait(self.spec)
        self.spec.run()
        print('spectrometer run complete')
        if pixel_num == 0:
            self.log.info("pixel 0: creating data arrays")
            #if self.settings['debug']: print("pixel 0: creating data arrays")
            self.time_array = np.zeros(self.scan_h_positions.shape)
            spec_map_shape = self.scan_shape + self.spec.spectrum.shape
            
            self.spec_map = np.zeros(spec_map_shape, dtype=np.float)
            if self.settings['save_h5']:
                self.spec_map_h5 = self.h5_meas_group.create_dataset(
                                      'spec_map', spec_map_shape, dtype=np.float)
            else:
                self.spec_map_h5 = np.zeros(spec_map_shape)
 
            self.wls = np.array(self.spec.wls)
            if self.settings['save_h5']:
                self.h5_meas_group['wls'] = self.wls

        # store in arrays
        t = self.time_array[pixel_num] = time.time()
        print('time', t)
        spec = np.array(self.spec.spectrum)
        print(f"collect_pixel, {pixel_num}, {k}, {j}, {i}, {spec.shape}")
        self.spec_map[k,j,i,:] = spec
        self.spec_map_h5[k,j,i,:] = spec
        s = self.display_image_map[k,j,i] = spec.sum()
        #if self.settings['debug']: print('time', t, 'sum', s)
        
    def post_scan_cleanup(self):
        self.settings.save_h5.change_readonly(False)
        self.stage.other_observer = False
        self.ui.setWindowTitle(self.name)

    def update_display(self):
        #self.update_time()
        AttocubeANC300_2DSlowScan.update_display(self)
        self.spec.update_display()
