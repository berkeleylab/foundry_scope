'''
Created on Dec 15, 2020

@author: Camille Stavrakas - Sunphil Kim

Updated 2021-06 Ed Barnard, Jonas Zipfel
'''
from ScopeFoundry import BaseMicroscopeApp

import logging
import time
from ScopeFoundry.helper_funcs import load_qt_ui_file, sibling_path
from collections import OrderedDict

'''logging.basicConfig(level='DEBUG')#, filename='m3_log.txt')
#logging.getLogger('').setLevel(logging.WARNING)
logging.getLogger("ipykernel").setLevel(logging.WARNING)
logging.getLogger('PyQt4').setLevel(logging.WARNING)
logging.getLogger('PyQt5').setLevel(logging.WARNING)
logging.getLogger('pyvisa').setLevel(logging.WARNING)
'''

#logging.getLogger('LoggedQuantity').setLevel(logging.INFO)
logging.basicConfig(level=logging.INFO)

class MontanaCryoMicroscopeApp(BaseMicroscopeApp):
    
    
    name = 'MontanaCryo_microscope'
    
    def setup(self):
        
        print("Adding Hardware Components")
        
        '''from ScopeFoundryHW.acton_spec import ActonSpectrometerHW
        self.add_hardware(ActonSpectrometerHW(self))'''

        #add all hardware components first!!

        from ScopeFoundryHW.andor_camera import AndorCCDHW
        self.add_hardware(AndorCCDHW(self))

        from ScopeFoundryHW.ni_daq.hw.ni_freq_counter_callback import NI_FreqCounterCallBackHW
        self.add_hardware(NI_FreqCounterCallBackHW(self, name='apd_counter'))
        
        '''from ScopeFoundryHW.winspec_remote import WinSpecRemoteClientHW
        self.add_hardware_component(WinSpecRemoteClientHW(self))
        
        from ScopeFoundryHW.winspec_remote import WinSpecRemoteReadoutMeasure
        self.add_measurement(WinSpecRemoteReadoutMeasure(self))
        '''
        
        from ScopeFoundryHW.andor_spec.andor_spec_hw import AndorShamrockSpecHW
        self.add_hardware(AndorShamrockSpecHW(self))

        #from ScopeFoundryHW.toupcam.toupcam_hw import ToupCamHW
        #self.add_hardware(ToupCamHW(self))

        from ScopeFoundryHW.flircam import FlirCamHW
        self.add_hardware(FlirCamHW(self))

        from ScopeFoundryHW.thorlabs_powermeter import ThorlabsPowerMeterHW
        self.add_hardware(ThorlabsPowerMeterHW(self))

        from ScopeFoundryHW.newport_esp300.esp300_xyz_stage_hw import ESP300XYZStageHW
        self.add_hardware(ESP300XYZStageHW(self, name='apd_stage', ax_names='_xy'))

        from ScopeFoundryHW.attocube_anc350.anc350_hw import AttocubeANC350StageHW
        self.add_hardware(AttocubeANC350StageHW(self, name='Nanopositioner_stage',ax_names='xyz'))

        from ScopeFoundryHW.attocube_anc300.anc300_hw import AttocubeANC300StageHW
        self.add_hardware(AttocubeANC300StageHW(self, name='Piezo_xy_stage',ax_names='xy'))

        from ScopeFoundryHW.picoharp import PicoHarpHW
        self.add_hardware(PicoHarpHW(self))

        from ScopeFoundryHW.andor3.andor3_hw import Andor3CameraHW
        self.add_hardware(Andor3CameraHW(self))


        # Dynamixel Servos
        from ScopeFoundryHW.dynamixel_servo import DynamixelXServosHW, DynamixelFilterWheelHW, DynamixelServoHW
        self.add_hardware(DynamixelXServosHW(self,
                                             devices={
                                                      'beamsplitter_select': 61,
                                                      'detection_select': 62,
                                                      'output_select': 63,
                                                      'power_meter_flip': 64,
                                                      'var_ND1': 65,
                                                      'var_ND2': 66,
                                                      'fix_ND': 67,
                                                      'beam_block':68,
                                                      '532nm_flip': 69,
                                                      'TiSa_flip': 70,
                                                      'rot_filter_exc':71,
                                                      'wire_coupling_select':72,
                                                      'lens_fourier100':73,
                                                      'lens_fourier300':74,
                                                      'lens_fourier400':75,
                                                      'rot_filter_det':76,
                                                      'filter_wheel_det':77,
                                                      'rot_filter_det_flip':78
                                                      })),
        self.add_hardware(DynamixelFilterWheelHW(self, name='output_select', 
                                                 named_positions={
                                                     'detector':2048,
                                                     'Flircam':1540},
                                                 release_at_target=False))
        self.add_hardware(DynamixelFilterWheelHW(self, name='power_meter_flip', 
                                                 named_positions={'open':3072,
                                                          'power_meter':2048},
                                                 release_at_target=False))
        self.add_hardware(DynamixelFilterWheelHW(self, name='detection_select', 
                                                  named_positions={
                                                      'Spectrometer':2682,
                                                      'APD':2048},
                                                  release_at_target=False))
        self.add_hardware(DynamixelFilterWheelHW(self, name='beamsplitter_select', 
                                                 named_positions={'A/Open':1,
                                                                  'B/?':1024,
                                                                  'C/50:50':2048,
                                                                  'D/Open':3072},
                                                 release_at_target=False))

        self.add_hardware(DynamixelServoHW(self, name='var_ND1',
                                              lq_kwargs={'spinbox_decimals':3, 'unit':'deg'}))       
        self.add_hardware(DynamixelServoHW(self, name='var_ND2',
                                              lq_kwargs={'spinbox_decimals':3, 'unit':'deg'}))       
        self.add_hardware(DynamixelFilterWheelHW(self, name='fix_ND', 
                                                 named_positions={'A/ND0.0':1,
                                                                  'B/ND0.5':683,
                                                                  'C/ND1.0':1365,
                                                                  'D/ND2.0':2048,
                                                                  'E/ND3.0':2731,
                                                                  'F/ND4.0':3413},
                                                 release_at_target=False))
        self.add_hardware(DynamixelFilterWheelHW(self, name='beam_block', 
                                                 named_positions={'block_path':2048,
                                                          'open':1024},
                                                 release_at_target=False))
        self.add_hardware(DynamixelFilterWheelHW(self, name='532nm_flip', 
                                                 named_positions={'532nm':1024,
                                                          'open':2048},
                                                 release_at_target=False))
        self.add_hardware(DynamixelFilterWheelHW(self, name='TiSa_flip', 
                                                 named_positions={'TiSa':1024,
                                                          'open':2048},
                                                 release_at_target=False))
        self.add_hardware(DynamixelServoHW(self, name='rot_filter_exc',
                                              lq_kwargs={'spinbox_decimals':3, 'unit':'deg'}))
        self.add_hardware(DynamixelFilterWheelHW(self, name='wire_coupling_select', 
                                                 named_positions={'WireCoupling':3072,
                                                          'open':2048},
                                                 release_at_target=False))
        self.add_hardware(DynamixelFilterWheelHW(self, name='lens_fourier100', 
                                                 named_positions={'in path':1024,
                                                          'open':2048},
                                                 release_at_target=False))
        self.add_hardware(DynamixelFilterWheelHW(self, name='lens_fourier300', 
                                                 named_positions={'in path':2048,
                                                          'open':3072},
                                                 release_at_target=False))
        self.add_hardware(DynamixelFilterWheelHW(self, name='lens_fourier400', 
                                                 named_positions={'in path':2048,
                                                          'open':3072},
                                                 release_at_target=False))
        self.add_hardware(DynamixelServoHW(self, name='rot_filter_det',
                                              lq_kwargs={'spinbox_decimals':3, 'unit':'deg'}))
        self.add_hardware(DynamixelFilterWheelHW(self, name='filter_wheel_det', 
                                                 named_positions={'790 LP':341,
                                                                  '550 LP':1024,
                                                                  'ND2?':1707,
                                                                  'open2':2390,
                                                                  'open1':3073,
                                                                  'open6':3756},
                                                 release_at_target=False))
        self.add_hardware(DynamixelFilterWheelHW(self, name='rot_filter_det_flip', 
                                                 named_positions={'filter_in':2048,
                                                          'open':4095},
                                                 release_at_target=False))  

        
        from confocal_measure.calibration_sweep import CalibrationSweep
        self.add_measurement(CalibrationSweep(self, spectrometer_hw_name='acton_spectrometer', 
                                                    camera_readout_measure_name='winspec_readout') )

        from ScopeFoundryHW.andor_camera.andor_ccd_readout_montana import AndorCCDReadoutMeasureMontana
        self.add_measurement(AndorCCDReadoutMeasureMontana)
              
        from ScopeFoundryHW.thorlabs_powermeter import PowerMeterOptimizerMeasure
        self.add_measurement(PowerMeterOptimizerMeasure(self))

        from ScopeFoundryHW.andor_camera import  AndorSpecCalibMeasure
        self.add_measurement(AndorSpecCalibMeasure)

        from confocal_measure.apd_optimizer_cb import APDOptimizerCBMeasurement
        self.add_measurement_component(APDOptimizerCBMeasurement(self))
    
        #from ScopeFoundryHW.apd_counter import APDCounterHW, APDOptimizerMeasure
        #self.a   dd_hardware(APDCounterHW(self))
        #self.add_measurement(APDOptimizerMeasure(self))

        from confocal_measure.ranged_optimization import RangedOptimization
        self.add_measurement(RangedOptimization(self))

        from mi_cryo_micro.piezo_hyperspec_scan import PiezoHyperspecScan
        self.add_measurement(PiezoHyperspecScan(self, stage_name='Piezo_xy_stage'))

        from mi_cryo_micro.Sample_Explore_Montana import SampleExploreMontana
        self.add_measurement(SampleExploreMontana(self))

        from ScopeFoundryHW.picoharp import PicoHarpHistogramMeasure
        self.add_measurement(PicoHarpHistogramMeasure(self))

        from ScopeFoundryHW.andor3.andor3_readout2 import Andor3ReadoutMeasure
        self.add_measurement(Andor3ReadoutMeasure(self))

        #ALWAYS load the ui last, as it needs all hardwares to be connnected to make the appropriate connections upon initializing
        from mi_cryo_micro.montana_setup_control import MontanaSetupControlPanel
        self.add_measurement(MontanaSetupControlPanel)

        # load default settings from file
        print("loading defaults")
        self.settings_load_ini(sibling_path(__file__, "MontanaCryoMicroscope_settings.ini"), ignore_hw_connect=True)

        #--------------load the Quickbar and connect everything-----------
        self.add_quickbar(load_qt_ui_file(sibling_path(__file__, 'montana_setup_quick_access.ui')))
        Q = self.quickbar
        self.nanopos_hw=self.hardware['Nanopositioner_stage']
        self.piezos_hw=self.hardware['Piezo_xy_stage']
        self.hw=self.hardware
        self.meas=self.measurements


        #Hardeware Connections and Quick Acess Buttons

        #Connect all checkboxes
        self.hw['TiSa_flip'].settings.connected.connect_to_widget(Q.checkBox_TiSa_In)
        self.hw['532nm_flip'].settings.connected.connect_to_widget(Q.checkBox_532nm_In)
        self.hw['beam_block'].settings.connected.connect_to_widget(Q.checkBox_Block)
        self.hw['rot_filter_exc'].settings.connected.connect_to_widget(Q.checkBox_Fil_exc)
        self.hw['fix_ND'].settings.connected.connect_to_widget(Q.checkBox_fix_ND)
        self.hw['var_ND1'].settings.connected.connect_to_widget(Q.checkBox_var_ND1)
        self.hw['var_ND2'].settings.connected.connect_to_widget(Q.checkBox_var_ND2)
        self.hw['power_meter_flip'].settings.connected.connect_to_widget(Q.checkBox_PwrMtr)
        self.hw['output_select'].settings.connected.connect_to_widget(Q.checkBox_Output)
        self.hw['beamsplitter_select'].settings.connected.connect_to_widget(Q.checkBox_Beamspl)
        self.hw['wire_coupling_select'].settings.connected.connect_to_widget(Q.checkBox_Wire_out)
        self.hw['rot_filter_det'].settings.connected.connect_to_widget(Q.checkBox_Fil_det)
        self.hw['rot_filter_det_flip'].settings.connected.connect_to_widget(Q.checkBox_Filterwheel_det_pos)
        self.hw['detection_select'].settings.connected.connect_to_widget(Q.checkBox_Detection)
        self.hw['andor_spec'].settings.connected.connect_to_widget(Q.checkBox_Spectro)
        self.hw['lens_fourier100'].settings.connected.connect_to_widget(Q.checkBox_Four_100)
        self.hw['lens_fourier300'].settings.connected.connect_to_widget(Q.checkBox_Four_300)
        self.hw['lens_fourier400'].settings.connected.connect_to_widget(Q.checkBox_Four_400)
        self.hw['andor_ccd'].settings.connected.connect_to_widget(Q.checkBox_CCD)
        self.hw['apd_counter'].settings.connected.connect_to_widget(Q.checkBox_APD_spec)
        self.hw['flircam'].settings.connected.connect_to_widget(Q.checkBox_Flircam)
        self.hw['dynamixel_servos'].settings.connected.connect_to_widget(Q.checkBox_Servos)
        self.hw['thorlabs_powermeter'].settings.connected.connect_to_widget(Q.checkBox_Powermeter)
        self.hw['filter_wheel_det'].settings.connected.connect_to_widget(Q.checkBox_Filterwheel_det)
        self.hw['apd_stage'].settings.connected.connect_to_widget(Q.checkBox_APD_stage)
        self.hw['Nanopositioner_stage'].settings.connected.connect_to_widget(Q.checkBox_NanoPos)
        self.hw['Piezo_xy_stage'].settings.connected.connect_to_widget(Q.checkBox_PiezoPos)
        self.hw['picoharp'].settings.connected.connect_to_widget(Q.checkBox_PicoHarp)
        self.hw['andor3_camera'].settings.connected.connect_to_widget(Q.checkBox_CMOS)

        #connect the "connect all hardware" button
        Q.Button_connectALLHardware.clicked.connect(self.measurements['montanta_setup_control_panel'].connectHardwareClicked)

        #quick acess buttons
        Q.toggle_flir_detection_pushButton.clicked.connect(self.Toggle_Flircam_Detection)
        Q.set_SpatOnCMOS_pushButton.clicked.connect(self.switch_to_SpatialOnCMOS)
        Q.set_SpecOnCCD_pushButton.clicked.connect(self.switch_to_SpectralOnCCD)
    
    


        #Light Control

        #Connect the Hardware Input widgets of the UI
        self.hw['TiSa_flip'].settings.named_position.connect_to_widget(Q.TiSa_comboBox)
        self.hw['532nm_flip'].settings.named_position.connect_to_widget(Q.dpss532_comboBox)
        self.hw['beam_block'].settings.named_position.connect_to_widget(Q.BeamBlock_comboBox)
        self.hw['rot_filter_exc'].settings.target_position.connect_to_widget(Q.RotFilterEx_doubleSpinBox)
        self.hw['fix_ND'].settings.named_position.connect_to_widget(Q.Fixed_ND_comboBox)
        self.hw['var_ND1'].settings.target_position.connect_to_widget(Q.Var_ND1_doubleSpinBox)
        self.hw['var_ND2'].settings.target_position.connect_to_widget(Q.Var_ND2_doubleSpinBox)
        self.hw['power_meter_flip'].settings.named_position.connect_to_widget(Q.PowerMeterPos_comboBox)
        self.hw['beamsplitter_select'].settings.named_position.connect_to_widget(Q.BeamSplitter_comboBox)
        self.hw['rot_filter_det'].settings.target_position.connect_to_widget(Q.RotFilterDet_doubleSpinBox)
        self.hw['filter_wheel_det'].settings.named_position.connect_to_widget(Q.FilterWheelDet_comboBox)
        self.hw['rot_filter_det_flip'].settings.named_position.connect_to_widget(Q.RotFilterDet_flip_comboBox)
        #self.hw['lens_fourier100'].settings.named_position.connect_to_widget(self.ui.Fliplens_Fourier_f100)
        #self.hw['lens_fourier300'].settings.named_position.connect_to_widget(self.ui.Fliplens_Fourier_f300)
        #self.hw['lens_fourier400'].settings.named_position.connect_to_widget(self.ui.Fliplens_Fourier_f400)
        self.hw['thorlabs_powermeter'].settings.wavelength.connect_to_widget(Q.Wavelength_doubleSpinBox)
        self.hw['thorlabs_powermeter'].settings.power.connect_bidir_to_widget(Q.Power_lineEdit)
        Q.start_PowerMeter_pushButton.clicked.connect(self.meas['powermeter_optimizer'].start)
        Q.stop_PowerMeter_pushButton.clicked.connect(self.meas['powermeter_optimizer'].interrupt)
        Q.zero_PowerMeter_pushButton.clicked.connect(self.hw['thorlabs_powermeter'].run_zero)


        #Sample Control

        #NanoPositioner_stage
        Q.NanoPos_stop_pushButton.pressed.connect(self.nanopos_hw.stop_all_movement)
        #self.nanopos_hw.settings.connected.connect_to_widget(self.ui.NanoPos_connected_checkBox)
        self.nanopos_hw.settings.x_position.connect_to_widget(Q.NanoPosPos_x_doubleSpinBox)
        self.nanopos_hw.settings.x_target_position.connect_to_widget(Q.NanoPosTarget_x_doubleSpinBox)
        self.nanopos_hw.settings.x_frequency.connect_to_widget(Q.NanoPosSpeed_x_doubleSpinBox)
        self.nanopos_hw.settings.y_position.connect_to_widget(Q.NanoPosPos_y_doubleSpinBox)
        self.nanopos_hw.settings.y_target_position.connect_to_widget(Q.NanoPosTarget_y_doubleSpinBox)
        self.nanopos_hw.settings.y_frequency.connect_to_widget(Q.NanoPosSpeed_y_doubleSpinBox)
        self.nanopos_hw.settings.z_position.connect_to_widget(Q.NanoPosPos_z_doubleSpinBox)
        self.nanopos_hw.settings.z_target_position.connect_to_widget(Q.NanoPosTarget_z_doubleSpinBox)
        self.nanopos_hw.settings.z_frequency.connect_to_widget(Q.NanoPosSpeed_z_doubleSpinBox)
        #move right button
        Q.NanoPos_r_pushButton.pressed.connect(lambda:self.nanopos_hw.move_continuously(0,1,'m'))
        Q.NanoPos_r_pushButton.released.connect(lambda:self.nanopos_hw.move_continuously(0,0,'m'))
        #move left button
        Q.NanoPos_l_pushButton.pressed.connect(lambda:self.nanopos_hw.move_continuously(0,1,'p'))
        Q.NanoPos_l_pushButton.released.connect(lambda:self.nanopos_hw.move_continuously(0,0,'p'))
        #move up button
        Q.NanoPos_u_pushButton.pressed.connect(lambda:self.nanopos_hw.move_continuously(2,1,'p'))
        Q.NanoPos_u_pushButton.released.connect(lambda:self.nanopos_hw.move_continuously(2,0,'p'))
        #move down button
        Q.NanoPos_d_pushButton.pressed.connect(lambda:self.nanopos_hw.move_continuously(2,1,'m'))
        Q.NanoPos_d_pushButton.released.connect(lambda:self.nanopos_hw.move_continuously(2,0,'m'))
        #focus up button
        Q.Focus_u_pushButton.pressed.connect(lambda:self.nanopos_hw.move_continuously(1,1,'m'))
        Q.Focus_u_pushButton.released.connect(lambda:self.nanopos_hw.move_continuously(1,0,'m'))
        #focus down button
        Q.Focus_d_pushButton.pressed.connect(lambda:self.nanopos_hw.move_continuously(1,1,'p'))
        Q.Focus_d_pushButton.released.connect(lambda:self.nanopos_hw.move_continuously(1,0,'p'))
        #focus single steps
        Q.Focus_1step_u_pushButton.clicked.connect(lambda:self.nanopos_hw.move_single_step(1,'m'))
        Q.Focus_1step_d_pushButton.clicked.connect(lambda:self.nanopos_hw.move_single_step(1,'p'))
        #move up right button
        Q.NanoPos_ur_pushButton.pressed.connect(lambda:self.nanopos_hw.move_continuously(2,1,'p'))
        Q.NanoPos_ur_pushButton.pressed.connect(lambda:self.nanopos_hw.move_continuously(0,1,'m'))
        Q.NanoPos_ur_pushButton.released.connect(lambda:self.nanopos_hw.move_continuously(2,0,'p'))
        Q.NanoPos_ur_pushButton.released.connect(lambda:self.nanopos_hw.move_continuously(0,0,'m'))
        #move down right button
        Q.NanoPos_dr_pushButton.pressed.connect(lambda:self.nanopos_hw.move_continuously(2,1,'m'))
        Q.NanoPos_dr_pushButton.pressed.connect(lambda:self.nanopos_hw.move_continuously(0,1,'m'))
        Q.NanoPos_dr_pushButton.released.connect(lambda:self.nanopos_hw.move_continuously(2,0,'m'))
        Q.NanoPos_dr_pushButton.released.connect(lambda:self.nanopos_hw.move_continuously(0,0,'m'))
        #move down left button
        Q.NanoPos_dl_pushButton.pressed.connect(lambda:self.nanopos_hw.move_continuously(2,1,'m'))
        Q.NanoPos_dl_pushButton.pressed.connect(lambda:self.nanopos_hw.move_continuously(0,1,'p'))
        Q.NanoPos_dl_pushButton.released.connect(lambda:self.nanopos_hw.move_continuously(2,0,'m'))
        Q.NanoPos_dl_pushButton.released.connect(lambda:self.nanopos_hw.move_continuously(0,0,'p'))
        #move up left button
        Q.NanoPos_ul_pushButton.pressed.connect(lambda:self.nanopos_hw.move_continuously(2,1,'p'))
        Q.NanoPos_ul_pushButton.pressed.connect(lambda:self.nanopos_hw.move_continuously(0,1,'p'))
        Q.NanoPos_ul_pushButton.released.connect(lambda:self.nanopos_hw.move_continuously(2,0,'p'))
        Q.NanoPos_ul_pushButton.released.connect(lambda:self.nanopos_hw.move_continuously(0,0,'p'))

        #Piezo_xy_stage
        #self.piezos_hw.settings.connected.connect_to_widget(self.ui.Piezo_connected_checkBox)
        self.piezos_hw.settings.x_position.connect_to_widget(Q.PiezoPos_x_doubleSpinBox)
        self.piezos_hw.settings.x_target_position.connect_to_widget(Q.PiezoTarget_x_doubleSpinBox)
        self.piezos_hw.settings.y_position.connect_to_widget(Q.PiezoPos_y_doubleSpinBox)
        self.piezos_hw.settings.y_target_position.connect_to_widget(Q.PiezoTarget_y_doubleSpinBox)
        self.piezos_hw.settings.x_jog_step.connect_to_widget(Q.PiezoStep_x_doubleSpinBox)
        self.piezos_hw.settings.y_jog_step.connect_to_widget(Q.PiezoStep_y_doubleSpinBox)

        Q.Piezo_r_pushButton.clicked.connect(lambda:self.piezos_hw.move_jog_step('x','p'))
        Q.Piezo_l_pushButton.clicked.connect(lambda:self.piezos_hw.move_jog_step('x','m'))
        Q.Piezo_u_pushButton.clicked.connect(lambda:self.piezos_hw.move_jog_step('y','p'))
        Q.Piezo_d_pushButton.clicked.connect(lambda:self.piezos_hw.move_jog_step('y','m'))


        #Detection

        self.hw['output_select'].settings.named_position.connect_to_widget(Q.MontanaOut_comboBox)
        self.hw['wire_coupling_select'].settings.named_position.connect_to_widget(Q.WireOutput_comboBox)
        self.hw['detection_select'].settings.named_position.connect_to_widget(Q.DetectionDevice_comboBox)
        self.hw['andor_spec'].settings.grating_id.connect_to_widget(Q.Grating_comboBox)
        self.hw['andor_spec'].settings.output_flipper.connect_to_widget(Q.SpectroOut_comboBox)
        self.hw['andor_spec'].settings.input_flipper.connect_to_widget(Q.SpectroIn_comboBox)
        self.hw['andor_spec'].settings.center_wl.connect_to_widget(Q.CenterWavelength_doubleSpinBox)
        self.hw['andor_spec'].settings.slit_input_side.connect_to_widget (Q.Slit_width_doubleSpinBox)

    def switch_to_SpatialOnCMOS(self):
        self.hw['andor_spec'].settings['slit_input_side']=2500
        self.hw['andor_spec'].settings['input_flipper']='side'
        self.hw['andor_spec'].settings['output_flipper']='side'
        self.hw['andor_spec'].settings['grating_id']=4
        self.quickbar.CCD_or_CMOS_text.setPlainText('Spatial Mode \n on CMOS')


    def switch_to_SpectralOnCCD(self):
        self.hw['andor_spec'].settings['slit_input_side']=100
        self.hw['andor_spec'].settings['input_flipper']='side'
        self.hw['andor_spec'].settings['output_flipper']='direct'
        self.hw['andor_spec'].settings['grating_id']=2
        self.quickbar.CCD_or_CMOS_text.setPlainText('Spectral Mode \n on CCD')

    def Toggle_Flircam_Detection(self):
            if self.sender().isChecked(): #checked is flircam position
                self.stored_PowerSettings=(self.hw['fix_ND'].settings['named_position'],self.hw['var_ND1'].settings['target_position'],self.hw['var_ND2'].settings['target_position'])
                self.hw['fix_ND'].settings['named_position']='F/ND4.0'
                self.sender().setText('Back to Detection')
                time.sleep(1)
                self.hw['output_select'].settings['named_position']='Flircam'
                self.quickbar.flircam_or_detection_text.setPlainText('On Flircam')
                
            else:
                self.hw['output_select'].settings['named_position']='detector'
                if hasattr(self,'stored_PowerSettings'):
                    self.hw['fix_ND'].settings['named_position']=self.stored_PowerSettings[0]
                    self.hw['var_ND1'].settings['target_position']=self.stored_PowerSettings[1]
                    self.hw['var_ND2'].settings['target_position']=self.stored_PowerSettings[2]
                self.sender().setText('Switch to Flircam')
                self.quickbar.flircam_or_detection_text.setPlainText('On Detection')

        



    
        
if __name__ == '__main__':
    import sys
    app = MontanaCryoMicroscopeApp(sys.argv)
    #app.settings_load_ini('MontanaCryoMicroscope_settings.ini')
    sys.exit(app.exec_())