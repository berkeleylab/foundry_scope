from ScopeFoundry import Measurement
import pyqtgraph as pg
from qtpy import QtWidgets
from qtpy import QtGui
import time
import os
from ScopeFoundry.helper_funcs import load_qt_ui_file, sibling_path
from pyqtgraph.functions import makeQImage

class SampleExploreMontana(Measurement):
    
    name = 'Sample_Explore'
    
    def setup(self):
        self.settings.New('auto_level', dtype=bool, initial=False)
        self.settings.New('crosshairs', dtype=bool, initial=False)
        self.settings.New('flip_x', dtype=bool, initial=False)
        self.settings.New('flip_y', dtype=bool, initial=False)
    
    def setup_figure(self):

        #Image Calibration
        self.cal_factor=0.0566 #in um/pixel   this value is taken from the calibration of 17. Aug. 2021

        self.ui = load_qt_ui_file(sibling_path(__file__,'sample_explore_montana.ui'))
        self.cam_hw = self.app.hardware['flircam']
        self.nanopos_hw=self.app.hardware['Nanopositioner_stage']
        self.piezos_hw=self.app.hardware['Piezo_xy_stage']
        self.settings.activation.connect_to_widget(self.ui.live_checkBox)        
        self.settings.auto_level.connect_to_widget(self.ui.auto_level_checkBox)
        self.settings.crosshairs.connect_to_widget(self.ui.crosshairs_checkBox)
        
        self.cam_hw.settings.connected.connect_to_widget(self.ui.cam_connect_checkBox)
        self.cam_hw.settings.cam_index.connect_to_widget(self.ui.cam_index_doubleSpinBox)
        self.cam_hw.settings.frame_rate.connect_to_widget(self.ui.framerate_doubleSpinBox)
        self.cam_hw.settings.exposure.connect_to_widget(self.ui.exp_doubleSpinBox)

        #connect all the Stage Controls here
        #NanoPositioner_stage
        self.ui.Stop_all_Button.pressed.connect(self.nanopos_hw.stop_all_movement)
        self.nanopos_hw.settings.connected.connect_to_widget(self.ui.NanoPos_connected_checkBox)
        self.nanopos_hw.settings.x_position.connect_to_widget(self.ui.NanoPos_xPos_SpinBox)
        self.nanopos_hw.settings.x_target_position.connect_to_widget(self.ui.NanoPos_xTarget_SpinBox)
        self.nanopos_hw.settings.x_frequency.connect_to_widget(self.ui.NanoPos_xSpeed_SpinBox)
        self.nanopos_hw.settings.y_position.connect_to_widget(self.ui.NanoPos_yPos_SpinBox)
        self.nanopos_hw.settings.y_target_position.connect_to_widget(self.ui.NanoPos_yTarget_SpinBox)
        self.nanopos_hw.settings.y_frequency.connect_to_widget(self.ui.NanoPos_ySpeed_SpinBox)
        self.nanopos_hw.settings.z_position.connect_to_widget(self.ui.NanoPos_zPos_SpinBox)
        self.nanopos_hw.settings.z_target_position.connect_to_widget(self.ui.NanoPos_zTarget_SpinBox)
        self.nanopos_hw.settings.z_frequency.connect_to_widget(self.ui.NanoPos_zSpeed_SpinBox)
        #move right button
        self.ui.NanoPos_right_Button.pressed.connect(lambda:self.nanopos_hw.move_continuously(0,1,'m'))
        self.ui.NanoPos_right_Button.released.connect(lambda:self.nanopos_hw.move_continuously(0,0,'m'))
        #move left button
        self.ui.NanoPos_left_Button.pressed.connect(lambda:self.nanopos_hw.move_continuously(0,1,'p'))
        self.ui.NanoPos_left_Button.released.connect(lambda:self.nanopos_hw.move_continuously(0,0,'p'))
        #move up button
        self.ui.NanoPos_up_Button.pressed.connect(lambda:self.nanopos_hw.move_continuously(2,1,'p'))
        self.ui.NanoPos_up_Button.released.connect(lambda:self.nanopos_hw.move_continuously(2,0,'p'))
        #move down button
        self.ui.NanoPos_down_Button.pressed.connect(lambda:self.nanopos_hw.move_continuously(2,1,'m'))
        self.ui.NanoPos_down_Button.released.connect(lambda:self.nanopos_hw.move_continuously(2,0,'m'))
        #focus up button
        self.ui.Focus_up_Button.pressed.connect(lambda:self.nanopos_hw.move_continuously(1,1,'m'))
        self.ui.Focus_up_Button.released.connect(lambda:self.nanopos_hw.move_continuously(1,0,'m'))
        #focus down button
        self.ui.Focus_down_Button.pressed.connect(lambda:self.nanopos_hw.move_continuously(1,1,'p'))
        self.ui.Focus_down_Button.released.connect(lambda:self.nanopos_hw.move_continuously(1,0,'p'))
        #focus single steps
        self.ui.Single_step_up_Button.clicked.connect(lambda:self.nanopos_hw.move_single_step(1,'m'))
        self.ui.Single_step_down_Button.clicked.connect(lambda:self.nanopos_hw.move_single_step(1,'p'))
        #move up right button
        self.ui.NanoPos_ur_Button.pressed.connect(lambda:self.nanopos_hw.move_continuously(2,1,'p'))
        self.ui.NanoPos_ur_Button.pressed.connect(lambda:self.nanopos_hw.move_continuously(0,1,'m'))
        self.ui.NanoPos_ur_Button.released.connect(lambda:self.nanopos_hw.move_continuously(2,0,'p'))
        self.ui.NanoPos_ur_Button.released.connect(lambda:self.nanopos_hw.move_continuously(0,0,'m'))
        #move down right button
        self.ui.NanoPos_dr_Button.pressed.connect(lambda:self.nanopos_hw.move_continuously(2,1,'m'))
        self.ui.NanoPos_dr_Button.pressed.connect(lambda:self.nanopos_hw.move_continuously(0,1,'m'))
        self.ui.NanoPos_dr_Button.released.connect(lambda:self.nanopos_hw.move_continuously(2,0,'m'))
        self.ui.NanoPos_dr_Button.released.connect(lambda:self.nanopos_hw.move_continuously(0,0,'m'))
        #move down left button
        self.ui.NanoPos_dl_Button.pressed.connect(lambda:self.nanopos_hw.move_continuously(2,1,'m'))
        self.ui.NanoPos_dl_Button.pressed.connect(lambda:self.nanopos_hw.move_continuously(0,1,'p'))
        self.ui.NanoPos_dl_Button.released.connect(lambda:self.nanopos_hw.move_continuously(2,0,'m'))
        self.ui.NanoPos_dl_Button.released.connect(lambda:self.nanopos_hw.move_continuously(0,0,'p'))
        #move up left button
        self.ui.NanoPos_ul_Button.pressed.connect(lambda:self.nanopos_hw.move_continuously(2,1,'p'))
        self.ui.NanoPos_ul_Button.pressed.connect(lambda:self.nanopos_hw.move_continuously(0,1,'p'))
        self.ui.NanoPos_ul_Button.released.connect(lambda:self.nanopos_hw.move_continuously(2,0,'p'))
        self.ui.NanoPos_ul_Button.released.connect(lambda:self.nanopos_hw.move_continuously(0,0,'p'))

        #Piezo_xy_stage
        self.piezos_hw.settings.connected.connect_to_widget(self.ui.Piezo_connected_checkBox)
        self.piezos_hw.settings.x_position.connect_to_widget(self.ui.Piezo_xPos_SpinBox)
        self.piezos_hw.settings.x_target_position.connect_to_widget(self.ui.Piezo_xTarget_SpinBox)
        self.piezos_hw.settings.y_position.connect_to_widget(self.ui.Piezo_yPos_SpinBox)
        self.piezos_hw.settings.y_target_position.connect_to_widget(self.ui.Piezo_yTarget_SpinBox)
        self.piezos_hw.settings.x_jog_step.connect_to_widget(self.ui.Piezo_xJogstep_SpinBox)
        self.piezos_hw.settings.y_jog_step.connect_to_widget(self.ui.Piezo_yJogstep_SpinBox)

        self.ui.Piezo_right_Button.clicked.connect(lambda:self.piezos_hw.move_jog_step('x','p'))
        self.ui.Piezo_left_Button.clicked.connect(lambda:self.piezos_hw.move_jog_step('x','m'))
        self.ui.Piezo_up_Button.clicked.connect(lambda:self.piezos_hw.move_jog_step('y','p'))
        self.ui.Piezo_down_Button.clicked.connect(lambda:self.piezos_hw.move_jog_step('y','m'))

        #Quick Controls
        self.app.hardware['fix_ND'].settings.named_position.connect_to_widget(self.ui.Quick_fixed_ND)
        self.app.hardware['var_ND1'].settings.target_position.connect_to_widget(self.ui.Quick_var1_ND)
        self.app.hardware['var_ND2'].settings.target_position.connect_to_widget(self.ui.Quick_var_ND2)
        self.app.hardware['output_select'].settings.named_position.connect_to_widget(self.ui.Quick_Detection)
        
        #self.imview = pg.ImageView()
        self.img_label = QtWidgets.QLabel()
        
        self.graph_layout = pg.GraphicsLayoutWidget()
        self.plot = self.graph_layout.addPlot()
        self.img_item = pg.ImageItem()
        self.img_item.scale(self.cal_factor,self.cal_factor)
        self.plot.addItem(self.img_item)
        self.plot.setAspectLocked(lock=True, ratio=1)
        self.plot.setLabel('bottom','x(um)')
        self.plot.setLabel('left','y(um)')

        
        self.ui.plot_groupBox.layout().addWidget(self.img_label)
        
        def switch_camera_view():
            self.ui.plot_groupBox.layout().addWidget(self.graph_layout)
            self.graph_layout.showMaximized() 
        self.ui.show_pushButton.clicked.connect(switch_camera_view)
        #self.ui.plot_groupBox.layout().addWidget(self.imview)
        self.ui.plot_groupBox.layout().addWidget(self.graph_layout)
        
        
        self.ui.auto_exposure_comboBox.addItem("placeholder")
        self.ui.auto_exposure_comboBox.setCurrentIndex(0)
        def apply_auto_exposure_index():
            self.cam_hw.cam.set_auto_exposure(self.ui.auto_exposure_comboBox.currentIndex())
        self.ui.auto_exposure_comboBox.currentIndexChanged.connect(apply_auto_exposure_index)
        self.ui.save_pushButton.clicked.connect(self.save_image)
        
        self.crosshairs = [pg.InfiniteLine(movable=False, angle=90, pen=(255,0,0,200)),
                           pg.InfiniteLine(movable=False, angle=0, pen=(255,0,0,200))]
        for ch in self.crosshairs:
            self.plot.addItem(ch)
            ch.setZValue(100)
        

        

    def run(self):
        self.cam_hw.settings['connected'] = True
        if self.ui.auto_exposure_comboBox.count() == 1:
            self.ui.auto_exposure_comboBox.addItems(self.cam_hw.cam.get_auto_exposure_options())
            self.ui.auto_exposure_comboBox.removeItem(0)
            self.ui.auto_exposure_comboBox.setCurrentIndex(2)

        while not self.interrupt_measurement_called:
            time.sleep(0.5)
            self.cam_hw.settings.exposure.read_from_hardware()

            
            
    def get_rgb_image(self):
        if not self.cam_hw.img_buffer:
            return False
        else:
            return self.cam_hw.img_buffer.pop(0).copy()
        
                    
    def update_display(self):
        self.display_update_period = 0.01
        self.im = im = self.get_rgb_image()
        if type(im)==bool:
            return
        
        #print('imshape', im.shape)
        # print("buffer len:", len(self.hw.img_buffer))
        # self.hw.img.copy()
        #self.imview.setImage(im.swapaxes(0,1),autoLevels=self.settings['auto_level'])
        self.img_item.setImage(im.swapaxes(0,1),autoLevels=self.settings['auto_level'])
        
        for ch,(x,y) in zip(self.crosshairs, [(im.shape[1]*self.cal_factor/2,0), (0,im.shape[0]*self.cal_factor/2)]):
            ch.setPos((x,y))
            #ch.setVisible(self.settings['crosshairs'])
            ch.setZValue({True:1, False:-1}[self.settings['crosshairs']])
            
        #self.img_label.setPixmap(QtGui.QPixmap.fromImage(
        #    makeQImage(imgData=im, alpha=False, copy=False, transpose=True)))

        
            
            
    def save_image(self):
        print('flircam_live_measure save_image')
        t = time.localtime(time.time())
        t_string = "{:02d}{:02d}{:02d}_{:02d}{:02d}{:02d}".format(int(str(t[0])[2:4]), t[1], t[2], t[3], t[4], t[5])
        fname = os.path.join(self.app.settings['save_dir'], "%s_%s_%s" % (t_string,self.app.settings.sample.val, self.name))
        #self.imview.export(fname + ".tif")
        self.img_item.save(fname + ".tif")
        self.app.settings_save_ini(fname + ".ini")

     
