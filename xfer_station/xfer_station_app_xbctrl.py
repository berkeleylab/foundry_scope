from ScopeFoundry import BaseMicroscopeApp

from ScopeFoundryHW.asi_stage.asi_stage_hw import ASIStageHW
from ScopeFoundryHW.asi_stage.asi_stage_control_measure import ASIStageControlMeasure
#from xfer_station.powermate_listener import PowermateListener
from ScopeFoundryHW.thorlabs_elliptec.elliptec_hw import ThorlabsElliptecSingleHW

class XferStationApp(BaseMicroscopeApp):
    
    name ='xfer_station'
    
    #xbctrl attributes
    speedZDict = {'1.0000':1.0000, '0.1000':0.1000, '0.0100':0.0100, '0.0010':0.0010, '0.0001':0.0001}
    
    #sample locator attributes
    canRotate = False
    microscope = 'xfer_station'
    
    def setup(self):
        
        self.add_hardware(ASIStageHW(self, name='stage_top',  swap_xy=True))
        self.add_hardware(ASIStageHW(self, name='stage_bottom',  swap_xy=True))
        
        self.add_measurement(ASIStageControlMeasure(self, name='stamp',  hw_name='stage_top'))
        self.add_measurement(ASIStageControlMeasure(self, name='objective', hw_name='stage_bottom'))
        
        #self.add_measurement(PowermateListener(self))
        
        #from xfer_station.rotation_calibration import RotationCalibration
        #self.add_measurement(RotationCalibration(self))
        
        from ScopeFoundryHW.toupcam import ToupCamHW, ToupCamLiveMeasure
        self.add_hardware(ToupCamHW(self))
        self.add_measurement(ToupCamLiveMeasure(self))
        
        from ScopeFoundryHW.xbox_controller.xbox_controller_hw import XboxControllerHW
        from xfer_station.xbox_controller_xfer_measure import XboxControllerXferMeasure
        self.add_hardware(XboxControllerHW(self))
        self.add_measurement(XboxControllerXferMeasure(self))
    
        
        from xfer_station.LocatorLiveCamXfer import LocatorLiveCamXfer
        self.add_measurement(LocatorLiveCamXfer(self))
        
        self.add_hardware(ThorlabsElliptecSingleHW(self))
        
        self.settings_load_ini("xfer_station_defaults.ini")

if __name__ == '__main__':
    import sys
    app = XferStationApp(sys.argv)
    app.exec_()