'''
Created on Jun 14, 2021

@author: Sriram Sridhar

'''

from ScopeFoundryHW.xbox_controller.xbox_controller_base_measure import XboxBaseMeasure
import time
import pygame.event
#from pygame.constants import JOYAXISMOTION, JOYHATMOTION, JOYBUTTONDOWN, JOYBUTTONUP
import math

class XboxControllerXferMeasure(XboxBaseMeasure):
    
    x_sensitivity = 0.2
    y_sensitivity = 0.22
    z_sensitivity = 0.10
    frameUpdateTime = 0.5
    hwNames = ['stage_bottom', 'stage_top']
    hwNameCounter = 0
    measureNames = ['objective', 'stamp']
    measureNameCounter = 0
    continuousBool = False
    speedXYBool = False
    focusBool = False
    invertX = 1
    invertY = 1
    invertZ = -1
    objectivesList = ['5x', '10x', '20x', '50x']
    
    def setup(self):
        XboxBaseMeasure.setup(self)
        '''self.settings.New(name='max_speed', initial=5.0,
                                            dtype=float, fmt="%.3f", 
                                            ro=False, vmin=0.0, vmax=10.0)
        self.settings.New(name='speed_x', initial=0.0,
                                            dtype=float, fmt="%.3f", 
                                            ro=False, vmin=-10.0, vmax=10.0)
        self.settings.New(name='speed_y', initial=0.0,
                                            dtype=float, fmt="%.3f", 
                                            ro=False, vmin=-10.0, vmax=10.0)
        self.settings.New(name='speed_z', initial=0.0,
                                            dtype=float, fmt="%.3f", 
                                            ro=False, vmin=-10.0, vmax=10.0)'''
        self.settings.New(name='objective', initial='10x', dtype=str, choices=self.objectivesList)
        self.settings.New(name='dx', initial=0.5,
                                            dtype=float, fmt="%.3f",
                                            ro=False, vmin=0.0, vmax=10.0)
        self.settings.New(name='dy', initial=0.5,
                                            dtype=float, fmt="%.3f",
                                            ro=False, vmin=0.0, vmax=10.0)
        self.settings.New(name='dz', initial=0.5,
                                            dtype=float, fmt="%.3f",
                                            ro=False, vmin=0.0, vmax=10.0)
    
    def start_measure(self):
        if self.controller.settings['Start'] == True:
            for x in self.hwNames:
                if not self.app.hardware[x].is_connected:
                    print(f'{x} is not connected, please connect and start measurement again')
                    return
            self.app.measurements['xbcontrol_mc'].start()
            if (self.measureNames[self.measureNameCounter] != ''):
                print()
                print('Connected to', self.measureNames[self.measureNameCounter])
                print()
            
            
            '''if (not self.app.hardware['stage_bottom'].is_connected) or (not self.app.hardware['stage_top'].is_connected):
                print('Stages are not connected')
            else:
                self.app.measurements['xbcontrol_mc'].start()
                if (self.measureNames[self.measureNameCounter] != ''):
                    print()
                    print('Connected to', self.measureNames[self.measureNameCounter])
                    print()'''
        
    def interrupt_measure(self):
        if self.controller.settings['Back'] == True:
            self.app.measurements['xbcontrol_mc'].interrupt()
            print('Interrupted Xbox Controller Measurement')
        
    def move_continuous_x(self):
        if self.controller.settings['Y'] == True:
            self.continuousBool = True
            print('Continuous movement activated')
            print()
        if self.continuousBool:
            stage = self.app.hardware[self.hwNames[self.hwNameCounter]]
            stageS = self.app.hardware[self.hwNames[self.hwNameCounter]].settings
            if math.fabs(self.controller.settings['Axis_0']) < self.x_sensitivity:
                stage.settings.x_position.read_from_hardware()
                if stageS['speed_xy'] >= 0.10: #under speed of 0.10 mm/s, the next line of code causes drift
                    stageS['x_target'] = stageS['x_position']
            else:
                joy_x = self.controller.settings['Axis_0']
                x_pos = stage.settings.x_position.read_from_hardware()
                dx = self.settings['dx'] = self.frameUpdateTime * stageS['speed_xy']
                stageS['x_target'] = x_pos + joy_x * dx * self.invertX

    def move_continuous_y(self):
        if self.controller.settings['Y'] == True:
            self.continuousBool = True
            print('Continuous movement activated')
            print()
        if self.continuousBool:
            stage = self.app.hardware[self.hwNames[self.hwNameCounter]]
            stageS = self.app.hardware[self.hwNames[self.hwNameCounter]].settings
            if math.fabs(self.controller.settings['Axis_1']) < self.y_sensitivity:
                stage.settings.y_position.read_from_hardware()
                if stageS['speed_xy'] >= 0.10: #under speed of 0.10 mm/s, the next line of code causes drift
                    stageS['y_target'] = stageS['y_position']
            else:
                joy_y = self.controller.settings['Axis_1']
                y_pos = stage.settings.y_position.read_from_hardware()
                dy = self.settings['dy'] = self.frameUpdateTime * stageS['speed_xy']
                stageS['y_target'] = y_pos + joy_y * dy * self.invertY
    
    def move_continuous_z(self):
        if self.controller.settings['Y'] == True:
            self.continuousBool = True
            print('Continuous movement activated')
            print()
        if self.continuousBool:
            stage = self.app.hardware[self.hwNames[self.hwNameCounter]]
            stageS = self.app.hardware[self.hwNames[self.hwNameCounter]].settings
            if math.fabs(self.controller.settings['Axis_3']) < self.z_sensitivity:
                stage.settings.z_position.read_from_hardware()
                if stageS['speed_z'] >= 0.10: #under speed of 0.10 mm/s, the next line of code causes drift
                    stageS['z_target'] = stageS['z_position']
            else:
                joy_z = self.controller.settings['Axis_3']
                z_pos = stage.settings.z_position.read_from_hardware()
                dz = self.settings['dz'] = self.frameUpdateTime * stageS['speed_z']
                stageS['z_target'] = z_pos + joy_z * dz * self.invertZ
    
    def toggle_stage(self):
        if self.controller.settings['A'] == True:            
            self.hwNameCounter = (self.hwNameCounter + 1) % len(self.hwNames)
            self.measureNameCounter = (self.measureNameCounter + 1) % len(self.measureNames)
            print()
            print('Connected to', self.measureNames[self.measureNameCounter])
            print()
    
    '''Need to find z coordinate of sample (vacuum) position
        Don't know what the z position of the stamp is referring to (Is it the bottom of the tilt cube or is it the top? Do I need to account for sample holder height?)'''
    def switch_focus(self):
        if self.controller.settings['B'] == True and not self.focusBool:
            self.focusBool = True
            self.continuousBool = False #removes continuous movement control so that the stage can refocus
            print('Continuous movement turned off, press Y to activate continuous movement')
            print('If stamp is close to sample on 4x objective, press X to switch focus')
            print()
            self.controller.settings['B'] = False
            return
        if self.controller.settings['X'] == True and self.focusBool: #second B press
            stageBotS = self.app.hardware['stage_bottom'].settings
            stageTopS = self.app.hardware['stage_top'].settings
            if self.settings['objective'] == '5x':
                stampFocusDistance = 14.04421 - (4.57552) #focus distance between stamp z coord and objective z coord (POST HOMING)
                sampleFocusPos = -6.29222 #objective position where sample is in focus (POST HOMING)
                stampMaxHeight = 20 #max height stamp can be at so that sample focusing doesn't crash objective into stamp (POST HOMING)
            if self.settings['objective'] == '10x':
                stampFocusDistance = 5.63177 - (-3.26122)
                sampleFocusPos = -5.77120
                stampMaxHeight = 14
            if self.settings['objective'] == '20x':
                stampFocusDistance = 5.82737 - (-2.93890)
                sampleFocusPos = -5.63244
                stampMaxHeight = 7
            if self.settings['objective'] == '50x':
                stampFocusDistance = 3.98261 - (-4.70258)
                sampleFocusPos = -5.55800
                stampMaxHeight = 4.2
            if (round(stageTopS['z_position'] - stageBotS['z_position'], 2)) != round(stampFocusDistance, 2):
                if stageTopS['z_position'] > stampMaxHeight:
                    print('Stamp is too close to objective, lower stamp')
                    print()
                else:
                    stageBotS['z_target'] = stageTopS['z_position'] - stampFocusDistance #focuses on stamp
                    print('Focused on stamp')
                    print()
            else:
                stageBotS['z_target'] = sampleFocusPos #focuses on sample
                print('Focused on sample')
                print()
            self.focusBool = False
            
    '''def change_speed_bottom(self):
        if self.controller.settings['X'] == True:
            self.speedXYBool = True
            self.status = 'Change objective speed'
            print(1)
        if self.speedXYBool == True:
            print('2')
            stageBotS = self.app.hardware['stage_bottom'].settings
            if self.controller.settings['N'] == True:
                print('3')
                if stageBotS['speed_z'] >= 1.00:
                    stageBotS['speed_z'] += 0.50
                elif 0.10 <= stageBotS['speed_z'] < 1.00:
                    stageBotS['speed_z'] += 0.10
                elif stageBotS['speed_z'] < 0.10:
                    stageBotS['speed_z'] += 0.01
            if self.controller.settings['S'] == True:
                if stageBotS['speed_z'] >= 1.00:
                    stageBotS['speed_z'] -= 0.50
                elif 0.10 <= stageBotS['speed_z'] < 1.00:
                    stageBotS['speed_z'] -= 0.10
                elif stageBotS['speed_z'] < 0.10:
                    stageBotS['speed_z'] -= 0.01
            if self.controller.settings['E'] == True:
                if stageBotS['speed_xy'] >= 1.00:
                    stageBotS['speed_xy'] += 0.50
                elif 0.10 <= stageBotS['speed_xy'] < 1.00:
                    stageBotS['speed_xy'] += 0.10
                elif stageBotS['speed_xy'] < 0.10:
                    stageBotS['speed_xy'] += 0.01
            if self.controller.settings['W'] == True:
                if stageBotS['speed_xy'] >= 1.00:
                    stageBotS['speed_xy'] -= 0.50
                elif 0.10 <= stageBotS['speed_xy'] < 1.00:
                    stageBotS['speed_xy'] -= 0.10
                elif stageBotS['speed_xy'] < 0.10:
                    stageBotS['speed_xy'] -= 0.01
        self.speedXYBool = False'''
    
    def run(self):
        """This function is run after having clicked "start" in the ScopeFoundry GUI.
        It essentially runs and listens for Pygame event signals and updates the status
        of every button in a specific category (such as hats, sticks, or buttons) in
        intervals of self.dt seconds."""
        self.log.debug("run")
        
        #Access equipment class:
        self.controller.connect()
        self.xb_dev = self.controller.xb_dev 
        self.joystick = self.xb_dev.joystick
        
        self.log.debug("ran")
        
        self.controller.settings['Controller_Name'] = self.joystick.get_name()
        
        #self.dt = 0.001
        
        #t0 = time.time()
        
        
        #self.app.hardware['stage_top'].other_observer = True
        #self.app.hardware['stage_bottom'].other_observer = True
        for x in self.hwNames:
            self.app.hardware[x].other_observer = True

        try:
            while not self.interrupt_measurement_called:  
                #time.sleep(self.dt)
                event_list = pygame.event.get()
                for event in event_list:
                    if event.type == pygame.JOYAXISMOTION:
                        for i in range(self.xb_dev.num_axes):
                            self.controller.settings['Axis_' + str(i)] = self.joystick.get_axis(i)
     
                    elif event.type == pygame.JOYHATMOTION:
                        for i in range(self.xb_dev.num_hats):
                            # Clear Directional Pad values
                            for k in set(self.direction_map.values()):
                                self.controller.settings[k] = False
     
                            # Check button status and record it
                            resp = self.joystick.get_hat(i)
                            try:
                                self.controller.settings[self.direction_map[resp]] = True
                            except KeyError:
                                self.log.error("Unknown dpad hat: "+ repr(resp))
     
                    elif event.type in [pygame.JOYBUTTONDOWN, pygame.JOYBUTTONUP]:
                        button_state = (event.type == pygame.JOYBUTTONDOWN)
     
                        for i in range(self.xb_dev.num_buttons):
                            if self.joystick.get_button(i) == button_state:
                                try:
                                    self.controller.settings[self.button_map[i]] = button_state
                                except KeyError:
                                    self.log.error("Unknown button: %i (target state: %s)" % (i,
                                        'down' if button_state else 'up'))
    
                    else:
                        self.log.error("Unknown event type: {} {}".format(event, event.type))
                
                self.move_continuous_x()
                self.move_continuous_y()
                self.move_continuous_z()
                self.toggle_stage()
                self.switch_focus()
                #self.change_speed_bottom()
                
                #t1 = time.time()
                #print("dt actual", t1-t0)
                #t0 = t1
        finally:
            #self.app.hardware['stage_top'].other_observer = False
            #self.app.hardware['stage_bottom'].other_observer = False
            for x in self.hwNames:
                self.app.hardware[x].other_observer = False

                
