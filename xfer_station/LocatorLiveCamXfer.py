'''
Created on Sep 3, 2021

@author: Sriram Sridhar
'''

#from ScopeFoundryHW.flircam.flircam_live_measure import FlirCamLiveMeasure
from confocal_measure.stage_live_cam import StageLiveCam
from uv_microscope.sample_locator import SampleLocator
from confocal_measure.base_rotation_calibration import BaseRotationCalibration
from ScopeFoundryHW.toupcam.toupcam_live_measure import ToupCamLiveMeasure

class LocatorLiveCamXfer(ToupCamLiveMeasure, StageLiveCam, SampleLocator, BaseRotationCalibration):
    
    name = 'Sample Locator'
    hwName = 'toupcam'
    #canRotate = True
    #microscopeDict = {'survey_microscope', 'xfer_station'}
    
    def setup(self):
        ToupCamLiveMeasure.setup(self)
        StageLiveCam.setup(self)
        BaseRotationCalibration.setup(self)
        SampleLocator.setup(self)
    
    def setup_figure(self):
        ToupCamLiveMeasure.setup_figure(self)
        StageLiveCam.setup_figure(self)
    
    def update_display(self):
        StageLiveCam.update_display(self)
        #ToupCamLiveMeasure.update_display(self)
    
    def get_current_stage_position(self):
        stageS = self.app.hardware["stage_bottom"].settings
        return stageS["x_position"] * 1e3, stageS["y_position"] * 1e3, stageS["z_position"] * 1e3

    def set_stage_position(self, x, y, z=None):
        stageS = self.app.hardware["stage_bottom"].settings
        stageS["x_target"] = x * 1e-3
        stageS["y_target"] = y * 1e-3
        if z != None:
            stageS["z_target"] = z * 1e-3
    
    def jog_rot_z(self, angle_z):
        motor = self.app.hardware["elliptec"]
        #motor.settings["target_position"] = motor.settings["position"] + angle_z
        motor.settings["position"] = motor.settings["position"] + angle_z