from ScopeFoundry.base_app import BaseMicroscopeApp
import qdarktheme

class BioGlowApp(BaseMicroscopeApp):
    
    name = 'BioGlow'
    
    def setup(self):
        qdarktheme.setup_theme()

        from ScopeFoundryHW.mf_crucible.mf_crucible_hw import MFCrucibleHW
        self.add_hardware(MFCrucibleHW(self))
        
        from ScopeFoundryHW.mf_crucible.mf_crucible_controlpanel import MFCrucibleControlPanel
        self.add_measurement(MFCrucibleControlPanel(self))

        from ScopeFoundryHW.zaber_motion.zaber_linear_hw import ZaberLinearHW
        self.add_hardware(ZaberLinearHW(self))

        from ScopeFoundryHW.thorlabs_fw.thorlabs_fw102c_hw import ThorlabsFW102cHW
        
        bottom_names = ['400', 'SKIP1', '450', 'SKIP2', '500', 'SKIP3', '550', 'SKIP4', '600', 'SKIP5', 'EMPTY', 'SKIP6']
        fw1 = self.add_hardware(ThorlabsFW102cHW(self, name='filter_wheel_BOTTOM',
                                                position_names=bottom_names))
        top_names = ['650', 'SKIP1', '700', 'SKIP2', '750', 'SKIP3', '800', 'SKIP4', '850', 'SKIP5', 'EMPTY', 'SKIP6']        
        fw2 = self.add_hardware(ThorlabsFW102cHW(self, name='filter_wheel_TOP',
                                                position_names=top_names))

        from bioglow.shutter_arduino_hw import ShutterArduinoHW
        self.add_hardware(ShutterArduinoHW(self))

        from ScopeFoundryHW.picoquant.multiharp_hw import MultiHarpHW
        self.add_hardware(MultiHarpHW(self, n_channels=4))
        
        from ScopeFoundryHW.picoquant.multiharp_optimizer import MultiHarpOptimizerMeasure
        self.add_measurement(MultiHarpOptimizerMeasure(self))
        from ScopeFoundryHW.picoquant.multiharp_t2_measure import MultiHarpT2Measure
        self.add_measurement(MultiHarpT2Measure(self))

        from bioglow.bioglow_spec_measure import BioGlowSpecMeasure
        self.add_measurement(BioGlowSpecMeasure)
        
        self.settings_load_ini("bioglow_defaults.ini")


if __name__ == '__main__':

    import sys
    app = BioGlowApp(sys.argv)
    app.qtapp.setStyle('Fusion')
    sys.exit(app.exec_())
