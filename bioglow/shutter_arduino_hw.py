from ScopeFoundry import HardwareComponent
import time
class ShutterArduinoHW(HardwareComponent):

    name = 'shutter_arduino'

    def setup(self):

        self.settings.New('port', dtype=str, initial='/dev/ttyACM0')
        self.settings.New('shutter_A', dtype=bool)
        self.settings.New('shutter_B', dtype=bool)

    def connect(self):

        import serial
        self.ser = serial.Serial(self.settings['port'], baudrate=9600, timeout=1.0)
        self.ser.flush()
        time.sleep(1.0)
        print(self.ser.readline())
        #print(self.ser.readline())

        self.settings.shutter_A.connect_to_hardware(
            read_func = self.read_shutter_A,
            write_func = self.write_shutter_A,
        )
        self.settings.shutter_A.read_from_hardware()

        self.settings.shutter_B.connect_to_hardware(
            read_func = self.read_shutter_B,
            write_func = self.write_shutter_B,
        )
        self.settings.shutter_B.read_from_hardware()

    def disconnect(self):
        self.settings.disconnect_all_from_hardware()
        if hasattr(self, 'ser'):
            self.ser.close()
            del self.ser

    def read_shutter_A(self):
        self.ser.write(b"A?\n")
        resp = self.ser.readline()
        if self.settings['debug_mode']: print(f"read_shutter_A {resp=}")
        if resp == b'A0\r\n': return False
        if resp == b'A1\r\n': return True
        else: raise IOError(f"{self.name}: read shutter failed {resp=}")
    
    def  write_shutter_A(self, open_shutter):
        if open_shutter:
            self.ser.write(b"A1\n")
            resp = self.ser.readline()
            if self.settings['debug_mode']: print(f"write_shutter_A {open_shutter} --> {resp=}")
        else:
            self.ser.write(b"A0\n")
            resp = self.ser.readline()
            if self.settings['debug_mode']: print(f"write_shutter_A {open_shutter} --> {resp=}")

    def read_shutter_B(self):
        self.ser.write(b"B?\n")
        resp = self.ser.readline()
        if self.settings['debug_mode']: print(f"read_shutter_B {resp=}")
        if resp == b'B0\r\n': return False
        if resp == b'B1\r\n': return True
        else: raise IOError(f"{self.name}: read shutter failed {resp=}")
    
    def  write_shutter_B(self, open_shutter):
        if open_shutter:
            self.ser.write(b"B1\n")
            resp = self.ser.readline()
            if self.settings['debug_mode']: print(f"write_shutter_B {open_shutter} --> {resp=}")
        else:
            self.ser.write(b"B0\n")
            resp = self.ser.readline()
            if self.settings['debug_mode']: print(f"write_shutter_B {open_shutter} --> {resp=}")
