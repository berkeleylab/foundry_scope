from ScopeFoundry import Measurement
from ScopeFoundry.h5_io import h5_measurement_file, h5_base_file,\
    create_extendable_h5_dataset, extend_h5_dataset_along_axis, h5_create_measurement_group

from ScopeFoundry.logged_quantity import LQCollection
from ScopeFoundryHW.picoquant.multiharp_hw import MultiHarpHW
from ScopeFoundryHW.zaber_motion.zaber_linear_hw import ZaberLinearHW
import numpy as np
import time
import os

class BioGlowSpecMeasure(Measurement):

    name = 'bioglow_spec'

    def setup(self):

        for i in range(4):
            self.settings.New(f'sample{i}_name', dtype=str, initial=f"Sample{i}")
            self.settings.New(f'sample{i}_pos', dtype=float, unit="mm", initial = 1.0)
            self.settings.New(f'sample{i}_enable', dtype=bool, initial=False)

        self.settings.New("Tacq", dtype=float, unit='s', si=True, initial=1.0)

        # 7,23,39, 50

    def run(self):
        S: LQCollection = self.settings
        MH: MultiHarpHW = self.app.hardware.multiharp
        Z: ZaberLinearHW = self.app.hardware.zaber_linear
        
        self.fw_sequence = [#Bottom , Top
                    ('EMPTY', 'EMPTY'), 
                    ('400',   'EMPTY'),
                    ('450',   'EMPTY'),
                    ('500',   'EMPTY'),
                    ('550',   'EMPTY'),
                    ('600',   'EMPTY'),
                    ('EMPTY', '650'  ),
                    ('EMPTY', '700'  ),
                    ('EMPTY', '750'  ),
                    ('EMPTY', '800'  ),
                    ('EMPTY', '850'  ),
                    ]

        # make sure in T2 mode
        # TODO
        
        # setup h5 file
        self.h5_M = h5_measurement_file(self)
        self.h5_M['fw_sequence'] = self.fw_sequence
        unique_id = self.h5_M.file['/'].attrs['unique_id']
        print(self.h5_M.file.filename)

        # Acquisition block index
        block_i = 0
        try:
            while True:
                #Exit conditions:
                if self.interrupt_measurement_called:
                    break
            
                #filter loop
                for fwBOTTOM_posname, fwTOP_posname in self.fw_sequence:

                    if self.interrupt_measurement_called:
                        break

                    # Close Shutter
                    self.app.hardware.shutter_arduino.settings['shutter_A'] = False
                    self.app.hardware.shutter_arduino.settings['shutter_B'] = False

                    # move filter wheel
                    fwB = self.app.hardware.filter_wheel_BOTTOM
                    fwB.settings["named_position"] =fwBOTTOM_posname
                    fwT = self.app.hardware.filter_wheel_TOP
                    fwT.settings["named_position"] =fwTOP_posname

                    # sample loop
                    for sample_i in range(4):
                        if self.interrupt_measurement_called:
                            break
                        if not S[f'sample{sample_i}_enable']:
                            continue
                        sample_name = S[f'sample{sample_i}_name']

                        # setup h5 sub records
                        block_name = f'{block_i:05d}-{fwBOTTOM_posname}-{fwTOP_posname}-{sample_i}_{sample_name}'

                        print(block_name)
                        
                        self.records_h5 = create_extendable_h5_dataset(self.h5_M, 
                                                                    name=f'record_blocks/{block_name}',
                                                                    shape=(1,),
                                                                    axis=0,
                                                                    dtype=np.uint32)
                        self.num_records = 0

                        # create separate h5 measurement file for this block
                        fdir = os.path.join(self.app.settings['save_dir'], f'{unique_id}_bioglow_spec_blocks')
                        fname = os.path.join(fdir, f'{block_name}.h5')        
                        os.makedirs(fdir,exist_ok=True)
                        block_h5 = h5_base_file(self.app, fname=fname)
                        print(block_h5.filename)
                        self.block_h5_M = h5_create_measurement_group(self, block_h5)
                        self.records_block_h5 = create_extendable_h5_dataset(self.block_h5_M, 
                                                                    name=f'record_blocks/{block_name}',
                                                                    shape=(1,),
                                                                    axis=0,
                                                                    dtype=np.uint32)

                        # store countrates
                        self.countrate_time_h5 = create_extendable_h5_dataset(self.h5_M, 
                                                                    name=f'countrate_blocks/{block_name}_time',
                                                                    shape=(1,), chunks=(1024,),
                                                                    axis=0,
                                                                    dtype=np.uint64)
                        self.countrate_counts_h5 = create_extendable_h5_dataset(self.h5_M, 
                                                                    name=f'countrate_blocks/{block_name}_counts',
                                                                    shape=(1,4), chunks=(1024,4),
                                                                    axis=0,
                                                                    dtype=np.uint32)
                        countrate_i = 0



                        # move zaber to sample position
                        Z.settings['target'] = S[f"sample{sample_i}_pos"]

                        # wait until motion finished
                        Z.axis.wait_until_idle()

                        # Open Shutter
                        self.app.hardware.shutter_arduino.settings['shutter_A'] = True
                        self.app.hardware.shutter_arduino.settings['shutter_B'] = True


                        # MULTIHARP T2 acquisition
                        try:
                            # start measurement
                            tacq_sec = self.settings['Tacq']
                            tacq_ms = int(1000*tacq_sec)
                            MH.dev.StartMeas(tacq=tacq_ms)
                            time.sleep(0.100)

                            while True:
                                # Exit conditions
                                if MH.dev.GetFlag_FIFOFULL():
                                    raise IOError("MultiHarp FiFo Overrun!")                    
                                if MH.dev.check_done_scanning():
                                    break
                                if self.interrupt_measurement_called:
                                    break

                                # Read fifo
                                new_records = MH.dev.ReadFiFo_to_numpy()
                                
                                # Save to H5
                                N = self.num_records
                                M = len(new_records)
                                print(N,M)
                                extend_h5_dataset_along_axis(self.records_h5, N+M, axis=0)
                                self.records_h5[N:N+M] = new_records
                                extend_h5_dataset_along_axis(self.records_block_h5, N+M, axis=0)
                                self.records_block_h5[N:N+M] = new_records
                                self.num_records = N + M

                                # save count rates
                                new_time = time.time_ns()
                                new_countrates = (MH.settings['CountRate0'], MH.settings['CountRate1'], MH.settings['CountRate2'], MH.settings['CountRate3'])
                                extend_h5_dataset_along_axis(self.countrate_time_h5,     countrate_i+1,axis=0)
                                extend_h5_dataset_along_axis(self.countrate_counts_h5, countrate_i+1,axis=0)
                                self.countrate_time_h5[countrate_i] = new_time
                                self.countrate_counts_h5[countrate_i,:] = new_countrates
                                countrate_i += 1
                        finally:
                            MH.dev.StopMeas()
                            self.h5_M.file.flush()

                            self.block_h5_M[f'countrate_blocks/{block_name}_time'] = np.array(self.countrate_time_h5)
                            self.block_h5_M[f'countrate_blocks/{block_name}_counts'] = np.array(self.countrate_counts_h5)
                            self.block_h5_M.file.close()

                            # Close Shutter
                            self.app.hardware.shutter_arduino.settings['shutter_A'] = False
                            self.app.hardware.shutter_arduino.settings['shutter_B'] = False

                            block_i += 1
        finally:
            print(f"{self.name} Done. Saved to {self.h5_M.file.filename}")
            self.h5_M.file.close()
            # Close Shutter
            self.app.hardware.shutter_arduino.settings['shutter_A'] = False
            self.app.hardware.shutter_arduino.settings['shutter_B'] = False








