
const int INPUTBUFFER_LENGTH = 150;

String inputString = "";         // a string to hold incoming data
boolean stringComplete = false;  // whether the string is complete


#define SHUTTER_A 4
#define SHUTTER_B 5

// the setup function runs once when you press reset or power the board
void setup() {
  // initialize digital pin LED_BUILTIN as an output.
  Serial.begin(9600);             // set up Serial library at 9600 bps
  pinMode(SHUTTER_A, OUTPUT);
  digitalWrite(SHUTTER_A, LOW);
  pinMode(SHUTTER_B, OUTPUT);
  digitalWrite(SHUTTER_B, LOW);
}

// Open shutter A: A1\n
// Close shutter A: A0\n
// check shutter A status: A? --> A1\n or A0\n

// the loop function runs over and over again forever
void loop() {

  serialEvent();

  if (stringComplete) {
    if (inputString == "A0\n") {
      digitalWrite(SHUTTER_A, LOW);
      Serial.println("A0");
    }
    if (inputString == "A1\n") {
      digitalWrite(SHUTTER_A, HIGH);
      Serial.println("A1");
    }
    if (inputString == "A?\n") {
      bool a = digitalRead(SHUTTER_A);
      if(a) Serial.println("A1");
      else Serial.println("A0");
    }    
    

    if (inputString == "B0\n") {
      digitalWrite(SHUTTER_B, LOW);
      Serial.println("B0");
    }
    if (inputString == "B1\n") {
      digitalWrite(SHUTTER_B, HIGH);
      Serial.println("B1");
    }
    if (inputString == "B?\n") {
      bool a = digitalRead(SHUTTER_B);
      if(a) Serial.println("B1");
      else Serial.println("B0");
    }
    


    // clear the string:
    inputString = "";
    stringComplete = false;
  }


  // digitalWrite(SHUTTER_A, HIGH);  // turn the LED on (HIGH is the voltage level)
  // delay(5000);                      // wait for a second
  // digitalWrite(SHUTTER_A, LOW);   // turn the LED off by making the voltage LOW
  // delay(5000);                      // wait for a second
}

// serial interrupt handler
void serialEvent() {
    while (Serial.available()) {
      // get the new byte:
      char inChar = (char)Serial.read(); 
      // add it to the inputString:
      inputString += inChar;
      // if the incoming character is a newline, set a flag
      // so the main loop can do something about it:
      if (inChar == '\n') {
          stringComplete = true;
          //Serial.println(inputString);
  
      } 
    }
}

