#!/bin/sh

docker build -t bioglow . && \
docker run -d --name=bioglow  \
	--security-opt seccomp=unconfined `#optional` \
	--cap-add SYS_ADMIN \
	--privileged \
	 -e PUID=1002   -e PGID=100   -e TZ=Etc/UTC \
	 -e SUBFOLDER=/ `#optional`   -e TITLE=BioGlow `#optional`  \
	 -p 127.0.0.1:3001:3001   \
	-v /home/lab/foundry_scope:/foundry_scope  \
	-v /home/lab/foundry_scope/bioglow/config:/config  \
	 -v /var/run/docker.sock:/var/run/docker.sock `#optional` \
	-v /dev:/dev \
	  --device /dev/dri:/dev/dri `#optional`  \
	 --shm-size="1gb" `#optional` \
	  --restart unless-stopped  \
	-e CUSTOM_USER=lab -e PASSWORD=$HTTP_AUTH_PASS \
	bioglow


