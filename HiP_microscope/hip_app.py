import sys
from ScopeFoundry import BaseMicroscopeApp
import time
from ScopeFoundry.helper_funcs import load_qt_ui_file, sibling_path
import numpy as np
from collections import OrderedDict

import qdarktheme # pip install pyqtdarktheme

# Import Hardware Components
#from hardware_components.dummy_xy_stage import DummyXYStage

# Import Measurement Components

#from HiP_microscope.measure.hip_dual_temperature import HiPMicroscopeDualTemperature

class HiPMicroscopeApp(BaseMicroscopeApp):

    name = "HiP_Microscope"


    def setup(self):
        qdarktheme.setup_theme()

        #Add hardware components
        from ScopeFoundryHW.mcl_stage import MclXYZStageHW
        self.add_hardware_component(MclXYZStageHW(self))

        from ScopeFoundryHW.acton_spec import ActonSpectrometerHW
        self.add_hardware_component(ActonSpectrometerHW(self))
        
        from ScopeFoundryHW.picam import PicamHW, PicamReadoutMeasure
        self.add_hardware_component(PicamHW(self))
        self.add_measurement_component(PicamReadoutMeasure(self))
        
        from ScopeFoundryHW.asi_stage.asi_stage_hw import ASIStageHW
        asi_stage = self.add_hardware(ASIStageHW(self,invert_x=True, invert_y = True,enable_z=True))
        from ScopeFoundryHW.asi_stage.asi_stage_control_measure import ASIStageControlMeasure
        asi_control = self.add_measurement(ASIStageControlMeasure(self))

        # from ScopeFoundryHW.pololu_servo.single_servo_hw import PololuMaestroServoHW
        # self.add_hardware(PololuMaestroServoHW(self, name='power_wheel'))
        
        
        # from ScopeFoundryHW.thorlabs_elliptec.elliptec_hw import ThorlabsElliptecSingleHW
        # self.add_hardware(ThorlabsElliptecSingleHW(self, name='laser_in_shutter', 
        #                                            named_positions=OrderedDict( (('CLOSED',0.0),('OPEN_ND3.0', 31.0), ('OPEN_1', 62.), ('OPEN_2', 93.)))))
        
        
        #from hardware_components.omega_pt_pid_controller import OmegaPtPIDControllerHardware        
        #self.add_hardware_component(OmegaPtPIDControllerHardware(self))


        from ScopeFoundryHW.dynamixel_servo import DynamixelXServosHW, DynamixelFilterWheelHW, DynamixelServoHW
        self.add_hardware(DynamixelXServosHW(self,
                                             devices={'focus_knob':25,
                                                      'output_select':26,
                                                      'white_light_flip': 27,
                                                      'power_wheel': 22,
                                                      'nd_wheel': 21,
                                                      'laser_select': 122}))
        self.add_hardware(DynamixelFilterWheelHW(self, name='output_select', 
                                                 named_positions={'R':0,
                                                          'AUX':1529,
                                                          'L':3067,
                                                          'EYE':4597},
                                                 release_at_target=True))
        self.add_hardware(DynamixelFilterWheelHW(self, name='white_light_flip', 
                                                 named_positions={
                                                     'white_light':2048+1024,
                                                     'laser':2048+0},
                                                 release_at_target=False))
        focus_knob = self.add_hardware(DynamixelServoHW(self, name='focus_knob',
                                            lq_kwargs={'spinbox_decimals':3, 'unit':'um'}))
        
        self.add_hardware(DynamixelServoHW(self, name='power_wheel'))
        self.add_hardware(DynamixelFilterWheelHW(self, name='nd_wheel',
                                                 named_positions={
                                                     'A_OPEN': 2048, #0,
                                                     'B':2731,# 683,
                                                     'C':3413,#1365,
                                                     'D': 0, #2048,
                                                     'E':683,#2731,
                                                     'F_CLOSED':1365 , #3413,
                                                    }
                                                 ))
        
        self.add_hardware(DynamixelFilterWheelHW(self, name='laser_select',
                                                  named_positions={'532': 2048,
                                                                   '488': 1024}))
        
        #Add measurement components
        print("Create Measurement objects")
        from HiP_microscope.measure.hyperspec_picam_mcl import HyperSpecPicam2DScan, HyperSpecPicam3DStack
        self.add_measurement(HyperSpecPicam2DScan(self))
        #self.add_measurement_component(HiPMicroscopeDualTemperature(self))
        self.add_measurement(HyperSpecPicam3DStack(self))
        
        
        from HiP_microscope.measure.picam_calibration_sweep import PicamCalibrationSweep
        self.add_measurement(PicamCalibrationSweep(self))        
        
        from ScopeFoundryHW.thorlabs_powermeter import ThorlabsPowerMeterHW, PowerMeterOptimizerMeasure
        self.add_hardware(ThorlabsPowerMeterHW(self))
        self.add_measurement(PowerMeterOptimizerMeasure(self))

        from ScopeFoundryHW.pygrabber_camera import PyGrabberCameraHW, PyGrabberCameraLiveMeasure
        self.add_hardware(PyGrabberCameraHW(self))
        self.add_measurement(PyGrabberCameraLiveMeasure(self))
        

        from ScopeFoundryHW.toupcam import  ToupCamHW, ToupCamLiveMeasure
        self.add_hardware(ToupCamHW(self))
        self.add_measurement(ToupCamLiveMeasure(self))
        
        
        from ScopeFoundryHW.lakeshore_331.lakeshore_hw import Lakeshore331HW
        from ScopeFoundryHW.lakeshore_331.lakeshore_measure import LakeshoreMeasure
        self.add_hardware(Lakeshore331HW(self))
        self.add_measurement(LakeshoreMeasure(self))
                
        from confocal_measure.sequencer import Sequencer, SweepSequencer
        self.add_measurement(SweepSequencer(self))
        
        from confocal_measure.picam_asi_hyperspec_scan import PicamAsiHyperSpec2DScan
        self.add_measurement(PicamAsiHyperSpec2DScan(self))
        
        #set some default logged quantities
        #self.hardware_components['apd_counter'].debug_mode.update_value(True)
        #self.hardware_components['apd_counter'].dummy_mode.update_value(True)
        #self.hardware_components['apd_counter'].connected.update_value(True)
        

        from confocal_measure.tiled_large_area_map import  ASIMCLToupcamTiledLargeAreaMapMeasure
        self.add_measurement(ASIMCLToupcamTiledLargeAreaMapMeasure(self))
        #self.add_measurement(AsiMclToupcamTiledLargeAreaMapMeasure(self))



        
        
        # GPCAM hyperspec
        # from HiP_microscope.measure.gpcam_hyperspec import GPCAMHyperspecMeasure
        # self.add_measurement(GPCAMHyperspecMeasure(self))        

        from HiP_microscope.measure.laser_anneal import LaserAnnealGridMeasure
        self.add_measurement(LaserAnnealGridMeasure(self))
        
        from HiP_microscope.measure.Power_sweep import PowerMeterWheelSweepMeasure
        self.add_measurement(PowerMeterWheelSweepMeasure(self))

        from HiP_microscope.measure.junction_centering import JunctionCenteringMeasure
        self.add_measurement(JunctionCenteringMeasure(self))

        from HiP_microscope.hip_microscope_control_panel import HiPMicroscopeControlPanel
        self.add_measurement(HiPMicroscopeControlPanel(self))
        
        from HiP_microscope.measure.auto_focus import AutoFocusMeasure
        self.add_measurement(AutoFocusMeasure(self))
        
        from HiP_microscope.measure.depth_spectrum_measure import DepthSpectrumMeasure
        self.add_measurement(DepthSpectrumMeasure(self))
        
        
        ###### Quickbar connections #################################
        self.add_quickbar(load_qt_ui_file(sibling_path(__file__, 'hip_quick_access.ui')))
        Q = self.quickbar
         
        # 2D Scan Area
#         oo_scan.settings.h0.connect_to_widget(Q.h0_doubleSpinBox)
#         oo_scan.settings.h1.connect_to_widget(Q.h1_doubleSpinBox)
#         oo_scan.settings.v0.connect_to_widget(Q.v0_doubleSpinBox)
#         oo_scan.settings.v1.connect_to_widget(Q.v1_doubleSpinBox)
#         oo_scan.settings.h_span.connect_to_widget(Q.hspan_doubleSpinBox)
#         oo_scan.settings.v_span.connect_to_widget(Q.vspan_doubleSpinBox)
#        Q.center_pushButton.clicked.connect(oo_scan.center_on_pos)
#        scan_steps = [5e-3, 3e-3, 1e-3, 5e-4]
#        scan_steps_labels = ['5.0 um','3.0 um','1.0 um','0.5 um']
#         Q.scan_step_comboBox.addItems(scan_steps_labels)
#         def apply_scan_step_value():
#             oo_scan.settings.dh.update_value(scan_steps[Q.scan_step_comboBox.currentIndex()])
#             oo_scan.settings.dv.update_value(scan_steps[Q.scan_step_comboBox.currentIndex()])
#         Q.scan_step_comboBox.currentIndexChanged.connect(apply_scan_step_value)
        
        # MCL Stage
        mcl = self.hardware['mcl_xyz_stage']
        mcl.settings.x_position.connect_to_widget(Q.mcl_x_pos_doubleSpinBox)
        mcl.settings.y_position.connect_to_widget(Q.mcl_y_pos_doubleSpinBox)
        mcl.settings.z_position.connect_to_widget(Q.mcl_z_pos_doubleSpinBox)
        mcl.settings.x_target.connect_to_widget(Q.mcl_x_target_doubleSpinBox)
        mcl.settings.y_target.connect_to_widget(Q.mcl_y_target_doubleSpinBox)
        mcl.settings.z_target.connect_to_widget(Q.mcl_z_target_doubleSpinBox)
        mcl.settings.connected.connect_to_widget(Q.mcl_connect_checkBox)
        Q.mcl_goto_center_pushButton.clicked.connect(mcl.go_to_center_xy)
        
        p = Q.mcl_plotwidget
        p.showAxis('bottom', False)
        p.showAxis('left', False)
        import pyqtgraph as pg
        from qtpy import QtWidgets
        #r = QtWidgets.QGraphicsRectItem(0, 0, 0.4, 1)
        #r.setBrush(pg.mkBrush('r'))
        #p.addItem(r)
        p.plot([0,1,1,0,0],[0,0,1,1,0])
        
        self.current_stage_pos_arrow = pg.ArrowItem()
        self.current_stage_pos_arrow.setZValue(100)
        self.current_stage_pos_arrow.setScale(0.5)
        self.current_stage_pos_arrow.setBrush(pg.mkBrush('r'))
        p.addItem(self.current_stage_pos_arrow)

        mcl.settings.x_position.updated_value.connect(self.update_mcl_arrow_pos) 
        mcl.settings.y_position.updated_value.connect(self.update_mcl_arrow_pos)

        
        #
        # ASI Stage
        asi_stage.settings.x_position.connect_to_widget(Q.x_pos_doubleSpinBox)
        Q.x_up_pushButton.clicked.connect(asi_control.x_up)
        Q.x_down_pushButton.clicked.connect(asi_control.x_down)
  
        asi_stage.settings.y_position.connect_to_widget(Q.y_pos_doubleSpinBox)
        Q.y_up_pushButton.clicked.connect(asi_control.y_up)
        Q.y_down_pushButton.clicked.connect(asi_control.y_down)
        
        
  
#         asi_stage.settings.z_position.connect_to_widget(Q.z_pos_doubleSpinBox)
#         Q.z_up_pushButton.clicked.connect(asi_control.z_up)
#         Q.z_down_pushButton.clicked.connect(asi_control.z_down)
         
        asi_control.settings.jog_step_xy.connect_to_widget(Q.xy_step_doubleSpinBox)
#         asi_control.settings.jog_step_z.connect_to_widget(Q.z_step_doubleSpinBox)
         
        stage_steps = np.array([5e-4, 1e-3, 1e-2, 1e-1, 1e0])
        stage_steps_labels = ['0.0005 mm','0.001 mm','0.010 mm','0.100 mm','1.000 mm']
        Q.xy_step_comboBox.addItems(stage_steps_labels)
#         Q.z_step_comboBox.addItems(stage_steps_labels)
         
        def apply_xy_step_value():
            asi_control.settings.jog_step_xy.update_value(stage_steps[Q.xy_step_comboBox.currentIndex()])
        Q.xy_step_comboBox.currentIndexChanged.connect(apply_xy_step_value)
         
        def apply_z_step_value():   
            asi_control.settings.jog_step_z.update_value(stage_steps[Q.z_step_comboBox.currentIndex()])
        #Q.z_step_comboBox.currentIndexChanged.connect(apply_z_step_value)
         
        def halt_stage_motion():
            asi_stage.halt_xy()
            asi_stage.halt_z()
        Q.stop_stage_pushButton.clicked.connect(halt_stage_motion)

        asi_stage.settings.x_target.connect_to_widget(Q.asi_x_target_doubleSpinBox)
        asi_stage.settings.y_target.connect_to_widget(Q.asi_y_target_doubleSpinBox)

        #Q.asi_xy_target_groupBox.toggled.connect(Q.asi_xy_target_groupBox.setEnabled)

        # Power Wheel (Dynamixel Servo)
        pw = self.hardware['power_wheel']
        pw.settings.target_position.connect_to_widget(Q.power_wheel_encoder_pos_doubleSpinBox)
        pw.settings.jog.connect_to_widget(Q.powerwheel_move_steps_doubleSpinBox)
        Q.powerwheel_move_fwd_pushButton.clicked.connect(pw.jog_fwd)
        Q.powerwheel_move_bkwd_pushButton.clicked.connect(pw.jog_bkwd)


        # Picam
        picam = self.hardware['picam']
        picam.settings.ccd_status.connect_to_widget(Q.andor_ccd_status_label)
        picam.settings.SensorTemperatureReading.connect_to_widget(Q.andor_ccd_temp_doubleSpinBox)
        picam.settings.ExposureTime.connect_to_widget(Q.andor_ccd_int_time_doubleSpinBox)


        # Spectrometer
        aspec = self.hardware['acton_spectrometer']
        aspec.settings.center_wl.connect_to_widget(Q.acton_spec_center_wl_doubleSpinBox)
        aspec.settings.exit_mirror.connect_to_widget(Q.acton_spec_exitmirror_comboBox)
        aspec.settings.grating_name.connect_to_widget(Q.acton_spec_grating_lineEdit)        
        
        # power meter
        pm = self.hardware['thorlabs_powermeter']
        pm.settings.wavelength.connect_to_widget(Q.power_meter_wl_doubleSpinBox)
        pm.settings.power.connect_to_widget(Q.power_meter_power_label)
        
        pm_opt = self.measurements['powermeter_optimizer']
        pm_opt.settings.activation.connect_to_widget(Q.power_meter_acquire_cont_checkBox)

        # Laser in shutter
        #shutter = self.hardware['laser_in_shutter']
        #shutter.settings.connected.connect_to_widget(Q.laser_in_shutter_connected_checkBox)
        #shutter.settings.named_position.connect_to_widget(Q.laser_in_shutter_namepos_comboBox)
        nd_wheel = self.hardware['nd_wheel']
        nd_wheel.settings.connected.connect_to_widget(Q.nd_wheel_connected_checkBox)
        nd_wheel.settings.named_position.connect_to_widget(Q.nd_wheel_pos_comboBox)
        def on_nd_wheel_close(x, nd_wheel=nd_wheel):
            nd_wheel.settings['named_position'] = 'F_CLOSED'
        Q.nd_wheel_close_pushButton.clicked.connect(on_nd_wheel_close)

        
        # # Light Source ASI Z slider
        # asi_stage.settings.z_position.connect_to_widget(Q.asi_z_pos_doubleSpinBox)
        # def on_light_src_whightlight():
        #     asi_stage.settings['z_target'] = 22.0
        # def on_light_src_laser():
        #     asi_stage.settings['z_target'] = -5.0
        # Q.light_src_whitelight_pushButton.clicked.connect(on_light_src_whightlight)
        # Q.light_src_laser_pushButton.clicked.connect(on_light_src_laser)
        #
        
        # Light Source servo
        white_light_flip = self.hardware['white_light_flip']
        def on_light_src_whightlight():
            white_light_flip.settings['named_position'] = 'white_light'
        def on_light_src_laser():
            white_light_flip.settings['named_position'] = 'laser'
        Q.light_src_whitelight_pushButton.clicked.connect(on_light_src_whightlight)
        Q.light_src_laser_pushButton.clicked.connect(on_light_src_laser)
        white_light_flip.settings.named_position.connect_to_widget(Q.light_src_pos_comboBox)
        white_light_flip.settings.connected.connect_to_widget(Q.light_src_hw_connect_checkBox)
        
        # Output Select
        output_select = self.hardware['output_select']
        output_select.settings.connected.connect_to_widget(Q.output_select_hw_checkBox)
        output_select.settings.named_position.connect_to_widget(Q.output_select_pos_comboBox)
        
        # Laser Select
        laser_select = self.hardware['laser_select']
        laser_select.settings.connected.connect_to_widget(Q.laser_select_hw_checkBox)
        laser_select.settings.named_position.connect_to_widget(Q.laser_select_pos_comboBox)

        def on_wl_imaging_action():
            nd_wheel.settings['named_position'] = 'F_CLOSED'
            white_light_flip.settings['named_position'] = 'white_light'
            output_select.settings['named_position'] = 'R'
            
        def on_laser_spec_action():
            white_light_flip.settings['named_position'] = 'laser'
            output_select.settings['named_position'] = 'L'
            
        Q.wl_imaging_action_pushButton.clicked.connect(on_wl_imaging_action)
        Q.laser_spec_action_pushButton.clicked.connect(on_laser_spec_action)
        ####################




        # load default settings from file
        self.settings_load_ini("hip_settings.ini", 
                               #ignore_hw_connect=True
                               )


    def setup_ui(self):
        return
        self.hardware['mcl_xyz_stage'].settings['connected']=True
        #self.hardware['picam'].settings['connected']=True
        #time.sleep(0.5)
        self.hardware['picam'].settings['roi_y_bin'] = 100
        #self.hardware['picam'].commit_parameters()
        
        #self.hardware['acton_spectrometer'].settings['connected']=True
        self.hardware['asi_stage'].settings['connected'] = True
        
        
        #self.set_subwindow_mode()
        
    def update_mcl_arrow_pos(self):
        stage = self.hardware['mcl_xyz_stage']
        x = stage.settings['x_position']
        y = stage.settings['y_position']
        self.current_stage_pos_arrow.setPos(x/stage.settings['x_max'],y/stage.settings['y_max'])



if __name__ == '__main__':

    app = HiPMicroscopeApp(sys.argv)
    #app.tile_layout()
    sys.exit(app.exec_())