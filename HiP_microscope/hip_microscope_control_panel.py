from ScopeFoundry import Measurement
from ScopeFoundry.helper_funcs import load_qt_ui_file, sibling_path

class HiPMicroscopeControlPanel(Measurement):
    
    name = 'hip_microscope'
    
    def setup(self):
        
        self.ui = load_qt_ui_file(sibling_path(__file__, 'hip_microscope_control_panel.ui'))
        HW = self.app.hardware
        
        HW['acton_spectrometer'].settings.connected.connect_to_widget(
            self.ui.acton_spec_hw_checkBox)
        HW['acton_spectrometer'].settings.center_wl.connect_to_widget(
            self.ui.acton_spec_wl_doubleSpinBox)
        HW['acton_spectrometer'].settings.grating_id.connect_to_widget(
            self.ui.acton_spec_grating_comboBox)
        
        # HW['laser_in_shutter'].settings.connected.connect_to_widget(
        #     self.ui.laser_in_shutter_hw_checkBox)
        # HW['laser_in_shutter'].settings.named_position.connect_to_widget(
        #     self.ui.laser_in_shutter_comboBox)

        HW['white_light_flip'].settings.connected.connect_to_widget(
            self.ui.wl_flip_hw_checkBox)        
        HW['white_light_flip'].settings.named_position.connect_to_widget(
            self.ui.wl_flip_comboBox)

        HW['output_select'].settings.connected.connect_to_widget(
            self.ui.output_select_hw_checkBox)
        HW['output_select'].settings.named_position.connect_to_widget(
            self.ui.output_select_comboBox)
        
        HW['asi_stage'].settings.connected.connect_to_widget(
            self.ui.asi_hw_checkBox)
        
        HW['picam'].settings.connected.connect_to_widget(
            self.ui.picam_hw_checkBox)
        
        HW['toupcam'].settings.connected.connect_to_widget(
            self.ui.toupcam_hw_checkBox)
        HW['toupcam'].settings.exposure.connect_to_widget(
            self.ui.toupcam_exposure_doubleSpinBox)
        
        HW['mcl_xyz_stage'].settings.connected.connect_to_widget(
            self.ui.mcl_hw_checkBox)

        HW['nd_wheel'].settings.connected.connect_to_widget(
            self.ui.nd_wheel_hw_checkBox)

        HW['nd_wheel'].settings.named_position.connect_to_widget(
            self.ui.nd_wheel_named_position_comboBox)
        
        
        HW['focus_knob'].settings.connected.connect_to_widget(
            self.ui.focus_knob_hw_checkBox)
        HW['focus_knob'].settings.position.connect_to_widget(
            self.ui.focus_knob_pos_doubleSpinBox)
        HW['focus_knob'].settings.jog.connect_to_widget(
            self.ui.focus_knob_jog_doubleSpinBox)
        
        self.ui.focus_knob_up_pushButton.clicked.connect(HW['focus_knob'].jog_fwd)
        self.ui.focus_knob_down_pushButton.clicked.connect(HW['focus_knob'].jog_bkwd)
        
        HW['power_wheel'].settings.connected.connect_to_widget(
            self.ui.power_wheel_hw_checkBox)
        HW['power_wheel'].settings.target_position.connect_to_widget(
            self.ui.power_wheel_doubleSpinBox)
        
        HW['laser_select'].settings.connected.connect_to_widget(
            self.ui.laser_select_hw_checkBox)
        HW['laser_select'].settings.named_position.connect_to_widget(
            self.ui.laser_select_comboBox)
        
        
# <?xml version="1.0" encoding="UTF-8"?>
# <ui version="4.0">
#   <widget class="QDoubleSpinBox" name="toupcam_exposure_doubleSpinBox">
#    <property name="geometry">
#     <rect>
#      <x>720</x>
#      <height>22</height>
#     </rect>
#    </property>
#   </widget>
#   <widget class="QCheckBox" name="mcl_hw_checkBox">

#     </rect>
#    </property>
#    <property name="text">
#     <string>laser_in_shutter</string>
#    </property>
#   </widget>
#    <property name="geometry">
#     <rect>
#      <x>468</x>
#      <y>260</y>
#      <width>91</width>
#      <height>22</height>
#     </rect>
#    </property>
#   </widget>
#   <widget class="QCheckBox" name="toupcam_checkBox">
#    <property name="geometry">
#     <rect>
#      <x>720</x>
#      <y>440</y>
#      <width>70</width>
#      <height>17</height>
#     </rect>
#    </property>
#    <property name="text">
#     <string>toupcam</string>
#    </property>
#   </widget>
#  </widget>
#  <resources/>
# </ui>
