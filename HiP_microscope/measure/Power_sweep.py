from ScopeFoundry import Measurement
import time
import numpy as np
import pyqtgraph as pg
from ScopeFoundry import h5_io


         
         
class PowerMeterWheelSweepMeasure(Measurement):

    name = "powermeter_wheel_sweep"
    
    def setup(self):        
        self.display_update_period = 0.1 #seconds

        # logged quantities
        self.settings.New(name='update_period', dtype=float, si=True, initial=0.1, unit='s')
        self.settings.New(name='time_per_power', dtype=float, si=True, initial=10., unit='s')
        
        self.power_wheel_range = self.settings.New_Range(name='power_wheel_angles', dtype=float, initials = [0, 270, 10],)
        
        # create data array

        # hardware
        self.powermeter = self.app.hardware['thorlabs_powermeter']

        
    def setup_figure(self):
        
        self.ui = self.graph_layout=pg.GraphicsLayoutWidget(border=(100,100,100))
        
        # history plot
        self.plot = self.graph_layout.addPlot(title="Power Meter vs Power wheel")
        self.plot_line = self.plot.plot()        


    def run(self):
        
        self.app.hardware['power_wheel'].settings['target_position']  = 0
        time.sleep(0.5)

        
        self.display_update_period = 0.02 #seconds
        
        pw_pos = self.power_wheel_range.array
        
        M = len(pw_pos)
        N = int(self.settings['time_per_power']/ self.settings['update_period'])
        
        
        self.data = np.zeros((M, N), dtype=float)
        
        self.plot_data = []
        
        self.h5_file = h5_io.h5_base_file(self.app, measurement=self )
        H = self.h5_meas_group  =  h5_io.h5_create_measurement_group(self, self.h5_file)

        H['power_wheel_angles'] = pw_pos
        H['data'] = self.data

        try:                
            for ii, x in enumerate(pw_pos):
                
                self.app.hardware['power_wheel'].settings['target_position']  = x
                
                
                if self.interrupt_measurement_called:
                    break            
                for jj in range(N):
                    self.set_progress(100*(ii/M+jj/(M*N)))
                    if self.interrupt_measurement_called:
                        break
                    pow_reading = self.powermeter.settings.power.read_from_hardware()
                    self.data[ii, jj] = pow_reading
                    H['data'][ii,jj] = [pow_reading]
                    self.plot_data.append(pow_reading)            
                    time.sleep(self.settings['update_period'])
                
    
        finally:
            self.h5_file.close()
            
    
    def update_display(self):        
        self.plot_line.setData(self.plot_data)