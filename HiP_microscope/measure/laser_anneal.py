from ScopeFoundry import Measurement
import time
import csv
import random
import os
import numpy as np
from astropy.table import row

class LaserAnnealGridMeasure(Measurement):
    
    name = 'laser_anneal_grid'
    
    def setup(self):
        pass
        #self.settings.New(
        
    def run(self):
        
        # close laser shutter
        laser_in_shutter = self.app.hardware['laser_in_shutter']
        laser_in_shutter.settings['named_position'] = 'CLOSED'
        #laser_in_shutter.settings['named_position'] = 'OPEN_ND3.0'
        
        # read coordinate csv file        
        dat = []
        with open('measure/coord_transform.csv', newline='') as csvfile:
            datrows = csv.reader(csvfile, delimiter=' ', quotechar='|')
            for row in datrows:
                dat.append(row)
        coords = np.zeros((10,4))
        transf = np.zeros((10,4))
        
        for i in np.linspace(1, len(dat)-1, len(dat)-1):
            data = dat[int(i)][0].split(',')
            for j in range(len(data)):
                if j>0 and j<5:
                    transf[int(i-1)][j-1] = float(data[j])
                elif j>4:
                    coords[int(i-1)][j-5] = float(data[j])
       
        # generate target coordinates 
        N = 81
        target_coords = np.zeros((N,4))
        for i in range(20,37):
            if i%2 ==1:
                continue
            else:
                for k in range(20,37):
                    if k%2 ==1:
                        continue
                    else:
                        index = int((i-20)/2)*9 + int((k-20)/2)
                        for j in range(4):
                            if j==3:
                                target_coords[index][j] =1
                            elif j==2:
                                continue
                            elif j ==1:
                                target_coords[index][j] = k
                            else:
                                target_coords[index][j] = i
        
        print(target_coords)
        # Calculate lstsq transformation matrix
        # target_transf = ([x, y, z, t])
        A = self.convert_to_stage_pos(coords, transf)
        target_transf = np.matmul(A, target_coords.transpose()).transpose()
        
        # time and power wheel settings
        times = [60]
        power_wheel_vals = [270]
        
        # switch to laser mode
        self.app.hardware['white_light_flip'].settings['named_position'] = 'laser'

        # run sweep
        for position in target_transf:
            for exposure_time in times:
                for wheel in power_wheel_vals:
                    print(position, exposure_time, wheel)
            
                    if self.interrupt_measurement_called:
                        break
                    
                    x,y,z,_ = position
                    
                    asi_stage = self.app.hardware['asi_stage']
                
                    asi_stage.settings['x_target'] = x
                    asi_stage.settings['y_target'] = y
                    
                    pos_x = asi_stage.settings.x_position.read_from_hardware()
                    pos_y = asi_stage.settings.y_position.read_from_hardware()
                                            
                    # Check current position - target < delta
                    while abs(pos_x -x)>0.010 or abs(pos_y-y)>0.010:
                        pos_x = asi_stage.settings.x_position.read_from_hardware()
                        pos_y = asi_stage.settings.y_position.read_from_hardware()
                        if self.interrupt_measurement_called:
                            break
                        time.sleep(0.5)
                    
                    # change z position
                    focus_knob = self.app.hardware['focus_knob']
                    focus_knob.settings['target_position'] = z
                    
                    # mcl_stage = self.app.hardware[]
                    # mcl_stage.settings[] = z
                    
                    
                    # set power
                    power_wheel = self.app.hardware['power_wheel']
                    power_wheel.settings['position'] = wheel
                    
                    time.sleep(0.5)
                                
                    # open shutter
                    laser_in_shutter.settings['named_position'] = 'OPEN_1'
                    
                    # wait for exposure time
                    t0 = time.time()
                    dt = 0
                    while dt < exposure_time:
                        time.sleep(0.01)
                        if self.interrupt_measurement_called:
                            break
                        dt = time.time() - t0
                
                    # close shutter
                    laser_in_shutter.settings['named_position'] = 'CLOSED'

                    # take a picture
                    laser_in_shutter.settings['named_position'] = 'CLOSED'
                    self.app.hardware['white_light_flip'].settings['named_position'] = 'white_light'
                    time.sleep(1)
                    self.app.measurements['tiled_large_area_map'].snap()
                    time.sleep(1)
                    self.app.hardware['white_light_flip'].settings['named_position'] = 'laser'

            
    def convert_to_stage_pos(self, coords, transf):
        
        # coords = [(row, column, z, 1)]
        # transf = [(x, y, z, 1)]
        
        coords.transpose()
        transf.transpose()
        
        A = np.linalg.lstsq(coords, transf)
        
        # transf' = matmul(A[0].transpose(), coords')
        return A[0].transpose()
    
    
    
    
    
    