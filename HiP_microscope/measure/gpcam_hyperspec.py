from ScopeFoundry.measurement import Measurement
import numpy as np
import time
import os

class GPCAMHyperspecMeasure(Measurement):
    
    name =  'gpcam_hyperspec'
    
    def setup(self):
        
        self.settings.New("command_file", dtype=str, initial=r'C:\Users\lab\Documents\gpcam\data\command\command.npy')
        self.settings.New("result_file", dtype=str,  initial=r'C:\Users\lab\Documents\gpcam\data\result\result.npy')
    
        self.settings.New("num_initial_points", dtype=int, initial=10)
    
    def run(self):
        
        #print("welcome to gpcam! asdfasdf", self.settings['num_initial_points'])
        
        #self.settings['num_initial_points'] =10
        
        
        #### CCD / Spectrometer settings
        self.picam = self.app.hardware['picam']

        self.spec_map = np.zeros( (100,) + (1340,), dtype=np.float)
#        if self.settings['save_h5']:
#            self.spec_map_h5 = self.h5_meas_group.create_dataset('spec_map', self.scan_shape + (1340,), dtype=np.float)
        self.app.measurements.picam_readout.interrupt()

        self.picam.commit_parameters()


        for i in range(self.settings['num_initial_points']):
            # collect initial data points for gpcam
            pass
        
        # send initial data to gpcam
        # write data in approprate format to result_file
        
        #### Stage settings
        
        stage = self.app.hardware['mcl_xyz_stage']
        
        
        while not self.interrupt_measurement_called:
            print("waiting for command file...")
            time.sleep(1)
            # wait for command file to appear
            
            cmd_filename = self.settings['command_file']
            result_filename = self.settings['result_file']

            ii = 0
            
            if os.path.isfile(cmd_filename):
                print("cmmand file received")
                a = np.load(cmd_filename, encoding="ASCII", allow_pickle=True)
                for entry in a:
                    if entry["measured"] == True: 
                        continue
                    
                    
                    i = entry['position']["x1"]
                    j = entry['position']['x2']
                    
                    x = 10 + 30*i/128
                    y = 10 + 30*j/128
                    
                    # Move to new location:
                    stage.settings['x_target'] = x
                    stage.settings['y_target'] = y
                    time.sleep(0.010)
                    
                    
                    
                    spec =  self.collect_spec(ii)
                    print("spectrum sum at {} {} ==> {}".format(x,y, np.sum(spec)))
                    ii += 1
                    entry["measured"] = True
                    #entry["measurement values"]["values"] = [np.sin(entry["position"]["x1"])]
                    #entry["measurement values"]["values"] = [np.random.rand()]
                    #entry["measurement values"]["values"] = [np.sum(spec)]
                    #entry["measurement values"]["value positions"] = [[0]]
                    entry["measurement values"]["values"] = np.array([np.sum(spec)])
                    entry["measurement values"]["value positions"] = np.array([0])
        
        
                os.remove(cmd_filename)
                np.save(result_filename, a)
                print("results written")
            

#                 # ffor entry in command file, move stage and acquire data
#                 for entry in commands:
#                     if self.interrupt_measurement_called:
#                         break
#                     # move
#                     stage.settings['x_target'] = x
#                     stage.settings['y_target'] = y
#     
#                     # collect analog and counters (point detector)
#                     
#                     # collect spectrum
#                     self.collect_pixel()

    def update_display(self):
        Measurement.update_display(self)
        self.app.measurements['picam_readout'].update_display()
            
            
    def collect_spec(self, pixel_num):
        # collect data
        # store in arrays        
        print("collect_spec:",  pixel_num)
        dat = self.picam.cam.acquire(readout_count=1, readout_timeout=-1)
            
        self.roi_data = self.picam.cam.reshape_frame_data(dat)
        spec =  self.roi_data[0].sum(axis=0)
        
        #if self.settings['save_h5']:
        #    self.spec_map_h5[k,j,i,:] = self.spec
        
        #self.display_image_map[k,j,i] = np.sum(self.spec)

        #tot_sec_to_go = (1.0 - self.settings.progress.val / 100) * self.Npixels * self.picam.settings['ExposureTime'] / 1000
        #h_left = tot_sec_to_go / 3600
        #min_left = (tot_sec_to_go % 3600) / 60
        #sec_left = (tot_sec_to_go % 3600 ) % 60
        
        #print('{0:1.0f}h {1:2.0f}min {2:2.0f}s left'.format(h_left, min_left, sec_left) )
        

        if pixel_num == 0:
            self.log.info("pixel 0: creating data arrays")

            self.picam_measure = self.app.measurements['picam_readout']

            self.wls = np.array(self.picam_measure.wls)
            self.wave_numbers = np.array(self.picam_measure.wave_numbers)
            self.raman_shifts = np.array(self.picam_measure.raman_shifts)

#            if self.settings['save_h5']:
#                self.h5_meas_group['wls'] = self.wls
#                self.h5_meas_group['wave_numbers'] = self.wave_numbers
#                self.h5_meas_group['raman_shifts'] = self.raman_shifts


        return spec