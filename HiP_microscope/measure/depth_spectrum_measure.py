from ScopeFoundry import Measurement
import pyqtgraph as pg

class DepthSpectrumMeasure(Measurement):
    
    name = 'depth_spectrum'
    
    def setup(self):
        
        self.settings.New_Range('depth', unit='um')
        
    def setup_figure(self):
        
        self.ui = self.graph_layout = pg.GraphicsLayoutWidget()
        
        
    def run(self):
        
        self.first_graph = True
        
        import time
        from ScopeFoundry import h5_io
        import numpy as np

        focus_knob = self.app.hardware['focus_knob']

        
        original_z = focus_knob.settings['position']
        
        self.depths = depths = self.settings.ranges['depth'].array
        
        self.picam = self.app.hardware['picam']
        self.picam_measure = self.app.measurements['picam_readout']
        self.picam_measure.interrupt()
        self.picam.commit_parameters()
        

        self.h5_file = h5_io.h5_base_file(self.app, measurement=self )
        H = self.h5_meas_group  =  h5_io.h5_create_measurement_group(self, self.h5_file)


        
        try:
            
            self.wls = np.array(self.picam_measure.wls)
            self.wave_numbers = np.array(self.picam_measure.wave_numbers)
            self.raman_shifts = np.array(self.picam_measure.raman_shifts)
    
            H['wls'] = self.wls
            H['wave_numbers'] = self.wave_numbers
            H['raman_shifts'] = self.raman_shifts
            H['spectra'] = np.zeros((len(depths), len(self.wls)), dtype=float)
            
            self.spectra = np.zeros((len(depths), len(self.wls)), dtype=float)*np.NaN
            
            H['depths'] = depths
            H['z0'] = original_z
            
            
            for ii, z in enumerate(depths):
                print(ii,z)
                

                
                
                focus_knob.settings['target_position'] = original_z + z
                time.sleep(0.1)
            
                dat = self.picam.cam.acquire(readout_count=1, readout_timeout=-1)
            
                self.roi_data = self.picam.cam.reshape_frame_data(dat)
                self.spec =  self.roi_data[0].sum(axis=0)
                
                print(self.spec)
                H['spectra'][ii] = self.spec
                self.spectra[ii] = self.spec
            
        finally:
            focus_knob.settings['target_position'] = original_z
            self.h5_file.close()
    
    def update_display(self):
        if self.first_graph:
            self.graph_layout.clear()
            
            self.plot = self.graph_layout.addPlot(title="Spectra vs depth")
            self.plot_line = self.plot.plot()        
        
            self.first_graph = False
            
        self.plot_line.setData(self.depths, self.spectra.sum(axis=1))
        
        if hasattr(self, 'roi_data'):
            self.app.measurements.picam_readout.roi_data = self.roi_data
            self.app.measurements.picam_readout.update_display()
