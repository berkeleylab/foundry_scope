from ScopeFoundry import Measurement
import pyqtgraph as pg
import numpy as np
from skimage import io
from skimage import feature, exposure
from skimage import data
from skimage.color import rgb2gray
from skimage.transform import hough_line, hough_line_peaks
from skimage.draw import line



class JunctionCenteringMeasure(Measurement):
    
    name = 'junction_centering'
    
    def setup(self):
        
        self.settings.New("center_x", dtype=float, unit='%', initial=50)
        self.settings.New("center_y", dtype=float, unit='%', initial=50)

        
        self.settings.New("roi_x", dtype=int, unit='px', initial=100)
        self.settings.New('angle_tol', dtype=float, unit='deg', initial=1.0)
        
        self.settings.New('adjust_log_gain', dtype=float, initial=10)
        
        self.settings.New('canny_sigma', dtype=float, initial=1.0)
        
    def setup_figure(self):
        
        self.ui = self.graph_layout=pg.GraphicsLayoutWidget(border=(100,100,100))
        
        # history plot
        self.plot_live = self.graph_layout.addPlot(title="Live")
        self.plot_live_imgitem = pg.ImageItem()
        self.plot_live.addItem(self.plot_live_imgitem)

        self.graph_layout.nextRow()
                
        self.plot_snap = self.graph_layout.addPlot(title="Snap")
        self.plot_snap_imgitem = pg.ImageItem()
        self.plot_snap.addItem(self.plot_snap_imgitem)
        
        self.plot_canny = self.graph_layout.addPlot(title="Junction centering")
        self.plot_canny_imgitem = pg.ImageItem()
        self.plot_canny.addItem(self.plot_canny_imgitem)
        
        

    def run(self):
        S = self.settings
        
        # TAke a snapshot
        self.orig_im = self.get_rgb_image()


        Nx, Ny, _ = self.orig_im.shape

        cx = int(S['center_x']*Nx/100.)
        cy = int(S['center_y']*Ny/100.)
        roi_x = roi_y = S['roi_x']//2
        
        self.orig_im  = self.orig_im [cx-roi_x:cx+roi_x, cy-roi_y:cy+roi_y,:]



        
        # process image
        image = rgb2gray(self.orig_im)
        
        image = exposure.adjust_log(image, S['adjust_log_gain'])
        self.edges1 = edges1 = feature.canny(image,sigma=S['canny_sigma'])
        
        # angles1 = np.linspace(-np.pi / 2-np.pi/180, -np.pi / 2+np.pi/180, 5, endpoint=False)
        # angles2 = np.linspace(np.pi / 2-np.pi/180, np.pi / 2+np.pi/180, 5, endpoint=False)
        # angles3 = np.linspace(-np.pi -np.pi/180, -np.pi +np.pi/180, 5, endpoint=False)
        # angles4 = np.linspace(np.pi -np.pi/180, np.pi +np.pi/180, 5, endpoint=False)

        tested_angles1 = np.linspace(-np.pi / 2-np.pi/180, -np.pi / 2+np.pi/180, 10, endpoint=False)
        tested_angles2 = np.linspace(-np.pi -np.pi/180, -np.pi +np.pi/180, 10, endpoint=False)
        self.tested_angles = np.concatenate((tested_angles1, tested_angles2))
        h1, theta1, d1 = hough_line(edges1, theta=tested_angles1)
        h2, theta2, d2 = hough_line(edges1, theta=tested_angles2)
        
        x1s = []
        x2s = []
        slope1s = []
        slope2s = []
        try:
            line_peaks = hough_line_peaks(h1, theta1, d1, min_angle=1)
        except IndexError:
            return 
        for _, angle1, dist1 in zip(*line_peaks):
            (x1, y1) = dist1 * np.array([np.cos(angle1), np.sin(angle1)])
            slope1=np.tan(angle1+ np.pi/2)
            x1s.append( (x1,y1))
            slope1s.append(slope1)
        for _, angle2, dist2 in zip(*hough_line_peaks(h2, theta2, d2, min_angle=1)):
            (x2, y2) = dist2 * np.array([np.cos(angle2), np.sin(angle2)])
            slope2=np.tan(angle2+ np.pi/2)
            x2s.append( (x2,y2))
            slope2s.append(slope2)
        # compute intersection pixel
        (x1,y1) = x1s[0]
        (x2,y2) = x2s[0]
        slope1 = slope1s[0]
        slope2 = slope2s[0]
        self.x_int = (slope1*slope2*(x1-x2)+slope1*y2-slope2*y1)/(slope1-slope2)
        self.y_int = (slope1*x1-slope2*x2+y2-y1)/(slope1-slope2)

        # convert delta pixel --> delta mm
        d_x = -(self.y_int - 50) * (0.3316/1024) # (delta pixel) * (mm/pixel conversion) 
        d_y = (self.x_int - 50) * (0.3316/1024) # (delta pixel) * (mm/pixel conversion)
        
        asi_stage = self.app.hardware['asi_stage']
        pos_x = asi_stage.settings.x_position.read_from_hardware()
        pos_y = asi_stage.settings.y_position.read_from_hardware()
        # move stage
        d = np.sqrt(d_x**2 + d_y**2)
        if d > 0.02:
            pass
        else:
            x = pos_x + d_x
            y = pos_y + d_y
            asi_stage.settings['x_target'] = x
            asi_stage.settings['y_target'] = y
        
        # display new image
        
        
    def get_rgb_image(self):
        cam = self.app.hardware['toupcam'].cam

        data = cam.get_image_data()
        raw = data.view(np.uint8).reshape(data.shape + (-1,))
        bgr = raw[..., :3]
        return bgr[..., ::-1]
    
    def update_display(self):
        
        self.plot_snap_imgitem.setImage(self.orig_im)
        
        self.plot_canny.clear()
        self.plot_canny_imgitem = pg.ImageItem()
        self.plot_canny.addItem(self.plot_canny_imgitem)
        self.plot_canny_imgitem.setImage(self.edges1)
        
        self.plot_canny.plot([self.x_int], [self.y_int], symbol ='x', color='red')

        h, theta, d = hough_line(self.edges1, theta=self.tested_angles)
        for _, angle, dist in zip(*hough_line_peaks(h, theta, d)):
            (x0, y0) = dist * np.array([np.cos(angle), np.sin(angle)])
            l = pg.InfiniteLine((x0,y0), angle=180.(angle+np.pi/2)/np.pi)
            self.plot_canny.addItem(l)
            #ax[1].axline((x0, y0), slope=np.tan(angle + np.pi/2))



def test():
    import numpy as np
    import matplotlib.pyplot as plt
    from matplotlib import cm
    import os
    
    from skimage import io
    from skimage import feature, exposure
    from skimage import data
    from skimage.color import rgb2gray
    from skimage.transform import hough_line, hough_line_peaks
    from skimage.draw import line
    
    filename1 = 'cross.jpg'
    filename2 = 'junction2.png'
    junction = io.imread(filename2)
    
    original = junction
    image = rgb2gray(original)
    image = exposure.adjust_log(image, 10)
    
    edges1 = feature.canny(image)
    
    angles1 = np.linspace(-np.pi / 2-np.pi/180, -np.pi / 2+np.pi/180, 5, endpoint=False)
    angles2 = np.linspace(np.pi / 2-np.pi/180, np.pi / 2+np.pi/180, 5, endpoint=False)
    angles3 = np.linspace(-np.pi -np.pi/180, -np.pi +np.pi/180, 5, endpoint=False)
    angles4 = np.linspace(np.pi -np.pi/180, np.pi +np.pi/180, 5, endpoint=False)
    
    tested_angles1 = np.concatenate((angles1, angles2))
    tested_angles2 = np.concatenate((angles3, angles4))
    tested_angles = np.concatenate((tested_angles1, tested_angles2))
    h1, theta1, d1 = hough_line(edges1, theta=tested_angles1)
    h2, theta2, d2 = hough_line(edges1, theta=tested_angles2)
    
    for _, angle1, dist1 in zip(*hough_line_peaks(h1, theta1, d1)):
        (x1, y1) = dist1 * np.array([np.cos(angle1), np.sin(angle1)])
        slope1=np.tan(angle1+ np.pi/2)
    for _, angle2, dist2 in zip(*hough_line_peaks(h2, theta2, d2)):
        (x2, y2) = dist2 * np.array([np.cos(angle2), np.sin(angle2)])
        slope2=np.tan(angle2+ np.pi/2)
    x_int = (slope1*x1-slope2*x2+y2-y1)/(slope1-slope2)
    y_int = (slope1*slope2*(x1-x2)+slope1*y2-slope2*y1)/(slope1-slope2)
    
    # Generating figure 1
    fig, axes = plt.subplots(1, 3, figsize=(15, 6))
    ax = axes.ravel()
    
    ax[0].imshow(image, cmap=cm.gray)
    ax[0].set_title('Input image')
    ax[0].set_axis_off()
    
    
    ax[1].imshow(image, cmap=cm.gray)
    ax[1].set_ylim((image.shape[0], 0))
    ax[1].set_axis_off()
    ax[1].set_title('Detected lines')
    
    for _, angle, dist in zip(*hough_line_peaks(h, theta, d)):
        (x0, y0) = dist * np.array([np.cos(angle), np.sin(angle)])
        ax[1].axline((x0, y0), slope=np.tan(angle + np.pi/2))
    ax[1].scatter(x_int,y_int, color='r')
    
    ax[2].imshow(edges1, cmap='gray')
    ax[2].set_title(r'Canny filter, $\sigma=1$', fontsize=20)    
    
    plt.tight_layout()
    plt.show()
    
    # # display results
    # fig, ax = plt.subplots(nrows=1, ncols=3, figsize=(16, 6))
    
    # ax[0].imshow(image, cmap='gray')
    # ax[0].set_title('noisy image', fontsize=20)
    
    # ax[1].imshow(edges1, cmap='gray')
    # ax[1].set_title(r'Canny filter, $\sigma=1$', fontsize=20)
    
    # ax[2].imshow(edges2, cmap='gray')
    # ax[2].set_title(r'Canny filter, $\sigma=3$', fontsize=20)
    
    # for a in ax:
    #     a.axis('off')
    
    # fig.tight_layout()
    # plt.show()
            