from ScopeFoundry import Measurement
import pyqtgraph as pg
import numpy as np
from skimage.color import rgb2gray
from skimage import exposure
import time
from scipy.signal import peak_widths


class AutoFocusMeasure(Measurement):
    
    name = 'auto_focus'
    
    def setup(self):
        self.settings.New("z_range", dtype=float, unit='um', initial=20)
        self.settings.New('adjust_log_gain', dtype=float, initial=10)
        
        self.settings.New("center_x", dtype=float, unit='%', initial=50)
        self.settings.New("center_y", dtype=float, unit='%', initial=50)

        
        self.settings.New("roi_x", dtype=int, unit='px', initial=100)
        
    
    def setup_figure(self):
        
        self.ui = self.graph_layout=pg.GraphicsLayoutWidget(border=(100,100,100))
        
        # history plot
        self.plot_live = self.graph_layout.addPlot(title="Live")
        self.plot_live_imgitem = pg.ImageItem()
        self.plot_live.addItem(self.plot_live_imgitem)
        
        self.graph_layout.nextRow()
                
        self.plot_snap = self.graph_layout.addPlot(title="Snap")
        self.plot_snap_imgitem = pg.ImageItem()
        self.plot_snap.addItem(self.plot_snap_imgitem)
        
        

    def run(self):
        S = self.settings

        
        num_z = 20
        focus_knob = self.app.hardware['focus_knob']
        z0 = focus_knob.settings['target_position'] # current position of focus knob stage
        self.zs = zs = np.linspace(z0-S['z_range'], z0+S['z_range'], num_z) # list of positions to sweep
        
        self.sharps = sharps = np.zeros(num_z, dtype=float) # corresponding array of image sharpness values
        self.count = 0
        
        self.centers = np.zeros(num_z, dtype=float)
        self.widths  = np.zeros(num_z, dtype=float)
        
        for z in zs:
            ii = self.count
            # TAke a snapshot
            focus_knob.settings['target_position'] = z
            time.sleep(0.5)
            self.orig_im = self.get_rgb_image()

            # process image
            image = rgb2gray(self.orig_im)
            self.image = image #= exposure.adjust_gamma(image, S['adjust_log_gain'])
            
            
            #self.image_slice = image[max_ind-60:max_ind+60,700]
            self.image_slice = image[:,700]
            
            # plot moments from here: https://scipy-cookbook.readthedocs.io/items/FittingData.html
            X = np.arange(len(self.image_slice)) 
            data =  self.image_slice
            x = np.sum(X*data)/np.sum(data)
            
            # get width using peak_widths from scipy.signal
            max_pixel = max(data)
            max_pixel_ind = np.array(np.where(data == max_pixel))
            width = peak_widths(data, [int(max_pixel_ind[0][0])], rel_height=0.5)[0][0]

            
            self.centers[ii] = x
            self.widths[ii]  = width
            
            sharpness = 1./width
            
            sharps[ii] = sharpness
            self.count +=1
        
        # find focus knob position corresponding to maximum sharpness   
        max_value = max(sharps)
        max_index = np.array(np.where(sharps == max_value))
        z_max = zs[max_index[0][0]]

        focus_knob.settings['target_position'] = z_max # set to optimum position
            
        
        
        
        
    def get_rgb_image(self):
        cam = self.app.hardware['toupcam'].cam

        data = cam.get_image_data()
        raw = data.view(np.uint8).reshape(data.shape + (-1,))
        bgr = raw[..., :3]
        return bgr[..., ::-1]
    
    def update_display(self):
        
        self.plot_snap_imgitem.setImage(self.image)
        
        self.plot_live.clear()
        #self.plot_live.plot(self.zs, self.sharps)
        self.plot_live.plot(self.image_slice)
        x = self.centers[self.count-1]
        w = self.widths[self.count-1]/2.
        self.plot_live.plot([x,x], [0,1], pen='r')
        self.plot_live.plot([x+w,x+w], [0,1], pen='g')
        self.plot_live.plot([x-w,x-w], [0,1], pen='g')
        