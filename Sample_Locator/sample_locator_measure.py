'''
Created on Sep 27, 2021

@author: Sriram Sridhar
'''

import pyqtgraph as pg
from ScopeFoundry.helper_funcs import load_qt_ui_file, sibling_path
from uv_microscope.sample_locator import SampleLocator
from PyQt5.Qt import QFileDialog, QStringListModel
import h5py
import numpy as np

class SampleLocatorMeasure(SampleLocator):
    
    def setup(self):
        if not hasattr(self.settings, 'crosshairs'):
            self.settings.New('crosshairs', dtype=bool, initial=False)
        if not hasattr(self.settings, 'auto_level'):
            self.settings.New('auto_level', dtype=bool, initial=False)
        if not hasattr(self.settings, 'flip_x'):
            self.settings.New('flip_x', dtype=bool, initial=False)
        if not hasattr(self.settings, 'flip_y'):
            self.settings.New('flip_y', dtype=bool, initial=False)
        if not hasattr(self.settings, 'center_x'):
            self.settings.New("center_x", dtype=float, unit='%', initial=50)
        if not hasattr(self.settings, 'center_y'):
            self.settings.New("center_y", dtype=float, unit='%', initial=50)
        if not hasattr(self.settings, 'img_scale'):
            self.img_scale = self.settings.New("img_scale", dtype=float, unit='um', initial=50.)
        
        self.add_operation('save_image', self.save_image)
    
    def setup_figure(self):
        self.ui = load_qt_ui_file(sibling_path(__file__,'sample_locator_measure.ui'))
        self.hw = self.app.hardware[self.hwName]
        
        self.settings.activation.connect_to_widget(self.ui.live_checkBox)
        self.hw.settings.connected.connect_to_widget(self.ui.hw_connect_checkBox)
        self.settings.crosshairs.connect_to_widget(self.ui.crosshairs_checkBox)
        self.settings.auto_level.connect_to_widget(self.ui.auto_level_checkBox)
        
        self.ui.start_calibration_pushButton.clicked.connect(self.start_calibration)
        self.ui.complete_calibration_pushButton.clicked.connect(self.complete_calibration)
        self.ui.save_key_point_pushButton.clicked.connect(self.save_key_point)
        self.ui.image_select_pushButton.clicked.connect(self.image_select)
        self.ui.save_image_pushButton.clicked.connect(self.save_image)
        self.ui.add_chip_pushButton.clicked.connect(self.add_chip)
        self.ui.add_feature_pushButton.clicked.connect(self.add_feature)
        self.ui.locate_feature_pushButton.clicked.connect(self.locate_feature)
        
        def update_chip_name():
            self.settings['Chips'] = self.ui.chip_name_lineEdit.text()
        self.ui.chip_name_lineEdit.textChanged.connect(update_chip_name)
        def update_feature_name():
            self.settings['Features'] = self.ui.feature_name_lineEdit.text()
        self.ui.feature_name_lineEdit.textChanged.connect(update_feature_name)
        
        
        
        self.keyPointList = ['Key Point 1', 'Key Point 2', 'Key Point 3']
        self.ui.key_point_select_comboBox.addItems(self.keyPointList)
        self.key_point_number = self.keyPointList[0]
        def set_key_point_number():
            self.key_point_number = self.ui.key_point_select_comboBox.currentText()
        self.ui.key_point_select_comboBox.currentIndexChanged.connect(set_key_point_number)
        self.ui.key_point_select_comboBox.currentIndexChanged.connect(self.update_calib_status)
        
        #main camera feed
        self.graph_layout_live = pg.GraphicsLayoutWidget()
        #self.graph_layout_live.resize(200,100)
        self.plot = self.plot_live = self.graph_layout_live.addPlot()
        self.img_item_live = pg.ImageItem()
        #self.plot_live.autoRange()
        self.plot_live.addItem(self.img_item_live)
        self.plot_live.setAspectLocked(lock=True, ratio=1)
        self.ui.live_feed_groupBox.layout().addWidget(self.graph_layout_live)
        
        
        
        #image display
        self.graph_layout_image_display = pg.GraphicsLayoutWidget()
        #self.graph_layout_image_display.resize(200,100)
        self.plot_image_display = self.graph_layout_image_display.addPlot()
        self.img_item_image_display = pg.ImageItem()
        #self.plot_image_display.autoRange()
        self.plot_image_display.addItem(self.img_item_image_display)
        self.plot_image_display.setAspectLocked(lock=True, ratio=1)
        self.ui.image_display_groupBox.layout().addWidget(self.graph_layout_image_display)
        
        
        #crosshairs
        self.crosshairs_live_feed = [pg.InfiniteLine(movable=False, angle=90, pen=(255,0,0,200)),
                           pg.InfiniteLine(movable=False, angle=0, pen=(255,0,0,200))]
        self.crosshairs_image_display = [pg.InfiniteLine(movable=False, angle=90, pen=(255,0,0,200)),
                           pg.InfiniteLine(movable=False, angle=0, pen=(255,0,0,200))]
        for ch in self.crosshairs_live_feed:
            self.plot.addItem(ch)
            ch.setZValue(100)
        for ch in self.crosshairs_image_display:
            self.plot_image_display.addItem(ch)
            ch.setZValue(100)
        
        
    def image_select(self):
        dlg = QFileDialog()
        filenames = QStringListModel()
        
        if dlg.exec_():
            filenames = dlg.selectedFiles()
            if filenames[0][-3:] != '.h5':
                print('Only h5 file display is currently supported, please choose an h5 file')
                self.ui.image_select_lineEdit.setText('MUST SELECT AN H5 FILE')
                return
            self.ui.image_select_lineEdit.setText(filenames[0])
            with h5py.File(filenames[0], "r") as f:
                keysIter = iter(f.keys())
                dName = next(keysIter)
                img = np.array(f.get(dName))
            self.img_item_image_display.setImage(img, autoLevels=self.settings['auto_level'])
            def set_scale_image_display():
                S = self.settings
                scale = S['img_scale']
                im_aspect = self.img_item_image_display.height() / self.img_item_image_display.width()
                self.img_rect = pg.QtCore.QRectF(0 - S['center_x'] * scale / 100,
                                        0 - S['center_y'] * scale * im_aspect / 100,
                                        scale,
                                        scale * im_aspect)
                self.img_item_image_display.setRect(self.img_rect)
            set_scale_image_display()
    
    def save_image(self):
        im = self.get_image()
        dlg = QFileDialog()
        name = dlg.getSaveFileName(parent=None, caption='Save file', directory='',
                                        filter='h5 (*.h5);;All Files(*.*)')
        with h5py.File(name[0], "w") as f:
            dset = f.create_dataset("name", data=im)
            
    def set_scale(self):
        S = self.settings
        scale = S['img_scale']
        im_aspect = self.img_item_live.height() / self.img_item_live.width()
        self.img_rect = pg.QtCore.QRectF(0 - S['center_x'] * scale / 100,
                                0 - S['center_y'] * scale * im_aspect / 100,
                                scale,
                                scale * im_aspect)
        self.img_item_live.setRect(self.img_rect)

        for ch in self.crosshairs_live_feed:
            ch.setPos((0, 0))
            ch.setZValue({True:1, False:-1}[self.settings['crosshairs']])

        if hasattr(self, 'center_roi'):
            self.center_roi.setVisible(False)
        if hasattr(self, 'roi_label'):
            self.roi_label.setVisible(False)

    def get_image(self):
        img = self.get_rgb_image()
        if type(img) == bool:
            return False
        img = np.flip(img.swapaxes(0, 1), 0)

        if self.settings['flip_x']:
            img = img[::-1,:,:]
        if self.settings['flip_y']:
            img = img[:,::-1,:]
        return img

    def update_display(self):
        self.display_update_period = 0.01
        
        self.img = img = self.get_image()

        self.img_item_live.setImage(img, autoLevels=self.settings['auto_level'])
        self.img = img
        
        self.set_scale()

        if type(self.status) == dict:
            #self.plot.SetWordWrap(True)
            self.plot.setTitle(**self.status)
        elif type(self.status) == str:
            #self.plot.
            self.plot.setTitle(self.status)
    
    
    
    
            