from ScopeFoundry import BaseMicroscopeApp,helper_funcs
import numpy as np
from ScopeFoundryHW.zwo_camera.zwo_camera_hw import ZWOCameraHW
from ScopeFoundryHW.zwo_camera.zwo_camera_capture_measure import ZWOCameraCaptureMeasure
from qtpy.QtGui import QFont
import numpy as np


class SurveyScanApp(BaseMicroscopeApp):
    
    #xbctrl attributes
    name = 'survey_scan_pl_app'
    speedZDict = {'1.0000':1.0000, '0.1000':0.1000, '0.0100':0.0100, '0.0010':0.0010, '0.0001':0.0001}
    speedXYDict = {'3.00':3.00, '1.00':1.00, '0.50':0.50, '0.10':0.10, '0.01':0.01}

    
    
    def setup(self):
        self.qtapp.setStyle('Fusion')
        #self.qtapp.setStyleSheet('QWidget {font-size: 16;}')
        self.qtapp.setFont(QFont('Futura', 12))
        
        
        from ScopeFoundryHW.asi_stage.asi_stage_hw import ASIStageHW
        asi_stage = self.add_hardware(ASIStageHW(self, name='asi_stage', invert_x=False, invert_y = False))
        from ScopeFoundryHW.asi_stage.asi_stage_control_measure import ASIStageControlMeasure
        asi_control = self.add_measurement(ASIStageControlMeasure(self))
        
        asi_stage.settings['port'] = "/dev/tty.usbserial-AJ03ACG2"
        #asi_stage.settings['port'] = "COM4"
        
        hw = self.add_hardware(ZWOCameraHW(self))
        self.add_measurement(ZWOCameraCaptureMeasure(self))
        
        
        #self.settings_load_ini('survey_microscope_defaults.ini')
        
        
        from survey_pl_microscope.measurements.simple_tiled_image_zwo import SimpledTiledImageZWO
        self.add_measurement(SimpledTiledImageZWO(self))
        
        #from survey_pl_microscope.measurements.scicat_simple_tiled_image_zwo import ScicatSimpledTiledImageZWO
        #self.add_measurement(ScicatSimpledTiledImageZWO(self))
        
        from confocal_measure.stage_ref_pos_measure import StageReferencedPositionMeasure
        self.add_measurement(StageReferencedPositionMeasure(self))
            
            ### Q
        Q = self.add_quickbar(helper_funcs.load_qt_ui_file("survey_microscope_quickbar.ui"))
        
        self.setup_quickbar()
        
        # load default settings from file
        self.settings_load_ini("survey_microscope_defaults.ini")
        
        
    def setup_quickbar(self):
        Q = self.quickbar
        
        """
        p = Q.mcl_plotwidget
        p.showAxis('bottom', False)
        p.showAxis('left', False)
        import pyqtgraph as pg
        from qtpy import QtWidgets
        #r = QtWidgets.QGraphicsRectItem(0, 0, 0.4, 1)
        #r.setBrush(pg.mkBrush('r'))
        #p.addItem(r)
        p.plot([0,1,1,0,0],[0,0,1,1,0])
        
        self.current_stage_pos_arrow = pg.ArrowItem()
        self.current_stage_pos_arrow.setZValue(100)
        self.current_stage_pos_arrow.setScale(0.5)
        self.current_stage_pos_arrow.setBrush(pg.mkBrush('r'))
        p.addItem(self.current_stage_pos_arrow)

        mcl.settings.x_position.updated_value.connect(self.update_mcl_arrow_pos) 
        mcl.settings.y_position.updated_value.connect(self.update_mcl_arrow_pos)
        """
        
        #
        # ASI Stage
        asi_stage = self.hardware['asi_stage']
        asi_control = self.measurements['ASI_Stage_Control']
        
        asi_stage.settings.connected.connect_to_widget(Q.asi_hw_connect_checkBox)
        
        asi_stage.settings.x_position.connect_to_widget(Q.x_pos_doubleSpinBox)
        Q.x_up_pushButton.clicked.connect(asi_control.x_up)
        Q.x_down_pushButton.clicked.connect(asi_control.x_down)
  
        asi_stage.settings.y_position.connect_to_widget(Q.y_pos_doubleSpinBox)
        Q.y_up_pushButton.clicked.connect(asi_control.y_up)
        Q.y_down_pushButton.clicked.connect(asi_control.y_down)
        
        
  
        asi_stage.settings.z_position.connect_to_widget(Q.z_pos_doubleSpinBox)
        Q.z_up_pushButton.clicked.connect(asi_control.z_up)
        Q.z_down_pushButton.clicked.connect(asi_control.z_down)
         
        asi_control.settings.jog_step_xy.connect_to_widget(Q.xy_step_doubleSpinBox)
        asi_control.settings.jog_step_z.connect_to_widget(Q.z_step_doubleSpinBox)
         
        stage_steps = np.array([5e-4, 1e-3, 1e-2, 1e-1, 1e0])
        stage_steps_labels = ['0.0005 mm','0.001 mm','0.010 mm','0.100 mm','1.000 mm']
        Q.xy_step_comboBox.addItems(stage_steps_labels)
#         Q.z_step_comboBox.addItems(stage_steps_labels)
         
        def apply_xy_step_value():
            asi_control.settings.jog_step_xy.update_value(stage_steps[Q.xy_step_comboBox.currentIndex()])
        Q.xy_step_comboBox.currentIndexChanged.connect(apply_xy_step_value)
         
        def apply_z_step_value():   
            asi_control.settings.jog_step_z.update_value(stage_steps[Q.z_step_comboBox.currentIndex()])
        #Q.z_step_comboBox.currentIndexChanged.connect(apply_z_step_value)
         
        def halt_stage_motion():
            asi_stage.halt_xy()
            asi_stage.halt_z()
        Q.stop_stage_pushButton.clicked.connect(halt_stage_motion)
        Q.z_halt_pushButton.clicked.connect(halt_stage_motion)


        asi_stage.settings.x_target.connect_to_widget(Q.asi_x_target_doubleSpinBox)
        asi_stage.settings.y_target.connect_to_widget(Q.asi_y_target_doubleSpinBox)
        
        
        #### Zwo camera
        cam = self.hardware['zwo_camera']
        cam.settings.connected.connect_to_widget(Q.zwo_hw_checkBox)
        
        #cam.settings.iso.connect_to_widget(Q.zwo_iso_comboBox)
        #cam.settings.exp_time.connect_to_widget(Q.zwo_exp_comboBox)
        #cam.settings.color_temp.connect_to_widget(Q.zwo_color_temp_comboBox)
        
        Q.snap_save_pushButton.clicked.connect(
            self.measurements['zwo_camera_capture'].snap_and_save)

        

if __name__ == '__main__':
    import sys
    app = SurveyScanApp(sys.argv)
    #app.tile_layout()
    sys.exit(app.exec_())
    

        
                
    