from ScopeFoundryHW.asi_stage.asi_stage_raster import ASIStage2DScan
import numpy as np
import os
import imageio
import time


class SimpledTiledImageZWO(ASIStage2DScan):
    
    name = 'simple_tiled_image_zwo'
    
    def pre_scan_setup(self):
        cam = self.app.hardware['zwo_camera']

        #cam.camera.activate_liveview(size='medium', cameradisplay='on')
        cam.camera.start_video_capture()
    def collect_pixel(self, pixel_num, k, j, i):
        
        time.sleep(1.5)
        
        cam = self.app.hardware['zwo_camera']
        
        img = cam.camera.capture_video_frame()

        self.display_image_map[k,j,i] = img.sum()
        
        if pixel_num == 0:
            self.log.info("pixel 0: creating data arrays")
            print("pixel 0: creating data arrays")
            
            live_img_map_shape = self.scan_shape + img.shape


            self.live_img_map = np.zeros(live_img_map_shape, dtype=np.uint8)
            if self.settings['save_h5']:
                self.img_map_h5 = self.h5_meas_group.create_dataset(
                                      'img_map', shape=live_img_map_shape,
                                      chunks = (1,1,1,512,512,3),
                                       dtype=np.uint8)
            
                self.img_dir = self.h5_filename + "_images"
                os.makedirs(self.img_dir, exist_ok=True)
                
        self.live_img_map[k,j,i] = img
        if self.settings['save_h5']:
            self.img_map_h5[k,j,i,:,:,:] = img
            imageio.imsave(os.path.join(self.img_dir, f"img_{k}_{j}_{i}.jpg"), img, quality=100)
            
            print("acquiring", pixel_num, k, j, i)
            #new_files = cam.camera.capture_video_frame()
            
            # print (new_files)
            # for f in new_files:
            #     ext = f.split('.')[-1]
            #     print("downloading", f)
            #     #cam.cam.download_img_url(f,os.path.join(self.img_dir, f"full_{k}_{j}_{i}.{ext}") )
            #     print("deleting", f)
            #     #cam.cam.delete_img_url(f)