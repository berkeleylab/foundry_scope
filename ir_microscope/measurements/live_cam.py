'''
Created on Jun 10, 2021

@author: Benedikt Ursprung
'''
from ScopeFoundryHW.toupcam.toupcam_live_measure import ToupCamLiveMeasure
from confocal_measure.stage_live_cam import AutoFocusStageLiveCam
from confocal_measure.base_rotation_calibration import BaseRotationCalibration


class LiveCam(ToupCamLiveMeasure, AutoFocusStageLiveCam, BaseRotationCalibration):

    def setup(self):
        ToupCamLiveMeasure.setup(self)
        AutoFocusStageLiveCam.setup(self)
        BaseRotationCalibration.setup(self)

    def setup_figure(self):
        ToupCamLiveMeasure.setup_figure(self)
        AutoFocusStageLiveCam.setup_figure(self)

    def update_display(self):
        AutoFocusStageLiveCam.update_display(self)

    def get_current_stage_position(self):
        stageS = self.app.hardware["attocube_xyz_stage"].settings
        return stageS["x_position"] * 1e3, stageS["y_position"] * 1e3, stageS["z_position"] * 1e3

    def set_stage_position(self, x, y, z=None):
        stageS = self.app.hardware["attocube_xyz_stage"].settings
        stageS["x_target_position"] = x * 1e-3
        stageS["y_target_position"] = y * 1e-3
        if z != None:
            stageS["z_target_position"] = z * 1e-3

    def jog_rot_z(self, angle_z):
        motor = self.app.hardware["rotation_motor"]
        motor.settings["target_position"] = motor.settings["position"] + angle_z
