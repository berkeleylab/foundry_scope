from ScopeFoundry import BaseMicroscopeApp, helper_funcs
import numpy as np

class SurveyScanApp(BaseMicroscopeApp):
    
    #xbctrl attributes
    name = 'survey_scan_app'
    speedZDict = {'1.0000':1.0000, '0.1000':0.1000, '0.0100':0.0100, '0.0010':0.0010, '0.0001':0.0001}
    speedXYDict = {'3.00':3.00, '1.00':1.00, '0.50':0.50, '0.10':0.10, '0.01':0.01}

    
    #sample locator attributes
    canRotate = True
    microscope = 'survey_microscope'
    
    def setup(self):
        
        from ScopeFoundryHW.asi_stage.asi_stage_hw import ASIStageHW
        asi_stage = self.add_hardware(ASIStageHW(self, name='asi_stage', invert_x=False, invert_y = False))
        from ScopeFoundryHW.asi_stage.asi_stage_control_measure import ASIStageControlMeasure
        asi_control = self.add_measurement(ASIStageControlMeasure(self))
        asi_stage.settings['port'] = "COM1"
        
        
        from ScopeFoundryHW.dynamixel_servo import DynamixelXServosHW, DynamixelFilterWheelHW, DynamixelServoHW
        self.add_hardware(DynamixelXServosHW(self, devices=dict(rotation_motor=1,)))                                                                    
        self.add_hardware(DynamixelServoHW(self, name='rotation_motor'))        
       
        
        from ScopeFoundryHW.flircam import FlirCamHW
        self.add_hardware(FlirCamHW(self))
        
        
        from ScopeFoundryHW.xbox_controller.xbox_controller_hw import XboxControllerHW
        self.add_hardware(XboxControllerHW(self))

        from survey_microscope.measurements.xbox_controller_survey_measure import XboxControllerSurveyMeasure
        self.add_measurement(XboxControllerSurveyMeasure)
        
        from ScopeFoundryHW.mf_crucible.mf_crucible_hw import MFCrucibleHW
        self.add_hardware(MFCrucibleHW(self))

        from ScopeFoundryHW.mf_crucible.mf_crucible_controlpanel import MFCrucibleControlPanel
        self.add_measurement(MFCrucibleControlPanel(self))
        
        from survey_microscope.measurements.live_cam import LiveCam
        self.add_measurement(LiveCam)
        
        from survey_microscope.measurements.locator_live_cam import LocatorLiveCam
        self.add_measurement(LocatorLiveCam)
        
        from confocal_measure.survey_scan_map import SurveyScanMap, SurveyScanMapCalib
        self.add_measurement(SurveyScanMap(self))
        self.add_measurement(SurveyScanMapCalib(self))
        
        
        from confocal_measure.tiled_large_area_map import ASIFlircamTiledLargeAreaMapMeasure
        self.add_measurement(ASIFlircamTiledLargeAreaMapMeasure(self))
        
        from ScopeFoundryHW.canon_ccapi.canon_ccapi_camera_hw import CanonCCAPICameraHW
        self.add_hardware(CanonCCAPICameraHW(self))
        
        from ScopeFoundryHW.canon_ccapi.canon_camera_capture_measure import CanonCCAPICaptureMeasure
        self.add_measurement(CanonCCAPICaptureMeasure(self))
        
        
        from survey_microscope.measurements.simple_tiled_image import SimpledTiledImage
        M = self.add_measurement(SimpledTiledImage(self))
        M.settings.v0.change_min_max(-100,100)        

        
        # Q
        Q = self.add_quickbar(helper_funcs.load_qt_ui_file("survey_microscope_quickbar.ui"))
        
        self.setup_quickbar()
        

        self.settings_load_ini('survey_microscope_defaults.ini')
        
    def setup_quickbar(self):
        Q = self.quickbar
        
        """
        p = Q.mcl_plotwidget
        p.showAxis('bottom', False)
        p.showAxis('left', False)
        import pyqtgraph as pg
        from qtpy import QtWidgets
        #r = QtWidgets.QGraphicsRectItem(0, 0, 0.4, 1)
        #r.setBrush(pg.mkBrush('r'))
        #p.addItem(r)
        p.plot([0,1,1,0,0],[0,0,1,1,0])
        
        self.current_stage_pos_arrow = pg.ArrowItem()
        self.current_stage_pos_arrow.setZValue(100)
        self.current_stage_pos_arrow.setScale(0.5)
        self.current_stage_pos_arrow.setBrush(pg.mkBrush('r'))
        p.addItem(self.current_stage_pos_arrow)

        mcl.settings.x_position.updated_value.connect(self.update_mcl_arrow_pos) 
        mcl.settings.y_position.updated_value.connect(self.update_mcl_arrow_pos)
        """
        
        # ASI Stage
        asi_stage = self.hardware['asi_stage']
        asi_control = self.measurements['ASI_Stage_Control']
        
        asi_stage.settings.connected.connect_to_widget(Q.asi_hw_connect_checkBox)
        
        asi_stage.settings.x_position.connect_to_widget(Q.x_pos_doubleSpinBox)
        Q.x_up_pushButton.clicked.connect(asi_control.x_up)
        Q.x_down_pushButton.clicked.connect(asi_control.x_down)
  
        asi_stage.settings.y_position.connect_to_widget(Q.y_pos_doubleSpinBox)
        Q.y_up_pushButton.clicked.connect(asi_control.y_up)
        Q.y_down_pushButton.clicked.connect(asi_control.y_down)
        
        
  
        asi_stage.settings.z_position.connect_to_widget(Q.z_pos_doubleSpinBox)
        Q.z_up_pushButton.clicked.connect(asi_control.z_up)
        Q.z_down_pushButton.clicked.connect(asi_control.z_down)
         
        asi_control.settings.jog_step_xy.connect_to_widget(Q.xy_step_doubleSpinBox)
        asi_control.settings.jog_step_z.connect_to_widget(Q.z_step_doubleSpinBox)
         
        stage_steps = np.array([5e-4, 1e-3, 1e-2, 1e-1, 1e0])
        stage_steps_labels = ['0.0005 mm','0.001 mm','0.010 mm','0.100 mm','1.000 mm']
        Q.xy_step_comboBox.addItems(stage_steps_labels)
#         Q.z_step_comboBox.addItems(stage_steps_labels)
         
        def apply_xy_step_value():
            asi_control.settings.jog_step_xy.update_value(stage_steps[Q.xy_step_comboBox.currentIndex()])
        Q.xy_step_comboBox.currentIndexChanged.connect(apply_xy_step_value)
         
        def apply_z_step_value():   
            asi_control.settings.jog_step_z.update_value(stage_steps[Q.z_step_comboBox.currentIndex()])
        #Q.z_step_comboBox.currentIndexChanged.connect(apply_z_step_value)
         
        def halt_stage_motion():
            asi_stage.halt_xy()
            asi_stage.halt_z()
        Q.stop_stage_pushButton.clicked.connect(halt_stage_motion)
        Q.z_halt_pushButton.clicked.connect(halt_stage_motion)


        asi_stage.settings.x_target.connect_to_widget(Q.asi_x_target_doubleSpinBox)
        asi_stage.settings.y_target.connect_to_widget(Q.asi_y_target_doubleSpinBox)
        
        
        #### Canon camera
        cam = self.hardware['canon_camera']
        cam.settings.connected.connect_to_widget(Q.canon_hw_checkBox)
        
        cam.settings.iso.connect_to_widget(Q.canon_iso_comboBox)
        cam.settings.exp_time.connect_to_widget(Q.canon_exp_comboBox)
        cam.settings.color_temp.connect_to_widget(Q.canon_color_temp_comboBox)
        
        Q.snap_save_pushButton.clicked.connect(
            self.measurements['canon_camera_capture'].snap_and_save)


if __name__ == '__main__':
    import sys
    app = SurveyScanApp(sys.argv)
    #app.tile_layout()
    sys.exit(app.exec_())