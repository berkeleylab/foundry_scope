from ScopeFoundryHW.asi_stage.asi_stage_raster import ASIStage2DScan
import numpy as np
import os
import imageio
from ScopeFoundryHW.asi_stage.asi_stage_raster_tilt import ASIStage2DScanTilt

class SimpledTiledImage(ASIStage2DScanTilt):
    
    name = 'simple_tiled_image'
    
    def pre_scan_setup(self):
        cam = self.app.hardware['canon_camera']

        cam.cam.activate_liveview(size='medium', cameradisplay='on')
    
    def collect_pixel(self, pixel_num, k, j, i):
        
        cam = self.app.hardware['canon_camera']
        
        live_img = cam.cam.get_live_img()

        self.display_image_map[k,j,i] = live_img.sum()
        
        if pixel_num == 0:
            self.log.info("pixel 0: creating data arrays")
            print("pixel 0: creating data arrays")
            
            live_img_map_shape = self.scan_shape + live_img.shape


            self.live_img_map = np.zeros(live_img_map_shape, dtype=np.uint8)
            if self.settings['save_h5']:
                self.live_img_map_h5 = self.h5_meas_group.create_dataset(
                                      'live_img_map', shape=live_img_map_shape, dtype=np.uint8)
            
                self.img_dir = self.h5_filename + "_images"
                os.makedirs(self.img_dir, exist_ok=True)
                
        self.live_img_map[k,j,i] = live_img
        if self.settings['save_h5']:
            self.live_img_map_h5[k,j,i,:,:,:] = live_img
            imageio.imsave(os.path.join(self.img_dir, f"thumb_{k}_{j}_{i}.jpg"), live_img)
            
            print("acquiring", pixel_num, k, j, i)
            new_files = cam.cam.acquire_img_and_wait()
            
            print (new_files)
            for f in new_files:
                ext = f.split('.')[-1]
                print("downloading", f)
                cam.cam.download_img_url(f,os.path.join(self.img_dir, f"full_{k}_{j}_{i}.{ext}") )
                print("deleting", f)
                cam.cam.delete_img_url(f)