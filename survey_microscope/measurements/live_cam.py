'''
Created on Jun 14, 2021

@author: lab
'''
from ScopeFoundryHW.flircam.flircam_live_measure import FlirCamLiveMeasure
from confocal_measure.stage_live_cam import AutoFocusStageLiveCam, StageLiveCam
from confocal_measure.base_rotation_calibration import BaseRotationCalibration


class LiveCam(FlirCamLiveMeasure, StageLiveCam, BaseRotationCalibration):

    def setup(self):
        FlirCamLiveMeasure.setup(self)
        StageLiveCam.setup(self)
        BaseRotationCalibration.setup(self)

    def setup_figure(self):
        FlirCamLiveMeasure.setup_figure(self)
        StageLiveCam.setup_figure(self)

    def update_display(self):
        StageLiveCam.update_display(self)
        #FlirCamLiveMeasure.update_display(self)
        
        #self.set_scale()

        
    def get_current_stage_position(self):
        stageS = self.app.hardware["asi_stage"].settings
        return stageS["x_position"] * 1e3, stageS["y_position"] * 1e3, stageS["z_position"] * 1e3

    def set_stage_position(self, x, y, z=None):
        stageS = self.app.hardware["asi_stage"].settings
        stageS["x_target"] = x * 1e-3
        stageS["y_target"] = y * 1e-3
        if z != None:
            stageS["z_target"] = z * 1e-3

    def jog_rot_z(self, angle_z):
        motor = self.app.hardware["rotation_motor"]
        motor.settings["target_position"] = motor.settings["position"] + angle_z