'''
Created on Jul 7, 2021

@author: Sriram Sridhar
'''

from ScopeFoundryHW.flircam.flircam_live_measure import FlirCamLiveMeasure
from confocal_measure.stage_live_cam import StageLiveCam
from uv_microscope.sample_locator import SampleLocator
from confocal_measure.base_rotation_calibration import BaseRotationCalibration
from Sample_Locator.sample_locator_measure import SampleLocatorMeasure

class LocatorLiveCam(SampleLocatorMeasure, FlirCamLiveMeasure, StageLiveCam, SampleLocator, BaseRotationCalibration):
#class LocatorLiveCam(SampleLocatorMeasure, SampleLocator, BaseRotationCalibration):
    
    name = 'Sample Locator'
    hwName = 'flircam'
    
    def setup(self):
        #FlirCamLiveMeasure.setup(self)
        #StageLiveCam.setup(self)
        BaseRotationCalibration.setup(self)
        self.status = '' #since baserotationcalibration class displays a status from the start
        SampleLocator.setup(self)
        SampleLocatorMeasure.setup(self)
    
    def setup_figure(self):
        #FlirCamLiveMeasure.setup_figure(self)
        #StageLiveCam.setup_figure(self)
        SampleLocatorMeasure.setup_figure(self)
    
    def update_display(self):
        #StageLiveCam.update_display(self)
        #FlirCamLiveMeasure.update_display(self)
        SampleLocatorMeasure.update_display(self)
    
    def get_current_stage_position(self):
        stageS = self.app.hardware["asi_stage"].settings
        return stageS["x_position"] * 1e3, stageS["y_position"] * 1e3, stageS["z_position"] * 1e3

    def set_stage_position(self, x, y, z=None):
        stageS = self.app.hardware["asi_stage"].settings
        stageS["x_target"] = x * 1e-3
        stageS["y_target"] = y * 1e-3
        if z != None:
            stageS["z_target"] = z * 1e-3
    
    def jog_rot_z(self, angle_z):
        motor = self.app.hardware["rotation_motor"]
        motor.settings["target_position"] = motor.settings["position"] + angle_z

