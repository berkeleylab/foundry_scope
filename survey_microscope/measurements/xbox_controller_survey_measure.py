'''
Created on Aug 23, 2021

@author: Sriram Sridhar
'''
from xfer_station.xbox_controller_xfer_measure import XboxControllerXferMeasure
import pygame

class XboxControllerSurveyMeasure(XboxControllerXferMeasure):
    
    hwNames = ['asi_stage']
    measureNames = ['stage']
    invertX = 1
    invertY = -1
    invertZ = -1
    
    
    def run(self):
        """This function is run after having clicked "start" in the ScopeFoundry GUI.
        It essentially runs and listens for Pygame event signals and updates the status
        of every button in a specific category (such as hats, sticks, or buttons) in
        intervals of self.dt seconds."""
        self.log.debug("run")
        
        #Access equipment class:
        self.controller.connect()
        self.xb_dev = self.controller.xb_dev 
        self.joystick = self.xb_dev.joystick
        
        self.log.debug("ran")
        
        self.controller.settings['Controller_Name'] = self.joystick.get_name()
        
        #self.dt = 0.001
        
        #t0 = time.time()
        
        #print('LOCATOR RUN CHECK')
        
        #self.app.hardware['asi_stage'].other_observer = True
        for x in self.hwNames:
            self.app.hardware[x].other_observer = True

        try:
            while not self.interrupt_measurement_called:  
                #time.sleep(self.dt)
                event_list = pygame.event.get()
                for event in event_list:
                    if event.type == pygame.JOYAXISMOTION:
                        for i in range(self.xb_dev.num_axes):
                            self.controller.settings['Axis_' + str(i)] = self.joystick.get_axis(i)
     
                    elif event.type == pygame.JOYHATMOTION:
                        for i in range(self.xb_dev.num_hats):
                            # Clear Directional Pad values
                            for k in set(self.direction_map.values()):
                                self.controller.settings[k] = False
     
                            # Check button status and record it
                            resp = self.joystick.get_hat(i)
                            try:
                                self.controller.settings[self.direction_map[resp]] = True
                            except KeyError:
                                self.log.error("Unknown dpad hat: "+ repr(resp))
     
                    elif event.type in [pygame.JOYBUTTONDOWN, pygame.JOYBUTTONUP]:
                        button_state = (event.type == pygame.JOYBUTTONDOWN)
     
                        for i in range(self.xb_dev.num_buttons):
                            if self.joystick.get_button(i) == button_state:
                                try:
                                    self.controller.settings[self.button_map[i]] = button_state
                                except KeyError:
                                    self.log.error("Unknown button: %i (target state: %s)" % (i,
                                        'down' if button_state else 'up'))
    
                    else:
                        self.log.error("Unknown event type: {} {}".format(event, event.type))
                
                self.move_continuous_x()
                self.move_continuous_y()
                self.move_continuous_z()
                #self.toggle_stage()
                #self.switch_focus()
                #self.change_speed_bottom()
                
                #t1 = time.time()
                #print("dt actual", t1-t0)
                #t0 = t1
        finally:
            #self.app.hardware['asi_stage'].other_observer = False
            for x in self.hwNames:
                self.app.hardware[x].other_observer = False
