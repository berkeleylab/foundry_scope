from ScopeFoundry import Measurement
from ScopeFoundry.h5_io import h5_measurement_file, \
    create_extendable_h5_dataset, extend_h5_dataset_along_axis
import ctypes
import numpy as np
from  .multiharp_hw import MultiHarpHW
import time

class MultiHarpT2Measure(Measurement):

    name = 'multiharp_T2'

    def setup(self):
        self.settings.New("Tacq", dtype=float, unit='s', si=True, initial=1.0)

    def run(self):
        MH: MultiHarpHW = self.app.hardware.multiharp

        # make sure in T2 mode

        # setup h5 file
        self.h5_M = h5_measurement_file(self)
        self.records_h5 = create_extendable_h5_dataset(self.h5_M, 
                                                       name='records',
                                                       shape=(1,),
                                                       axis=0,
                                                       dtype=np.uint32)
        self.num_records = 0

        try:
            # start measurement
            tacq_sec = self.settings['Tacq']
            tacq_ms = int(1000*tacq_sec)
            MH.dev.StartMeas(tacq=tacq_ms)
            time.sleep(0.100)
            # loop

            while True:
                # Exit conditions
                if MH.dev.GetFlag_FIFOFULL():
                    raise IOError("MultiHarp FiFo Overrun!")                    
                if MH.dev.check_done_scanning():
                    break
                if self.interrupt_measurement_called:
                    break

                # Read fifo
                new_records = MH.dev.ReadFiFo_to_numpy()
                
                # Save to H5
                N = self.num_records
                M = len(new_records)
                print(N,M)
                extend_h5_dataset_along_axis(self.records_h5, N+M, axis=0)
                self.records_h5[N:N+M] = new_records
                self.num_records = N + M
        finally:
            MH.dev.StopMeas()
            self.h5_M.file.close()


