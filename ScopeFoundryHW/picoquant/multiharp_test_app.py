'''
Created on Apr 23, 2024

@author: Edward Barnard
'''

from ScopeFoundry.base_app import BaseMicroscopeApp
from ScopeFoundryHW.picoquant.multiharp_hw import MultiHarpHW
from ScopeFoundryHW.picoquant.multiharp_optimizer import MultiHarpOptimizerMeasure
from ScopeFoundryHW.picoquant.multiharp_t2_measure import MultiHarpT2Measure
#from ScopeFoundryHW.picoquant.hydraharp_hist_measure import HydraHarpHistogramMeasure
#from ScopeFoundryHW.picoquant.trpl_2d_scan_base import TRPL2DScan

class MultiHarpTestApp(BaseMicroscopeApp):
    
    name = 'multiharp_test_app'
    
    def setup(self):
        
        self.add_hardware(MultiHarpHW(self))
        self.add_measurement(MultiHarpOptimizerMeasure(self))
        self.add_measurement(MultiHarpT2Measure(self))
#        self.add_measurement(HydraHarpHistogramMeasure(self))
        #self.add_measurement(TRPL2DScan(self))


app = MultiHarpTestApp()
app.exec_()