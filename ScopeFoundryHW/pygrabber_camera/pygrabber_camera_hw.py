from ScopeFoundry import HardwareComponent
import time

class PyGrabberCameraHW(HardwareComponent):
    '''
    Requires pygrabber - https://github.com/bunkahle/pygrabber
    
    2022: Updated to use 
    
    https://github.com/andreaschiavinato/python_grabber
    
    (pip install pygrabber
    '''
    name = 'pygrabber_camera'
    
    def setup(self):
        
        self.settings.New('cam_id', dtype=int, initial=0, choices=(0,))
        self.settings.New('cam_name', dtype=str, ro=True)
        self.settings.New('format', dtype=int, choices=(0,))
    
        self.add_operation('Refresh_Cameras', self.refresh_camera_list)
    
    def connect(self):
        S =self.settings
        from pygrabber.dshow_graph import FilterGraph

        self.graph = FilterGraph()
        
        self.graph.add_video_input_device(S['cam_id'])

        S['cam_name'] = self.graph.get_input_devices()[S['cam_id']]
        raw_formats = self.graph.get_input_device().get_formats()
        print('raw_formats', raw_formats)
        #formats = [("{}: {} {}x{} {}bit".format(*x), x[0])
        #            for x in raw_formats]
        formats = []
        for x in raw_formats:
            print(x)
            desc = "{index}: {width}x{height} {media_type_str}".format(**x)
            formats.append( (desc, x['index']) )
        
        S.format.change_choice_list(formats)
        self.graph.get_input_device().set_format(S['format'])
        
        self.img_buffer = []

        self.graph.add_sample_grabber(self.image_callback)
        self.graph.add_null_render()
        self.graph.prepare_preview_graph()
        self.graph.run()

    def threaded_update(self):
        self.graph.grab_frame()
        time.sleep(0.1)
        
    def disconnect(self):
        
        self.settings.disconnect_all_from_hardware()
        
        if hasattr(self, 'graph'):
            self.graph.stop()
            del self.graph
            
    def refresh_camera_list(self):
        from pygrabber.dshow_graph import FilterGraph

        graph = FilterGraph()
            
        cams = graph.get_input_devices()
        camera_choices = [("{}: {}".format(cam_id, cam_name), cam_id) 
                          for cam_id, cam_name in enumerate(cams)]
        self.settings.cam_id.change_choice_list(camera_choices)
        
        del graph

    def image_callback(self, img):
        self.img_buffer.append(img)
        if len(self.img_buffer) > 20:
            self.img_buffer = self.img_buffer[-20:]
