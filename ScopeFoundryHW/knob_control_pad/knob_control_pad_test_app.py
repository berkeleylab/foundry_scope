from ScopeFoundry import BaseMicroscopeApp
from ScopeFoundryHW.knob_control_pad import KnobControlPadHW

class KnobControlPadTestApp(BaseMicroscopeApp):
    
    name = 'knob_test_app'
    
    def setup(self):
        self.hw = hw = self.add_hardware(KnobControlPadHW(self))
        
        #self.add_measurement(FlirCamLiveMeasure(self))
        
        hw.settings.delta1.add_listener(self.on_delta1)
        
    def on_delta1(self):
        print("on_delta1")
        self.settings['sample'] = str(self.hw.settings['delta1'])
                
if __name__ == '__main__':
    import sys
    app = KnobControlPadTestApp(sys.argv)
    app.exec_()