import hid # https://github.com/trezor/cython-hidapi
import time

for device in hid.enumerate():
    print(f"0x{device['vendor_id']:04x}:0x{device['product_id']:04x} {device['product_string']}")

        # Reuse this bytearray to send mouse reports.
        # Typically controllers start numbering buttons at 1 rather than 0.
        # report[0] buttons 1-8 (LSB is button 1)
        # report[1] buttons 9-16
        # report[2] joystick 0 x: -127 to 127
        # report[3] joystick 0 y: -127 to 127
        # report[4] joystick 1 x: -127 to 127
        # report[5] joystick 1 y: -127 to 127


gamepad = hid.device()
gamepad.open(0x239a,0x80f4)
gamepad.set_nonblocking(True)
print("serial_number_string", gamepad.get_serial_number_string())

from struct import pack, unpack
#signed = unpack('l', pack('uu', lv & 0xffffffff))[0]


while True:
    report = gamepad.read(64)
    # report returns a list of unsigned byte ints -- need to convert them later (see unpack)
    
    if report:
        print(time.time(), report)
    #time.sleep(0.5)
    
        # Full joystick report
        #example [4, 0, 0, 5, 0, 0, 0]
        # REPORT_ID == 4 is first byte
        
        if report[0] == 4 and len(report) == 7:
            x = unpack('cccbbbb', bytes(report))
            print(x)
        
        
        # sometimes reports come back like [3, 234, 0]