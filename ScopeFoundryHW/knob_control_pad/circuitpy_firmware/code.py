import board
import digitalio
import time
import pwmio
from adafruit_debouncer import Debouncer

led = digitalio.DigitalInOut(board.LED)
led.direction = digitalio.Direction.OUTPUT

switch1pin = digitalio.DigitalInOut(board.GP17)
switch1pin.direction = digitalio.Direction.INPUT
switch1pin.pull = digitalio.Pull.UP

switch2pin = digitalio.DigitalInOut(board.GP21)
switch2pin.direction = digitalio.Direction.INPUT
switch2pin.pull = digitalio.Pull.UP


switch1 = Debouncer(switch1pin)
switch2 = Debouncer(switch2pin)


led1 = pwmio.PWMOut(board.GP16, frequency=5000, duty_cycle=0)

led2 = pwmio.PWMOut(board.GP20, frequency=5000, duty_cycle=0)


while False:
    #print("Hello, CircuitPython!2")
    led.value = True
    time.sleep(0.1)
    led.value = False
    time.sleep(0.1)


import rotaryio

encoder = rotaryio.IncrementalEncoder(board.GP27, board.GP26)
last_position = 0

import usb_hid

from adafruit_hid import find_device
print(find_device(usb_hid.devices, usage_page=0x1, usage=0x05))


from adafruit_hid.consumer_control import ConsumerControl
from adafruit_hid.consumer_control_code import ConsumerControlCode


cc = ConsumerControl(usb_hid.devices)


from hid_gamepad import Gamepad

gp = Gamepad(usb_hid.devices)
print(gp)



# Raise volume.
#cc.send(ConsumerControlCode.VOLUME_INCREMENT)


while True:
    position = encoder.position



    led.value = (position % 10 > 5)

    dc = int(position * 65535 / 60) % 65535
    led1.duty_cycle = dc
    led2.duty_cycle = dc

    if  position != last_position:
        delta = position - last_position
        print("%+i -- delta %i" % (position, delta))
        gp.move_joysticks(x=delta)
#        if delta > 10:
#            cc.send(ConsumerControlCode.VOLUME_INCREMENT)
#        else:
#            cc.send(ConsumerControlCode.VOLUME_DECREMENT)
    last_position = position

    switch1.update()
    if switch1.fell:
        print('Just pressed')
        cc.send(ConsumerControlCode.VOLUME_INCREMENT)
    if switch1.rose:
        print('Just released')
        gp.click_buttons(1)
    if switch1.value:
        #print('not pressed')
        pass
    else:
        led1.duty_cycle = 65535
        #print('pressed')

    switch2.update()
    if switch2.fell:
        print('Just pressed')
        cc.send(ConsumerControlCode.VOLUME_DECREMENT)

    if not switch2.value:
        led2.duty_cycle = 65535


