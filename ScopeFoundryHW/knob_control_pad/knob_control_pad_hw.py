from ScopeFoundry import HardwareComponent
import time
from struct import unpack

class KnobControlPadHW(HardwareComponent):
    
    name = 'knob_control_pad'
    
    def setup(self):
        
        self.settings.New('encoder1', dtype=int, ro=True)
        self.settings.New('delta1', dtype=int, ro=True)
        
        
    def connect(self):
        import hid # https://github.com/trezor/cython-hidapi

        
        self.dev = dev = hid.device()
        dev.open(0x239a,0x80f4)
        dev.set_nonblocking(True)

        pass
    
    def disconnect(self):
        
        self.settings.disconnect_all_from_hardware()
        
        if hasattr(self, 'dev'):
            self.dev.close()
            
    def threaded_update(self):
        
        data = self.dev.read(64)
        # report returns a list of unsigned byte ints -- need to convert them later (see unpack)
        
        if data:
            report_id = data[0]
            report = data[1:]

            print(time.time(), report)
            #time.sleep(0.5)
        
            # Full joystick data
            #example [4, 0, 0, 5, 0, 0, 0]
            # REPORT_ID == 4 is first byte
            # Typically controllers start numbering buttons at 1 rather than 0.
            # report[0] buttons 1-8 (LSB is button 1)
            # report[1] buttons 9-16
            # report[2] joystick 0 x: -127 to 127
            # report[3] joystick 0 y: -127 to 127
            # report[4] joystick 1 x: -127 to 127
            # report[5] joystick 1 y: -127 to 127            
            
            if report_id == 4 and len(report) == 6:
                x = unpack('ccbbbb', bytes(report))
                print(x)
                self.settings['delta1'] = x[2]
                self.settings['encoder1'] += x[2]
            
            # sometimes reports come back like [3, 234, 0]        