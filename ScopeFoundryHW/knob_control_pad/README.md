ScopeFoundryHW.knob_control_pad
=====================

A custom knob control pad for interacting with ScopeFoundry.

A raspberry pi pico (RPi2040) based control pad with rotary encoders and buttons
running CircuitPy 7 firmware

https://learn.adafruit.com/welcome-to-circuitpython/installing-circuitpython


Firmware is based on the adafruit_hid.gamepad example
(https://circuitpython.readthedocs.io/projects/hid/en/3.1.3/_modules/adafruit_hid/gamepad.html)

https://learn.adafruit.com/customizing-usb-devices-in-circuitpython/hid-devices



ScopeFoundry is a Python platform for controlling custom laboratory 
experiments and visualizing scientific data

<http://www.scopefoundry.org>


Authors
----------

Edward S. Barnard <esbarnard@lbl.gov>


Requirements
------------

	* ScopeFoundry
	* hidapi (https://github.com/trezor/cython-hidapi)
