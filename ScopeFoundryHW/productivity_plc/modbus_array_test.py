
from pyModbusTCP.client import ModbusClient
from pyModbusTCP import utils



def read_float32_array(addr, length):
    """
    Address is the modbus address
    length is the number of floats to read
    each float32 is stored in sequential 
    16-bit modbus registers
    Default for Productivity series PLCs is little endian
    Modbus reads are limited to 125 registers
    so we read the array in 64 register chunks
    """
    n_reg = length*2
    
    full_reads = n_reg//64
    n_reg_partial = n_reg%64
    
    out = []
    for i in range(full_reads):
        regs = c.read_holding_registers(addr+i*64, 64)
        longs = utils.word_list_to_long(regs, big_endian=False)
        floats = [ utils.decode_ieee(l) for l in longs]
        out += floats
    
    if n_reg_partial >0:
        regs = c.read_holding_registers(addr+full_reads*64, n_reg_partial)
        longs = utils.word_list_to_long(regs, big_endian=False)
        floats = [ utils.decode_ieee(l) for l in longs]
        out += floats
    
    return out    

def write_float32_array(addr, x):
    """
    Address is the modbus address
    x is a list or array of floats
    each float32 is stored in sequential 
    16-bit modbus registers
    Default for Productivity series PLCs is little endian
    Modbus writes are limited to 123 registers
    so we write the array in 64 register chunks
    """

    length = len(x)
    n_reg = length*2

    full_writes = n_reg//64
    n_reg_partial = n_reg%64

    for i in range(full_writes):
        floats = x[i*32:(i+1)*32]
        longs = [utils.encode_ieee(f) for f in floats]
        regs = utils.long_list_to_word(longs, big_endian=False)
        c.write_multiple_registers(addr+i*64, regs)
    if n_reg_partial >0:
        floats = x[full_writes*32:]
        longs = [utils.encode_ieee(f) for f in floats]
        regs = utils.long_list_to_word(longs, big_endian=False)
        c.write_multiple_registers(addr+i*64, regs)


DataXfer_state_reg = 8192
DataXfer_f32_array_reg = 0

def read_heater_temp_history(n=0):
    c.write_single_register(DataXfer_state_reg, n+1)
    f = read_float32_array(DataXfer_f32_array_reg, 4096)
    c.write_single_register(DataXfer_state_reg, 0) # Idles
    return f

def read_heater_output_history(n=0):
    c.write_single_register(DataXfer_state_reg, n+11)
    f = read_float32_array(DataXfer_f32_array_reg, 4096)
    c.write_single_register(DataXfer_state_reg, 0) # Idles
    return f

def write_heater_program(n=0, program):
    prog_len = len(program)
    c.write_single_register(heater_prog_len_reg[n], prog_len)
    c.write_single_register(DataXfer_state_reg, -1) # clear buffer
    c.write_single_register(DataXfer_state_reg, 0) # idle buffer
    time.sleep(0.005)
    write_float32_array(DataXfer_f32_array_reg, program)
    c.write_single_register(DataXfer_state_reg, n + 21) # copy to Heater program
    time.sleep(0.005)
    c.write_single_register(DataXfer_state_reg, 0) # idle buffer
    


try:
    c = ModbusClient(host="192.168.1.22", port=502, debug=False)
    c.open()
    
    regs = c.read_holding_registers(0, 64)
    print(regs)
    longs = utils.word_list_to_long(regs, big_endian=False)
    print(longs)
    floats = [ utils.decode_ieee(l) for l in longs]
    print(floats)
    
    n_reg = 514
    
    print(n_reg/64, n_reg//64, n_reg%64 )
    
    print("="*80)
    f = read_float32_array(0, 4096)
    print(len(f), f[:10], f[-10:])
    
    write_float32_array(0, range(4096))
    
finally:
    c.close()
    