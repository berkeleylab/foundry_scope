    
from pyModbusTCP.client import ModbusClient
from pyModbusTCP import utils
import pandas
import re
import json
import threading
import struct

class ProductivityPLCModbus(object):
    
    
    def __init__(self, ip_address, tags_csv_filename, override_json=None, skip_array_elements=True, connect=True):
        
        self.ip_address = ip_address
        
        self.create_tagdb(tags_csv_filename, override_json, skip_array_elements=skip_array_elements)
        
        if connect:
            self.connect()
            
        self.lock = threading.Lock()
        
    
    def connect(self, ip_address=None, debug=False):
        if not (ip_address is None):
            self.ip_address = ip_address
        self.c = c = ModbusClient(host=ip_address, port=502, debug=debug, timeout=1.0)
        c.open()

    def disconnect(self):
        if hasattr(self, 'c'):
            self.c.close()
            del self.c
    
    def create_tagdb(self, tags_csv_filename, override_json=None, skip_array_elements=True):
        self.tags_csv_filename = tags_csv_filename
        # Parse tag database
        self.tags_df = df = pandas.read_csv(self.tags_csv_filename, engine='python', 
                                       skiprows = [1],  header=0, index_col=False)
        
        self.modbus_tags_df = df[df['MODBUS Start Address'].notnull()]
        
        self.tag_db = dict()
        
        print(self.tags_df.columns)
        
        clean = lambda varStr: re.sub('\W|^(?=\d)','_', varStr) # convert tag names to valid python variable names
        
        for i, x in self.modbus_tags_df.iterrows():
            tag_info = dict()
            
            tag_name = x['Tag Name']
            tag_info['Array']=False
            tag_info['ArrayElement'] = False
            
            # check for array
            if "()" in tag_name:
                # This is the start of the array 
                tag_info['Array'] = True
            elif y :=re.findall("\(.*?\)", tag_name):
                #sprint (y)
                # this an element of array ie array(234)
                tag_info['ArrayElement'] = True
                if skip_array_elements:
                    continue
                
            tag_info['name']  = clean( tag_name )
            #tag_info['orig_name'] = tag_name
            tag_info['modbus_start'] = int( x['MODBUS Start Address'] )
            tag_info['modbus_stop'] = int( x['MODBUS End Address'] )
            tag_info['initial_value'] = x['Initial Value']
            tag_info.update(x)
            
            tag_info['dtype'] = 'int'
            tag_info['unit'] = None
            tag_info['include'] = True
            
            tag_info['mb0'] = mb0 = tag_info['modbus_start'] - 1     # Productivity PLC does 1-indexing, but pyModbusTCP uses 0-index       
            tag_info['mb1'] = mb1 = tag_info['modbus_stop']  
            tag_info['nreg'] = mb1-mb0
            
            tag_info['read_only'] = False
            
            if 0 <= mb0 < 100000:
                #val = c.read_coils(mb0)[0]
                # 
                tag_info['type'] = 'coil'
                tag_info['read_only'] = False
                tag_info['dtype'] = bool
                tag_info['m0'] = mb0 # store the addresses without the shifts
                tag_info['m1'] = mb1 # store the addresses without the shifts

            elif 100000 <= mb0 < 200000:
                tag_info['type'] = 'discrete_input'
                tag_info['read_only'] = True
                tag_info['dtype'] = bool
                #val = c.read_discrete_inputs(mb0-100000)[0]
                tag_info['m0'] = mb0-100000 # store the addresses without the shifts
                tag_info['m1'] = mb1-100000 # store the addresses without the shifts

            elif 300000 <= mb0 < 400000:
                tag_info['type'] = 'input_register'
                tag_info['read_only'] = True
                tag_info['m0'] = mb0-300000 # store the addresses without the shifts
                tag_info['m1'] = mb1-300000 # store the addresses without the shifts

            elif 400000 <= mb0 < 500000:
                tag_info['type'] = 'holding_register'
                tag_info['read_only'] = False
                tag_info['m0'] = mb0-400000 # store the addresses without the shifts
                tag_info['m1'] = mb1-400000 # store the addresses without the shifts

            
            if 'F32' in tag_info['Data Type']:# in ('AIF32', 'AR1F32', 'F32'
                tag_info["dtype"] = 'float'
                
            self.tag_db[tag_info['name']] = tag_info
        
        
            #self.tag_db_no_arrayelements = {k:v for k,v in self.tag_db.items() if v['ArrayELement'] == False}

        if override_json:
            with open(override_json, 'r') as f:
                self.overrides = json.load(f)
            for tag_name, ov in self.overrides.items():
                tag_info = self.tag_db[tag_name]
                tag_info.update(ov)
                
                
                
    def read_single(self, name):
        #print("read_single",name)
        with self.lock:
            t = self.tag_db[name]
            data_type = t['Data Type']
            
            if t['type'] == 'coil':
                resp = self.c.read_coils(t['m0'], bit_nb=1)[0]
                return bool(resp)
            elif t['type'] == 'discrete_input':
                resp = self.c.read_discrete_inputs(t['m0'], bit_nb=1)[0]
                return bool(resp)
            elif t['type'] == 'input_register':
                resp = self.c.read_input_registers(t['m0'], t['nreg'])
            elif t['type'] == 'holding_register':
                resp = self.c.read_holding_registers(t['m0'], t['nreg'])
            else:
                raise ValueError(f"Unknown tag type ({t['type']}) for {name}")
            
            # Handle different data types for input and holding registers
            if '16' in data_type:
                if data_type in ('S16', 'AR1S16', 'AR2S16'):
                    val = struct.unpack('h', struct.pack('H', resp[0]))[0]
                    return val
                elif data_type in ('US16', 'AR1US16', 'AR2US16'):
                    val = resp[0]
                    #print("read_single", name, data_type, val)
                    return val
                else:
                    raise ValueError(f"unknown data type {data_type} for tag {name}")
            elif '32' in data_type:
                longs = utils.word_list_to_long(resp, big_endian=False)
                if data_type in ('S32', 'AR1S32', 'AR2S32', 'AIS32', 'AOS32'):
                    val = struct.unpack('l', struct.pack('L', longs[0]))[0]
                    #print("read_single", name, data_type, val)
                    return val
                elif data_type in ('US32'):
                    val = longs[0]
                    #print("read_single", name, data_type, val)
                    return val                    
                elif 'F32' in data_type:
                    floats = [ utils.decode_ieee(l) for l in longs]
                    #print(floats[0])
                    val = floats[0]
                    #print("read_single", name, data_type, val)
                    return val                    
                else:
                    raise ValueError(f"unknown data type {data_type} for tag {name}")
            
    def write_single(self, name, val):
        with self.lock:
            t = self.tag_db[name]
            data_type = t['Data Type']

            if data_type == 'C':
                self.c.write_single_coil(t['m0'], val)
            elif '16' in data_type:
                if data_type in ('S16', 'AR1S16', 'AR2S16'):
                    v = struct.unpack('H', struct.pack('h', val))[0]
                elif data_type in ('US16', 'AR1US16', 'AR2US16'):
                    v = val
                else:
                    raise ValueError(f"unknown data type {data_type} for tag {name}")
                self.c.write_single_register(t['m0'], v)
            elif '32' in data_type:
                # will write two 16-bit modbus with little-endian 
                # v will be list of len=2
                if data_type in ('S32', 'AR1S32', 'AR2S32', 'AIS32', 'AOS32'):
                    v = struct.unpack('L', struct.pack('l', val))[0]
                    v = utils.long_list_to_word([v,], big_endian=False)
                    # v = struct.unpack('HH', struct.pack('l', val))
                elif data_type in ('US32'):
                    v = utils.long_list_to_word([v,], big_endian=False)
                elif 'F32' in data_type:
                    v = utils.encode_ieee(val)
                    v = utils.long_list_to_word([v,], big_endian=False)
                else:
                    raise ValueError(f"unknown data type {data_type} for tag {name}")
                self.c.write_multiple_registers(t['m0'], v)
            else:
                raise ValueError(f"unknown data type {data_type} for tag {name}")

        
    def read_float32_array(self, addr, length):
        """
        Address is the modbus address
        length is the number of floats to read
        each float32 is stored in sequential 
        16-bit modbus registers
        Default for Productivity series PLCs is little endian
        Modbus reads are limited to 125 registers
        so we read the array in 64 register chunks
        """
        
        c = self.c
        n_reg = length*2
        
        N = 124 # number of registers to read for each Modbus read
                # This should be even, to align to the 2-register floats
        
        full_reads = n_reg//124
        n_reg_partial = n_reg%124
        
        print("full_reads",full_reads)
        out = []
        for i in range(full_reads):
            with self.lock:
                regs = c.read_holding_registers(addr+i*N, N)
            longs = utils.word_list_to_long(regs, big_endian=False)
            floats = [ utils.decode_ieee(l) for l in longs]
            out += floats
        
        if n_reg_partial >0:
            with self.lock:
                regs = c.read_holding_registers(addr+full_reads*N, n_reg_partial)
            longs = utils.word_list_to_long(regs, big_endian=False)
            floats = [ utils.decode_ieee(l) for l in longs]
            out += floats
        
        return out    
    
    def write_float32_array(self, addr, x):
        """
        Address is the modbus address
        x is a list or array of floats
        each float32 is stored in sequential 
        16-bit modbus registers
        Default for Productivity series PLCs is little endian
        Modbus writes are limited to 123 registers
        so we write the array in 64 register chunks
        """
        with self.lock:
            c = self.c
    
            length = len(x)
            n_reg = length*2
        
            full_writes = n_reg//64
            n_reg_partial = n_reg%64
        
            for i in range(full_writes):
                floats = x[i*32:(i+1)*32]
                longs = [utils.encode_ieee(f) for f in floats]
                regs = utils.long_list_to_word(longs, big_endian=False)
                c.write_multiple_registers(addr+i*64, regs)
            if n_reg_partial >0:
                floats = x[full_writes*32:]
                longs = [utils.encode_ieee(f) for f in floats]
                regs = utils.long_list_to_word(longs, big_endian=False)
                c.write_multiple_registers(addr+i*64, regs)
            
### Test
#
# plc = ProductivityPLCModbus(ip_address='192.168.1.22',
#                             tags_csv_filename=r'C:\Users\esbarnard\Documents\foundry_scope\SpinBot\tec_8way_structured_extended.csv', 
#                             override_json=r'C:\Users\esbarnard\Documents\foundry_scope\SpinBot\plc_tag_overrides.json',
#                             connect=False)
# from pprint import pprint
# pprint(set(plc.tags_df['Data Type']))
