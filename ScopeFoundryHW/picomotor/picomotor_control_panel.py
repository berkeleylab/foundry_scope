from ScopeFoundry.measurement import Measurement
from ScopeFoundry.helper_funcs import load_qt_ui_file, sibling_path
from qtpy import QtWidgets

class PicoMotorControlPanel(Measurement):
    
    name = 'picomotor_control_panel'
    
    def __init__(self, app, hw=['picomotor'], name=None):
        self.hw_list = hw
        Measurement.__init__(self, app, name=name)
    
    def setup_figure(self):
        
        self.ui = QtWidgets.QWidget()
        self.layout = QtWidgets.QGridLayout()
        self.ui.setLayout(self.layout)
        
        
        for hw_i, hw_name in enumerate(self.hw_list):
            hw = self.app.hardware[hw_name]
            
            for axis_i in [1,2,3,4]:
                
                ax_ui = load_qt_ui_file(sibling_path(__file__, 'axis_control.ui'))
                self.layout.addWidget(ax_ui, hw_i, axis_i-1)
                
                ax_ui.axis_groupBox.setTitle(f"{hw_name}/axis_{axis_i}")
                
                hw.settings.connected.connect_to_widget(
                    ax_ui.hw_connect_checkBox)

                ax_ui.zero_pushButton.setEnabled(False)
                
                hw.settings.get_lq(f'axis_{axis_i}_target').connect_to_widget(
                    ax_ui.target_doubleSpinBox)
                hw.settings.get_lq(f'axis_{axis_i}_position').connect_to_widget(
                    ax_ui.pos_doubleSpinBox)
                hw.settings.get_lq(f'axis_{axis_i}_velocity').connect_to_widget(
                    ax_ui.vel_doubleSpinBox)
                
                ax_ui.left_pushButton.clicked.connect(
                    lambda x, hw_name=hw_name, axis_i=axis_i, direction=-1: \
                        self.jog_axis(hw_name, axis_i, direction))
                ax_ui.right_pushButton.clicked.connect(
                    lambda x, hw_name=hw_name, axis_i=axis_i, direction=+1: \
                        self.jog_axis(hw_name, axis_i, direction))
        
    def jog_axis(self, hw_name, axis_i, direction):
        print("jog_axis:", hw_name, axis_i, direction)
            
            
                #hw.settings
#         
#         self.stage.settings.connected.connect_to_widget(
#             self.ui.asi_stage_connect_checkBox)
#         
#         self.stage.settings.x_position.connect_to_widget(
#             self.ui.x_pos_doubleSpinBox)
#         self.stage.settings.y_position.connect_to_widget(
#             self.ui.y_pos_doubleSpinBox)
# 
#         self.stage.settings.z_position.connect_to_widget(
#             self.ui.z_pos_doubleSpinBox)
# 
#         
#         self.settings.jog_step_xy.connect_to_widget(
#             self.ui.xy_step_doubleSpinBox)
#         
#         self.settings.jog_step_z.connect_to_widget(
#             self.ui.z_step_doubleSpinBox)
