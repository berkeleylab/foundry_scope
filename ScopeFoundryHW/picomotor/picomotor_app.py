'''
Created on Jun 29, 2017

'''

from ScopeFoundry.base_app import BaseMicroscopeApp
import logging

logging.basicConfig(level=logging.DEBUG)

class PicomotorApp(BaseMicroscopeApp):
    
    name = 'picomotor_app'
    
    def setup(self):
        
        from ScopeFoundryHW.picomotor.picomotor_hw import PicomotorHW
        self.add_hardware(PicomotorHW(self))
        
        from ScopeFoundryHW.picomotor.picomotor_control_panel import PicoMotorControlPanel
        self.add_measurement(PicoMotorControlPanel(self, hw=['picomotor', 'picomotor']))

if __name__ == '__main__':
    import sys
    app = PicomotorApp(sys.argv)
    sys.exit(app.exec_())