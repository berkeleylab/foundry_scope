'''
Updated 2020-07-06 Edward Barnard

Original Created on Jun 28, 2017 @author: Alan Buckley
'''
from ScopeFoundry import HardwareComponent
import time

import logging

logger = logging.getLogger(__name__)

try:
    from ScopeFoundryHW.picomotor.picomotor_dev import PicomotorDev
except Exception as err:
    logger.error("Cannot load required modules for PicomotorDev, {}".format(err))
    
class PicomotorHW(HardwareComponent):
    
    name = "picomotor"
    
    def setup(self):
        self.port = self.settings.New(name="port", initial="socket://192.168.1.23:23", dtype=str, ro=False)
        #self.ip_address = self.settings.New(name="ip_address", initial="192.168.1.2", dtype=str, ro=False)
        #self.ip_port = self.settings.New(name="ip_port", initial="23", dtype=str, ro=False)
        
        for i in [1,2,3,4]:
            self.settings.New(name="axis_{}_target".format(i), initial=0, dtype=int, ro=False)
            self.settings.New(name="axis_{}_position".format(i), initial=0, dtype=int, ro=False)
            self.settings.New(name="axis_{}_velocity".format(i), initial=20, dtype=int, ro=False)
                
                   
    def connect(self):
        
        self.dev = dev = PicomotorDev(port=self.port.value, debug=self.debug_mode.val)
            
        for i in [1,2,3,4]:
            pos = self.settings.get_lq('axis_{}_position'.format(i))
            pos.connect_to_hardware(read_func = lambda n=i: self.dev.read_actual_pos_abs(n))
            pos.read_from_hardware()
            
            vel = self.settings.get_lq('axis_{}_velocity'.format(i))
            vel.connect_to_hardware(
                                    write_func = lambda v, n=i: dev.write_velocity(n, v),
                                    read_func = lambda n=i: dev.read_velocity(n))
            vel.read_from_hardware()

            target = self.settings.get_lq('axis_{}_target'.format(i))
            target.connect_to_hardware(
                read_func  = lambda n=i: dev.read_target_pos_abs(n),
                write_func = lambda x, n=i: dev.write_target_pos_abs(n, x))
            target.read_from_hardware()
            
    def disconnect(self):
        
        self.settings.disconnect_all_from_hardware()
        
        if hasattr(self, 'dev'):
            self.dev.close()
            del self.dev
            
    def threaded_update(self):
#         print("---")
        for i in [1,2,3,4]:
            self.settings.get_lq('axis_{}_position'.format(i)).read_from_hardware()
        time.sleep(0.10)
            