'''
Created on Jun 28, 2017

@author: Alan Buckley

Updated 2020-07-06 Edward Barnard
'''

import serial
import time
import logging
import threading

logger = logging.getLogger(__name__)

class PicomotorDev(object):
    
    name="picomotor_dev"
    
    def __init__(self, port="COM3",debug=False):
        """
        
        port : COM port or socket url.
                Examples:
                    "COM3"
                    "socket://192.168.1.23:23"
        """
        
        
        self.debug = debug
        self.port = port
        self.lock = threading.Lock()
        
        self.ser = serial.serial_for_url(url=port, baudrate=9600, timeout=1.0)
        
        # If using TCP, read telnet header (and then ignore it)
        if port.startswith("socket://"):
            self.ser.read(6)
        
        self.ser.flush()
            
        
        time.sleep(0.2)

        
    def ask_cmd(self, cmd):
        if self.debug: 
            logger.debug("ask_cmd: {}".format(cmd))
        message = cmd.encode()+b'\r'
        with self.lock:
            self.ser.write(message)
            resp = self.ser.readline()
            self.ser.flush()
#         print(message, "-->", resp)
        resp = resp.strip().decode()
        if self.debug:
            logger.debug("readout: {}".format(resp))
        return resp

    def send_cmd(self, cmd):
        if self.debug: 
            logger.debug("ask_cmd: {}".format(cmd))
        message = cmd.encode()+b'\r'
        self.ser.write(message)
        self.ser.flush()
        
    def read_identification(self):
        """
        This query will cause the instrument to return a unique identification string. This similar
        to the Version (VE) command but provides more information. In response to this
        command the controller replies with company name, product model name, firmware
        version number, firmware build date, and controller serial number. No two controllers
        share the same model name and serial numbers, therefore this information can be used to
        uniquely identify a specific controller.
        """
        return self.ask_cmd("*IDN?")
    
    def abort_motion(self):
        """For use with velocity command. Does not use deceleration."""
        self.send_cmd("AB")
        
    def stop_motion(self, axis):
        """For use with acceleration motion of the motor. Uses deceleration."""
        assert (1 <= int(axis) <= 4)
        message = "{:d}ST".format(int(axis))
        self.send_cmd(message)

    
    def read_target_pos_abs(self, axis):
        assert  (1 <= int(axis) <= 4)
        message = "{:d}PA?".format(int(axis))
        resp = self.ask_cmd(message)
        return int(resp)
    
    def write_target_pos_abs(self, axis, steps):
        assert (1 <= int(axis) <= 4)
        assert (-2147483648 <= steps <= +2147483647)
        print ("writing position to axis",int(axis))
        message = "{:d}PA{:+}".format(int(axis), int(steps))
        self.send_cmd(message)
    
    def read_rel_pos(self, axis):
        assert  (1 <= int(axis) <= 4)
        message = "{:d}PR?".format(int(axis))
        resp = self.ask_cmd(message)
        return int(resp)
    
    def write_rel_pos(self, axis, steps):
        assert (1 <= int(axis) <= 4)
        assert (-2147483648 <= steps <= 2147483647)
        message = "{:02d}PR{}".format(int(axis), int(steps))
        self.send_cmd(message)
        
    def read_actual_pos_abs(self,axis):
        """This command is used to query the actual position of an axis. The actual position
        represents the internal number of steps made by the controller relative to its position
        when controller was powered ON or a system reset occurred or Home (DH) command
        was received.
        Note that the real or physical position of the actuator/motor may differ as a function of
        mechanical precision and inherent open-loop positioning inaccuracies."""
        assert  (1 <= int(axis) <= 4)
        message = "{:d}TP?".format(int(axis))
        resp = self.ask_cmd(message)
        return int(resp)

        
    def read_velocity(self, axis):
        """ returns velocity of axis in steps/sec"""
        assert (1 <= int(axis) <= 4)
#         assert (1 <= int(axis) <= 2000)
        message = "{:d}VA?".format(int(axis))
        resp = self.ask_cmd(message)
        return int(resp)
    
    def write_velocity(self, axis, step_rate):
        """
        This command is used to set the velocity value for an axis. The velocity setting specified
        will not have any effect on a move that is already in progress. If this command is issued
        when an axis motion is in progress, the controller will accept the new value but it will
        use it for subsequent moves only. The maximum velocity for a 'Standard' Picomotor is
        2000 steps/sec, while the same for a 'Tiny' Picomotor is 1750 steps/sec.
        
        axis is axis number 1,2,3, or 4
        step_rate is new velocity in steps/sec (min 1, max 2000)
        """

        assert (1 <= int(axis) <= 4)
        assert (1 <= int(step_rate) <= 2000)
        message = "{:d}VA{:d}".format(int(axis), int(step_rate))
        self.send_cmd(message)
        
    def write_accel(self, axis, acc):
        """
        This command is used to set the acceleration value for an axis. The acceleration setting
        specified will not have any effect on a move that is already in progress. If this command
        is issued when an axis motion is in progress, the controller will accept the new value but
        it will use it for subsequent moves only.
        
        acc -- units of steps/sec^2 
            1 to 200k, default 100k
        """
        assert (1 <= int(axis) <= 4)
        assert (1 <= int(acc) <= 200000)       
        message = "{:d}AC{:d}".format(int(axis), int(acc))
        self.send_cmd(message)
    
    def read_accel(self, axis):
        """returns units of steps/sec^2"""
        assert (1 <= int(axis) <= 4)
        resp = self.ask_cmd("{:d}AC?".format(int(axis)))
        return int(resp)
    
    def read_home_pos(self, axis):
        "This command is used to query the home position value for an axis."
        assert (1 <= int(axis) <= 4)
        resp = self.ask_cmd("{:d}DH?".format(int(axis)))
        return int(resp)

    def write_home_pos(self, axis, home_pos=0):
        """This command is used to define the "home" position for an axis. The home position is set
        to 0 if this command is issued without "nn" value. Upon receipt of this command, the
        controller will set the present position to the specified home position. The move to
        absolute position command (PA) uses the "home" position as reference point for moves.
        
        Example
            1DH (set motor 1 counter to 0)
            2DH1000 (set motor 2 counter equal to 1000)
        """
        assert (1 <= int(axis) <= 4)
        assert (-2147483648 <= home_pos <= +2147483647)
        message = "{:d}DH{:+}".format(int(axis), int(home_pos))
        self.send_cmd(message)


        
    def close(self):
        self.ser.close()


if __name__ == '__main__':
    
    dev = PicomotorDev('socket://192.168.1.169:23')
    
    print(dev.ask_cmd("*IDN?"))
    for i in [1,2,3,4]:
        print(i, "vel", dev.read_velocity(i))
        print(i, "abs",dev.read_actual_pos_abs(i))
        print(i, 'acc', dev.read_accel(i))
        print(i, 'home_pos', dev.read_home_pos(i))
        
        print(dev.ask_cmd("{}DH?".format(i)))
        