from ScopeFoundry import BaseMicroscopeApp
from ScopeFoundryHW.zaber_motion.zaber_linear_hw import ZaberLinearHW

class ZaberTestApp(BaseMicroscopeApp):
    
    name = 'zaber_test_app'
    
    def setup(self):
        hw = self.add_hardware(ZaberLinearHW(self))
        #hw.settings['port'] = 'COM5'
        
        #self.add_measurement(ASIStageControlMeasure(self))
        
                
if __name__ == '__main__':
    import sys
    app = ZaberTestApp(sys.argv)
    app.exec_()