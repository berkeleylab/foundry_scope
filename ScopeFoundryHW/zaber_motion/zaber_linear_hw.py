from ScopeFoundry import HardwareComponent
from zaber_motion import Units


class ZaberLinearHW(HardwareComponent):
    
    name = 'zaber_linear'
    
    
    def setup(self):
        
        self.settings.New('port', dtype=str, initial='COM3')
        self.settings.New('axis', dtype=int, initial=1)
        self.settings.New('position', dtype=float, unit='mm', ro=True)
        self.settings.New('target', dtype=float, unit='mm', ro=False)
        
    def connect(self):
        
        from zaber_motion import Units
        from zaber_motion.ascii import Connection
        
        self.conn = Connection.open_serial_port(self.settings['port'])
        self.conn.enable_alerts()
        
        self.device_list = self.conn.detect_devices()
        self.device = self.device_list[0]
        self.axis = self.device.get_axis(1)
        
        
        self.settings.position.connect_to_hardware(
            read_func=self.get_pos)

        self.settings.target.connect_to_hardware(
            write_func=self.move_absolute)
        
        self.settings.position.read_from_hardware()
        self.settings['target']=self.settings['position']
        
        print(self.axis)
        
    def disconnect(self):
        if hasattr(self, 'conn'):
            self.conn.close()
        
        self.settings.disconnect_all_from_hardware()
        
        
        
    def get_pos(self):
        from zaber_motion import Units

        #print('get_pos')
        return self.axis.get_position(Units.LENGTH_MILLIMETRES)
    
    
    def move_absolute(self, x):
        from zaber_motion import Units
        self.axis.move_absolute(position=x, unit=Units.LENGTH_MILLIMETRES, wait_until_idle=False)#, velocity, velocity_unit, acceleration, acceleration_unit)
    
    
    def threaded_update(self):
        import time
        self.settings.position.read_from_hardware()
        time.sleep(1.0)
