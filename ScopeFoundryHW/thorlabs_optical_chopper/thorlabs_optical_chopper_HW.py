'''
Created on Sep 18, 2014

@author: Benedikt Ursprung
'''

from ScopeFoundry import HardwareComponent
from ScopeFoundryHW.thorlabs_optical_chopper.thorlabs_optical_chopper import ThorlabsOpticalChopper


class ThorlabsOpticalChopperHW(HardwareComponent):  # object-->HardwareComponent
    
    name = 'thorlabs_optical_chopper'
    
    def setup(self):

        self.settings.New('port', str, initial='COM4')
        self.freq = self.settings.New('chopp_frequency', dtype=int, unit='Hz', ro=False, si=True)
        # if hasattr(self.gui.ui, 'photocurrent2D_chopp_frequency_doubleSpinBox'):
        #    self.freq.connect_to_widget(self.gui.ui.photocurrent2D_chopp_frequency_doubleSpinBox)
        self.spinning = self.settings.New('spinning', dtype=bool, unit='Hz', ro=False)        
        # if hasattr(self.gui.ui, 'photocurrent2D_spinning_checkBox'):
        #    self.spinning.connect_to_widget(self.gui.ui.photocurrent2D_spinning_checkBox)
        self.settings.New('blade', int, initial=2)
        
        
    def connect(self):
        S = self.settings

        if S['debug_mode']: 
            print("connecting to thorlabs optical chopper")
        
        # Open connection to hardware
        self.dev = ThorlabsOpticalChopper(port=S['port'],
                                          debug=S['debug_mode'])
    
        # connect logged quantities
        S.chopp_frequency.connect_to_hardware(self.dev.read_freq,
                                      self.dev.write_freq)
        S.spinning.connect_to_hardware(
            self.dev.read_enable,
            self.dev.write_enable)
        
        S.blade.connect_to_hardware(self.dev.read_blade, 
                                    self.dev.write_blade)
            
        print ('connected to ', self.name)

    def disconnect(self):
        # disconnect hardware
        self.dev.close()
        
        # clean up hardware object
        del self.dev
        
        print('disconnected ', self.name)
