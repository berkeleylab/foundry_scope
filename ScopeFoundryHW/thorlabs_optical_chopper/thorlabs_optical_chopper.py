'''
Created on Sep 18, 2014

@author: Benedikt Ursprung
'''

from __future__ import division
import time
import serial


class ThorlabsOpticalChopper:
    '''
    classdocs
    '''
    ThorlabsOpticalChopperBaudRate = 115200
    
    def __init__(self, port="COM9", debug=False):
        self.port = port
        self.debug = debug
        
        self.ser = serial.Serial(port=self.port, baudrate=self.ThorlabsOpticalChopperBaudRate, bytesize=serial.EIGHTBITS,
                                 stopbits=1, parity=serial.PARITY_NONE, xonxoff=0, rtscts=0, timeout=0.5)  # ,  stopbits=1, xonxoff=0, rtscts=0, timeout=5.0       
        self.ser.flushInput()
        
        # sio = io.TextIOWrapper(io.BufferedRWPair(ser, ser))
        time.sleep(0.3)


    def write(self, cmd):
        if self.debug: print("cmd: ", cmd, cmd.encode())
        self.ser.write(f'{cmd}\r'.encode())
        
    def ask(self, cmd):
        if self.debug: print("cmd: ", cmd)
        self.write(cmd)        
        resp = self.ser.readline().decode()
        time.sleep(0.05)
        if self.debug: print("cmd: ", cmd , resp, resp.split('\r'), resp.split('\r')[-2])
        
        return resp.split('\r')[-2]
                
    def close(self):
        self.ser.close()
        print ('closed thorlabs_optical_chopper')
    
    def read_enable(self):
        return bool(int(self.ask('enable?')))

    def write_enable(self, enable=True):
        if enable:
            self.write('enable=1')
        else:
            self.write('enable=0')
            
    def read_freq(self):
        fstr = self.ask('freq?')
        return float(fstr)
    
    def write_freq(self, f):
        self.write(f'freq={int(f)}')

    def read_blade(self):
        self.write(f'blade?')
                
    def write_blade(self, n):
        self.write(f'blade={n}')


if __name__ == '__main__':

    TOC1 = ThorlabsOpticalChopper(debug=True)

    TOC1.write_enable(True)
    TOC1.write_freq(200)
    print(TOC1.read_freq())
    print(TOC1.read_enable())
