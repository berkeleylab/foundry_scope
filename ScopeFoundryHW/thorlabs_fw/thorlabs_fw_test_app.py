from ScopeFoundry.base_app import BaseMicroscopeApp
from ScopeFoundryHW.thorlabs_fw.thorlabs_fw102c_hw import ThorlabsFW102cHW

class ThorlabsFWTestApp(BaseMicroscopeApp):
    
    name = 'thorlabs_fw_test_app'
    
    def setup(self):
        
        names = ['ONE', 'TWO','THREE', 'FOUR', 'FIVE', 'SIX']
        fw = self.add_hardware(ThorlabsFW102cHW(self, 
                                                position_names=names))
        fw2 = self.add_hardware(ThorlabsFW102cHW(self, name='fw_no_names'))


if __name__ == '__main__':

    import sys
    app = ThorlabsFWTestApp(sys.argv)
    # app.qtapp.setStyle('Fusion')
    sys.exit(app.exec_())
