from ScopeFoundry import HardwareComponent

class ThorlabsFW102cHW(HardwareComponent):

    name = 'thorlabs_fw102c'

    def __init__(self, app, debug=False, name=None,
                 position_names = [],# None#{'Other':[0, 'red']},  # {'name':pos} or {'name',[pos,'color']} are valid.
                 ):
            
        self.colors = []
        self.position_names = position_names
        self.named_positions = {name:(pos_m1+1) for pos_m1, name in enumerate(self.position_names)}
        # if self.position_names:        
        #     for pos, name in self.position_names:
        #         if hasattr(_val, '__len__'):
        #             val = _val[0]
        #             self.colors.append(_val[1])
        #         else:
        #             val = _val
                    
        #         self.named_positions.update({pos:int(val)})  

        HardwareComponent.__init__(self, app, debug=debug, name=name)

    def setup(self):

        self.settings.New('port', dtype=str, initial='/dev/ttyUSB1')
        self.settings.New('position', dtype=int,initial=1, vmin=1, vmax=12,)


        if self.position_names:
            self.settings.New('named_position', dtype=str,
                              initial=list(self.position_names)[0],
                              choices=tuple(self.position_names),
                              )
            
            for pos_m1,name in enumerate(self.position_names):
                def goto_pos(name=name):
                    self.settings['named_position'] = name
                self.add_operation('Goto ' + name, goto_pos)

        if 'named_position' in self.settings:
            self.settings.named_position.connect_to_hardware(
                read_func=self.read_named_position,
                write_func=self.goto_named_position
                )
            
        self.read_from_hardware()
            
    def read_named_position(self):
        pos = self.settings['position']
        return self.position_names[pos-1]
        
        
    def goto_named_position(self, name):
        S = self.settings
        self.settings['position'] = self.named_positions[name]


    def connect(self):
        from ScopeFoundryHW.thorlabs_fw.thorlabs_fw102c import ThorlabsFW102C
        
        S = self.settings

        self.dev:ThorlabsFW102C = ThorlabsFW102C(S['port'], S['debug_mode'])

        
        S.position.connect_to_hardware(
            read_func = self.dev.get_pos,
            write_func = self.dev.set_pos
        )

        if 'named_position' in self.settings:
            self.settings.named_position.connect_to_hardware(
                read_func=self.read_named_position,
                write_func=self.goto_named_position
                )
            self.settings.position.add_listener(self.settings.named_position.read_from_hardware)
            
        self.read_from_hardware()

    def disconnect(self):

        self.settings.disconnect_all_from_hardware()
        
        if hasattr(self, 'dev'):
            self.dev.close()
            del self.dev


