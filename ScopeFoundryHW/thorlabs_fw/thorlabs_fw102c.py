import serial
from threading import Lock

class ThorlabsFW102C(object):


    def __init__(self, port, debug=False):
        self.debug = debug
        self.lock = Lock()
        self.ser = serial.Serial(port, baudrate=115200, timeout=10.)

    def ask(self, cmd):
        with self.lock:
            #self.ser.flush()
            msg = (cmd+"\r").encode()
            if self.debug: print("sending|",repr(msg))
            self.ser.write(msg)
            resp = self.ser.read_until(b'> ')
            if self.debug: print("received|", repr(resp))
        resp_parts = resp.split(b'\r')
        assert resp_parts[0] == cmd.encode()
        assert resp_parts[-1] == b'> '
        return resp_parts[1]

    def get_pos_count(self)->int:
        return int(self.ask("pcount?"))

    def get_pos(self)->int:
        return int(self.ask("pos?"))
    
    def set_pos(self, pos:int):
        return self.ask(f"pos={pos:d}")

    def get_ext_trigger_mode(self) -> bool:
        """returns external trigger state
        False: internal trigger
        True: external trigger
        """
        return bool(int(self.ask("trig?")))
    
    def set_ext_trigger_mode(self, external:bool):
        """sets external trigger state
        False: internal trigger
        True: external trigger
        """
        trig_num = [0,1][external]
        return self.ask(f"trig={trig_num}")

    # def set_speed_mode(self, mode):

if __name__ == '__main__':
    
    fw = ThorlabsFW102C("/dev/ttyUSB1", debug=True)
    print(repr(fw.ask("pos?")))
    print(repr(fw.ask("posa?")))
    print(repr(fw.ask("pos?")))
    print(repr(fw.get_pos_count()))
    print(fw.get_pos())
    print(fw.set_pos(9))