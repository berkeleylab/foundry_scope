from ScopeFoundry import HardwareComponent
from .optotune_lens import OptotuneLens
import time

class OptotuneLensHW(HardwareComponent):
    name = 'optotune_lens'
    
    def setup(self):
        
        self.settings.New('port', dtype=str, initial='COM1')
        self.settings.New('actual_current', 
                          dtype=float, fmt='%10.1f', ro=True,
                          unit='mA',
                          vmin=-292, vmax=292)
        self.settings.New('target_current', 
                          dtype=float, fmt='%10.1f', ro=False,
                          unit='mA',
                          vmin=-292, vmax=292)
        
    def connect(self):
        if self.settings['debug_mode']: print("connecting to Optotune")
        
        # Open connection to hardware
        self.lens = OptotuneLens(port=self.settings['port'], debug=self.settings['debug_mode'])
        
        # connect logged quantities
        self.settings.target_current.connect_to_hardware(
            write_func = self.lens.set_current)
        self.settings.actual_current.connect_to_hardware(
            read_func = self.lens.get_current)

    def disconnect(self):
        
        self.settings.disconnect_all_from_hardware()
        
        if hasattr(self, 'lens'):
            self.lens.close()
            del self.lens

    def threaded_update(self):
        
        self.settings.actual_current.read_from_hardware()
        time.sleep(0.25)