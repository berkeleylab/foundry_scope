from avaspec_h import *
import ctypes
from ctypes import c_uint, byref, sizeof, c_uint8, c_uint16, c_short, c_double, c_uint32
import numpy as np

def _err(retval):
    if retval >= 0:
        return retval
    else:
        err_name = err_by_num.get(retval, "Unknown Error")
        raise IOError("AvaSpec Error {} {}".format(retval, err_name))
    
    
port_enum = dict(any=-1, usb=0, ethernet=256)


def get_available_avaspec( port='usb'):
    assert port in port_enum.keys()

    lib = ctypes.WinDLL("avaspecx64.dll")
    num_devs = _err(lib.AVS_Init(port_enum[port]))
    print("num_devs", num_devs)
    a_pRequiredSize = c_uint(0)
    id_arr = (AvsIdentityType*num_devs)()
    print(repr(id_arr))
    n_dev =_err(lib.AVS_GetList(
                        sizeof(AvsIdentityType)*num_devs,
                        byref(a_pRequiredSize),
                        byref(id_arr) ))
    print(repr(id_arr))
    print("n_dev",n_dev)
    #ret = AVS_GetList(75, 0, 0)
    devs = []
    for id in id_arr:
        dev = dict()
        print(id.SerialNumber, id.UserFriendlyName, id.Status)
        dev['SerialNumber'] =id.SerialNumber.decode()
        dev['UserFriendlyName'] = id.UserFriendlyName.decode()
        dev['Status'] = DEVICE_STATUS[id.Status]
        dev['AvsIdentity'] = id
        devs.append(dev)
    return  devs


lib = ctypes.WinDLL("avaspecx64.dll")


class AvaSpec(object):
    
    def __init__(self, port='usb'):

        avail_devices = get_available_avaspec(port)
        
        self.dev_id = avail_devices[0]
        
        self.dev_handle = _err(lib.AVS_Activate(self.dev_id['AvsIdentity']))
        
        self.dev_config = DeviceConfigType()
        
        a_pRequiredSize = c_uint()
        _err(lib.AVS_GetParameter(self.dev_handle, 
                                  sizeof(DeviceConfigType), 
                                  byref(a_pRequiredSize),
                                  byref(self.dev_config)))
        
        
        self.meas_conf = MeasConfigType()
        self.meas_conf.m_StopPixel = 4093
        
        CB_CTYPE = ctypes.CFUNCTYPE(ctypes.c_int32, 
                         ctypes.POINTER(ctypes.c_long),
                         ctypes.POINTER(ctypes.c_int) )
        def cb(a,b):
            print("CB", a[0], b[0])
            return 0
        self.cb_c = CB_CTYPE(cb)
        
        n_px = c_short()
        _err(lib.AVS_GetNumPixels(self.dev_handle, byref(n_px)))
        self.n_px = n_px.value
        
        self.get_wavelength_array()
        # set defaults
        self.set_StartPixel(0)
        self.set_StopPixel(0)
        self.set_IntegrationTime(5.0)
        self.set_IntegrationDelay(0)
        self.set_NrAverages(1)
        self.set_CorDynDark(False, 0)
        self.set_Smoothing(0)
        self.set_SaturationDetection(False)
        self.set_Trigger()
        self.set_Control_Strobe(0)
        self.set_Control_LaserPulse(0, 0)
        self.set_Control_StoreToRam(False)
        
        
        
    def get_wavelength_array(self):
        n_px = c_short()
        _err(lib.AVS_GetNumPixels(self.dev_handle, byref(n_px)))
        self.wavelengths = np.zeros(n_px.value, dtype=c_double)
        _err(lib.AVS_GetLambda(self.dev_handle, self.wavelengths.ctypes))
        return self.wavelengths
    
#    def prepare_meas(self, meas_settings):
        
        
    def set_StartPixel(self, x=0):
        self.meas_conf.m_StartPixel = c_uint16(x)
    
    def set_StopPixel(self, x=0):
        if x <= 0:
            x = self.n_px - 1 + x
        self.meas_conf.m_StopPixel = c_uint16(x)
        
    def set_IntegrationTime(self, x=5.0):
        self.meas_conf.m_IntegrationTime = float(x)
        
    def set_IntegrationDelay(self, x=0):
        """
        integration delay, unit is internal FPGA clock cycle
        ( 0 = one unit before laser start)
        """
        self.meas_conf.m_IntegrationDelay = c_uint32(x)
        
    def set_NrAverages(self, x=1):
        """
        number of averages in a single measurement
        """
        self.meas_conf.m_NrAverages = c_uint32(x)
    
    def set_CorDynDark(self, en=True, forget_percent=0):
        """
        en :: Enable dynamic dark correction (sensor dependent)
        forget_percent:: Percentage of the new dark value pixels 
        that has to be used.
        e.g., a percentage of 100 means only new dark values are used.
        A percentage of 10 means that 10 percent of the new dark
        values is used and 90 percent of the old values is used for drift
        correction
        (Size = 2 bytes)
        """
        self.meas_conf.m_CorDynDark_m_Enable = c_uint8(bool(en))
        self.meas_conf.m_CorDynDark_m_ForgetPercentage = c_uint8(forget_percent)

       
    def set_Smoothing(self, pix=0, model=0 ): 
        """
        pix = 0-2048 number of neighbor pixels used for smoothing, max. has to be
        smaller than half the selected pixel range because both the
        pixels on the left and on the right are used
        model = Only one model defined so far
        (Size = 3 bytes)
        """
        self.meas_conf.m_Smoothing_m_SmoothPix = c_uint16(pix)
        self.meas_conf.m_Smoothing_m_SmoothModel = c_uint8(model)
    
    def set_SaturationDetection(self, en=True):
        """
        0 = disabled,
        1 = enabled, determines during each measurement if pixels
        are saturated (ADC value = 2^16 -1)
        2 = enabled, and also corrects inverted pixels (only ILX554)
        """
        self.meas_conf.m_SaturationDetection = c_uint8(en)
        
    def set_Trigger(self, mode='software', source='external', src_type='edge'):
        """
        Trigger parameters:
        mode, (0 = Software, 1 = Hardware, 2 = Single Scan)
        trigger source, (0 = external trigger, 1 = sync input)
        source type, (0 = edge trigger, 1 = level trigger)
        See section 4.2.8 for detailed information on trigger types.
        (Size = 3 bytes)
        """
        modes = {'software':0, 'hardware':1, 'single_scan': 2}
        assert mode in modes.keys()
        self.meas_conf.m_Trigger_m_Mode = modes[mode]
        sources = {'external':0, 'sync_input':1}
        assert source in sources.keys()
        self.meas_conf.m_Trigger_m_Source = sources[source]
        src_types = {'edge':0, 'level':1}
        assert src_type in src_types.keys()
        self.meas_conf.m_Trigger_m_SourceType = src_types[src_type]

    def set_Control_Strobe(self, n=0):
        """Number of strobe pulses during integration period
        (high time of pulse is 1 ms), (0 = no strobe pulses)
        
        A pulsed light source like the AvaLight-XE needs to be synchronized with the integration time cycle.
        The m_StrobeControl parameter determines the number of pulses the spectrometer sends out at pin 5
        at the DB26 connector during one integration time cycle. The maximum frequency at which the
        AvaLight-XE operates is 100 Hz. This means that the minimum integration time for 1 pulse per scan is
        10 ms. When setting the number of pulses e.g. to 3, the minimum integration time should be 30 ms.
        The AvaSpec DLL does not check for this limitation because other light sources may operate at higher
        frequencies, and should also be controllable by the AvaSpec DLL
        """
                 
    def set_Control_LaserPulse(self, delay=0, width=0):
        """
        
        delay:: Laser delay since trigger, unit is internal FPGA clock cycle
        width:: Laser pulse width , unit is internal FPGA clock cycle, (0 = nolaser pulse)
        
        For the fast trigger detectors ILX554, ILX511, S11155 and S11639 in the AvaSpec-2048, 2048L,
        2048XL and 2048CL, pin 23 at the DB26 connector can be used to send out a TTL signal which is
        related to the start of the integration time cycle. In the figure below, a measurement is started at the
        rising edge of the Trig signal. This can be a hardware or software trigger, see also section 4.2.8. The
        TTL signal at pin 23 (Laser) is set after the laserdelay (T1) expires. The pulsewidth for the laser pulse
        (T3) is set by the m_LaserWidth parameter. The integration time cycle starts after the integration delay
        parameter (see section 4.2.3) expires.        
        """
        self.meas_conf.m_Control_m_LaserDelay = c_uint32(delay)
        self.meas_conf.m_Control_m_LaserWidth = c_uint32(width)
        self.meas_conf.m_Control_m_LaserWaveLength = 0.0
    
    def set_Control_StoreToRam(self, n):
        """
        0 = no storage to RAM
        > 0 = number of spectra to be stored
        
        StoreToRam
        
        As of AS5216 firmware version 0.20.0.0 the StoreToRam function has been implemented. To use this
        function, you must set the requested number of scans in the m_StoreToRam control setting, and start
        measuring with a call to AVS_Measure() using 1 as the number of measurements (a_Nmsr).
        For the AS5216, there is an amount of 4MB available for scans, corresponding with 1013 scans of
        2048 pixels. The AvaSpec Mini and the AS7010 have much more memory, allowing for 7783 and
        16383 scans of 2048 pixels respectively. Scanning less pixels will in each case yield a larger capacity
        in scans. The AVS_Measure() message signaling the arrival of data will have a WParam value equal
        to the number of scans stored in RAM. In regular measurements, this value only signals success (with
        value ERR_SUCCESS) or failure (with a negative error message).
        Alternatively, when using AVS_PollScan() instead of a message driven interface, the AVS_PollScan()
        function will return 1 when the StoreToRam scans are available, and 0 as long as they are not.
        The scans can subsequently be read with a corresponding number of calls to AVS_GetScopeData().
        If you request more scans than will fit in memory, scanning will continue until the memory is fully used,
        therefore you should always request the number of scans that is returned in WParam (when using
        Windows Messaging.
        If using StoreToRAM in combination with AVS_PollScan(), the number of scans that can be processed
        by subsequently calling AVS_GetScopeData() will normally be equal to the requested number of
        scans in the StoreToRAM parameter. If more scans are requested than can be stored (e.g. 1500
        scans of 2048 pixels), it can happen that AVS_GetScopeData() will be called too many times. In case
        of the example, only the first 1013 calls to AVS_GetScopeData() will return SUCCESS. The next call
        will return the error code ERR_NO_SPECTRA_IN_RAM, which can be used by the application
        software as an additional stop condition for reading spectra from RAM. However, reading beyond the
        number of scans that can be stored in RAM is a time consuming event, so it is not recommended to
        request more scans than the maximum that can be stored.
        """
        self.meas_conf.m_Control_m_StoreToRam = c_uint16(n)
        
    def prepare_measurement(self):
        _err(lib.AVS_PrepareMeasure(self.dev_handle,self.meas_conf))
    
    def start_measure(self, n=1):
        _err(lib.AVS_Measure(self.dev_handle, None, n))

    def measure_with_callback(self, n=1):
        _err(lib.AVS_MeasureCallback(self.dev_handle, self.cb_c, n))
        
    def poll_scan(self):
        return bool(_err(lib.AVS_PollScan(self.dev_handle)))

    def get_ScopeData(self):
        """
        Returns the pixel values of the last performed measurement. 
        Should be called by the application after the notification 
        on AVS_Measure() is triggered.
        """
        data = np.zeros(self.n_px, dtype=c_double)
        #print(data.shape)
        #time_stamp = c_uint32()
        #c_double_array_p = ctypes.POINTER(ctypes.c_double*4096)
        #x = c_double_array_p()
        #x = (c_double*4094)()
        #_err(lib.AVS_GetScopeData(self.dev_handle,
        #                          byref(time_stamp), byref(x)
        #                          ))
        #print("x", x[0],x[1])
        #d = np.ctypeslib.as_array(x)[:]
        #print("d")
        #data[:] = d
        #print("d2")
        time_stamp = c_uint32()

        _err(lib.AVS_GetScopeData(self.dev_handle,
                                  byref(time_stamp),
                                  data.ctypes
                                  ))
        
        
        
        return time_stamp.value, data

    def close(self):
        _err(lib.AVS_Deactivate(self.dev_handle))
        
        
        
        
if __name__ == '__main__':
    import time
    
    print(sizeof(AvsIdentityType))
    print(get_available_avaspec('usb'))
    
    ava = AvaSpec('usb')
    print('handle', ava.dev_handle)
    print("conf", ava.dev_config)
    
    def print_c_struct(x):
        print(x)
        for field in x._fields_:
            print('\t',field[0], getattr(x, field[0]))
    print_c_struct(ava.dev_config)
    print("wavelength", ava.get_wavelength_array())
    
    ava.set_StartPixel(0)
    ava.set_StopPixel(0)
    ava.set_IntegrationTime(10.0)
    ava.set_IntegrationDelay(0)
    ava.set_NrAverages(1)
    ava.set_CorDynDark(False, 0)
    ava.set_Smoothing(10)
    ava.set_SaturationDetection(False)
    ava.set_Trigger()
    ava.set_Control_Strobe(0)
    ava.set_Control_LaserPulse(0, 0)
    ava.set_Control_StoreToRam(False)
      
    print_c_struct(ava.meas_conf)
    
    ava.prepare_measurement()
    ava.start_measure(1)
    while not ava.poll_scan():
        time.sleep(0.01)
        #print("wait")
    time.sleep(0.1)
    ts, dat = ava.get_ScopeData()
    print("timestamp", ts)
    print("data", dat)
    #ava.start_measure(1)
    #time.sleep(0.1)
    #ts, dat = ava.get_ScopeData()

    ava.close()
    
    import matplotlib.pyplot as plt
    plt.figure()
    plt.plot(dat)
    plt.show()
#     ava.measure_cb(1)
#     print(1)
#     ava.measure_cb(-1)
# 
#     time.sleep(1.0)
