from ScopeFoundryHW.sync_stream_scan.sync_stream_scan import SyncStreamScan
import numpy as np
import scipy.signal

class SyncStreamRasterScan(SyncStreamScan):
    
    name = 'sync_stream_raster_scan'
    
    def setup(self):
        SyncStreamScan.setup(self)
        
        S = self.settings
        
        S.New('Nx', dtype=int, initial=10)
        S.New('Ny', dtype=int, initial=10)
        S.New('x_span', dtype=float, initial=10, unit='V')
        S.New('y_span', dtype=float, initial=10, unit='V')
        S.New('x_center', dtype=float, initial=0, unit='V')
        S.New('y_center', dtype=float, initial=0, unit='V')
        
        S.New('pixel_time', dtype=float, unit='s', initial=1e-3, si=True)
        
        S.New('flybock_ratio', dtype=float, initial=0.1, vmin=0)
        
    #def compute_pixel_time(self):
        
    def ao_stream_gen(self, T):
        S = self.settings
        Nx = S['Nx'] # pixels per line
        Ny = S['Ny'] # lines
        dt = S['pixel_time'] # pixel time
        x_span = S['x_span']
        y_span = S['y_span']
        x_center = S['x_center']
        y_center = S['y_center']
        
        dx = x_span/(Nx)
        dy = y_span/(Ny)
        self.x_velocity = dx/dt
        
        self.line_time  = dt*Nx
        self.total_line_time = self.line_time * (1+S['flyback_ratio'])

        
        ao_stream = np.zeros((len(T), self.ao_chan_count), dtype=float)


        ao_stream[:,0] = self.x_stream(T)
        ao_stream[:,1] = self.y_stream(T)

        return ao_stream
    
    def x_stream(self, T):
        X = np.piecewise(T, 
             condlist=[(T % self.total_line_time) < self.line_time,
                       (T % self.total_line_time) >= self.line_time ],
             funclist=[self.x_linear_ramp,
                       self.x_flyback_sin])
        return X
    
    def y_stream(self, T):
        Y = np.piecewise(T, 
             condlist=[(T % self.total_line_time) < self.line_time,
                       (T % self.total_line_time) >= self.line_time ],
             funclist=[self.y_pos,
                       self.y_flyback])
        return Y


    def x_linear_ramp(self, T):
        S = self.settings
        Tp = T % self.total_line_time # periodic time array
        return S['x_center'] - S['x_span']/2 + self.x_velocity * Tp
    
    def x_flyback_linear(self, T):
        Tp = T % total_line_time
        Tfb = Tp - line_time
        return x_center + x_span/2 - dx/dt * Tfb / flyback_ratio
    
    def x_flyback_sin(self, T):
        Tp = T % total_line_time
        Tfb = Tp - line_time
        return x_center + x_span/2 * np.cos(np.pi*Tfb/flyback_time)
    
    def y_pos(self, T):
        return ((T // total_line_time)/(Ny)-0.5)*y_span + y_center + dy/2
    
    def y_flyback(self, T):
        y_start = self.y_pos(T)
        y_stop  = self.y_pos(T + dt*Nx)
        
        Tp = T % total_line_time
        Tfb = Tp - line_time
        return y_start + (y_stop-y_start)*Tfb/flyback_time

    
    