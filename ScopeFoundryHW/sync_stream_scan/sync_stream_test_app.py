from ScopeFoundry import BaseMicroscopeApp
from ScopeFoundryHW.sync_stream_scan.sync_stream_scan import SyncStreamScan
from ScopeFoundryHW.sync_stream_scan.lj_scan import LJScan
from ScopeFoundryHW.sync_stream_scan.sync_stream_raster_scan import SyncStreamRasterScan

class SyncStreamTestApp(BaseMicroscopeApp):
    
    name = 'sync_stream_test'
    
    def setup(self):

        self.add_measurement(SyncStreamScan(self))
        self.add_measurement(SyncStreamRasterScan(self))
        
        self.add_measurement(LJScan)
        
if __name__ == "__main__":
    #import cProfile
    #profile = cProfile.Profile()
    #profile.enable()
    app = SyncStreamTestApp([])
    app.exec_()