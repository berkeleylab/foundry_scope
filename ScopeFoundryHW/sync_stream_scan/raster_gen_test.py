import numpy as np

class raster_stream_gen(object):
    
    def __init__(self):
        s = self.stream_params = dict(
            Nx = 40,
            Ny = 10,
            x_span = 2,
            y_span = 2,
            x_center = 0,
            y_center = 0,
            pixel_time = 10,
            flyback_ratio = 0.5,
        )#self.settings.as_value_dict()
        
        s['dt'] = s['pixel_time']
        
        s['dx'] = s['x_span']/(s['Nx'])
        s['dy'] = s['y_span']/(s['Ny'])
        s['x_velocity'] = s['dx']/s['dt']
        s['line_time'] = s['dt']*s['Nx']
        s['total_line_time'] = s['line_time'] * (1+s['flyback_ratio'])  
        s['frame_time'] = s['total_line_time']*s['Ny']
        s['total_frame_time'] = s['total_line_time']*(s['Ny']) + s['line_time']*s['flyback_ratio'] # add one for y-flyback
        
        self.ao_chan_count = 2
    
    def ao_stream_gen(self, T):
        
        ao_stream = np.zeros((len(T), self.ao_chan_count), dtype=float)

        ao_stream[:,0] = self.x_stream(T)
        ao_stream[:,1] = self.y_stream(T)

        return ao_stream
    
    def x_stream(self, T):
        s = self.stream_params
        
        Tframe = T % s['total_frame_time']

        X = np.piecewise(T, 
             condlist=[(Tframe % s['total_line_time']) <  s['line_time'],
                       (Tframe % s['total_line_time']) >= s['line_time'],
                       Tframe > s['frame_time']
                       ],
             funclist=[self.x_linear_ramp,
                       self.x_flyback_sin,
                       self.x_hold])
        return X
    
    def y_stream(self, T):
        
        s = self.stream_params
        
        Y = np.piecewise(T, 
             condlist=[(T % s['total_frame_time']) <  s['frame_time'],
                       (T % s['total_frame_time']) >= s['frame_time']],
             funclist=[self.y_pos,
                       self.y_flyback])
        return Y
        
    
    #def y_stream(self, T):
    #    return self.y_pos(T)

    def x_hold(self, T):
        s = self.stream_params        
        return np.ones_like(T)*(s['x_center'] - s['x_span']/2)

    def x_linear_ramp(self, T):
        s = self.stream_params
        Tframe = T % s['total_frame_time']
        Tp = Tframe % s['total_line_time'] # periodic time array
        return s['x_center'] - s['x_span']/2 + s['x_velocity'] * Tp
    
    def x_flyback_linear(self, T):
        s = self.stream_params
        Tp = T % s['total_line_time']
        Tfb = Tp - s['line_time']
        return s['x_center'] + s['x_span']/2 - s['dx']/s['dt'] * Tfb / s['flyback_ratio']
    
    def x_flyback_sin(self, T):
        s = self.stream_params
        Tframe = T % s['total_frame_time']        
        Tp = Tframe % s['total_line_time']
        Tfb = Tp - s['line_time']
        flyback_time = s['total_line_time'] - s['line_time']
        return s['x_center'] + s['x_span']/2 * np.cos(np.pi*Tfb/flyback_time)
    
    def y_pos(self, T):
        
        # want new y_pos during x flyback
        
        s = self.stream_params
        Tframe = T % s['total_frame_time']
        y = s['y_center'] + s['dy'] * (((Tframe - s['line_time'] -s['dt'])  // s['total_line_time']) - s['Ny']/2 + 1 + 0.5)
        y = y.clip(s['y_center'] - 0.5*s['y_span'], s['y_center'] + 0.5*s['y_span'])
        print(y)
        return y
        
        #return ( ( Tframe - s['line_time'])  // s['total_line_time']  + 1 -s['Ny']) / (s['Ny']-0.5) *s['y_span'] + s['y_center'] + s['dy']/2
    
    def y_flyback(self, T):
        s = self.stream_params
        flyback_time = s['total_frame_time'] - s['frame_time']

        Tframe = T % s['total_frame_time']
        Ty_fb = Tframe - s['frame_time']
        return s['y_center'] + s['y_span']/2 * np.cos(np.pi*Ty_fb/flyback_time)


    def frame_start(self, T):
        s = self.stream_params
        Tframe = T % s['total_frame_time']        
        return Tframe == 0

    def frame_stop(self, T):
        s = self.stream_params
        Tframe = T % s['total_frame_time']        
        return Tframe == s['frame_time']

    def line_start(self, T):
        s = self.stream_params
        Tframe = T % s['total_frame_time']        
        return np.logical_and(
            (Tframe % s['total_line_time']) == 0,
            Tframe < s['frame_time'] )

    def line_stop(self, T):
        s = self.stream_params
        Tframe = T % s['total_frame_time']        
        return np.logical_and(
            (Tframe % s['total_line_time']) == s['line_time'],
            Tframe < s['frame_time'] )

    def pixel_start(self,T):
        s = self.stream_params
        Tframe = T % s['total_frame_time']
        Tline = Tframe % s['total_line_time']
        #return (Tline % s['pixel_time']) ==0
         
        return np.logical_and(
                    np.logical_and(
                        (Tline % s['pixel_time']) == 0,
                        Tline <= s['line_time']), # <= gives an extra (long) pixel during flyback, < does not
                    Tframe < s['frame_time'])
        
        
        
        #    Tframe < s['frame_time'] )        
    
#%matplotlib inline
#import matplotlib.pyplot as plt
import pyqtgraph as pg

plot = pg.PlotWindow()

sg = raster_stream_gen()
T = np.arange(0,120000, 1, dtype=float)


stream = sg.ao_stream_gen(T)

plot.plot(T, stream[:,0])#, symbol='x')
plot.plot(T, stream[:,1], pen=pg.mkPen('r'))#, symbol='x', symbolPen=pg.mkPen('r'))

plot.plot(T, sg.frame_start(T)-3, pen=pg.mkPen('y'))#, symbol='x', symbolPen=pg.mkPen('y'))
plot.plot(T, sg.frame_stop(T)-3, pen=pg.mkPen('g'))#, symbol='x', symbolPen=pg.mkPen('g'))

plot.plot(T, sg.line_start(T)-4, pen=pg.mkPen('y'))#, symbol='x', symbolPen=pg.mkPen('y'))
plot.plot(T, sg.line_stop(T)-4, pen=pg.mkPen('g'))#, symbol='x', symbolPen=pg.mkPen('g'))

plot.plot(T, sg.pixel_start(T)-5, pen=pg.mkPen('y'))#, symbol='x', symbolPen=pg.mkPen('y'))

s = sg.stream_params

def vline(x, **kwargs):
    return plot.plot([x,x], [-8, 5], **kwargs)

vline(s['frame_time'], pen=pg.mkPen('y') )
vline(s['total_frame_time'],pen=pg.mkPen('g') )
for i in range(10):
    vline(i*s['pixel_time'], pen=pg.mkPen('r') )

#plot.plot([0,1,2])
plot.show()
pg.QtGui.QApplication.exec_()


