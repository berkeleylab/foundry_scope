from ScopeFoundry import HardwareComponent
import serial
import time


class Keithley622xHW(HardwareComponent):
    
    name = 'keithley622x'
    
    def setup(self):
        
        self.port = self.settings.New('port', dtype=str, initial='COM6')
        
        self.settings.New('current', dtype=float, initial=0, unit='A', si=True, vmin=-105e-3, vmax=105e-3 )
        
        self.settings.New('compliance', dtype=float, unit='V', initial=10, vmin=0.1, vmax=105)

        self.settings.New('output_enable', dtype=bool)
        
    
    def connect(self):
        
        self.ser = serial.Serial(port=self.settings['port'], 
                                 baudrate=19200, stopbits=1, xonxoff=0, rtscts=0, timeout=5.0)#,         

        self.ser.flush()
        time.sleep(0.1)
    
        print( self.ask("*IDN?"))
        
        
        self.settings.current.connect_to_hardware(
            read_func = self.read_current,
            write_func = self.write_current
            )
        
        self.settings.compliance.connect_to_hardware(
            read_func = self.read_compliance,
            write_func = self.write_compliance
            )
        
        
        self.settings.output_enable.connect_to_hardware(
            read_func = self.read_output_enable,
            write_func = self.write_output_enable
            )
    
    def disconnect(self):
        
        self.settings.disconnect_all_from_hardware()
        
        if hasattr(self, 'ser'):
            self.ser.close()
            
    
    
    def ask(self, cmd):
        self.send(cmd)
        resp = self.ser.read_until(b'\r')
        if self.settings['debug_mode']: print('response', resp.decode().strip('\n'))
        return resp.decode().strip('\n')
    
    def send(self, cmd):
        cmd += '\r\n'
        if self.settings['debug_mode']: print('send', cmd)
        self.ser.write(cmd.encode())               
    
    
    def read_output_enable(self):
        """
        EXAMPLE:
        
        output:state?
        0
        output:state ON
        output:state?
        1
        """
        resp = self.ask("OUTPUT:STATE?")
        return(bool(int(resp)))
    
    def write_output_enable(self, enable):
        """
        output:state ON
        output:state OFF
        """
        enable_str = ["OFF", "ON"][bool(enable)]
        self.send("OUTPUT:STATE " + enable_str)
    
    
    def read_compliance(self):
        """
        source:current:compliance?
        1.500000E+01
        """
        resp = self.ask("SOURCE:CURRENT:COMPLIANCE?")
        return float(resp)
    
    def write_compliance(self, volts):
        """
        source:current:compliance 1.200000E+01
        
        "{:E}".format(12) --> '1.200000E+01'
        """
        self.send("SOURCE:CURRENT:COMPLIANCE {:E}".format(volts))
    
    
    def read_current(self):
        """
        source:current:level?
        1.00000E-03
        """
        resp = self.ask("SOURCE:CURRENT:LEVEL?")
        return float(resp)
    
    def write_current(self, amps):
        """
        source:current:level 3.00000E-03
        "{:E}".format(0.001) --> '1.000000E-03'
        """
        self.send("SOURCE:CURRENT:LEVEL {:E}".format(amps))        
        
        
    
        
        
    