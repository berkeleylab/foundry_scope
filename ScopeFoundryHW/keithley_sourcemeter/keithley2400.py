import serial
import io
import time

# if no range is given on Keithley.__init__, use these to set the compliance
VMAX = 50
IMAX = 1.5


class Keithley2400(object):

    def __init__(self, source=None, range=None, port="COM11", debug=False):
        """Initialize keithley in given source mode with specified range.
           Source can be either 'Voltage' or 'Current'."""
        
        if source not in ['Voltage', 'Current']:
            logger.write("ERROR : bad arg : source must be 'Voltage' or 'Current'")
            return None
            
        #self.name = name
        self.source = source
        self.range = range
        self.port = port
        self.debug = debug
        
        # setup serial
        self.serial = serial.Serial(self.port, baudrate = 9600, bytesize=8, parity='N', 
                    stopbits=1, xonxoff=0, rtscts=0, timeout=0.2)
        
        #self.ser_io = io.TextIOWrapper(io.BufferedRWPair(self.serial, self.serial)) #newline=u"\r"

        
        
        # query device ID, check for response
        self.id = self.ask('*IDN?')
        if self.id == '':
            print 'ERROR : Keithley.init failed!' 
            raise IOError
        
        # restore GPIB defaults
        self.write('*RST')
        
        self.id = self.ask('*IDN?')

        
        # turn off concurrent functions
        self.write('SENSe:FUNCtion:CONCurrent OFF')
        if source == 'Voltage':
            # select voltage source
            self.write(':SOURce:FUNCtion VOLTage')
            # set fixed voltage source mode
            self.write(':SOURce:VOLTage:MODE FIXED')
            # set source range
            if range == 'auto': self.write(':SOURce:VOLTage:RANGe:AUTO 1')
            else: self.write(':SOURce:VOLTage:RANGe %g' % range)
            # set source protection (won't go above this level)
            if range != 'auto': self.write(':SOURce:VOLTage:PROTection %g' % (2*range))
            else: self.write(':SOURce:VOLTage:PROTection %g' % VMAX)
            # select current measurement
            self.write(':SENSE:FUNC "CURR"')
            # and set a IMAX compliance..
            self.write(':SENSe:CURRent:PROTection %g' % IMAX)
        elif source == 'Current':
            # select current source
            self.write(':SOURce:FUNCtion CURRent')
            # set fixed current source mode
            self.write(':SOURce:CURRent:MODE FIXED')
            # set source range
            if range == 'auto':
                self.write(':SOURce:CURRent:RANGe:AUTO 1')
            else:
                self.write(':SOURce:CURRent:RANGe %g' % range)
            # set source protection (won't go above this level)
            #if range != 'auto': 
            #    print self.ask(':SOURce:CURRent:PROTection %g' % (2*range))
            #else: 
            #    print self.ask(':SOURce:CURRent:PROTection %g' % IMAX)
            # select voltage measurement
            self.write(':SENSE:FUNC "VOLT"')
            # and set a VMAX compliance..
            self.write(':SENSe:VOLTage:PROTection %g' % VMAX)

        self.source = source
        self.source_range = range
        self.output_on = False
        self.source_level = 0

    
    #---------------------------------
    
    def write(self, cmd):
        out = str(cmd + "\r")
        if self.debug: print 'WRITE: %s' % repr(out)
        #self.ser_io.write(out)
        #self.ser_io.flush()
        self.serial.write(out)
        self.serial.flush()
    
    """def readser(self):
        out =  self.ser_io.read(100) #self.ser_io.readline()
        #out = self.serial.read(100)
        #out = self.serial.readline()
        self.serial.flush()
        if self.debug: print ' READ: %s' % repr(out)
        return out
    """   
        
    def readser(self):
        response = ''
        tstart = time.time()
        while True:
            char = self.serial.read()
            response += char
            #print 'SerialInstrument.read : response = %s, char = %s' % (response, char)
            if char == "\r": break
            if time.time() - tstart > 0.5:
                print '****SerialInstrument.read : timed out..'
                break
        out = response.rstrip("\r")
        if self.debug: print ' READ: %s' % repr(out)
        return out

            
    def ask(self, cmd):
        if self.debug: print "ASK"
        self.write(cmd)
        return self.readser()
        
    #---------------------------------
        
    def set_source_level(self, level):
        # make sure source level is within +-range
        if self.source_range != 'auto':
            level = min(self.source_range, max(-self.source_range, level))
        self.source_level = level
        if self.source == 'Voltage':
            self.write(':SOURce:VOLTage:LEVel %g' % level)
        elif self.source == 'Current':
            self.write(':SOURce:CURRent:LEVel %g' % level)
    
    def set_source_range(self, range):
        self.source_range = range
        if self.source == 'Voltage':
            if range == 'auto': self.write('SOURCe:VOLTage:RANGe:AUTO 1')
            else: self.write('SOURCe:VOLTage:RANGe %g' % range)
        elif self.source == 'Current':
            if range == 'auto': self.write('SOURCe:CURRent:RANGe:AUTO 1')
            else: self.write('SOURCe:CURRent:RANGe %g' % range)
    
    def set_measure_compliance(self, compliance):
        self.measure_compliance = compliance
        if self.source == 'Voltage':
            self.write(':SENSe:CURRent:PROTection %g' % compliance)
        elif self.source == 'Current':
            self.write(':SENSe:VOLTage:PROTection %g' % compliance)
    
    def set_measure_range(self, range):
        self.measure_range = range
        if self.source == 'Voltage':
            if range == 'auto': self.write(':SENSe:CURRent:Range:AUTO 1')
            else: self.write(':SENSe:CURRent:RANGe %g' % range)
        elif self.source == 'Current':
            if range == 'auto': self.write(':SENSe:VOLTage:Range:AUTO 1')
            else: self.write(':SENSe:VOLTage:RANGe %g' % range)
            
    def status(self):
        d = {}
        d['name'] = self.name
        d['source'] = self.source
        d['source_range'] = self.source_range
        d['debug'] = self.debug
        d['measure_range'] = self.measure_range
        d['measure_compliance'] = self.measure_compliance
        d['source_level'] = self.source_level
        return d
    
    def start(self):
        if not(self.output_on):
            self.write(':OUTPut ON')
            self.output_on = True
    
    def stop(self):
        if self.output_on:
            self.write(':OUTPut OFF')
            self.output_on = False

    def read(self):
        """return  current, voltage, time from keithley"""
        if not(self.output_on):
            print 'keithley.read called with self.output_on == False'
            return None
        
        measurementString = self.ask(':READ?')
        measurement = measurementString.split(',')
        if self.debug: print measurement
        voltageReading = float(measurement[0])
        currentReading = float(measurement[1])
        resistanceReading = float(measurement[2]) #not sure about this
        timeReading = float(measurement[3])
        statusReading = float(measurement[4]) # not sure about this
        return currentReading, voltageReading,  timeReading
            
    def close(self):
        self.stop()
        self.serial.close()

# TEST CASE
"""
if __name__ == '__main__':
    print "======== __init__ ============"
    keithley = Keithley2400(source='Voltage', range=5, port="/dev/tty.usbserial-FTVNWTWUA", debug=True)
    print "======== set_source_level ============"
    keithley.set_source_level(1.22)
    #keithley.ask("*IDN?")
    print "======== start ============"
    keithley.start()
    print "======== CONF? ============"
    keithley.ask(":CONF?")
    print "======== measure ============"
    print keithley.ask(":MEAS?")    
    print "======== read ============"
    current, voltage, t0, = keithley.read()
    print 'current = %g, voltage = %g, resistance = %g, time = %g' % (current, voltage, voltage/current, t0)
    keithley.close()
    
"""
