from ScopeFoundry import Measurement
import pyqtgraph as pg
from qtpy import QtWidgets
from qtpy import QtGui
from skimage.io import imsave

from ScopeFoundry.helper_funcs import load_qt_ui_file, sibling_path
import time

class GenICamLiveMeasure(Measurement):
    
    name= 'genicam_live'
    
    def setup(self):
        pass
    
    
    def setup_figure(self):
        
        self.ui = QtWidgets.QWidget()
        l = QtWidgets.QGridLayout()
        self.ui.setLayout(l)
        
        l.addWidget(self.settings.New_UI(), 0,0)
        
        self.hw = self.app.hardware['genicam']
        l.addWidget(self.hw.settings.New_UI(), 1,0)
        
        
        self.graph_layout = pg.GraphicsLayoutWidget()
        self.plot = self.graph_layout.addPlot()
        self.img_item = pg.ImageItem()
        self.plot.addItem(self.img_item)
        self.plot.setAspectLocked(lock=True, ratio=1)
        
        self.hist_lut = pg.HistogramLUTItem(self.img_item)
        self.graph_layout.addItem(self.hist_lut)
        
        l.addWidget(self.graph_layout, 0,1,2,1)

        l.setColumnStretch(0,0)
        l.setColumnStretch(1,1)

    def run(self):
        self.hw.settings['connected'] = True
        
        """        ia = self.hw.ia
        ia.start_image_acquisition()
        
        IMAGE_BUFFER_SIZE = 1000
        self.img_buffer = []
        
        try:
            t0 = time.time()
            ts0 = 0
            while not self.interrupt_measurement_called:
#                 #time.sleep(0.1)
#                 ts, im = self.hw.fetch_image(return_timestamp=True) 
#                 self.last_img = im.swapaxes(0,1)#.copy()
#                     #imsave('test.png', im)
#                 dt = time.time() - t0
#                 dts = ts*1e-9 - ts0
#                 if dt == 0:
#                     fps = 0
#                     fps2 = 0
#                 else:
#                     fps = 1/dt
#                     fps2 = 1/dts
#                 #print("dt", dt, 'fps', fps, 'fps2', fps2)
#                 t0 = time.time()
#                 ts0 = ts*1e-9
#                 #print(im.flags.f_contiguous, self.last_img.flags.f_contiguous)
                    
                ts, im = self.hw.fetch_image(return_timestamp=True) 
                        
                self.last_img = im.swapaxes(0,1)#.copy()                    
                self.img_buffer.append(self.last_img)
                if len(self.img_buffer) > IMAGE_BUFFER_SIZE:
                    self.img_buffer = self.img_buffer[-IMAGE_BUFFER_SIZE:]

        finally:
            ia.stop_image_acquisition()
"""            
            
        while not self.interrupt_measurement_called:
            time.sleep(0.1)


    def update_display(self):
        #return
        self.display_update_period = 1/15.
        
        #print(self.last_img.dtype)
        #return
        #self.img_item.setImage(self.last_img.sum(axis=-1), autoLevels=False)
        #self.img_item.setImage(self.last_img[::2,::2], autoLevels=False)
        self.img_item.setImage(self.hw.img_buffer.pop(0)[::4,::4], autoLevels=False)
        #print(len(self.img_buffer))
    