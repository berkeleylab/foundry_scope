from ScopeFoundry import BaseMicroscopeApp
from ScopeFoundryHW.genicam.genicam_hw import GenICamHW
from ScopeFoundryHW.genicam.genicam_live_measure import GenICamLiveMeasure

class GenICamTestApp(BaseMicroscopeApp):
    
    name = 'genicam_test_app'
    
    def setup(self):
        hw = self.add_hardware(GenICamHW(self))
        
        self.add_measurement(GenICamLiveMeasure(self))
                
if __name__ == '__main__':
    import sys
    app = GenICamTestApp(sys.argv)
    app.exec_()