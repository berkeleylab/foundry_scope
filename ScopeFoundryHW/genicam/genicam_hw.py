from ScopeFoundry import HardwareComponent
import time

default_features = {
    # lq_name: ('category', 'feature_name', dtype)
    'exp_mode': ('AcquisitionControl', 'ExposureMode', 'enum'),
    'exp_auto': ('AcquisitionControl', 'ExposureAuto', 'enum'),
    'exp_time': ('AcquisitionControl', 'ExposureTime', 'float'),
    #'exp_time_abs': ('AcquisitionControl', 'ExposureTimeAbs', 'float'),
    'frame_rate': ('AcquisitionControl', 'AcquisitionFrameRate', 'float'),
    'AutoExposureTimeLowerLimit': ('AcquisitionControl', 'AutoExposureTimeLowerLimit', 'float'),
     'pixel_format': ('ImageFormatControl', 'PixelFormat', 'enum'),
#     ia.remote_device.node_map.PixelFormat
    }

lq_dtype_map = {
    'enum': str,
    'float': float
    }

class GenICamHW(HardwareComponent):
    
    name = 'genicam'
    

    def __init__(self, app, debug=False, name=None, features=default_features):
        self.features = features
        HardwareComponent.__init__(self, app, debug=debug, name=name)
    
    def setup(self):
        
        self.settings.New('cti_file', dtype=str, 
                          initial=r"C:\Program Files\FLIR Systems\Spinnaker\bin64\vs2015\FLIR_GenTL_v140.cti")
        
        self.settings.New('camera_id', dtype=int, initial=0)
        
        
        for lq_name, (node_name, feature_name, dtype) in self.features.items():
            print(lq_name, (node_name, feature_name, dtype))
            
            
            lq_dtype = lq_dtype_map[dtype]
            if dtype == 'enum':
                choices = ['?','?']
            else:
                choices = None
            self.settings.New(lq_name, dtype=lq_dtype, choices=choices)
        
    
    def connect(self):
        
        from harvesters.core import Harvester
        from genicam.genapi import NodeMap
        from genicam.genapi import EInterfaceType, EAccessMode, EVisibility
        _readable_access_modes = [EAccessMode.RW, EAccessMode.RO]

        self.h = h = Harvester()

        h.add_cti_file(self.settings['cti_file'])
        h.update_device_info_list()
        
        self.features_objs = dict()
        
        
        print(h.device_info_list)
        
        self.ia = ia = h.create_image_acquirer(self.settings['camera_id'])

#         for f in ia.remote_device.node_map.AcquisitionControl.features:
#             print(f.node.name, EInterfaceType(f.node.principal_interface_type))
        
        for lq_name, (cat_name, feature_name, dtype) in self.features.items():
            lq = self.settings.get_lq(lq_name)
            print(lq_name, (cat_name, feature_name, dtype))
            category = getattr(ia.remote_device.node_map, cat_name)
            print(category.node.name)
            for f in category.features:
                if f.node.name == feature_name:
                    break
        
            self.features_objs[lq_name] = f
            print("+"*10, lq_name, f.node.name,  EAccessMode(f.node.get_access_mode()))
            interface_type = f.node.principal_interface_type
            if f.node.get_access_mode() not in _readable_access_modes:
                print(f.node.name, 'Not Readable', EAccessMode(f.node.get_access_mode()))
            elif interface_type == EInterfaceType.intfIEnumeration:
                print(f.node.name, 'ENUM')
                for item in f.entries:
                    print('\t', item.symbolic, item.value, f.value)
                choices = [item.symbolic for item in f.entries]
                lq.change_choice_list(choices)
                lq.update_value(f.value)
            elif interface_type == EInterfaceType.intfIFloat:
                print(f.node.name, 'FLOAT', f.value)
                lq.update_value(f.value)
                lq.change_min_max(f.min, f.max)
                
            if f.node.get_access_mode() == EAccessMode.RW:
                lq.connect_to_hardware(read_func =lambda f=f: f.value, 
                                       write_func=lambda x, f=f: f.set_value(x))
                lq.reread_from_hardware_after_write = True
            else:
                lq.connect_to_hardware(read_func =lambda f=f: f.value)

            if f.node.get_access_mode() == EAccessMode.RW:
                lq.change_readonly(False)
            elif f.node.get_access_mode() == EAccessMode.RO:
                lq.change_readonly(True)

            print(lq_name, f.node.name, EInterfaceType(f.node.principal_interface_type).name, repr(f.value))


        self.img_buffer = []
        self.IMAGE_BUFFER_SIZE = 10
        time.sleep(1.0)
        self.ia.start_image_acquisition()


    def disconnect(self):
        
        if hasattr(self, 'ia'):
            self.ia.stop_image_acquisition()
            self.ia.destroy()
        
        if hasattr(self, 'h'):
            self.h.reset()
        
        
    def fetch_image(self, return_timestamp=False):
        with self.ia.fetch_buffer() as buffer:
            c = buffer.payload.components[0]
            #print(c.data.shape)
            im = c.data.copy()
        im = im.reshape(c.height, c.width, -1)
            #print(im.shape, im.min(), im.max())
            #print(buffer.timestamp)
            #im = im.copy()
        if return_timestamp:
            return buffer.timestamp, im
        return im
    
    def threaded_update(self):
        ts, im = self.fetch_image(return_timestamp=True) 
                
        self.last_img = im.swapaxes(0,1)#.copy()                    
        self.img_buffer.append(self.last_img)
        if len(self.img_buffer) > self.IMAGE_BUFFER_SIZE:
            self.img_buffer = self.img_buffer[-self.IMAGE_BUFFER_SIZE:]
        
        