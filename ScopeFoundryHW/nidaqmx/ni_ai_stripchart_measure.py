from ScopeFoundry import Measurement
import pyqtgraph as pg
import time
import numpy as np

class NI_AI_StripChartMeasure(Measurement):
    
    name ='ni_ai_stripchart'
    
    def setup(self):
        self.settings.New('offset', dtype=float, initial=0.0, unit='V')
        
    
    def setup_figure(self):
        
        self.ui = self.plot = pg.PlotWidget()
        
    
    def pre_run(self):
        self.plot.clear()
        if hasattr(self, 'legend'):
            self.legend.scene().removeItem(self.legend)
        self.legend = self.plot.addLegend()

        self.ni_ai = self.app.hardware['ni_ai']
        
        self.lines = dict()
        self.lines_avg = dict()
        
        for i, (chan, chan_name) in enumerate(self.ni_ai.channels):
            
            self.lines[chan_name] = self.plot.plot(pen=pg.intColor(i), name=chan_name)
            self.lines_avg[chan_name] = self.plot.plot(pen=pg.intColor(i))
        
        
    def run(self):
        
        while not self.interrupt_measurement_called:
            time.sleep(0.1)
    
    def update_display(self):
#         
#         x = self.app.hardware['ni_ai'].ai_buffer[0,:]
#         self.line.setData(x)
#         
#         x = self.app.hardware['ni_ai'].ai_avg_buffer[0,:]
#         self.line2.setData(x)

        x = (-np.arange(self.ni_ai.ai_buffer.shape[1]) + self.ni_ai.buffer_i) % self.ni_ai.buffer_size
        x = x/self.ni_ai.rate
        
        for i, (chan, chan_name) in enumerate(self.ni_ai.channels):
            
            y = self.ni_ai.ai_buffer[i,:]
            self.lines[chan_name].setData(x, y - i*self.settings['offset'])
            self.lines[chan_name].setVisible(False)

            y = self.ni_ai.ai_avg_buffer[i,:]
            self.lines_avg[chan_name].setData(x, y - i*self.settings['offset'])
        
#         from scipy.ndimage.filters import uniform_filter1d
#         y = uniform_filter1d(x, size=100)
#         self.line2.setData(y)
#         