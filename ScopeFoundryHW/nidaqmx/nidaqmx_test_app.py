from ScopeFoundry.base_app import BaseMicroscopeApp
#import logging

#logging.basicConfig(level=logging.DEBUG)

class NIDAQmxTestApp(BaseMicroscopeApp):
    
    name = 'nidaqmx_test_app'
    
    def setup(self):

        from ScopeFoundryHW.nidaqmx.ni_ai_hw import NI_AI_HW
        hw = self.add_hardware(NI_AI_HW(self, channels=[("ai4", "bias_neg"), 
                                                   ("ai0", "bias_pos"),
                                                   ("ai2", "det1_sum"), 
                                                   ("ai1", 'det1_X'),
                                                   ("ai5", 'det1_Y')]))
        '''hw = self.add_hardware(NI_AI_HW(self, channels=[("ai5", "bias_neg"), 
                                                   ("ai13", "bias_pos"),
                                                   ("ai4", "det1_sum"), 
                                                   ("ai3", 'det1_X'),
                                                   ("ai11", 'det1_Y'),
                                                   ("ai14", "det2_sum"), 
                                                   ("ai7", 'det2_X'),
                                                   ("ai15", 'det2_Y')]))'''
        
        hw.settings['input_mode'] = "RSE"
        
        from ScopeFoundryHW.nidaqmx.ni_ai_stripchart_measure import NI_AI_StripChartMeasure
        self.add_measurement(NI_AI_StripChartMeasure(self))
        
if __name__ == '__main__':
    import sys
    app = NIDAQmxTestApp(sys.argv)
    sys.exit(app.exec_())