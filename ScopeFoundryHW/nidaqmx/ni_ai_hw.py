from ScopeFoundry.hardware import HardwareComponent
import numpy as np
import time
try:
    import nidaqmx
    import nidaqmx.constants as mx
except Exception as err:
    print("Failed to load nidaqmx {}".format(err))

class NI_AI_HW(HardwareComponent):
    
    name = 'ni_ai'
    
    def __init__(self, app, debug=False, name=None, channels=None, n_channels=4 ):
        """
        Channels is a list of tuples like: [ ("ai1", "signal_x"), ("ai13", "signal_y"), ... ]
        if No channel list is defined, defaults to [ ("ai1", "ai1"), ("ai2", "ai2"), ... n_channels ]
        """
        
        
        self.channels = channels
        if self.channels is None:
            self.channels = [("ai{}".format(i), "ai{}".format(i)) for i in range(n_channels)]
            
        HardwareComponent.__init__(self, app, debug=debug, name=name)

    
    
    def setup(self):
        
        S = self.settings
        
        S.New("ni_device", dtype=str, initial='Dev1')
        
        
        S.New('input_mode', dtype=str, initial='DEFAULT', 
              choices=('DEFAULT', 'RSE', 'NRSE', 'DIFFERENTIAL', 'PSEUDODIFFERENTIAL'))

        S.New('rate', dtype=float, initial= 1000. , unit='Hz')
        S.New('chunk_size', dtype=int, initial=100, unit='samples')
        S.New("avg_time", dtype=float, initial=0.1, unit='s', si=True)
        S.New("buffer_time", dtype=float, initial=10.0, unit='s', si=True)
        

        for chan, chan_name in self.channels:
            S.New(chan_name, dtype=float, spinbox_decimals=10, unit='V ({})'.format(chan), ro=True)
            
        
            
    def connect(self):
        S = self.settings
        
        self.chunk_size = S['chunk_size']
        self.rate = S['rate']
        #self.buffer_size = self.chunk_size*100 # Buffer size probably should be an integer multiple of chunksize
        self.buffer_size = int(S['buffer_time']*S['rate'])
        self.buffer_i = 0
        
        
        # Channel path should look like "Dev1/ai3, Dev1/ai13"
        self.ai_chan_path = ", ".join(
            ["{d}/{chan}".format(d=S['ni_device'], chan=chan)
                for chan, name in self.channels])
        print(self.ai_chan_path)
        
        
        # Create and define task
        ai_task = self.ai_task = nidaqmx.Task()
        ai_task.ai_channels.add_ai_voltage_chan(physical_channel=self.ai_chan_path, 
                                                name_to_assign_to_channel='',
                                                terminal_config=mx.TerminalConfiguration[S['input_mode']], 
                                                min_val=-10.0, max_val=+10.0,
                                                units=mx.UnitsPreScaled.VOLTS,
                                                custom_scale_name='')
        self.ai_chan_count = ai_task.number_of_channels
        
        # Create Ring Buffers
        self.ai_buffer = np.zeros( (self.ai_chan_count, self.buffer_size), dtype=float)
        self.ai_avg_buffer = np.zeros( (self.ai_chan_count, self.buffer_size), dtype=float)
        self.ai_time_buffer = np.zeros( self.buffer_size, dtype=float)


        # Configure continuous acquisition timing and register callback
        
        self.ai_task.timing.cfg_samp_clk_timing(rate=self.rate, source="", 
                                                active_edge=mx.Edge.RISING, 
                                                sample_mode=mx.AcquisitionType.CONTINUOUS, 
                                                samps_per_chan=self.chunk_size*10 ) # samps_per_chan defines NI buffersize

        self.ai_task.register_every_n_samples_acquired_into_buffer_event(
            sample_interval=self.chunk_size,
            callback_method=self.ai_callback)
        
        self.ai_task.start()

    def ai_callback(self, task_handle, every_n_samples_event_type, 
                    number_of_samples, callback_data):

        # Read and store data from last chunk
        try:  
            data = np.array(self.ai_task.read(self.chunk_size,timeout=1.1*self.chunk_size/self.rate))
        except nidaqmx.DaqError as err:
            self.disconnect()
            return 1

        new_data_index = (self.buffer_i + np.arange(self.chunk_size)) % self.buffer_size
        self.ai_buffer[:,new_data_index] = data
        self.ai_time_buffer[new_data_index] = \
            np.arange(self.buffer_i, self.buffer_i+self.chunk_size) / self.rate
        
        #print ("ai_callback", self.buffer_i, data.shape, np.mean(data))
        
        self.buffer_i += self.chunk_size

        
        N_avg =  max(1, int(self.settings['avg_time']*self.rate))
        #print("N_avg", N_avg)
        
        
        """
        ai_buffer is a ring-buffer with buffer_i as its leading edge
        the data at ai_buffer[buffer_i] is the latest value received,
        while ai_buffer[buffer_i-1] is the previous result.
        
        Note that buffer_i does not reset to zero, but continues counting up
        Therefore we need to modulo by buffer_size
        """ 
        
        
        # Storing a smoothed version of data in ai_avg_buffer
        # TODO Fix this averaging, its not quite accurate and has issues at edges
        # Discussion of running averages on stackoverflow:
        # https://stackoverflow.com/questions/13728392/moving-average-or-running-mean/43200476#43200476
        
        if N_avg > self.chunk_size:
            avg_data_index = (self.buffer_i + np.arange(-4*N_avg,0)) % self.buffer_size
        else:
            avg_data_index = (self.buffer_i + np.arange(-2*self.chunk_size,0)) % self.buffer_size

        from scipy.ndimage.filters import uniform_filter1d
        avg_buf = uniform_filter1d(self.ai_buffer[:,avg_data_index], size=N_avg, axis=1, mode='reflect')
        self.ai_avg_buffer[:,avg_data_index[-2*self.chunk_size:-self.chunk_size]] = avg_buf[:,-2*self.chunk_size:-self.chunk_size]

#         def running_mean(x, N):
#             cumsum = np.cumsum(np.insert(x, 0, 0, axis=1),axis=1) 
#             return (cumsum[:,N:] - cumsum[:,-N]) / float(N)
#         
#         avg_buf = running_mean(self.ai_buffer[:,avg_data_index], N_avg)
#         self.ai_avg_buffer[:,avg_data_index] = avg_buf
        
        
        # Mean of last N_avg gets stored in settings (LQ's)
        avg_data_index = (self.buffer_i + np.arange(-N_avg,0)) % self.buffer_size
        #print("avg_data_index", avg_data_index)
        avg_chans = self.ai_buffer[:,avg_data_index].mean(axis=1)
        #print('avg_chans', avg_chans.shape, avg_chans)
        for i, (chan, chan_name) in enumerate(self.channels):
            #print(i, name)
            #self.settings[chan_name] = data[i,:].mean()
            self.settings[chan_name] = avg_chans[i]

        return 0

    def read_ai_single(self):
        data = self.ai_task.read(number_of_samples_per_channel=1, timeout=0.010)
        #print(data)
        ## Update LQ's 
        for i, (chan, chan_name) in enumerate(self.channels):
            #print(i, name)
            self.settings[chan_name] = data[i][0]
        return data

    def disconnect(self):
        #disconnect logged quantities from hardware
        self.settings.disconnect_all_from_hardware()
        

        #disconnect hardware
        if hasattr(self, 'ai_task'):
            self.ai_task.close()
            # clean up hardware object
            del self.ai_task

    def get_history_by_name(self, chan_name, avg=False, past_time=None, time_from_now=False):
        
        if past_time is None:
            N_buf = self.buffer_size
        else:
            N_buf = int(past_time*self.rate)
            
            
        data_index = np.arange(self.buffer_i-N_buf, self.buffer_i) % self.buffer_size
        
        
        t = self.ai_time_buffer[data_index]
        
        if time_from_now:
            t = t -  self.ai_time_buffer[self.buffer_i%self.buffer_size]

        for i, (chan, cname) in enumerate(self.channels):
            if cname == chan_name:
                chan_i = i
        
        if avg:
            y = self.ai_avg_buffer[chan_i,data_index]
        else:
            y = self.ai_buffer[chan_i,data_index]

        return t, y
        
        
        