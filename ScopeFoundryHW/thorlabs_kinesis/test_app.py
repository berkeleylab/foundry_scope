from ScopeFoundry.base_app import BaseMicroscopeApp
import logging

class ThorlabsKinesisTestApp(BaseMicroscopeApp):
    
    name="thorlabs_kinesis_test_app"
    
    def setup(self):
        from ScopeFoundryHW.thorlabs_kinesis.thorlabs_benchtop_piezo_hw import ThorlabsBenchtopPiezoHW
        self.add_hardware(ThorlabsBenchtopPiezoHW(self, ax_names='xyz'))
        
#         from ScopeFoundryHW.thorlabs_stepper_motors.thorlabs_stepper_raster import ThorlabsStepperDelay2DScan
#         self.add_measurement(ThorlabsStepperDelay2DScan(self))
        
if __name__ == '__main__':
    import sys
    app = ThorlabsKinesisTestApp(sys.argv)
    sys.exit(app.exec_())    
        