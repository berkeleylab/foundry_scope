from ScopeFoundry import HardwareComponent
from .thorlabs_benchtop_piezo import ThorlabsBenchtopPiezo
from collections import OrderedDict
import time

class ThorlabsBenchtopPiezoHW(HardwareComponent):
    
    name = 'thorlabs_benchtop_piezo'
    
    
    def __init__(self, app,debug=False, name=None, ax_names='xyz'):
        self.ax_names = ax_names
        self.ax_dict = OrderedDict() # dictionary ax_name: ax_num
        for axis_index, axis_name in enumerate(self.ax_names):
            axis_num = axis_index +1
            # skip hidden axes
            if not axis_name or axis_name == "_" or axis_name is None:
                continue
            self.ax_dict[axis_name] = axis_num
            
        HardwareComponent.__init__(self, app, debug=debug, name=name)
    
    def setup(self):
        
        
        for axis in self.ax_dict.keys():
            
            self.settings.New(axis + "_enable", dtype=bool, initial=True)
            
            self.settings.New(axis + "_position", 
                               dtype=float,
                               ro=True,
                               unit='um',
                               spinbox_decimals=6,
                               si=False
                               )
            
            
            self.settings.New(axis + "_target_position",
                                dtype=float,
                                ro=False,
                                vmin=0,
                                vmax=20,
                                unit='um',
                                spinbox_decimals=6,
                                spinbox_step=0.010,
                                si=False)

            self.settings.New(axis +"_step_convert", dtype=float, 
                              spinbox_decimals=0, unit="step/um", initial=32767/20.)
            
            self.add_operation("Zero_Axis_" + axis, lambda a=axis: self.zero_axis(a))
            
            
        self.settings.New('device_num', dtype=int, initial=0)
        self.settings.New('serial_num', dtype=str, initial="")
        


    def connect(self):
        S = self.settings
        
        p = self.dev = ThorlabsBenchtopPiezo(
                        dev_num=S['device_num'],
                        serial_num=S['serial_num'],
                        debug=S['debug_mode'])
        
        

        for axis_name, axis_num in self.ax_dict.items():
            
            scale = self.settings[axis_name + "_step_convert"] # step/mm
            
            en = self.settings.get_lq(axis_name + "_enable")
            en.connect_to_hardware(
                write_func = lambda enable, a=axis_num: 
                                self.dev.write_chan_enable(a, enable))
            en.write_to_hardware()
            
            self.settings.get_lq(axis_name + "_position").connect_to_hardware(
                lambda a=axis_num, scale=scale: 
                    self.dev.get_actual_position(a) / scale)
    
            self.settings.get_lq(axis_name + "_target_position").connect_to_hardware(
                write_func = lambda new_pos, a=axis_num, scale=scale: 
                                self.dev.set_position(a, int(round(scale*new_pos))))


        
        for i in range(1,p.num_chans+1):
            p.read_message_queue(i)
            print("Aa",i)
            #p.write_chan_enable(i)
            
            #p.set_zero(i)
            time.sleep(0.1)
            
            p.set_position_control_mode(i, 'open_loop')
            time.sleep(0.3)

            
            p.set_position_control_mode(i, 'closed_loop')
            time.sleep(0.1)
            print("B",i)
            p.get_position_control_mode(i)
             
            print(i, "pos",p.get_position(i), 'actual', p.get_actual_position(i))
            


        #time.sleep(1)
        self.read_from_hardware()
        
        S['serial_num'] = self.dev.get_serial_num()

    
    def disconnect(self):
        self.settings.disconnect_all_from_hardware()
        
        if hasattr(self, 'dev'):
            self.dev.close()
            del self.dev
        
        
    def zero_axis(self, ax_name):
        print("zero_axis", ax_name)
        self.dev.set_zero(self.ax_dict[ax_name])
    
    
    
    def threaded_update(self):
        self.read_position_from_hardware()
        time.sleep(0.1)
        
    def read_position_from_hardware(self):
        for axis_name, axis_num in self.ax_dict.items():
            self.settings.get_lq(axis_name + "_position").read_from_hardware()
            
    
    def read_message_queue(self,ax_name):
        return self.dev.read_message_queue(self.ax_dict[ax_name])
            