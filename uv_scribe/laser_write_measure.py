from ScopeFoundry import Measurement, h5_io
import PyDAQmx as mx
import time
import numpy as np
from ctypes import c_uint32, byref
from qtpy import QtWidgets, QtCore
import pyqtgraph as pg
from scipy import signal

class LaserWriteMeasure(Measurement):
    
    name = 'laser_write'
    
    def setup(self):
        
        self.settings.New('ni_device', dtype=str, initial='Dev1')
        self.settings.New('ao_chan', dtype=str, initial='ao0:3') # XYZL
        self.settings.New('ai_chan', dtype=str, initial='ai16:18') # XYZ
        self.settings.New('data_rate', dtype=float, initial=400*100, unit='Hz')
        self.settings.New('pulse_rate', dtype=float, initial=400, unit='Hz')
        self.settings.New('live_img', dtype=bool)
        
        self.settings.New('scale', dtype=float, si=True, unit='m/V', initial=200.e-6/10.)
        self.settings.New('feed_rate', dtype=float, si=True, unit='m/s', initial=80.e-6)
        self.settings.New('rapid_rate', dtype=float,si=True,  unit='m/s', initial=1000.e-6)
        
        

        img_scale = self.settings.New("img_scale", dtype=float, si=True, unit='m', initial=50.e-6)
        #img_scale.add_listener(self.on_new_img_scale)

        self.settings.New("img_center_x", dtype=float, unit='%', initial=50)
        self.settings.New("img_center_y", dtype=float, unit='%', initial=50)

        self.settings.New("img_flip_x", dtype=bool, initial=False)
        self.settings.New("img_flip_y", dtype=bool, initial=False)
        
 
        self.add_operation('compute_ao_data', self.compute_ao_data)

        self.add_operation('clear_and_plot', self.clear_and_plot)
        

    def setup_figure(self):
        
        self.ui = QtWidgets.QWidget()
        self.ui_layout = QtWidgets.QHBoxLayout()
        self.ui.setLayout(self.ui_layout)
        
        self.ui_settings = self.settings.New_UI()
        self.ui_layout.addWidget(self.ui_settings)
    
        self.graph_layout = pg.GraphicsLayoutWidget()
        self.graph_layout.clear()
        self.ui_layout.addWidget(self.graph_layout)
        
        self.live_img_update_timer = QtCore.QTimer(self)
        self.live_img_update_timer.timeout.connect(self._on_live_img_timer)        
        self.live_img_update_timer.start(100)
        
    def _on_live_img_timer(self):
        if self.settings['live_img']:
            buf = self.app.hardware['flircam'].img_buffer
            if len(buf)>0:
                im = buf.pop(0).copy()#.swapaxes(0,1)
                if self.settings['img_flip_x']:
                    im = im[::-1,:]
                if self.settings['img_flip_y']:
                    im = im[:,::-1]
                
                    
                self.live_img_item.setImage(image=im, 
                                            autoLevels=False)

                # get rectangle
                im_aspect = im.shape[1]/im.shape[0]
                stageS = self.app.hardware["mcl_xyz_stage"].settings
                x,y,z =  stageS["x_position"]*1e-6, stageS["y_position"]*1e-6, stageS["z_position"]*1e-6
    
                scale = self.settings['img_scale']
                S = self.settings
                rect= pg.QtCore.QRectF(y - S['img_center_x'] * scale / 100,
                                        x - S['img_center_y'] * scale * im_aspect / 100,
                                        scale,
                                        scale * im_aspect)
                self.live_img_item.setRect(rect)


    def clear_and_plot(self):
        scale = self.settings['scale'] # m/V
        
        self.graph_layout.clear()

        self.xy_plot = self.graph_layout.addPlot(0,0)
        self.xy_plot.setAspectLocked(lock=True, ratio=1)
        self.xy_plot.setLabels(left=('mcl y', 'm'), bottom=('mcl x', 'm'))
        self.live_img_item = pg.ImageItem()
        self.xy_plot.addItem(self.live_img_item)
        
        stageS = self.app.hardware["mcl_xyz_stage"].settings
        x,y,z =  stageS["x_position"]*1e-6, stageS["y_position"]*1e-6, stageS["z_position"]*1e-6
        x_max = stageS['x_max']*1e-6
        y_max = stageS['y_max']*1e-6
        self.xy_mcl_limits = self.xy_plot.plot([0,x_max,x_max,0,0],
                                               [0,0,y_max,y_max,0], pen='g')
        xc=100e-6
        yc=100e-6
        self.xy_plot.plot([0,x_max,x_max,xc,xc],
                                               [yc,yc,y_max,y_max,0], pen='g')
        
        print('ao_data', self.ao_data.shape, self.ao_data.min(), self.ao_data.max())


        L = self.ao_data[:,3]
        
        self.xy_ao_plotline = self.xy_plot.plot(100e-6 + self.ao_data[:,0]*scale, 
                                                100e-6 + self.ao_data[:,1]*scale)

        self.xy_ao_plotline2 = self.xy_plot.plot(100e-6 + self.ao_data[:,0][L>0]*scale, 
                                                 100e-6 + self.ao_data[:,1][L>0]*scale, pen=None, symbol='o')
        
        
        
        X = self.ai_data[:,0][L>0] *scale
        Y = self.ai_data[:,1][L>0] *scale
        
        
        self.xy_ai_plotline = self.xy_plot.plot(X,Y, pen=None, symbol='o')
        self.xy_ai_plotline2 = self.xy_plot.plot(self.ai_data[:,0]*scale,
                                                 self.ai_data[:,1]*scale, pen='r')
        
        
        


    def compute_ao_data(self):
        self.ao_data = self.merge_ao_segments(
            [self.ao_dwell(x=0, y=0, z=0, time=0.25, laser_on=False)]+
            self.cross(cx=0, cy=0, L=10e-6, feed_rate=None)+
            self.cross(cx=0, cy=5e-6, L=10e-6, feed_rate=None)+
            self.cross(cx=10e-6, cy=5e-6, L=10e-6, feed_rate=None)+
            self.cross(cx=20e-6, cy=0, L=10e-6, feed_rate=None)+
            self.cross(cx=20e-6, cy=20e-6, L=10e-6, feed_rate=None)+
            self.cross(cx=0, cy=20e-6, L=10e-6, feed_rate=None)+
            [self.ao_dwell(x=0, y=0, z=0, time=0.25, laser_on=False)]
            )
        return self.ao_data

    def compute_ao_data_old(self):
        S = self.settings
        
        ## Cross
        
        dt = 1.0/S['data_rate']

        L = 0.5 # 10 um -- scale is 200um/10v --> 20um/V --> 0.5 V 
        L = 1
        vel = 3.0 #V/s
        
        cx = 0
        cy = 0
        
        total_time = L/vel
        N = int(total_time/dt)
        
        segments = [
            (0,0, cx-L/2, cy, False),
            (cx-L/2, cy,     cx+L/2, cy, True),
            (cx+L/2, cy,     cx,    cy+L/2, False),
            (cx    , cy+L/2, cx,    cy-L/2, True),
            (cx,    cy-L/2, 0,0, False)
            ]
        
        x_segments = []
        y_segments = []
        l_segments = []
        
        for x0,y0,x1,y1,laser in segments:
            
            X = np.linspace(x0,x1,N)
            Y = np.linspace(y0,y1,N)
            pulse = np.zeros(N)
            
            if laser:
                dtL = 1/S['pulse_rate']
                n_skip = int(dtL/dt)
                pulse[::n_skip] = 4.0
                
            x_segments.append(X)
            y_segments.append(Y)
            l_segments.append(pulse)

        #print(len(x_segments), x_segments[0].shape, np.concatenate(x_segments).shape)
        X = np.concatenate(x_segments)
        Y = np.concatenate(y_segments)
        pulse = np.concatenate(l_segments)
        
        
        N = len(X)
        self.N_ao = self.N_ai = N
        
        self.ao_data =  np.zeros((N,4), dtype=float)

        self.ao_data[:,0] = X        
        self.ao_data[:,1] = Y        
        self.ao_data[:,3] = pulse
        
        return self.ao_data
    
    def run(self):
        S = self.settings
        
        ### Move to center of mcl stage
        stageS = self.app.hardware["mcl_xyz_stage"].settings
        stageS["x_target"] = 100 #um
        stageS["y_target"] = 100 #um
        time.sleep(0.2)
        

        #### Set up out data

        self.ao_data = self.compute_ao_data()

        self.N_ao = self.N_ai = N = self.ao_data.shape[0]

        
        #### Setup Tasks
        
        self.ao_rate = S['data_rate']
        self.ai_rate = S['data_rate']
        
        # Analog Out
        self.ao_chan_path = S['ni_device'] + '/' + S['ao_chan']
        ao_task = self.ao_task = mx.Task()
        ao_task.CreateAOVoltageChan(self.ao_chan_path, '', -10.0, +10.0, mx.DAQmx_Val_Volts, '')
        ao_task.CfgSampClkTiming("", self.ao_rate, mx.DAQmx_Val_Rising, mx.DAQmx_Val_FiniteSamps, self.N_ao) 
        self.ao_chan_count = task_get_num_chans(ao_task)

        # Analog In
        self.ai_chan_path = S['ni_device'] + '/' + S['ai_chan']
        ai_task = self.ai_task = mx.Task()
        ai_task.CreateAIVoltageChan(self.ai_chan_path, '', mx.DAQmx_Val_Diff, -10.0, +10.0, mx.DAQmx_Val_Volts,'')
        ai_task.CfgSampClkTiming("", self.ai_rate, mx.DAQmx_Val_Rising, mx.DAQmx_Val_FiniteSamps, self.N_ai) 
        self.ai_chan_count = task_get_num_chans(ai_task)

        ### Setup synchronous timing
        
        # Sync everything on Analog Out StartTigger
        start_trig_name = "/" + S['ni_device'] + '/ao/StartTrigger'
        ai_task.CfgDigEdgeStartTrig(start_trig_name, mx.DAQmx_Val_Rising)

        # ### Connect Callbacks        
        # set_n_sample_callback(self.ao_task, self.N_ao, self.on_ao_callback, 'out')
        # set_n_sample_callback(self.ai_task, self.N_ai, self.on_ai_callback, 'in')
        
        # load data in to output buffer
        writeCount = mx.int32(0)
        self.ao_task.WriteAnalogF64(
            numSampsPerChan=self.N_ao, 
            autoStart=False,
            timeout=self.N_ao/self.ao_rate, 
            dataLayout=mx.DAQmx_Val_GroupByScanNumber, 
            writeArray=self.ao_data.flatten(),
            sampsPerChanWritten=mx.byref(writeCount),
            reserved=None)

        
        
        # Start Input Tasks, they will wait for trigger from AO task        
        ai_task.StartTask()
        

        # start output        
        ao_task.StartTask()


        while not self.tasks_done():
            if self.interrupt_measurement_called:
                break
            time.sleep(0.1)
    
        try:
            
            # save data
            self.h5_file = h5_io.h5_base_file(self.app, measurement=self)
            self.h5_filename = self.h5_file.filename
            self.h5_m = h5_io.h5_create_measurement_group(measurement=self, h5group=self.h5_file)
            self.h5_m['ao_data'] = self.ao_data

            
            # readout ai data
            self.ai_data = np.NaN*np.zeros((self.N_ai,self.ai_chan_count), dtype = np.float64)
            read_size = mx.uInt32(self.N_ai*self.ai_chan_count)
            read_count = mx.int32(0)    #returns samples per chan read
                
            #int32 DAQmxReadAnalogF64 (TaskHandle taskHandle, int32 numSampsPerChan, float64 timeout, 
            #    bool32 fillMode, float64 readArray[], uInt32 arraySizeInSamps, int32 *sampsPerChanRead, bool32 *reserved);
            self.ai_task.ReadAnalogF64(
                numSampsPerChan=self.N_ai,
                timeout=0,
                fillMode=mx.DAQmx_Val_GroupByScanNumber,
                readArray=self.ai_data,
                arraySizeInSamps=read_size,
                sampsPerChanRead=mx.byref(read_count),
                reserved=None)

        finally:
            self.h5_m['ai_data'] = self.ai_data
            
            self.h5_file.close()
            self.ai_task.StopTask()
            self.ao_task.StopTask()
            
            self.ai_task.ClearTask()
            self.ao_task.ClearTask()



            print("done")
        


    def set_precomputed_ao_data(self, ao_data):
        self.ao_data = ao_data


    def tasks_done(self):
        #DAQmxIsTaskDone (TaskHandle taskHandle, bool32 *isTaskDone);
        isTaskDone = c_uint32(False);
        mx.DAQmxIsTaskDone(self.ao_task.taskHandle, byref(isTaskDone))
        return bool(isTaskDone.value)
    
    
    
    def abs_linear_move(self, x0,y0,z0, x,y,z, feed_rate=None, laser_on=False):
        
        dt = 1./self.settings['data_rate']
        if feed_rate is None:
            feedrate = self.settings['feed_rate']
        
        X0 = np.array([x0,y0,z0], dtype=float).reshape(3,1)
        X1 = np.array([x,y,z], dtype=float).reshape(3,1)
        path_length = np.linalg.norm(X1-X0)
        N = int((path_length/feedrate) / dt) # number of output datapoints
        T = np.arange(N)*dt # time coordinate of move (starts at zero at start of move)
        XYZ = X0 + np.arange(N).reshape(1,-1)/N * (X1-X0) # or linspace
        
        if laser_on:
            #L = self.laser_pulse_train(T)
            L =self.laser_pulse(N)
        else:
            L = np.zeros_like(T)
        
        XYZ_volts = XYZ.T/self.settings['scale']
        print("xyz_volts", XYZ_volts.shape)
        print("L", L.shape)
        
        
        ao =  np.hstack([XYZ_volts, L.reshape(-1,1)])
        print('abs_linear_move', ao.shape, ao.dtype)
        
        #ao_shape should be (N,4)
        
        return ao
    
    def ao_dwell(self, x,y,z, time, laser_on=False):
    
        dt = 1./self.settings['data_rate']
        N = int(time / dt) # number of output data points
        X0 = np.array([x,y,z], dtype=float).reshape(3,1)
        
        XYZ = np.ones(N).reshape(1,-1)*X0

        if laser_on:
            L =self.laser_pulse(N)
        else:
            L = np.zeros(N, dtype=float)
        
        XYZ_volts = XYZ.T/self.settings['scale']
        ao =  np.hstack([XYZ_volts, L.reshape(-1,1)])

        return ao
        
    def laser_pulse_train(self, T):
        
        pulse_rate = 400 # Hz
        amp = 4.0 # Volt
        y = amp/2*(1+signal.square(2 * np.pi *pulse_rate * T, duty=0.2))
        
        return y
    
    def laser_pulse(self,N):
        dt = 1./self.settings['data_rate']
        pulse = np.zeros(N, dtype=float)
        dtL = 1/self.settings['pulse_rate']
        n_skip = int(dtL/dt)
        pulse[::n_skip] = 4.0 #V
        return pulse


    
    def segmented_move(self, points, feed_rate=None, dwell=0.2):
        """
        points is a list of coordinates to travel to, with either laser on or off
        example:
            points = [ (x0, y0, z0, False), (x1,y1,z1, True) ... ]
            
        returns a list of ao_segments (analog outputs)
        """
        ao_segments = []
        x0, y0, z0, laser_on        = points[0]
        if (not laser_on) and dwell > 0:
            ao_segments.append(
                self.ao_dwell(x0, y0, z0, 
                              time=dwell, laser_on=False))
        
        for i in range(1, len(points)):

            
            x0, y0, z0, _        = points[i-1]
            x1, y1, z1, laser_on = points[i  ]
            
            if dwell > 0:
                ao_segments.append(
                    self.ao_dwell(x0, y0, z0, 
                                  time=dwell, laser_on=False))
            
            ao = self.abs_linear_move(x0, y0,z0, x1, y1, z1, 
                                      feed_rate=feed_rate, laser_on=laser_on)
            ao_segments.append(ao)
            
            if dwell > 0:
                ao_segments.append(
                    self.ao_dwell(x1, y1, z1, 
                                  time=dwell, laser_on=False))
        return ao_segments
            
        
    def cross(self, cx,cy,L, feed_rate=None):
        r = L/2
        points = [
            (cx  ,cy  , 0, False),
            (cx-r,cy  , 0, False),
            (cx+r,cy  , 0, True),
            (cx  ,cy+r, 0, False),
            (cx  ,cy-r, 0, True),
            (cx  ,cy  , 0, False)
            ]
        return self.segmented_move(points, feed_rate)
    
    def merge_ao_segments(self, ao_segments):
        ao = np.vstack(ao_segments)
        # scale = self.settings['scale']
        # ao[:,0] = ao[:,0]*scale # X
        # ao[:,1] = ao[:,1]*scale # Y
        # ao[:,2] = ao[:,2]*scale # Z
        return ao
    
#####################
# Helper Functions
#####################
def set_n_sample_callback(task, n_samples, cb_func, io='in'):
    """
    Setup callback functions for EveryNSamplesEvent
    *cb_func* will be called with when new data is available
    after every *n_samples* are acquired.
    
    cb_func should return 0 on completion
    """
    if io == 'in':
        event_type = mx.DAQmx_Val_Acquired_Into_Buffer        
    elif io == 'out':
        event_type = mx.DAQmx_Val_Transferred_From_Buffer
    
    print(n_samples)
    task.EveryNCallback = cb_func
    task.AutoRegisterEveryNSamplesEvent(
        everyNsamplesEventType=event_type, 
        nSamples=n_samples,
        #options=mx.DAQmx_Val_SynchronousEventCallbacks)
        options=0)
    
def task_get_num_chans(task):
    chan_count = mx.uInt32(0)
    task.GetTaskNumChans(mx.byref(chan_count))
    return chan_count.value


