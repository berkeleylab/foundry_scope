from ScopeFoundry import Measurement
from ScopeFoundry.helper_funcs import load_qt_ui_file, sibling_path

class UVScribeControlPanel(Measurement):
    
    name = 'uv_scribe'
    
    def setup(self):
        
        self.ui = load_qt_ui_file(sibling_path(__file__, 'uv_scribe_control_panel.ui'))
        HW = self.app.hardware
        
        HW['laser_shutter'].settings.connected.connect_to_widget(
            self.ui.laser_in_shutter_hw_checkBox)
        HW['laser_shutter'].settings.named_position.connect_to_widget(
            self.ui.laser_in_shutter_comboBox)

        HW['asi_stage'].settings.connected.connect_to_widget(
            self.ui.asi_hw_checkBox)

        HW['mcl_xyz_stage'].settings.connected.connect_to_widget(
            self.ui.mcl_hw_checkBox)

        HW['power_wheel'].settings.connected.connect_to_widget(
            self.ui.power_wheel_hw_checkBox)
        HW['power_wheel'].settings.target_position.connect_to_widget(
            self.ui.power_wheel_doubleSpinBox)

        HW['nd_wheel'].settings.connected.connect_to_widget(
            self.ui.nd_wheel_hw_checkBox)

        HW['nd_wheel'].settings.named_position.connect_to_widget(
            self.ui.nd_wheel_named_position_comboBox)

        HW['flircam'].settings.connected.connect_to_widget(
            self.ui.flircam_hw_checkBox)
        HW['flircam'].settings.exposure.connect_to_widget(
            self.ui.flircam_exposure_doubleSpinBox)
        


#'asi_stage', 'mcl_xyz_stage', 
# 'ni_digital_out', 'optotune_lens',
#'dynamixel_servos', 'laser_shutter',
#'power_wheel', 'nd_wheel'
