from ScopeFoundryHW.sync_stream_scan.sync_stream_scan import SyncStreamScan
import numpy as np
from scipy import signal

class LaserWriteShapeScan(SyncStreamScan):
    
    name = 'write_laser_shape'
    
    def setup(self):
        SyncStreamScan.setup(self)

        
        S = self.settings

        S.New('scale', dtype=float, unit='um/V', initial=200./10.)
        S.New('feedrate', dtype=float, unit='um/s', initial=200.)
        
        S['ni_device'] = 'Dev1'
        S['do_chan'] = 'port0/line0:3'
        S['di_chan'] = 'port0/line4:7'
        S['ao_chan'] = 'ao0:3'
        S['ai_chan'] = 'ai16:18'
        S['data_rate'] = 400*100 #Hz
        
        
    def ao_stream_gen(self, T):
        ao_stream = np.zeros((len(T), self.ao_chan_count), dtype=float)

        X, Y = self.circ_xy(T)
        
        L = self.laser_pulse_train(T)
        
        ao_stream[:,0] = X # ao0
        ao_stream[:,1] = Y # ao1
        #ao_stream[:,2] = Z # ao2
        ao_stream[:,3] = L # ao3
        return ao_stream
        
        
        
    def circ_xy(self, T):
        
        cx = 2
        cy = 0
        
        ax = 1
        ay = 1.
                
        #T = np.arange(0, 1.0, dt)
        X = cx + ax*np.cos(10*T/(2*np.pi))
        Y = cy + ay*np.sin(10*T/(2*np.pi))
        return X,Y
        
    def laser_pulse_train(self, T):
        
        pulse_rate = 400 # Hz
        amp = 4.0 # Volt
        y = amp/2*(1+signal.square(2 * np.pi *pulse_rate * T, duty=0.2))
        
        return y
    
    def abs_linear_move(self, x0,y0,z0, x,y,z, absolute=True, feedrate=None, laser_on=False):
        dt = 1./self.settings['data_rate']
        if feedrate is None:
            feedrate = self.settings['feedrate']
        
        X0 = np.array([x0,y0,z0], dtype=float).reshape(3,1)
        X1 = np.array([x,y,z], dtype=float).reshape(3,1)
        path_length = np.linalg.norm(X1-X0)
        N = (path_length/feedrate) / dt # number of output datapoints
        T = np.arange(N)*dt # time coordinate of move (starts at zero at start of move)
        XYZ = X0 + np.arange(N).reshape(1,-1)/N * (X1-X0) # or linspace
        
        if laser_on:
            L = self.laser_pulse_train(T)
        else:
            L = np.zeros_like(T)
        
        XYZ_volts = XYZ/self.settings['scale']
        
        ao =  np.vstack([XYZ_volts, L])
        print('abs_linear_move', ao.shape, ao.dtype)
        return ao
    
    def cross(self, cx,cy,L, absolute=True, feedrate=None):
        segments = [
            (cx-L/2, cy,     cx+L/2, cy, True),
            (cx+L/2, cy,     cx,    cy+L/2, False),
            (cx    , cy+L/2, cx,    cy-L/2, True),
            ]
        ao_segments = []
        
        for x0,y0,x1,y1,laser in segments:
            ao = self.abs_linear_move(x0, y0,0, x1, y1, 0, 
                                      absolute=absolute, feedrate=feedrate, laser_on=laser)
            ao_segments.append(ao)
        ao = np.hstack(ao_segments)
        print("cross", ao.shape,ao.dtype)
        return ao
            
        
    
    # def run(self):
    #     self.simple_run()