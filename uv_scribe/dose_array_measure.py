from ScopeFoundry import Measurement
import pyqtgraph as pg
import numpy as np

class DoseArrayMeasure(Measurement):
    
    name = 'dose_array'
    
    
    def setup(self):
        
        self.settings.New_Range('powerwheel')
        
        self.settings.New("pulse_rate", dtype=float, initial=400, unit='pulse/sec')
        self.settings.New('spacing')
        
        
        self.add_operation('Build Display', self.build_display)
    def setup_figure(self):
        
        self.ui = self.graph_layout = pg.GraphicsLayoutWidget()
        self.graph_layout.clear()


    def build_display(self):
        self.graph_layout.clear()
        
        self.plot = self.graph_layout.addPlot()
        self.img_item = pg.ImageItem()
        self.plot.addItem(self.img_item)
        self.plot.setAspectLocked(lock=True, ratio=1)
        

        self.im = im = self.app.measurements.flircam_live.get_rgb_image()
        self.img_item.setImage(self.im, autoLevels=True)


    def run(self):
        
        sync_stream_scan = self.app.measurements['write_laser_shape']

        dt = 1.0/sync_stream_scan.settings['data_rate']
        
        # N = int(10.0 / dt)
        # T = np.arange(0,N*dt, dt)
        # self.ao_data =  np.zeros((N,4), dtype=float)
        # # X
        # self.ao_data[:,0] = 2.0*np.cos(5*T/(2*np.pi))
        #

        #### Cross
        L = 0.5 # 10 um -- scale is 200um/10v --> 20um/V --> 0.5 V 
        L = 5
        vel = 3.0 #V/s
        
        cx = 0
        cy = 0
        
        
        total_time = L/vel
        N = int(total_time/dt)
        
        
        segments = [
            (cx-L/2, cy,     cx+L/2, cy, True),
            (cx+L/2, cy,     cx,    cy+L/2, True),
            (cx    , cy+L/2, cx,    cy-L/2, True),
            ]
        
        x_segments = []
        y_segments = []
        l_segments = []
        
        for x0,y0,x1,y1,laser in segments:
            
            X = np.linspace(x0,x1,N)
            Y = np.linspace(y0,y1,N)
            pulse = np.zeros(N)
            
            if laser:
                dtL = 1/400.
                n_skip = int(dtL/dt)
                pulse[::n_skip] = 4.0
                
            x_segments.append(X)
            y_segments.append(Y)
            l_segments.append(pulse)

        #print(len(x_segments), x_segments[0].shape, np.concatenate(x_segments).shape)
        X = np.concatenate(x_segments)
        Y = np.concatenate(y_segments)
        pulse = np.concatenate(l_segments)
        
        self.ao_data =  np.zeros((len(X),4), dtype=float)

        self.ao_data[:,0] = X        
        self.ao_data[:,1] = Y        
        self.ao_data[:,3] = pulse        
        
        
        sync_stream_scan.set_precomputed_ao_data(self.ao_data)
        
        
        # sync_stream_scan.set_precomputed_ao_data(
        #     sync_stream_scan.cross(0,0,10, ))

        
        self.start_nested_measure_and_wait(sync_stream_scan)#, nested_interrupt, polling_func, polling_time)
