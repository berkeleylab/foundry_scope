from ScopeFoundry import BaseMicroscopeApp
from uv_scribe.laser_write_measure import LaserWriteMeasure

class UVScribeApp(BaseMicroscopeApp):
    
    name = 'uv_scribe_app'
    
    def setup(self):
        
        from ScopeFoundryHW.asi_stage import ASIStageHW, ASIStageControlMeasure
        self.add_hardware(ASIStageHW(self, enable_xy=True, enable_z=True, enable_fw=False, swap_xy=False, invert_x=False,invert_y=False))
        self.add_measurement(ASIStageControlMeasure(self))
        
        from ScopeFoundryHW.mcl_stage import MclXYZStageHW
        self.add_hardware(MclXYZStageHW(self))
        
        from ScopeFoundryHW.flircam import FlirCamHW, FlirCamLiveMeasure
        self.add_hardware(FlirCamHW(self))
        self.add_measurement(FlirCamLiveMeasure(self))
        
        
        from ScopeFoundryHW.ni_daq.hw.ni_digital_out import NIDigitalOutHW
        ni_dout = self.add_hardware(NIDigitalOutHW(app=self, line_names=['backlight', 'topLED', 'bottomLED']))
        ni_dout.settings['port'] = "Dev1/port0/line8:10"
        
        
        
        from ScopeFoundryHW.optotune_lens import OptotuneLensHW
        self.add_hardware(OptotuneLensHW(self))
        
        from uv_scribe.laser_write_sync_stream import LaserWriteShapeScan
        self.add_measurement(LaserWriteShapeScan(self))
        
        from uv_scribe.dose_array_measure import DoseArrayMeasure
        self.add_measurement(DoseArrayMeasure(self))
        
        from uv_scribe.laser_write_measure import LaserWriteMeasure
        self.add_measurement(LaserWriteMeasure(self))
        
        
        
        from ScopeFoundryHW.dynamixel_servo import DynamixelXServosHW, DynamixelFilterWheelHW, DynamixelServoHW
        self.add_hardware(DynamixelXServosHW(self,
                                             devices={'laser_shutter':151,
                                                      'power_wheel':150,
                                                      'nd_wheel': 152,
                                                      }))
        # self.add_hardware(DynamixelFilterWheelHW(self, name='output_select', 
        #                                          named_positions={'R':0,
        #                                                   'AUX':1529,
        #                                                   'L':3067,
        #                                                   'EYE':4597},
        #                                          release_at_target=True))
        self.add_hardware(DynamixelFilterWheelHW(self, name='laser_shutter', 
                                                 named_positions={
                                                     'OPEN':2048+1024,
                                                     'CLOSED':2048},
                                                 release_at_target=False))
        # focus_knob = self.add_hardware(DynamixelServoHW(self, name='focus_knob',
        #                                     lq_kwargs={'spinbox_decimals':3, 'unit':'um'}))
        
        self.add_hardware(DynamixelServoHW(self, name='power_wheel'))
        
        self.add_hardware(DynamixelFilterWheelHW(self, name='nd_wheel',
                                                 named_positions={
                                                     'A_0.0':0,
                                                     'B_0.6':683,
                                                     'C_1.0':1365,
                                                     'D_??':2048,
                                                     'E_??':3413,}
                                                 ))

        from confocal_measure.stage_ref_pos_measure import StageReferencedPositionMeasure
        self.add_measurement(StageReferencedPositionMeasure(self))

        from uv_scribe.uv_scribe_control_panel import UVScribeControlPanel
        self.add_measurement(UVScribeControlPanel(self))

        
        # load default settings from file
        self.settings_load_ini("uv_scribe_defaults.ini")
        
        
        
if __name__ == '__main__':
    import sys
    app = UVScribeApp(sys.argv)
    #app.tile_layout()
    sys.exit(app.exec_())