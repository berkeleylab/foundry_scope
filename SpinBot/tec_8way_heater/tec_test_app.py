from ScopeFoundry import BaseMicroscopeApp

from SpinBot.tec_8way_heater.tec8way_hw import TEC8Way_PLC_HW
from SpinBot.tec_8way_heater.tec_8way_log_measure import Tec8WayLogMeasure
from SpinBot.tec_8way_heater.tec_8way_prog_measure import Tec8WayProgramMeasure
from SpinBot.tec_8way_heater.tec_8way_batch_prog_measure import Tec8WayBatchProgramMeasure

class TestApp(BaseMicroscopeApp):
    def setup(self):
        self.add_hardware(TEC8Way_PLC_HW(self))
        self.add_measurement(Tec8WayLogMeasure(self))
        self.add_measurement(Tec8WayProgramMeasure(self))
        self.add_measurement(Tec8WayBatchProgramMeasure(self))
app = TestApp()
app.exec_()