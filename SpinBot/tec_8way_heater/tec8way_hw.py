from ScopeFoundry.hardware import HardwareComponent
from ScopeFoundry.helper_funcs import sibling_path
import pandas
import re
import time
from ScopeFoundry.base_app import BaseMicroscopeApp
from threading import Lock
from ScopeFoundryHW.productivity_plc.productivity_plc_modbus import ProductivityPLCModbus

class TEC8Way_PLC_HW(HardwareComponent):
    
    name = "tec_8way_plc"
        
    
    def __init__(self, app, debug=False, name=None, ):
        self.plc = ProductivityPLCModbus(ip_address = None,
                                         tags_csv_filename=sibling_path(__file__,"tec_8way_structured_extended.csv"), 
                                         override_json=sibling_path(__file__,'plc_tag_overrides.json'),
                                         skip_array_elements=True, 
                                         connect=False)
        
        HardwareComponent.__init__(self, app, debug=debug, name=name)
        
    def setup(self):
        self.settings.New("ip_address", dtype=str, initial="192.168.1.22")

        for name, tag in self.plc.tag_db.items():
            if tag['Array'] == True:
                continue
            self.settings.New(name, dtype=tag['dtype'],
                              unit =tag['unit'],
                              ro = tag['read_only'])
            
        self.add_operation("read_all_tags", self.read_all_tags)
            
    def connect(self):
        self.plc.connect(self.settings['ip_address'], debug=self.settings['debug_mode'])
        
        for name, tag in self.plc.tag_db.items():
            if tag['Array'] == True:
                continue
            lq = self.settings.get_lq(name)
            
            if not tag['read_only']:
                write_func=lambda x, name=name:self.plc.write_single(name,x)

            lq.connect_to_hardware(
                read_func =lambda name=name:self.plc.read_single(name),
                write_func=write_func)

    def read_all_tags(self):
        t0 = time.monotonic()
        for name, tag in self.plc.tag_db.items():
            if tag['Array'] == True:
                continue
            val = self.plc.read_single(name)
            #if "HeaterA" in name: print("read_all_tags", name, val, tag)
            self.settings.get_lq(name).update_value(new_val=val, update_hardware=False)
        #print(f"read_all_tags took {time.monotonic()-t0}")
        
    def disconnect(self):
        self.settings.disconnect_all_from_hardware()
        self.plc.disconnect()

    def read_heater_temp_history(self, n, start=0, stop=4096):
        #state_tag = self.plc.tag_db['DataXfer_state']
        buffer_tag = self.plc.tag_db['DataXfer_f32_buffer__']
        self.plc.write_single("DataXfer_state", n+1)
        f = self.plc.read_float32_array(buffer_tag['m0']+2*start, stop)
        self.plc.write_single("DataXfer_state", 0) # Idles
        return f
    
    def read_heater_output_history(self, n, start=0, stop=4096):
        #state_tag = self.plc.tag_db['DataXfer_state']
        buffer_tag = self.plc.tag_db['DataXfer_f32_buffer__']        
        self.plc.write_single("DataXfer_state", n+11) # 
        f = self.plc.read_float32_array(buffer_tag['m0']+2*start, stop)
        self.plc.write_single("DataXfer_state", 0) # Idles
        return f
    
    def read_heater_program(self, n , start=0, stop=4096):
        #state_tag = self.plc.tag_db['DataXfer_state']
        buffer_tag = self.plc.tag_db['DataXfer_f32_buffer__']        
        self.plc.write_single("DataXfer_state", n+31) # 
        f = self.plc.read_float32_array(buffer_tag['m0']+2*start, stop)
        self.plc.write_single("DataXfer_state", 0) # Idles
        return f
    
    def write_heater_program(self, n, program):
        prog_len = len(program)
        heater_letter = 'ABCDEFGH'[n]
        # state_tag = self.plc.tag_db['DataXfer_state']
        buffer_tag = self.plc.tag_db['DataXfer_f32_buffer__']        
        
        print("asdf")
        self.plc.write_single(f'Heater{heater_letter}_program_len', prog_len)
        print(self.plc.read_single(f'Heater{heater_letter}_program_len'))
        
        self.plc.write_single("DataXfer_state", -1) # clear buffer
        time.sleep(0.005)
        self.plc.write_single("DataXfer_state", 0) # Idles buffer
        time.sleep(0.005)
        self.plc.write_float32_array(buffer_tag['m0'], program)
        self.plc.write_single("DataXfer_state", n + 21) # copy to Heater program
        #time.sleep(0.1)
        time.sleep(0.005)
        self.plc.write_single("DataXfer_state", 0) # Idles buffer
        
    def threaded_update(self):
        self.read_all_tags()
        time.sleep(1.0)
        
