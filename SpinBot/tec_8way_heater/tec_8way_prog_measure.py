from ScopeFoundry import Measurement
from qtpy import  QtWidgets
import pyqtgraph as pg
import time
import numpy as np
from ScopeFoundry.helper_funcs import load_qt_ui_file,sibling_path

class Tec8WayProgramMeasure(Measurement):
    
    name = 'tec_program'
    
    def setup(self):
        self.add_operation("Write Programs", self.write_programs)
        self.add_operation('Read Prog and Data', self.read_program_and_data)
    
    
    def setup_figure(self):
        plc = self.app.hardware['tec_8way_plc']

        self.ui = QtWidgets.QWidget()
        #self.ui = self.graph_layout = pg.GraphicsLayoutWidget()
        self.ui.setLayout(QtWidgets.QGridLayout())
        l = self.ui.layout()
        
        self.heaters = []
        
        for i in range(8):
            heater = dict()

            row, col = [(0,0), (0,1),(0,2), (0,3), (1,0), (1,1), (1,2),(1,3)][i]
            w = load_qt_ui_file(sibling_path(__file__, 'tec8_heater_panel.ui'))
            l.addWidget(w, row, col)

        
            self.heaters.append(heater)
            heater['i'] = i
            heater['letter'] = N = heater_letter = 'ABCDEFGH'[i]
            
            heater['widget'] = w
            w.groupBox.setTitle(f"Heater {N}")
            
            p = w.plotWidget #self.graph_layout.addPlot(row=0, col=i)
            #p.setTitle(f"Heater {N} ({i})")
            heater['plot'] = p

            heater['plotlines'] = dict()
            for x in ('temperature', 'set_point'):
                heater['plotlines'][x] = p.plot()

            # p = self.graph_layout.addPlot(row=1, col=i)
            # p.setTitle(f"Heater {heater_letter} ({i}) Output")
            # for x in ['output',]:
            #     heater['plotlines'][x] = p.plot()
            
            plc.settings.get_lq(f"Heater{N}_temperature").connect_to_widget(w.pv_doubleSpinBox)
            plc.settings.get_lq(f"Heater{N}_set_point").connect_to_widget(w.sv_doubleSpinBox)
            plc.settings.get_lq(f"Heater{N}_pid_output").connect_to_widget(w.outp_doubleSpinBox)
            plc.settings.get_lq(f"Heater{N}_is_heating").connect_to_widget(w.heat_checkBox)
            plc.settings.get_lq(f"Heater{N}_is_cooling").connect_to_widget(w.cool_checkBox)
            plc.settings.get_lq(f"Heater{N}_program_run").connect_to_widget(w.prog_run_checkBox)
            plc.settings.get_lq(f"Heater{N}_program_halt").connect_to_widget(w.prog_halt_checkBox)
            plc.settings.get_lq(f"Heater{N}_program_counter").connect_to_widget(w.prog_counter_doubleSpinBox)
            plc.settings.get_lq(f"Heater{N}_program_len").connect_to_widget(w.prog_len_doubleSpinBox)


    def run(self):
        plc = self.app.hardware['tec_8way_plc']


        self.write_programs()
                    
        self.read_program_and_data()
        
        while not self.interrupt_measurement_called:
            for h in self.heaters:
                X = h['letter']
                prog_run = plc.settings.get_lq(f'Heater{X}_program_run')
                if prog_run.read_from_hardware():
                    print(f"Heater{X} running")

        
        #
        # plc = self.app.hardware['tec_8way_plc']
        #
        # for h in self.heaters:
        #     h['temp_history'] = []
        #     h['setp_history'] = []
        #     h['outp_history'] = []
        #
        # while not self.interrupt_measurement_called:
        #
        #     for h in self.heaters:
        #         i = h['i']
        #         heater_letter = h['letter']
        #
        #         lq = plc.settings.get_lq(f'Heater{heater_letter}_temperature')
        #         temp = lq.read_from_hardware()
        #         h['temp_history'].append(temp)
        #
        #         lq = plc.settings.get_lq(f'Heater{heater_letter}_set_point')
        #         sp = lq.read_from_hardware()
        #         h['setp_history'].append(sp)
        #
        #         lq = plc.settings.get_lq(f'Heater{heater_letter}_pid_output')
        #         sp = lq.read_from_hardware()
        #         h['outp_history'].append(sp)
        #
        #
        #
        #     time.sleep(1.0)
        #

    def write_programs(self):
        plc = self.app.hardware['tec_8way_plc']
        for h in self.heaters:
            print("writing heater program", h['letter'])
            i = h['i']
            t0 = time.monotonic()
            plc.write_heater_program(i, np.sin(np.linspace(0,1,512+i)*10.0)+np.linspace((i+1)*10,(i+1)*9,512+i))
            print(f"\ttook {time.monotonic()-t0} sec")


    def read_program_and_data(self):
        plc = self.app.hardware['tec_8way_plc']
        
        for h in self.heaters:
            print("Reading heater prgram and data", h['letter'])
            i = h['i']
            t0 = time.monotonic()
            n = plc.plc.read_single(f'Heater{h["letter"]}_program_len')
            h['temp_history'] = plc.read_heater_temp_history(i, 0, n)
            h['setp_history'] = plc.read_heater_program(i, 0, n)
            h['outp_history'] = plc.read_heater_output_history(i, 0,n)
            print(f"\ttook {time.monotonic()-t0} sec")
            
    def update_display(self):
        
        for h in self.heaters:   
            h['plotlines']['temperature'].setPen(1)
            h['plotlines']['temperature'].setData(h['temp_history'])
            h['plotlines']['set_point'].setPen(2)
            h['plotlines']['set_point'].setData(h['setp_history'])
            #h['plotlines']['output'].setData(h['outp_history'])
            
