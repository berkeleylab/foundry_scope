from ScopeFoundry import Measurement
from qtpy import  QtWidgets, QtCore
import pyqtgraph as pg
import time
import numpy as np
from ScopeFoundry.helper_funcs import load_qt_ui_file,sibling_path
import os
import json
from ScopeFoundry import h5_io

class Tec8WayBatchProgramMeasure(Measurement):
    
    name = 'tec_batch_program'
    
    programs_loaded_sig = QtCore.Signal()
    
    
    def setup(self):
        
        self.settings.New('batch_size', dtype=int, vmin=1, vmax=8, initial=8)
        self.settings.New('batch_dir', dtype='file', initial='./', is_dir=True)
        for ii in range(8):
            self.settings.New(f'sample{ii}_uuid', dtype=str, initial='None')
            prog_run = self.settings.New(f'Heater{ii}_prog_run', dtype=bool, initial=False)
            self.settings.New(f'Heater{ii}_prog_counter', dtype=int, initial=False)
            self.settings.New(f'Heater{ii}_prog_len', dtype=int, initial=False)
            
            prog_run.add_listener(lambda startstop, heater_num=ii:
                                    self.startstop_heater_program(heater_num, startstop), argtype=(bool,))
            
            
        self.add_operation('Load Programs', self.load_programs)
        # self.add_operation("Write Programs", self.write_programs)
        # self.add_operation('Read Prog and Data', self.read_program_and_data)
        
        self.programs_loaded_sig.connect(self.update_display)
        

    def load_programs(self, fname=None):
        if not fname:
            fname = os.path.join(self.settings['batch_dir'], 'uuids.json')
        
        print(fname)
        with open(fname, 'r') as f:
            uuid_json = json.load(f)
            
        print(uuid_json)
        
        self.settings['batch_size'] = N = len(uuid_json)
        
        for ii in range(8):
            self.settings[f'sample{ii}_uuid'] = 'None'
        
        for ii in range(N):
            self.settings[f'sample{ii}_uuid'] = uuid_json[ii]
            
        # set batch_dir
        batch_dir = self.settings['batch_dir'] = os.path.dirname(fname)
        self.heater_run_dir = os.path.join(batch_dir, 'heater_run')
        os.makedirs( self.heater_run_dir, exist_ok=True) 
        
        # Read programs
        
        prog_fname = os.path.join(self.settings['batch_dir'], 'heater_programs.json')
        with open(prog_fname, 'r') as f:
            self.programs = json.load(f)
            # convert to numpy arrays:
            self.programs = [np.array(x) for x in self.programs]
            
        for ii, prog in enumerate(self.programs):
            h = self.heaters[ii]
            h['program'] = prog
            h['temp_history'] = np.zeros_like(prog)
            h['output_history'] = np.zeros_like(prog)
            
            self.settings[f'Heater{ii}_prog_run']  = False
            self.settings[f'Heater{ii}_prog_counter'] = 0
            self.settings[f'Heater{ii}_prog_len'] = len(prog)

        
        self.programs_loaded_sig.emit()
            
    
    
    def setup_figure(self):
        plc = self.app.hardware['tec_8way_plc']

        self.ui = QtWidgets.QWidget()
        #self.ui = self.graph_layout = pg.GraphicsLayoutWidget()
        self.ui.setLayout(QtWidgets.QGridLayout())
        l = self.ui.layout()
        w = self.ctrls = load_qt_ui_file(sibling_path(__file__, 'heater_batch_prog_control_groupbox.ui'))
        l.addWidget(self.ctrls, 0,0, 1,4)
        
        self.heaters = []
        
        self.settings.batch_dir.connect_to_browse_widgets(w.batch_dir_lineEdit, w.select_batch_dir_pushButton)
        self.settings.activation.connect_to_widget(w.run_batch_checkBox)
        self.settings.batch_size.connect_to_widget(w.batch_size_label)
        w.load_progs_pushButton.clicked.connect(self.load_programs)
        

        for ii in range(8):
            heater = dict()

            row, col = [(1,0), (1,1),(1,2), (1,3), (2,0), (2,1), (2,2),(2,3)][ii]
            w = load_qt_ui_file(sibling_path(__file__, 'tec8_heater_panel2.ui'))
            l.addWidget(w, row, col)

        
            self.heaters.append(heater)
            heater['i'] = ii
            heater['letter'] = N = heater_letter = 'ABCDEFGH'[ii]
            
            heater['widget'] = w
            w.groupBox.setTitle(f"Heater {N}")
            
            p = w.plotWidget #self.graph_layout.addPlot(row=0, col=i)
            #p.setTitle(f"Heater {N} ({i})")
            heater['plot'] = p

            heater['plotlines'] = dict()
            for x in ('temperature', 'set_point'):
                heater['plotlines'][x] = p.plot()

            # p = self.graph_layout.addPlot(row=1, col=i)
            # p.setTitle(f"Heater {heater_letter} ({i}) Output")
            # for x in ['output',]:
            #     heater['plotlines'][x] = p.plot()
            
            plc.settings.get_lq(f"Heater{N}_temperature").connect_to_widget(w.pv_doubleSpinBox)
            plc.settings.get_lq(f"Heater{N}_set_point").connect_to_widget(w.sv_doubleSpinBox)
            plc.settings.get_lq(f"Heater{N}_pid_output").connect_to_widget(w.outp_doubleSpinBox)
            plc.settings.get_lq(f"Heater{N}_is_heating").connect_to_widget(w.heat_checkBox)
            plc.settings.get_lq(f"Heater{N}_is_cooling").connect_to_widget(w.cool_checkBox)

            
            self.settings.get_lq(f"Heater{ii}_prog_counter").connect_to_widget(w.prog_counter_doubleSpinBox)
            self.settings.get_lq(f"Heater{ii}_prog_len").connect_to_widget(w.prog_len_doubleSpinBox)
                        
            self.settings.get_lq(f"sample{ii}_uuid").connect_to_widget(w.sample_lineEdit)
            self.settings.get_lq(f'Heater{ii}_prog_run').connect_to_widget(w.prog_run_checkBox)

    def startstop_heater_program(self, heater_num, startstop):
        h = self.heaters[heater_num]
        N = h['letter']
        if startstop:
            h['prog_start_time'] = time.monotonic()
            print(f"starting Heater {h['letter']} Program at {h['prog_start_time']}")
        else:
            # on stop, put heater to idle temperature
            plc = self.app.hardware['tec_8way_plc']
            plc.settings[f"Heater{N}_set_point"] = 30.0

    def run(self):
        plc = self.app.hardware['tec_8way_plc']
        self.t0 = time.time()
       
        # make batch_dir/spec_run
        os.makedirs( self.heater_run_dir, exist_ok=True)

        # Set save dir 
        self.app.settings['save_dir'] = self.heater_run_dir
        
        # Make sure  communications are connected
        plc.settings['connected'] = True
        
        # H5 Data file
        self.h5_file = h5_io.h5_base_file(self.app, measurement=self)
        self.h5_filename = self.h5_file.filename
        
        self.h5_file.attrs['time_id'] = self.t0
        H = self.h5_meas_group  =  h5_io.h5_create_measurement_group(self, self.h5_file)

        for h in self.heaters:
            ii = h['i']
            N = h['letter']
            h_h5 = H.create_group(f'Heater{N}')
            h_h5['program'] = h['program']
            h_h5.create_dataset('temp_history', data = h['temp_history'])
            h_h5.create_dataset('output_history', data = h['output_history'])
            h['h5'] = h_h5
            h_h5.attrs['sample_uuid'] = self.settings[f'sample{ii}_uuid']
        try:
            while not self.interrupt_measurement_called:
                for h in self.heaters:
                    ii = h['i']
                    N = h['letter']
                    prog_run = self.settings[f'Heater{ii}_prog_run']
                    h_h5 = h['h5']
                    if prog_run:
                        h_h5.attrs['prog_start_time'] = h['prog_start_time']
                        dt = time.monotonic()-h['prog_start_time']
                        kk = int(np.round(dt))
                        self.settings[f'Heater{ii}_prog_counter'] = kk
                        h_h5.attrs['prog_counter'] = kk
                        
                        if kk >= self.settings[f'Heater{ii}_prog_len']:
                            self.settings[f'Heater{ii}_prog_run'] = False
                            continue
                        setpoint = h['program'][kk]
                        print(f"Heater{N} running", dt, kk, )
                        plc.settings[f"Heater{N}_set_point"] = setpoint
                        h['temp_history'][kk] = plc.settings[f"Heater{N}_temperature"]
                        h['output_history'][kk] = plc.settings[f"Heater{N}_pid_output"]
                        
                        # write to H5
                        h_h5['temp_history'][kk] = h['temp_history'][kk]
                        h_h5['output_history'][kk] = h['output_history'][kk]
                        
                time.sleep(0.25)
        finally:
            print("finished", self.h5_filename)
            self.h5_file.close()

        
        #
        # plc = self.app.hardware['tec_8way_plc']
        #
        # for h in self.heaters:
        #     h['temp_history'] = []
        #     h['setp_history'] = []
        #     h['outp_history'] = []
        #
        # while not self.interrupt_measurement_called:
        #
        #     for h in self.heaters:
        #         i = h['i']
        #         heater_letter = h['letter']
        #
        #         lq = plc.settings.get_lq(f'Heater{heater_letter}_temperature')
        #         temp = lq.read_from_hardware()
        #         h['temp_history'].append(temp)
        #
        #         lq = plc.settings.get_lq(f'Heater{heater_letter}_set_point')
        #         sp = lq.read_from_hardware()
        #         h['setp_history'].append(sp)
        #
        #         lq = plc.settings.get_lq(f'Heater{heater_letter}_pid_output')
        #         sp = lq.read_from_hardware()
        #         h['outp_history'].append(sp)
        #
        #
        #
        #     time.sleep(1.0)
        #


            
    def update_display(self):
        
        for h in self.heaters:   
            h['plotlines']['temperature'].setPen(1)
            h['plotlines']['temperature'].setData(h['temp_history'])
            h['plotlines']['set_point'].setPen(2)
            h['plotlines']['set_point'].setData(h['program'])
            #h['plotlines']['output'].setData(h['output_history'])
            
