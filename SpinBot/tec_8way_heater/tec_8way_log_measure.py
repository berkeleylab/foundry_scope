from ScopeFoundry import Measurement
# from qtpy import  QtWidgets
import pyqtgraph as pg
import time

class Tec8WayLogMeasure(Measurement):
    
    name = 'tec_log'
    
    def setup(self):
        pass
    
    
    def setup_figure(self):
        
        #self.ui = QtWidgets.QWidget()
        
        
        self.ui = self.graph_layout = pg.GraphicsLayoutWidget()
        
        self.heaters = []
        
        for i in range(8):
            
            heater = dict()
            self.heaters.append(heater)
            heater['i'] = i
            heater['letter'] = heater_letter = 'ABCDEFGH'[i]
            
            p = self.graph_layout.addPlot(row=0, col=i)
            p.setTitle(f"Heater {heater_letter} ({i})")
            heater['plot'] = p

            heater['plotlines'] = dict()
            for x in ('temperature', 'set_point'):
                heater['plotlines'][x] = p.plot()

            p = self.graph_layout.addPlot(row=1, col=i)
            p.setTitle(f"Heater {heater_letter} ({i}) Output")
            for x in ['output',]:
                heater['plotlines'][x] = p.plot()


    def run(self):
        
        plc = self.app.hardware['tec_8way_plc']
                
        for h in self.heaters:
            h['temp_history'] = []
            h['setp_history'] = []
            h['outp_history'] = []
        
        while not self.interrupt_measurement_called:
            
            for h in self.heaters:
                i = h['i']
                heater_letter = N = h['letter']
                
                lq = plc.settings.get_lq(f'Heater{N}_temperature')
                temp = lq.read_from_hardware()
                h['temp_history'].append(temp)
                
                lq = plc.settings.get_lq(f'Heater{N}_set_point')
                sp = lq.read_from_hardware()
                h['setp_history'].append(sp)
                
                lq = plc.settings.get_lq(f'Heater{N}_pid_output')
                sp = lq.read_from_hardware()
                h['outp_history'].append(sp)

                plc.settings.get_lq(f'Heater{N}_is_heating').read_from_hardware()
                plc.settings.get_lq(f'Heater{N}_is_cooling').read_from_hardware()
                
            time.sleep(1.0)
            
    
    def update_display(self):
        
        for h in self.heaters:   
            h['plotlines']['temperature'].setPen(1)
            h['plotlines']['temperature'].setData(h['temp_history'])
            h['plotlines']['set_point'].setPen(2)
            h['plotlines']['set_point'].setData(h['setp_history'])
            h['plotlines']['output'].setData(h['outp_history'])
            
