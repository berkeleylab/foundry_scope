from ScopeFoundry import Measurement
from ScopeFoundry.helper_funcs import load_qt_ui_file, sibling_path

class CBoxControlPanelMeasure(Measurement):
    
    name = 'cbox_control_panel'
    
    def setup(self):
        
        pass
    
    def setup_figure(self):
        
        self.ui = load_qt_ui_file(sibling_path(__file__, 'cbox_control.ui'))
    