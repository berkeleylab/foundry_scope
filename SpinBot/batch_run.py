from ScopeFoundry import Measurement
import json
import os
import time

class BatchRun(Measurement):
    
    """
    Configures SpinBot to run a batch of samples
    from a json config file,
    triggers characterization spec_run when spinbot is done
    """
    
    
    name = 'batch_run'
    
    def setup(self):
        self.settings.New('batch_size', dtype=int, vmin=1, vmax=8, initial=8)
        self.settings.New('batch_dir', dtype='file', initial='./', is_dir=True)
        for ii in range(8):
            self.settings.New(f'sample{ii}_uuid', dtype=str)
            
        self.add_operation('Load UUIDs', self.load_uuid_json)

    def load_uuid_json(self, fname=None):
        if not fname:
            fname = os.path.join(self.settings['batch_dir'], 'uuids.json')
        
        print(fname)
        with open(fname, 'r') as f:
            uuid_json = json.load(f)
            
        print(uuid_json)
        
        self.settings['batch_size'] = N = len(uuid_json)
        
        for ii in range(8):
            self.settings[f'sample{ii}_uuid'] = 'None'
        
        for ii in range(N):
            self.settings[f'sample{ii}_uuid'] = uuid_json[ii]
            
        # set batch_dir
        batch_dir = self.settings['batch_dir'] = os.path.dirname(fname)
        self.spec_run_dir = os.path.join(batch_dir, 'spec_run')
        os.makedirs( self.spec_run_dir, exist_ok=True)
        
        
    def run(self):
        
        spec_run = self.app.measurements['spec_run']
        
        # make batch_dir/spec_run
        os.makedirs( self.spec_run_dir, exist_ok=True)

        # Set save dir 
        self.app.settings['save_dir'] = self.spec_run_dir
        
        # Make sure cbox_stage and communications are connected
        print("Make sure cbox_stage and communications are connected")
        spinbot_comm = self.app.hardware['spinbot_comm']
        spinbot_comm.settings['connected'] = True
        
        cbox_stage = self.app.hardware['cbox_stage']
        cbox_stage.settings['connected'] = True

        time.sleep(1.0) # wait for connections to finalize
        
    
        # eject cbox stage
        print("Eject CBox stage")
        if not cbox_stage.settings['is_at_load']:
            cbox_stage.go_to_load_pos()
        # check for eject
        assert cbox_stage.settings['is_at_load']


        
        # Loop over batch size
        for ii in range(self.settings['batch_size']):
            print(f"{ii}=================="*2)
            if self.interrupt_measurement_called:
                break            
        
            # Set sample to sample_uuid
            self.app.settings['sample'] = self.settings[f'sample{ii}_uuid']
        
            # Wait for sample loaded from SpinBot
            isSampleLoaded = spinbot_comm.settings.IsSampleLoaded
            while not isSampleLoaded.read_from_hardware():
                if self.interrupt_measurement_called:
                    break
                time.sleep(0.1)
            
            if self.interrupt_measurement_called:
                break
            
            # when sample loaded, send wait state to SpinBot
            spinbot_comm.write_GoWaitingState()
            
            # run spec_run
            # wait until spec_run is finished
            print("run_spec_run")
            self.start_nested_measure_and_wait(spec_run)
            
            if self.interrupt_measurement_called:
                break

            # eject sample
            time.sleep(0.1)
            print("Eject CBox stage")
            if not cbox_stage.settings['is_at_load']:
                cbox_stage.go_to_load_pos()
            time.sleep(0.5)
            
            # check for eject
            assert cbox_stage.settings['is_at_load']

            # send exit wait state to SpinBot
            spinbot_comm.write_ExWaitingState()

        
            