
from ScopeFoundry import BaseMicroscopeApp
from ScopeFoundryHW.oceanoptics_spec.oo_spec_odirect_hw import OceanOpticsSpectrometerODirectHW
from ScopeFoundryHW.oceanoptics_spec.oo_spec_measure import OOSpecLive
from SpinBot.tec_8way_heater import tec8way_hw

class SpinBotApp(BaseMicroscopeApp):
    
    name = "SpinBot"
    
    def setup(self):
        
        from ScopeFoundryHW.igus_dryve.igus_dryuveD1_hw import IgusDryveD1MotorHW
        
        self.add_hardware(IgusDryveD1MotorHW(self, name='y_over_stage'))
        self.add_hardware(IgusDryveD1MotorHW(self, name='x_under_stage'))
        
        from SpinBot.cbox_stage_hw import SpinBotCBoxStageHW
        self.add_hardware(SpinBotCBoxStageHW(self))
        
        
        pl_spec_hw = self.add_hardware(OceanOpticsSpectrometerODirectHW(self, name='pl_spec'))
        pl_spec_hw.settings['serial_num'] = 'FLMS14212'
        self.add_measurement(OOSpecLive(self, name='pl_spec_live', hw_name='pl_spec'))
        
        pl_spec_hw = self.add_hardware(OceanOpticsSpectrometerODirectHW(self, name='wl_spec'))
        pl_spec_hw.settings['serial_num'] = 'FLMS12833' #'FLMT07611'
        self.add_measurement(OOSpecLive(self, name='wl_spec_live', hw_name='wl_spec'))
        
        # from ScopeFoundryHW.pygrabber_camera.pygrabber_camera_hw import PyGrabberCameraHW
        # self.add_hardware(PyGrabberCameraHW(self))
        # from ScopeFoundryHW.pygrabber_camera.pygrabber_live_view import PyGrabberCameraLiveMeasure
        # self.add_measurement(PyGrabberCameraLiveMeasure(self))        

        from ScopeFoundryHW.zwo_camera.zwo_camera_hw import ZWOCameraHW
        self.add_hardware(ZWOCameraHW(self))
        from ScopeFoundryHW.zwo_camera.zwo_camera_capture_measure import ZWOCameraCaptureMeasure
        self.add_measurement(ZWOCameraCaptureMeasure(self))

        from SpinBot.spinbot_comm_hw import SpinBotCommHW
        self.add_hardware(SpinBotCommHW(self))
        
        from SpinBot.spec_line_scan import CBoxSpecLineScan
        self.add_measurement(CBoxSpecLineScan(self))

        from SpinBot.spec_run_measure import CBoxSpecRunMeasure
        self.add_measurement(CBoxSpecRunMeasure(self))
        
        from SpinBot.photo_run_measure import PhotoRunMeasure
        self.add_measurement(PhotoRunMeasure(self))

        from SpinBot.enviromon_hw.enviromon_hw import EnviroMonitorHW
        self.add_hardware(EnviroMonitorHW(self))
        
        from SpinBot.batch_run import BatchRun
        self.add_measurement(BatchRun(self))
        
        from SpinBot.tec_8way_heater.tec8way_hw import TEC8Way_PLC_HW
        self.add_hardware(TEC8Way_PLC_HW(self))
        from SpinBot.tec_8way_heater.tec_8way_log_measure import Tec8WayLogMeasure
        self.add_measurement(Tec8WayLogMeasure)
        from SpinBot.tec_8way_heater.tec_8way_batch_prog_measure import Tec8WayBatchProgramMeasure
        self.add_measurement(Tec8WayBatchProgramMeasure)

        from ScopeFoundryHW.mf_crucible.mf_crucible_hw import MFCrucibleHW
        self.add_hardware(MFCrucibleHW(self))

        from ScopeFoundryHW.mf_crucible.mf_crucible_controlpanel import MFCrucibleControlPanel
        self.add_measurement(MFCrucibleControlPanel(self))
        self.settings_load_ini('spinbot_defaults.ini', ignore_hw_connect=False)
        
if __name__ == '__main__':
    import sys
    app = SpinBotApp(sys.argv)
    app.exec_()
