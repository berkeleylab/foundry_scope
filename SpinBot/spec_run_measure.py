from ScopeFoundry import Measurement
from ScopeFoundry import h5_io
from ScopeFoundry.helper_funcs import sibling_path, load_qt_ui_file
import pyqtgraph as pg
import numpy as np
import imageio
import time
import os

class CBoxSpecRunMeasure(Measurement):
    
    name = 'spec_run'
    
    def setup(self):
        
        self.settings.New('pl_y', dtype=int, unit='um', initial=20_000)
        self.settings.New_Range('pl_x', dtype=int, unit='um', 
                                initials = [75_000,93_000,1_000],
                                include_center_span=True)
        
        self.settings.New('pl_x_dark', dtype=int, unit='um', initial=70_000)
        self.settings.New('pl_x_ref', dtype=int, unit='um', initial=30_000)
        self.settings.New('pl_repeats', dtype=int, vmin=1, initial=3)
        
        
        self.settings.New('wl_y', dtype=int, unit='um', initial=2_000)
        self.settings.New_Range('wl_x', dtype=int, unit='um', 
                                initials = [70_000,90_000,1_000],
                                include_center_span=True)
        
        self.settings.New('wl_x_dark', dtype=int, unit='um', initial=60_000)
        self.settings.New('wl_x_ref', dtype=int, unit='um', initial=30_000)
        self.settings.New('wl_repeats', dtype=int, vmin=1, initial=3)
        
        self.settings.New('wl_ref_int_time', dtype=float, initial=-1, unit='s', vmin=-1, vmax=1000,spinbox_decimals=4)
        
        self.settings.New('photo_enable', dtype=bool, initial=True)
        self.settings.New('photo_x', dtype=int, unit='um', initial= 1_000)
        self.settings.New('photo_y', dtype=int, unit='um', initial=10_000)
        
        
        
        self.settings.New('eject_after', dtype=bool, initial=False)

        
        self.pl_spec_hw = self.app.hardware['pl_spec']
        self.wl_spec_hw = self.app.hardware['wl_spec']
        
    def setup_figure(self):
        self.ui_filename = sibling_path(__file__,"spec_run_measure.ui")
        ui = self.ui = load_qt_ui_file(self.ui_filename)
        
        self.settings.activation.connect_to_widget(ui.run_checkBox)
                
        self.settings.eject_after.connect_to_widget(
            ui.eject_after_checkBox)
        
        self.app.settings.sample.connect_to_widget(ui.sample_lineEdit)
        
        
        self.stage_hw = self.app.hardware['cbox_stage']
        
        self.stage_hw.settings.connected.connect_to_widget(
            ui.stage_hw_connect_checkBox)
        self.stage_hw.settings.x_position.connect_to_widget(
            ui.x_pos_doubleSpinBox)
        self.stage_hw.settings.y_position.connect_to_widget(
            ui.y_pos_doubleSpinBox)
        self.stage_hw.settings.x_target.connect_to_widget(
            ui.x_target_doubleSpinBox)
        self.stage_hw.settings.y_target.connect_to_widget(
            ui.y_target_doubleSpinBox)
        
        
        ui.eject_pushButton.clicked.connect(
            self.stage_hw.go_to_load_pos)
        
        ui.insert_pushButton.clicked.connect(
            self.stage_hw.go_to_insert_pos)
        
        
        self.pl_spec_hw.settings.connected.connect_to_widget(
            ui.pl_spec_hw_checkBox)
        self.pl_spec_hw.settings.int_time.connect_to_widget(
            ui.pl_spec_int_time_doubleSpinBox)
        
        self.settings.pl_repeats.connect_to_widget(
            ui.pl_repeat_doubleSpinBox)
        
        self.wl_spec_hw.settings.connected.connect_to_widget(
            ui.wl_spec_hw_checkBox)
        self.wl_spec_hw.settings.int_time.connect_to_widget(
            ui.wl_spec_int_time_doubleSpinBox)        

        self.settings.wl_repeats.connect_to_widget(
            ui.wl_repeat_doubleSpinBox)

        
        self.plot = pg.PlotWidget()
        self.ui.plot_groupBox.layout().addWidget(self.plot)

        self.plotline = self.plot.plot()
        self.plot.setLabel('bottom', 'wavelength (nm)')
        
        self.ui.gen_sample_id_pushButton.clicked.connect(
            self.generate_sample_id)

        
    def run(self):
        S = self.settings
        self.t0 = time.time()
        
        self.pl_x_array = S.ranges['pl_x'].array
        self.wl_x_array = S.ranges['wl_x'].array
        



        # H5 Data file
        self.h5_file = h5_io.h5_base_file(self.app, measurement=self)
        self.h5_filename = self.h5_file.filename
        
        self.h5_file.attrs['time_id'] = self.t0
        H = self.h5_meas_group  =  h5_io.h5_create_measurement_group(self, self.h5_file)
        
        #create h5 data arrays
        H['pl_x_array'] = self.pl_x_array
        H['wl_x_array'] = self.wl_x_array

        self.status = "Starting"
        cam = self.app.hardware['zwo_camera']
        current_exposure = cam.settings['Exposure']/1e6

        try:
            
            self.status = "Moving to Insert Position"
            self.stage_hw.go_to_insert_pos()
            
            
            if S['photo_enable']:
                self.status = "Moving to Photo position"
                self.stage_hw.settings['y_target'] = S['photo_y']
                self.stage_hw.settings['x_target'] = S['photo_x']
                time.sleep(2.0)
                self.stage_hw.wait_until_on_target('xy', timeout=20.0)
                self.status = 'At Photo position, say cheese!'
                time.sleep(0.5)
                
                #cam = self.app.hardware['pygrabber_camera']
                #img = cam.img_buffer[-1].copy()
                #H['photo'] = img
                
                #cam.camera.start_video_capture()
                #img = cam.camera.capture_video_frame()
                
                img = cam.camera.capture()
                time.sleep(2*current_exposure)            
                H['photo'] = img

            
            for (prefix, spec_hw, x_array) in [('pl', self.pl_spec_hw, self.pl_x_array),
                                               ('wl', self.wl_spec_hw, self.wl_x_array)]:
                if self.interrupt_measurement_called:
                    break

                self.status = f"{prefix} Moving y (Reference Spectrum)"
                self.stage_hw.settings['y_target'] = S[f'{prefix}_y']
                time.sleep(0.5)
                self.stage_hw.wait_until_on_target('y', timeout=20.0)
                
                # Reference Spectrum (No sample)
                self.status = f"{prefix} Moving x (Reference Spectrum)"

                self.stage_hw.settings['x_target'] = S[f'{prefix}_x_ref']
                self.stage_hw.wait_until_on_target('x', timeout=20.0)
                self.status = f"{prefix} Acquiring Reference Spectrum"
                
                backup_int_time = spec_hw.settings['int_time']
                repeats = 5
                if prefix == 'wl':
                    if S['wl_ref_int_time'] > 0:
                        spec_hw.settings['int_time']=  S['wl_ref_int_time']
                        repeats = 25
                        
                int_time, self.spectrum = self.acquire_spec(spec_hw, repeats*S[f'{prefix}_repeats'])
                H[f'{prefix}_ref_spectrum'] = self.spectrum
                H[f'{prefix}_ref_int_time'] = int_time
                H[f'{prefix}_wls'] = self.wls
                
                # return spec_hw int_time back to original
                spec_hw.settings['int_time'] = backup_int_time
                
                if self.interrupt_measurement_called:
                    break


            for (prefix, spec_hw, x_array) in [('pl', self.pl_spec_hw, self.pl_x_array),
                                               ('wl', self.wl_spec_hw, self.wl_x_array)]:
                if self.interrupt_measurement_called:
                    break

                self.status = f"{prefix} Moving y"
                self.stage_hw.settings['y_target'] = S[f'{prefix}_y']
                time.sleep(0.5)
                self.stage_hw.wait_until_on_target('y', timeout=20.0)
                

                # Dark Spectrum (Beam blocked with stage)
                self.status = f"{prefix} Dark Moving x"
                self.stage_hw.settings['x_target'] = S[f'{prefix}_x_dark']
                self.stage_hw.wait_until_on_target('x', timeout=20.0)

                self.status = f"{prefix} Dark Acquiring"
                int_time, self.spectrum = self.acquire_spec(spec_hw, 5*S[f'{prefix}_repeats'])
                H[f'{prefix}_dark_spectrum'] = self.spectrum
                H[f'{prefix}_dark_int_time'] = int_time

                backup_int_time = spec_hw.settings['int_time']
                if prefix == 'wl':
                    if S['wl_ref_int_time'] > 0:
                        spec_hw.settings['int_time']=  S['wl_ref_int_time']
                        int_time, self.spectrum = self.acquire_spec(spec_hw, 25*S[f'{prefix}_repeats'])
                        H['wl_ref_dark_spectrum'] = self.spectrum
                        H['wl_ref_dark_int_time'] = int_time
                spec_hw.settings['int_time'] = backup_int_time


                
                
                # Spectrum line scan
                N = len(x_array)
                self.status = f"{prefix} Creating Data Arrays"
                self.spectra = np.zeros((N, len(spec_hw.wavelengths)), dtype=float)
                self.spectra_h5 = H.create_dataset(f'{prefix}_spectra', self.spectra.shape, dtype=float)
                self.spectra_int_times_h5 = H.create_dataset(f'{prefix}_spectra_int_times', N, dtype=float)
                
                for ii, x in enumerate(x_array):
                    if self.interrupt_measurement_called:
                        break
                    
                    self.status = f"{prefix} ({ii+1} of {N}) Moving X"
                    self.stage_hw.settings['x_target'] = x
                    self.stage_hw.wait_until_on_target('x', timeout=20.0)

                    self.status = f"{prefix} ({ii+1} of {N}) Acquiring Spectrum"
                    int_time, self.spectrum = self.acquire_spec(spec_hw, S[f'{prefix}_repeats'])
                    
                    self.status = f"{prefix} ({ii+1} of {N}) Saving Data"
                    self.spectra[ii] = self.spectrum
                    self.spectra_h5[ii] = self.spectrum
                    self.spectra_int_times_h5[ii] = int_time
            
            if self.interrupt_measurement_called:
                return
            self.status ="Moving to Back to Insert Position"
            self.stage_hw.go_to_insert_pos()

            if S['eject_after']:
                self.status = "Moving to Load position (ejecting)"
                self.stage_hw.go_to_load_pos()
        
        except Exception as err:
            self.status += f"\nException! {err}"
            raise err
        else:
            self.status = f"saved to {os.path.basename(self.h5_filename)}"
        finally:
            self.h5_file.close()
            print(f"saved to {self.h5_filename=}")
            print("save tif")
            imageio.imsave(self.h5_filename +".tif", img)
    
    def acquire_spec_simple(self, spec_hw, repeats):
        wls =  self.wls = spec_hw.wavelengths
        repeat_spectra = np.zeros( (repeats, len(wls)), dtype=float)
        for ii in range(repeats):
            spec_hw.acquire_spectrum()
            self.spectrum = spec_hw.get_spectrum()
            repeat_spectra[ii,:] = self.spectrum
        return repeat_spectra.mean(axis=0)
    
    def acquire_spec(self, spec_hw, repeats):
        wls =  self.wls = spec_hw.wavelengths
        backup_int_time = spec_hw.settings['int_time']
        spec_hw.acquire_spectrum()
        self.spectrum = spec_hw.get_spectrum()
        
        # check if spectrum is saturated
        saturated =  self.spectrum.max() > 60_000
        
        # if not, do the repeats and return
        while saturated:
            spec_hw.settings['int_time'] = 0.80*spec_hw.settings['int_time']
            spec_hw.acquire_spectrum()
            self.spectrum = spec_hw.get_spectrum()
            saturated =  self.spectrum.max() > 60_000


        repeat_spectra = np.zeros( (repeats, len(wls)), dtype=float)
        for ii in range(repeats):
            spec_hw.acquire_spectrum()
            self.spectrum = spec_hw.get_spectrum()
            repeat_spectra[ii,:] = self.spectrum
            
        acq_int_time = spec_hw.settings['int_time']
        spec_hw.settings['int_time'] = backup_int_time
        
        return acq_int_time, repeat_spectra.mean(axis=0)

    
    def generate_sample_id(self):
        from ScopeFoundry.cb32_uuid import cb32_uuid
        s, _  = cb32_uuid()
        self.app.settings['sample'] = s
        self.print_barcode()
    
    def update_display(self):
        if hasattr(self, 'spectrum'):
            self.plotline.setData(self.wls, self.spectrum)
            
        self.ui.status_label.setText(self.status)
        print("status:", self.status)
            

    def print_barcode(self):
        from image_print import make_qr, make_image, print_label
        unique_id = self.app.settings['sample']
        qr_img = make_qr(f"{unique_id}")
        make_image(qr_img,f"{unique_id}", "new.png")
        print_label("Brother PT-D610BT", "new.png")

        
        
        
        
        
        
        
        
        
        
        
