from ScopeFoundry.hardware import HardwareComponent
import socket


class SpinBotCommHW(HardwareComponent):
    """
    TCP/IP communication to SpinBot LabView software
    
    
    The IP adress is: "localhost" (or 127.0.0.1) and port is: 6341. 

        You can simply connect to the TCPserver and send commands. In the first instance I prepared 4 commands which are always of 14 bytes length (14  ASCII characters). 
        
        - 'UnknownCommand' -> 'UnknownCommand' (response is 14 bytes) 
        
        - 'IsSampleLoaded' -> 'YES' or 'NO!' (response is 3 bytes)
        
        -  'GoWaitingState' -> 'OK' (response is 2 bytes) 
        
        - 'ExWaitingState' -> 'OK' (response is 2 bytes)

    """
    
    name = 'spinbot_comm'
    
    
    def setup(self):
        
        self.settings.New('host', dtype=str, initial="localhost")
        self.settings.New('IsSampleLoaded', dtype=bool, ro=True)
        
        self.add_operation('Go_Wait', self.write_GoWaitingState)
        self.add_operation('Exit_Wait', self.write_ExWaitingState)
    
    def connect(self):
        S = self.settings

        self.sock =  socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.sock.settimeout(1.0)
        
        self.sock.connect((S['host'], 6341))
        
        S.IsSampleLoaded.connect_to_hardware(
            read_func=self.read_IsSampleLoaded)
        
        
    def disconnect(self):
        
        self.settings.disconnect_all_from_hardware()
        
        if hasattr(self, 'sock'):
            self.sock.close()
            del self.sock
        
    def read_IsSampleLoaded(self):
        self.sock.send(b"IsSampleLoaded")
        resp = self.sock.recv(3)
        assert resp in [b'YES', b'NO!']
        if resp == b'YES':
            return True
        else:
            return False
        
    def write_GoWaitingState(self):
        self.sock.send(b"GoWaitingState")
        resp = self.sock.recv(2)
        print(repr(resp))

        assert resp == b'OK'
        
    def write_ExWaitingState(self):
        self.sock.send(b"ExWaitingState")
        resp = self.sock.recv(2)
        print(repr(resp))
        assert resp == b'OK'