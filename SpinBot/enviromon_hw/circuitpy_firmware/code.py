"""
Hardware:
    MicroController:    Adafruit feather RP2040 https://www.adafruit.com/product/4884
    Sensor:             Adafruit AHT20 Temp and humidity sensor https://www.adafruit.com/product/4566
    Display:            Grayscale 1.5” 128x128 OLED https://www.adafruit.com/product/4741

"""

import board
import digitalio
import time
import neopixel
import displayio
import adafruit_ssd1327
import adafruit_ahtx0
import supervisor
import usb_cdc
import busio

i2c = board.STEMMA_I2C()
#i2c = board.I2C()
#i2c = busio.I2C( board.SCL, board.SDA, frequency=400_000)
#i2c = busio.I2C( board.GPIO3, board.GPIO2, frequency=400_000)
pixels = neopixel.NeoPixel(board.NEOPIXEL, 1)

led = digitalio.DigitalInOut(board.LED)
led.direction = digitalio.Direction.OUTPUT

sensor = adafruit_ahtx0.AHTx0(i2c)

displayio.release_displays()
display_bus = displayio.I2CDisplay(i2c, device_address=0x3D)
display = adafruit_ssd1327.SSD1327(display_bus, width=128, height=128, rotation=90, auto_refresh=False)

usb_cdc.data.timeout = 1.0


t0 = time.monotonic()

while True:
    now = time.monotonic()

    if ((now - t0) % 1.0) < 0.5:
        led.value = True
        pixels.fill((16, 0, 0))

    if ((now - t0) % 1.0) >= 0.5:
        led.value = False
        pixels.fill((0, 16, 0))

    if ((now - t0) % 5.0) < 0.01:
        display.refresh()

    if ((now - t0) % 1.1) < 0.01:
        print("({:0.3f},{:0.3f})".format(sensor.temperature, sensor.relative_humidity)) # Print tuple so Mu editor plotting works


    #print("%0.1f C, %0.1f %%" % (sensor.temperature, sensor.relative_humidity))
    #print((sensor.temperature, sensor.relative_humidity)) # Print tuple so Mu editor plotting works
    #print(now)
#    if supervisor.runtime.serial_bytes_available:
#        inText = input().strip()
#        print("input_text", inText)

    if usb_cdc.data.in_waiting >0:
        print("data ready", usb_cdc.data.in_waiting)
        cmd = usb_cdc.data.read(1)
        if cmd == b'?':
            #usb_cdc.data.write(b"asdf\n")
            outstr = "{:0.3f},{:0.3f}\n".format(sensor.temperature, sensor.relative_humidity)
            usb_cdc.data.write(outstr.encode())
