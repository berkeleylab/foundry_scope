from ScopeFoundry import HardwareComponent
import serial
from datetime import datetime
import time

class EnviroMonitorHW(HardwareComponent):
    
    name ='enviromon'
    
    def setup(self):
        
        self.settings.New('port', dtype=str, initial='COM5')
        self.settings.New('temperature', dtype=float, ro=True, unit='C', spinbox_decimals=3)
        self.settings.New('humidity', dtype=float, ro=True, unit=r'%', spinbox_decimals=3)
        self.settings.New('log_enable', dtype=bool, initial=True)
        self.settings.New('log_file', dtype=str, initial="enviromon_log_{date}.txt")
        self.settings.New('log_interval', dtype=float, vmin=0.1, initial=5.0, unit='s')
        
    
    def connect(self):
        S = self.settings
        self.ser = serial.Serial(S['port'], timeout=2.0)
        
    def disconnect(self):
        
        if hasattr(self, 'ser'):
            self.ser.close()
            del self.ser
        
        
    def read_state(self):
        self.ser.write(b"?")
        resp = self.ser.readline().decode()
        #print(resp.strip().split(','))
        t, h = [float(x) for x in resp.strip().split(',')]
        return t,h
    
    def write_log(self):
        S = self.settings
        now = datetime.now()
        fname = S['log_file'].format(date=now.strftime("%y%m%d"))
        #print(fname)
        with open(fname, 'a') as f:
            f.write("{},{:0.3f},{:0.3f}\n".format(
                now.isoformat(),
                S['temperature'],
                S['humidity']))
            f.flush()
                
                
    def threaded_update(self):
        S = self.settings
        try:
            t,h = self.read_state()
            S['temperature'] = t
            S['humidity'] = h
            if S['log_enable']:
                self.write_log()
        except Exception as err:
            print(self.name, "Error in treaded_update", err)
        time.sleep(self.settings['log_interval'])
