from ScopeFoundry.measurement import Measurement
from ScopeFoundry import h5_io
from ScopeFoundry.helper_funcs import sibling_path, load_qt_ui_file
import pyqtgraph as pg
import imageio
import time
import os



class PhotoRunMeasure (Measurement):
    
    name  = 'photo_run'
    
    def setup(self):
        
        self.settings.New('photo_x', dtype=int, unit='um', initial= 2000)
        self.settings.New('photo_y', dtype=int, unit='um', initial=10_000)
        
        self.settings.New('eject_after', dtype=bool, initial=False)
        
    def setup_figure(self):
        
        self.ui_filename = sibling_path(__file__,"photo_run_measure.ui")
        ui = self.ui = load_qt_ui_file(self.ui_filename)
        
        self.settings.activation.connect_to_widget(ui.run_checkBox)
            
        self.settings.eject_after.connect_to_widget(
            ui.ejectafter_checkBox)
        
        self.app.settings.sample.connect_to_widget(ui.sample_lineEdit)
        
        self.stage_hw = self.app.hardware['cbox_stage']
        
        self.stage_hw.settings.connected.connect_to_widget(
            ui.HW_checkBox)
        self.stage_hw.settings.x_position.connect_to_widget(
            ui.xpos_doubleSpinBox)
        self.stage_hw.settings.y_position.connect_to_widget(
            ui.ypos_doubleSpinBox)
        self.stage_hw.settings.x_target.connect_to_widget(
            ui.xtar_doubleSpinBox)
        self.stage_hw.settings.y_target.connect_to_widget(
            ui.ytar_doubleSpinBox)
        
        self.camera_hw = self.app.hardware['zwo_camera']
        
        self.camera_hw.settings.connected.connect_to_widget(
            ui.cameraHW_checkBox)
        
        ui.eject_pushButton.clicked.connect(
            self.stage_hw.go_to_load_pos)
        
        ui.insert_pushButton.clicked.connect(
            self.stage_hw.go_to_insert_pos)
                
    
    def run(self):
            
        S = self.settings
        self.t0 = time.time()

        # H5 Data file
        self.h5_file = h5_io.h5_base_file(self.app, measurement=self)
        self.h5_filename = self.h5_file.filename
        
        self.h5_file.attrs['time_id'] = self.t0
        H = self.h5_meas_group  =  h5_io.h5_create_measurement_group(self, self.h5_file)
        

        self.status = "Starting"
        cam = self.app.hardware['zwo_camera']
        current_exposure = cam.settings['Exposure']/1e6

        try:
            
            self.status = "Moving to Insert Position"
            self.stage_hw.go_to_insert_pos()
            
            
            self.status = "Moving to Photo position"
            self.stage_hw.settings['y_target'] = S['photo_y']
            self.stage_hw.settings['x_target'] = S['photo_x']
            time.sleep(2.0)
            self.stage_hw.wait_until_on_target('xy', timeout=20.0)
            self.status = 'At Photo position, say cheese!'
            time.sleep(0.5)
            
            
            
            #cam = self.app.hardware['pygrabber_camera']
            #img = cam.img_buffer[-1].copy()
            #H['photo'] = img
            
            # cam.camera.start_video_capture()
            #img = cam.camera.capture_video_frame()
            img = cam.camera.capture()
            time.sleep(2*current_exposure)            
            H['photo'] = img
            
            
            if self.interrupt_measurement_called:
                return
            
            self.status ="Moving to Back to Insert Position"
            self.stage_hw.go_to_insert_pos()

            if S['eject_after']:
                self.status = "Moving to Load position (ejecting)"
                self.stage_hw.go_to_load_pos()
        
        except Exception as err:
            self.status += f"\nException! {err}"
            raise err
        else:
            self.status = f"saved to {os.path.basename(self.h5_filename)}"
        finally:
            self.h5_file.close()
            print(f"saved to {self.h5_filename=}")
            print("save tif")
            imageio.imsave(self.h5_filename +".tif", img)
   

    def update_display(self):

        self.ui.status_label.setText(self.status)
        print("status:", self.status)
