from ScopeFoundry.scanning import BaseRaster2DSlowScan
import time
import numpy as np

class CBoxSpecMapMeasure(BaseRaster2DSlowScan):
    
    name = 'spec_map'
    
    def __init__(self, app):
        BaseRaster2DSlowScan.__init__(self, app, 
                                      h_limits=(0,93_000), v_limits=(0,20_000),
                                      h_spinbox_step = 100, v_spinbox_step=100,
                                      h_unit="um", v_unit="um",circ_roi_size=100)

    # def setup(self):
    #     BaseRaster2DSlowScan.setup(self)
    #     self.stage = self.app.hardware['cbox_stage']

    def move_position_start(self, h,v):
        print('start scan, moving to x={:.4f} , y={:.4f} '.format(h,v))
        self.stage.settings["x_target"] = h
        self.stage.settings["y_target"] = v
        while not self.stage.on_target('xy'):
            time.sleep(0.03)
                
    def move_position_slow(self, h,v,dh,dv):   
        print('new line, moving to x={:.4f} , y={:.4f} '.format(h,v))
        self.move_position_start(h, v)

    def move_position_fast(self, h,v,dh,dv):
        self.move_position_start(h, v)
    
    
        
    
    def scan_specific_setup(self):
        self.settings.New('spec_hw', dtype=str, initial='pl_spec')
        #Hardware                  
        self.stage = self.app.hardware['asi_stage']
        self.add_operation('center scan on position',self.center_scan_on_pos)
        self.add_operation('center view on scan', self.center_view_on_scan)
        
        # details_widget = QWidget()
        # details = QVBoxLayout()
        # details.addWidget(self.app.settings.New_UI(include=['save_dir','sample']))
        # scan_params_settings = ['h_span', 'pixel_time', 'v_span',  'frame_time']
        # details.addWidget(create_grid_layout_widget(self.settings, scan_params_settings))
        # details.addWidget(create_operation_grid(self.operations))
        # details_widget.setLayout(details)
        # self.set_details_widget(widget=details_widget)
        
    def pre_scan_setup(self):
        self.settings.save_h5.change_readonly(True)
        if 'bg_subtract' in self.spec.settings:
            self.spec.settings['bg_subtract'] = False
        self.spec.settings['continuous'] = False
        self.spec.settings['save_h5'] = False
        time.sleep(0.01)
        self.stage.other_observer = True
    
    
    def collect_pixel(self, pixel_num, k, j, i):
        if self.settings['debug']: print("collect_pixel", pixel_num, k,j,i)
        self.spec.interrupt_measurement_called = self.interrupt_measurement_called
        # self.start_nested_measure_and_wait(self.spec)
        self.spec.run()
        print('spectrometer run complete')
        if pixel_num == 0:
            self.log.info("pixel 0: creating data arrays")
            if self.settings['debug']: print("pixel 0: creating data arrays")
            self.time_array = np.zeros(self.scan_h_positions.shape)
            spec_map_shape = self.scan_shape + self.spec.spectrum.shape
            
            self.spec_map = np.zeros(spec_map_shape, dtype=np.float)
            if self.settings['save_h5']:
                self.spec_map_h5 = self.h5_meas_group.create_dataset(
                                      'spec_map', spec_map_shape, dtype=np.float)
            else:
                self.spec_map_h5 = np.zeros(spec_map_shape)
 
            self.wls = np.array(self.spec.wls)
            if self.settings['save_h5']:
                self.h5_meas_group['wls'] = self.wls

        # store in arrays
        t = self.time_array[pixel_num] = time.time()
        print('time', t)
        spec = np.array(self.spec.spectrum)
        print(f"collect_pixel, {pixel_num}, {k}, {j}, {i}, {spec.shape}")
        self.spec_map[k,j,i,:] = spec
        self.spec_map_h5[k,j,i,:] = spec
        s = self.display_image_map[k,j,i] = spec.sum()
        if self.settings['debug']: print('time', t, 'sum', s)
        
    def post_scan_cleanup(self):
        self.settings.save_h5.change_readonly(False)
        self.stage.other_observer = False
        self.ui.setWindowTitle(self.name)

    def update_time(self):
        if ('run' in self.settings['run_state']) and hasattr(self, 'time_array'):
            
            if self.time_array[2] != 0:
                print(len(self.time_array))
                px_times = np.diff(self.time_array)
                print(np.nonzero(px_times)[0])
                num_px = len(np.nonzero(px_times)[0])
                avg_pixel_time = np.average(px_times[0:(num_px-1)])
                print(avg_pixel_time)
                self.settings.pixel_time.update_value(avg_pixel_time)
                
                total_time = np.sum(px_times[0:num_px-1])
                total_time_str = seconds_to_time_string(total_time)
                elapsed_time_str = seconds_to_time_string(self.settings['total_time'] - total_time)
                # self.ui.status_lineEdit.setText("time elapsed: %s, time remaining: %s" % (total_time_str, elapsed_time_str))
                self.ui.setWindowTitle("%s time elapsed: %s, time remaining: %s" % (self.name, total_time_str, elapsed_time_str))
        else: 
            for kk in ['exposure_time', 'int_time']:
                if kk in list(self.spec.hw.settings.keys()):
                    if self.settings['pixel_time'] != self.spec.hw.settings[kk]:
                        self.settings.pixel_time.update_value(self.spec.hw.settings[kk])
                        #self.ui.status_lineEdit.setText("estimated scan duration: %s" % seconds_to_time_string(self.settings['total_time']))
    
    
    def center_scan_on_pos(self):
        self.settings.h_center.update_value(new_val=self.stage.settings.x_position.val)
        self.settings.v_center.update_value(new_val=self.stage.settings.y_position.val)
        
    def center_view_on_scan(self):
        delta = 0.2
        del_h = self.h_span.val*delta
        del_v = self.v_span.val*delta
        self.img_plot.setRange(xRange=(self.h0.val-del_h, self.h1.val+del_h), yRange=(self.v0.val-del_v, self.v1.val+del_v))

    def interrupt(self):
        ASIStage2DScan.interrupt(self)
        self.spec.interrupt()
    
    def update_display(self):
        self.update_time()
        ASIStage2DScan.update_display(self)
        self.spec.update_display()