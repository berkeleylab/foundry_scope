from ScopeFoundry import Measurement
from ScopeFoundry import h5_io
from ScopeFoundry.helper_funcs import sibling_path, load_qt_ui_file
import pyqtgraph as pg
import numpy as np
import time

class CBoxSpecLineScan(Measurement):
    
    name = 'spec_line_scan'
    
    def setup(self):
        
        self.settings.New('y', dtype=int, unit='um')
        self.settings.New_Range('x', dtype=int, unit='um', 
                                initials = [70_000,90_000,1_000],
                                include_center_span=True)
        
        self.settings.New('spec_hw', dtype=str, initial='pl_spec')
        
    def setup_figure(self):
        self.ui_filename = sibling_path(__file__,"spec_line_scan.ui")
        ui = self.ui = load_qt_ui_file(self.ui_filename)
        
        self.settings.activation.connect_to_widget(ui.run_checkBox)
        
        self.settings.spec_hw.connect_to_widget(ui.spec_hw_lineEdit)
        self.settings.y.connect_to_widget(ui.y_doubleSpinBox)
        self.settings.x_min.connect_to_widget(ui.x0_doubleSpinBox)
        self.settings.x_max.connect_to_widget(ui.x1_doubleSpinBox)
        self.settings.x_step.connect_to_widget(ui.xstep_doubleSpinBox)
        
        
        
        self.stage_hw = self.app.hardware['cbox_stage']
        
        self.stage_hw.settings.connected.connect_to_widget(
            ui.stage_hw_connect_checkBox)
        self.stage_hw.settings.x_position.connect_to_widget(
            ui.x_pos_doubleSpinBox)
        self.stage_hw.settings.y_position.connect_to_widget(
            ui.y_pos_doubleSpinBox)
        self.stage_hw.settings.x_target.connect_to_widget(
            ui.x_target_doubleSpinBox)
        self.stage_hw.settings.y_target.connect_to_widget(
            ui.y_target_doubleSpinBox)
        
        
        self.plot = pg.PlotWidget()
        self.ui.plot_groupBox.layout().addWidget(self.plot)

        self.plotline = self.plot.plot()
        
        self.plot.setLabel('bottom', 'wavelength (nm)')

        
    def run(self):
        self.t0 = time.time()
        
        self.spec_hw = self.app.hardware[self.settings['spec_hw']]
        self.x_array = self.settings.ranges['x'].array
        

        print(self.x_array)


        self.stage_hw.settings['y_target'] = self.settings['y']
        self.stage_hw.wait_until_on_target('y', timeout=20.0)
        
        
        # H5 Data file
        self.h5_file = h5_io.h5_base_file(self.app, measurement=self)
        self.h5_filename = self.h5_file.filename
        
        self.h5_file.attrs['time_id'] = self.t0
        H = self.h5_meas_group  =  h5_io.h5_create_measurement_group(self, self.h5_file)
        
        #create h5 data arrays
        H['x_array'] = self.x_array


        try:
    
            N = len(self.x_array)
            for ii, x in enumerate(self.x_array):
                if self.interrupt_measurement_called:
                    break
                
                self.stage_hw.settings['x_target'] = x
                self.stage_hw.wait_until_on_target('x', timeout=20.0)
                
                print(ii, x)
                self.spec_hw.acquire_spectrum()
                self.wls = self.spec_hw.wavelengths
                self.spectrum = self.spec_hw.get_spectrum() # make a copy
                
                if ii == 0:
                    self.spectra = np.zeros((N, len(self.spectrum)), dtype=float)
                    self.spectra_h5 = H.create_dataset('spectra', self.spectra.shape, dtype=float)
                    H['wls'] = self.wls
                    
                self.spectra[ii] = self.spectrum
                self.spectra_h5[ii] = self.spectrum

        finally:
            self.h5_file.close()
            print(f"saved to {self.h5_filename=}")
    
    def update_display(self):
        if hasattr(self, 'wls'):
            self.plotline.setData(self.wls, self.spectrum)

            

            
        
