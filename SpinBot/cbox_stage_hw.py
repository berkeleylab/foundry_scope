from ScopeFoundry import HardwareComponent
import time
import threading
from ScopeFoundry.logged_quantity import LoggedQuantity

class SpinBotCBoxStageHW(HardwareComponent):
    
    name = 'cbox_stage'
    
    def setup(self):

        # Position
        
        xy_kwargs = dict(initial = 0,
                          dtype=int,
                          unit='um')
        x_pos = self.settings.New('x_position', ro=True, **xy_kwargs)        
        y_pos = self.settings.New('y_position', ro=True, **xy_kwargs)
        
        x_target = self.settings.New('x_target', ro=False, **xy_kwargs)        
        y_target = self.settings.New('y_target', ro=False, **xy_kwargs)
        
        self.settings.New('x_on_target', dtype=bool, ro=True)
        self.settings.New('y_on_target', dtype=bool, ro=True)


        # Load / Insert Coords
        
        self.settings.New('load_x', dtype=int, initial=85_000,
                           ro=False, unit='um')  # also used for insert_x 

        self.settings.New('load_y', dtype=int, initial=100_000, unit='um')

        self.settings.New('insert_y', dtype=int, initial=20_000, unit='um')

        self.settings.New('is_at_load', dtype=bool, ro=True, initial=False)        


        # Safe Motion Limits (when inserted)
        self.settings.New('is_inserted', dtype=bool, ro=True, initial=False)
        
        self.settings.New('y_min', dtype=int, initial=0,
                           ro=False, unit='um')

        self.settings.New('y_max', dtype=int, initial=20_500,
                           ro=False, unit='um')


        self.settings.New('x_min', dtype=int, initial=0,
                           ro=False, unit='um')

        self.settings.New('x_max', dtype=int, initial=93_000,
                           ro=False, unit='um')
        
        
        
        
        # Named Positions
        self.settings.New('center_PL_x', dtype=int, initial=88_000,
                   ro=False, unit='um')
        self.settings.New('center_PL_y', dtype=int, initial=20_000,
                   ro=False, unit='um')

        self.settings.New('center_WL_x', dtype=int, initial=70_000,
                   ro=False, unit='um')
        self.settings.New('center_WL_y', dtype=int, initial= 2_000,
                   ro=False, unit='um')        

        self.settings.New('center_CAM_x', dtype=int, initial=10_000,
                   ro=False, unit='um')
        self.settings.New('center_CAM_y', dtype=int, initial=10_000,
                   ro=False, unit='um')
        
        # Operations
        
        self.add_operation('GOTO Load', self.go_to_load_pos)
        self.add_operation('GOTO Insert', self.go_to_insert_pos)
        self.add_operation('Home Axes', self.home_both_stages)

    def connect(self):
        
        self.x_hw = under_x_hw = self.app.hardware['x_under_stage']
        self.y_hw = over_y_hw = self.app.hardware['y_over_stage']
        
        self.x_hw.settings['connected'] = True
        self.y_hw.settings['connected'] = True
        
        time.sleep(1.0)
        
        self.x_hw_lq = under_x_hw.settings.position
        self.y_hw_lq =  over_y_hw.settings.position

        self.x_target_hw_lq = under_x_hw.settings.target_pos
        self.y_target_hw_lq =  over_y_hw.settings.target_pos


        # self.settings.x_position.connect_to_lq(self.x_hw_lq)
        # self.settings.y_position.connect_to_lq(self.y_hw_lq)
        
        self.x_hw_lq.updated_value.connect(self.settings.x_position.update_value)
        self.y_hw_lq.updated_value.connect(self.settings.y_position.update_value)

        self.settings['x_position'] = self.x_hw_lq.value
        self.settings['y_position'] = self.y_hw_lq.value
        
        self.x_hw_lq.add_listener(self.stage_position_update_listener)
        self.y_hw_lq.add_listener(self.stage_position_update_listener)
        
        self.settings.x_on_target.connect_to_lq(self.x_hw.settings.target_reached)
        self.settings.y_on_target.connect_to_lq(self.y_hw.settings.target_reached)
        
        
        # Preload target positions from stages
        self.settings['x_target'] = under_x_hw.settings['target_pos']
        self.settings['y_target'] = over_y_hw.settings['target_pos']
        
        # connect to hardware
        self.settings['x_target'] = self.x_target_hw_lq.value
        self.settings['y_target'] = self.y_target_hw_lq.value
        
        self.settings.x_target.connect_to_hardware(
            write_func = self.on_new_x_target)
        self.settings.y_target.connect_to_hardware(
            write_func = self.on_new_y_target)
        
        
    def disconnect(self):

        self.settings.disconnect_all_from_hardware()
                
        if hasattr(self, 'x_hw'):
            self.x_hw.settings['connected'] = False
            self.y_hw.settings['connected'] = False
        
        
    def stage_position_update_listener(self):
        x = self.x_hw_lq.read_from_hardware()
        y = self.y_hw_lq.read_from_hardware()
        
        # Tolerance for locations
        delta = 100 # um
        
        # is_inserted
        if y < (self.settings['insert_y'] + delta):
            self.settings['is_inserted'] = True
        else:
            self.settings['is_inserted'] = False
            
        # is_at_load
        at_load_x = (abs(x-self.settings['load_x']) < delta )
        at_load_y = (abs(y-self.settings['load_y']) < delta )
        if at_load_x and at_load_y:
            self.settings['is_at_load'] = True
        else: 
            self.settings['is_at_load'] = False
            

        # Safety check: stage should not extend past y=insert_y unless 
        # stage is at load_x
        # if it does, HALT both stages
        if y > (self.settings['insert_y'] + delta) and not at_load_x:
            self.x_hw.halt()
            self.y_hw.halt()
            print("HALT Stages due to out of bounds")
            
        
    def on_new_x_target(self, new_x):
        # Safety check any moves
        S = self.settings
        y_current = self.y_hw_lq.read_from_hardware()
        y_in_range = (S['y_min'] <= y_current <=S['y_max'])        
        new_x_in_range = (S['x_min'] <= new_x <=S['x_max'])
        
        if y_in_range and new_x_in_range:
            self.x_target_hw_lq.update_value(new_x)
        else:
            print(f"on_new_x_target Safety Check failed {y_in_range=} {new_x_in_range=}")
        
    def on_new_y_target(self, new_y):
        # Safety check any moves
        S = self.settings
        x_current = self.x_hw_lq.read_from_hardware()
        new_y_in_range = (S['y_min'] <= new_y <=S['y_max'])        
        x_in_range = (S['x_min'] <= x_current <=S['x_max'])    
        
        if new_y_in_range and x_in_range:
            self.y_target_hw_lq.update_value(new_y)
        else:
            print(f"on_new_y_target Safety Check failed {x_in_range=} {new_y_in_range=}")
            
        
    def wait_until_on_target(self, axes='xy', timeout=20.0):
        t0 = time.monotonic()
        time.sleep(0.2)
        while not self.on_target(axes):
            if (time.monotonic() - t0) > timeout:
                raise IOError("wait_until_on_target: Stage did not reach target during timeout period")
            time.sleep(0.25)

    def on_target_lq_test(self, axes='xy'):
        if axes == 'xy':
            return self.settings['x_on_target'] and self.settings['y_on_target']
        elif axes == 'x':
            return self.settings['x_on_target']
        elif axes == 'y':
            return self.settings['y_on_target']
        else:
            raise ValueError("_on_target_axes Axes must be xy, x, or y")

    def on_target(self, axes='xy', tolerance=50):
        S = self.settings 
        
        S['x_position'] = x = self.x_hw_lq.read_from_hardware()
        S['y_position'] = y = self.y_hw_lq.read_from_hardware()
                
        x_delta = abs(x-S['x_target'])
        y_delta = abs(y-S['y_target'])
        
        x_on_target = (x_delta < tolerance)
        y_on_target = (y_delta < tolerance)

        print("on_target2", axes, tolerance, x_delta, y_delta)

        if axes == 'xy':
            return x_on_target and y_on_target
        elif axes == 'x':
            return x_on_target
        elif axes == 'y':
            return y_on_target
        else:
            raise ValueError("_on_target_axes Axes must be xy, x, or y")
        


    # Operations

    def go_to_insert_pos(self):
        self.settings['x_target'] = self.settings['load_x']
        self.settings['y_target'] = self.settings['insert_y']
        self.wait_until_on_target('xy', timeout=30)
        
    def go_to_load_pos(self):
        print ("go to load")
        
        self.settings['x_target'] = self.settings['load_x']
        
        time.sleep(0.2)
        # wait until on target for x
        self.wait_until_on_target('x', timeout=20)
        
        print("\t x at target")
        
        # This will not actually cause motion due to safety checks    
        self.settings['y_target'] = self.settings['load_y']
        # Forcefully set low-level y_target (no safety check)
        self.y_hw.settings['target_pos'] = self.settings['load_y']
        self.wait_until_on_target('xy', timeout=30)
        
        print("\t y at target", self.settings['y_target'], self.y_hw.settings['target_pos'], self.y_hw_lq.value)
        

    def home_both_stages(self):
        # Home Y first (retract to y = 0)
        self.y_hw.start_home()
        self.wait_until_on_target('y', 30.0) # if homing fails, will raise IOError
        
        self.x_hw.start_home()
        self.wait_until_on_target('x', 30.0) # if homing fails, will raise IOError

        
        
        
        