from ScopeFoundry.scanning.base_raster_slow_scan import BaseRaster2DSlowScan
import PyDAQmx as mx
import numpy as np
import time


class LSC_SlowScan(BaseRaster2DSlowScan):
    
    name = 'lsc_slowscan'
    
    def __init__(self, app, 
        h_limits=(-7, +7), v_limits=(-7, +7), 
        h_unit='', v_unit='', 
        h_spinbox_decimals=3, v_spinbox_decimals=3, 
        h_spinbox_step=0.1, v_spinbox_step=0.1, 
        use_external_range_sync=False, 
        circ_roi_size=0.25):
        BaseRaster2DSlowScan.__init__(self, app, h_limits=h_limits, v_limits=v_limits, h_unit=h_unit, v_unit=v_unit, h_spinbox_decimals=h_spinbox_decimals, v_spinbox_decimals=v_spinbox_decimals, h_spinbox_step=h_spinbox_step, v_spinbox_step=v_spinbox_step, use_external_range_sync=use_external_range_sync, circ_roi_size=circ_roi_size)
    
    def setup(self):
        BaseRaster2DSlowScan.setup(self)

        self.stage_hw = self.app.hardware['galvo_scanner']
        
        
    def move_position_start(self,h,v):
        self.stage_hw.settings['x_target_deg'] =h
        self.stage_hw.settings['y_target_deg'] =v
        
    def move_position_slow(self, h, v, dh, dv):
        self.move_position_start(h, v)

    def move_position_fast(self, h, v, dh, dv):
        self.move_position_start(h, v)


class LSC_PMT_SlowScan(LSC_SlowScan):
    
    name = 'lsc_pmt_slowscan'
    
    
    def pre_scan_setup(self):
        pass
#         self.ai_chan_path = '/Dev2/ai1'
#         ai_task = self.ai_task = mx.Task()
#         ai_task.CreateAIVoltageChan(self.ai_chan_path, '', mx.DAQmx_Val_RSE, -10.0, +10.0, mx.DAQmx_Val_Volts,'')
#         self.ai_chan_count = task_get_num_chans(ai_task)
#         self.ai_rate = 100e3 # 100kHz
    
    def collect_pixel(self, pixel_num, k, j, i):
        
#         int_time = 1e-3 # seconds
#         
#         N = int(np.ceil(self.ai_rate*int_time))
#         
#         #ai_data = np.zeros( (N, self.ai_chan_count), dtype=float)
#         ai_data = np.zeros(self.ai_chan_count, dtype=float)
# 
#         #self.ai_task.CfgSampClkTiming("", self.ai_rate, mx.DAQmx_Val_Rising,
#         #                   mx.DAQmx_Val_FiniteSamps , N)
#         read_count = mx.int32(0)    #returns samples per chan read
# 
#         self.ai_task.ReadAnalogF64(1, 0.1, mx.DAQmx_Val_GroupByScanNumber,
#                                    ai_data, ai_data.size, mx.byref(read_count), None)
#         
#         self.display_image_map[k,j,i] = np.mean(ai_data)
# 
#         print(pixel_num,k,j,i, read_count, np.mean(ai_data))
        #self.ai_task.StopTask()
        
        self.display_image_map[k,j,i] = self.stage_hw.settings['pmt1']
        time.sleep(0.05)

        
    def post_scan_cleanup(self):
        pass
        #self.ai_task.StopTask()

def task_get_num_chans(task):
    chan_count = mx.uInt32(0)
    task.GetTaskNumChans(mx.byref(chan_count))
    return chan_count.value
