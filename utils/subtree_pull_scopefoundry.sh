#!/bin/sh
cd "$(dirname "$0")"/..

git subtree pull --prefix ScopeFoundry/ https://edbarnard@github.com/ScopeFoundry/ScopeFoundry.git master --squash