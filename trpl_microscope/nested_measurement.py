'''
Created on May 1, 2021

@author: lab


A bunch of measurements for people that should be on strike.
'''

from ScopeFoundry.measurement import Measurement
import time
import numpy as np


class NestedMeasure(Measurement):
    
    name = 'nested_measurement'
    
    def run(self):
        x0, y0 = 1.05560, -3.96695
        # self.collect_multiple_power_scan()

        M = self.app.measurements
        # measurements = [M['auto_focus'], M['andor_ccd_readout'], M['andor_ccd_readout'], M['andor_ccd_readout'], M['andor_ccd_readout'], M['andor_ccd_readout']]
        measurements = [M['power_scan']]
        # self.sweep_temperature(start=398, stop=67, step=-10, reps=2,
                               #measurements=measurements, delay=15)
        
        self.lakeshore_do_while()
        
    
        
    def lakeshore_do_while(self):
        M = self.app.measurements
        ToDos = [   ('hardware/polarizer/position', 98),
                    ('measurement/andor_ccd_readout/explore_mode', True),
                    M['auto_focus'],
                    ('measurement/andor_ccd_readout/explore_mode', False),
                  ]
        for i in range(110, 90, -2):
            ToDos.append(('hardware/polarizer/position', i))
            ToDos.append(M['andor_ccd_readout'])
            
        print(ToDos)
        #self.app.measurements.lakeshore_measure.do_while(None, 270, ToDos, 0.01 , 12 * 3600)
        self._lakeshore_do_while(None, 400, ToDos, None, 12 * 3600 )
      
      
    def _lakeshore_do_while(self, Tstart=None, Tstop=350, ToDos=[],
                        ramp_rate=None, timeout=30 * 60):
        """when stable at setpoint_T=*Tstart*, runs *ToDos* repeatedly until one of the following criterion is met:
            - lakeshore measurement interrupted
            - timeout passed
            - Tstop is reached
            
            
        *ToDos* is a list containing:
            - type Measurement object
            - ("lq_path", value)
        """
        TS = self.app.hardware['lakeshore331'].settings
        M = self.app.measurements['lakeshore_measure']
        MS = M.settings
        
        if Tstart:
            pass
            #self.queue_measurements(Tstart, [])
        else:
            Tstart = TS[f'T_{TS["control_input"]}']

        TS['setpoint_T'] = Tstop
        
        if ramp_rate != None:
            TS['setpoint_T'] = Tstop
            TS["ramp_rate"] = ramp_rate
            TS["ramp_on"] = True
        else:
            pass
            TS['heater_range'] = 'off' 

        t0 = time.time()
        
        MS['is_awake'] = False

        BREAK = False        
        
        while not BREAK:
            for item in ToDos:
                
                #self.reset()
                print(self.name, item)
                
                if time.time() - t0 > timeout:
                    msg = 'exit due to timeout'
                    #M.add_history_event('exit due to timeout')
                    BREAK = True
                    break
                
                elif self.interrupt_measurement_called or M.settings['is_awake']:
                    msg = 'exit: measure interrupted'
                    #M.add_history_event('exit: measure interrupted')
                    BREAK = True
                    break
                
                # elif not TS["is_ramping"]:
                #    self.add_history_event('exit ramping False')
                #    BREAK = True
                #    break
                
                if hasattr(item, 'run'):
                        msg = f'started {item.name}'
                        #M.add_history_event(f'started {item.name}')
                        self.start_nested_measure_and_wait(item, False)

                elif hasattr(item, '__getitem__'):
                    if len(item) == 2:
                        path, value = item
                        self.app.lq_path(path).update_value(value)
                        msg = f'set {path}={value}'
                        #M.add_history_event(f'set {path}={value}')
                        
                T = TS[f'T_{TS["control_input"]}']
                pct = ((T - Tstart) / (Tstop - Tstart)) * 100
                self.set_progress(pct)
                if pct >= 100:
                    msg = f'exit T_{TS["control_input"]} reached Tstop={Tstop}'
                    #M.add_history_event(f'exit T_{TS["control_input"]} reached Tstop={Tstop}')
                    BREAK = True
                    break

                print(msg)
                time.sleep(0.1)
                
        MS['is_awake'] = True
        TS['heater_range'] = 'off'
        M.set_history_end()    
      
        
    def successive_ToDos(self, ToDos=[], timeout=3 * 3600, break_condition=lambda:False):
        
        BREAK = False        
 
        t0 = time.time()
         
        dProgress = 100 / len(ToDos)

        while not BREAK:
            for i, item in enumerate(ToDos):
                
                print(self.name, item)
                
                if time.time() - t0 > timeout:
                    self.add_history_event('exit due to timeout')
                    BREAK = True
                    break
                
                elif self.interrupt_measurement_called or not self.settings['stabilization_interrupted']:
                    self.add_history_event('exit: measure interrupted')
                    BREAK = True
                    break
                
                if break_condition():
                    BREAK = True
                    break                    
                
                if hasattr(item, 'run'):
                        self.add_history_event(f'started {item.name}')
                        self.start_nested_measure_and_wait(item, False)

                elif hasattr(item, '__getitem__'):
                    if len(item) == 2:
                        path, value = item
                        self.app.lq_path(path).update_value(value)
                        self.add_history_event(f'set {path}={value}')
                        
                    if len(item) == 3:
                        t, path, value = item

                self.set_progress((i + 1) * dProgress)

                time.sleep(0.1)
        
    def sweep_temperature(self, start=66, stop=417, step=20, x0=None, y0=None,
                          reps=1,
                          measurements=None,
                          timeout=20 * 60,
                          delay=60):
        
        Ts = list(range(start, stop, step))    
        dProgress = 100 / len(Ts)

        for ii, setpoint_T in enumerate(Ts):
            self.set_progress(ii * dProgress)
            
            if self.interrupt_measurement_called:
                break
            
            S = self.app.measurements.lakeshore_measure.settings    
            if not S['activation']:
                S['activation'] = True
            
            S['is_awake'] = False
    
            t0 = time.time()
            
            # set temperature settings
            TS = self.app.hardware['lakeshore331'].settings
            if setpoint_T:
                TS['setpoint_T'] = setpoint_T
                time.sleep(delay)
            
            # wait 
            while time.time() - t0 < timeout:
                pct = 100 * ((timeout - (time.time() - t0)) / timeout)
                self.set_progress(pct)
                print(self.name, 'setpoint_T', setpoint_T,
                      'stabilization, press wake_up or wait',
                      timeout - (int(time.time() - t0)), 'sec')
                if S['is_awake']:
                    break
                if S['is_std_stable']:
                    break
                time.sleep(S['update_period'])
            S['is_awake'] = True
            
            for i in range(reps):
                self.set_progress((ii + (i + 1) / reps) * dProgress)
                if self.interrupt_measurement_called:
                    break        

                # run measurements
                if measurements:
                    if not hasattr(measurements, '__getitem__'):
                        measurements = [measurements]
                    for measure in measurements:
                        time.sleep(0.2)
                        if self.interrupt_measurement_called:
                            break
                        self.start_nested_measure_and_wait(measure, nested_interrupt=False)
                
    def _sweep_temperature(self, start=65, stop=410, step=0, x0=None, y0=None, reps=1):
        
        Ts = list(range(start, stop, step))
        tctrl = self.app.hardware['lakeshore331']

        print(self.name, 'sweep_temperature temperature setpoint', Ts)
        self.log.info(f'sweep_temperature temperature setpoint')

        dProgress = 100 / len(Ts)

        for ii, T in enumerate(Ts):
            
            self.set_progress(ii * dProgress)

            if self.interrupt_measurement_called:
                break
            print(self.name, 'sweep_temperature', ii, T)
            
            self.set_temperature_and_wait(T, 20 * 60)
            if self.interrupt_measurement_called:
                break
            
            for i in range(reps):
                self.app.measurements.lakeshore_measure.add_history_event('started power_scan', color=(0, 255, 255))
                self.set_progress((ii + (i + 1) / reps) * dProgress)
                self.log.info(f'starting power_san started {i+1} of {reps}')
                self.collect_power_scan()
                
            if self.interrupt_measurement_called:
                break
            
    def set_temperature_and_wait(self, T, timeout=180,
                                 delay_after_setpoint_update=10):
        '''uses feed back to stabilize the temperature, 
        NOTE. Does NOT care about Temperature error. Just returns when stable
        or after timeout period has passed. 
        Stability criterion: the standard deviation of the 15 most last data
        points is bellow a threshhold value.
        Recommend 'Manual PID' with 50,20,100'''

        self.log.info(f'set_temperature_and_wait {T} started')

        tctrl = self.app.hardware['lakeshore331']
        TS = tctrl.settings
        TS['setpoint_T'] = T
        
        ramp_on_state_0 = TS['ramp_on']
        TS['ramp_on'] = False
        TS['heater_range'] = 'high (50W)'    
        t0 = time.time()
        
        time.sleep(delay_after_setpoint_update) 
        
        control_temp = f'T_{TS["control_input"]}'
        
        std = 5  # metric for stability, if std < 0.005 then assumed to be stable. 
        series = []
        while time.time() - t0 < timeout and std > 0.005:
            series.append(TS[control_temp])
            time.sleep(1)
            if len(series) > 15:
                std = np.std(series[-15:])
            print('waiting T to settle dt, error_T, std',
                  timeout - (time.time() - t0),
                  abs(T - TS[control_temp]),
                  std)
            if self.interrupt_measurement_called:
                break
        TS['ramp_on'] = ramp_on_state_0  
        
    def prepare_for_aquisition(self):
        polS = self.app.hardware['polarizer'].settings
        pol0 = polS['position']
        polS['position'] = 195
        
        ssS = self.app.hardware['shutter_servo'].settings
        ssS['shutter_open'] = True 

        self.auto_focus(z_span=100, z_num=11)
        self.app.measurements.andor_ccd_readout.settings['explore_mode'] = False
        
        time.sleep(0.2)
        self.goto_max_intensity_spot(pw_pos=195, exposure_time=1)

        ssS = self.app.hardware['shutter_servo'].settings
        ssS['shutter_open'] = False             
        polS['position'] = pol0
        
    def auto_focus(self, z_span=100, z_num=11):
        print(self.name, 'auto_focus')
        afm = self.app.measurements['auto_focus']
        afmS = afm.settings
        
        self.afm0 = {}
        for lqname, lq in afm.settings.as_dict().items():
            self.afm0.update({lqname:lq.val})    
        afmS['z_span'] = z_span
        afmS['z_num'] = z_num
        self.app.measurements.andor_ccd_readout.settings['explore_mode'] = True
        time.sleep(0.2)

        self.start_nested_measure_and_wait(afm, nested_interrupt=False)
        
        for lqname, val in self.afm0.items():
            if lqname == 'connected':
                continue
            afm.settings[lqname] = val
            
        print(self.name, 'auto_focuse finished')
        
        self.app.measurements.andor_ccd_readout.settings['explore_mode'] = True
        time.sleep(0.2)
                
    def goto_max_intensity_spot(self, pw_pos=195, exposure_time=1):
        
        p0 = self.app.hardware.polarizer.settings['position']
        self.app.hardware.polarizer.settings['position'] = pw_pos        
        
        self.app.measurements.andor_ccd_readout.settings['explore_mode'] = False
        time.sleep(0.5)
        self.ccd_state0 = {}
        ccdHW = self.app.hardware.andor_ccd
        for lqname, lq in ccdHW.settings.as_dict().items():
            self.ccd_state0.update({lqname:lq.val})                

        self.app.hardware.andor_ccd.settings['exposure_time'] = exposure_time
        ccdHW.settings['acq_mode'] = 'single'            

        measure = self.app.measurements.asi_hyperspec_scan        
        self.start_nested_measure_and_wait(measure, nested_interrupt=False)
        time.sleep(0.1)

        a = measure.display_image_map[0, :, :]
        
        j, i = np.unravel_index(a.argmax(), a.shape)
        x = measure.h_array[i]
        y = measure.v_array[j]
        print(j, i, x, y)        
        
        sS = self.app.hardware['asi_stage'].settings
        sS['x_target'] = x
        sS['y_target'] = y
        
        # measure.settings['h_center'] = x
        # measure.settings['v_center'] = y

        for lqname, val in self.ccd_state0.items():
            if lqname == 'connected':
                continue
            ccdHW.settings[lqname] = val 
        self.app.hardware.polarizer.settings['position'] = p0
        time.sleep(0.1)
        
    def timed_measurement(self):
        
        for i in range(40):
            measure = self.app.measurements['andor_ccd_readout']
            if self.interrupt_measurement_called:
                break
            self.start_nested_measure_and_wait(measure, nested_interrupt=False)
        
    def sweep_temperature_down(self, start=300, stop=70, step=-5, x0=0, y0=0):
        
        Ts = list(range(start, stop, step))
        
        tctrl = self.app.hardware['lakeshore331']
        TS = tctrl.settings
        TS['heater_range'] = 'high (50W)'
        
        for ii, T in enumerate(Ts):
            
            if self.interrupt_measurement_called:
                break
            
            self.set_progress((ii + 1.0) * 100 / len(Ts))
            
            tctrl = self.app.hardware['lakeshore331']
            TS = tctrl.settings
            TS['setpoint_T'] = T
            print('current setpoint T', T)
                        
            while (abs(T - TS['T_A']) > 10):                
                print('heating/cooling to', T, 'deltaT', T - TS['T_A'])
                time.sleep(1)
                if self.interrupt_measurement_called:
                    break
            print('going to sleep ')
            t0 = time.time()
            
            sleep_t = 1
            while time.time() - t0 < sleep_t:
                time.sleep(1)
                print('waiting to equilib.', sleep_t - (time.time() - t0))
                if self.interrupt_measurement_called:
                    break
            print('walking up, assuming ')
            if self.interrupt_measurement_called:
                break
            self.collect_multiple_power_scan(x0, y0)
     
    def collect_multiple_power_scan(self):
        print('starting multiple power_san')
        
        sS = self.app.hardware['asi_stage'].settings
        # x0, y0 = sS['x_position'], sS['y_position']
        x0, y0 = 0.0608, -0.085963
        for ii, (x, y) in enumerate([(0, 0),
                     (-0.001, 0),
                     (0.001, 0.),
                     (0.002, -0.001),
                     (0.002, +0.001),
                    ]):
            
            if self.interrupt_measurement_called:
                break
            sS['x_target'] = x0 + x
            sS['y_target'] = y0 + y
            time.sleep(0.5)
            self.set_progress((ii + 1.0) * 100 / 4)
            self.collect_power_scan()
                        
    def collect_power_scan(self):
        psm = self.app.measurements['power_scan']
        psmS = psm.settings
        self.start_nested_measure_and_wait(psm, nested_interrupt=False)
    
    def collect_multiple_power_scan_(self, x0=0, y0=0):
        print('starting multiple power_san')
        psm = self.app.measurements['power_scan']
        psmS = psm.settings
        
        sS = self.app.hardware['asi_stage'].settings
        pwS = self.app.hardware['power_wheel'].settings
        ssS = self.app.hardware['shutter_servo'].settings
        fmS = self.app.hardware['flip_mirror'].settings
        afm = self.app.measurements['auto_focus']
        ccdS = self.app.measurements['andor_ccd_readout'].settings

        afmS = afm.settings

        if False: 
            print('collect_multiple_power_scan: autofocus')
            # auto focus
            afmS['z_span'] = 150
            afmS['use_fine_optimization'] = False
            pwS['target_position'] = 230
            ssS['shutter_open'] = True 
            self.start_nested_measure_and_wait(afm, nested_interrupt=False)
            afmS['z_span'] = 40
            afmS['use_fine_optimization'] = True
            pwS['target_position'] = 210
            ssS['shutter_open'] = False 
        
        for x, y in [  # (0, 0),
                     (0.001, 0),
                     (0.001, 0.001),
                     (0, 0.001)
                    ]:
            
            sS['x_target'] = x0 + x
            sS['y_target'] = y0 + y
            time.sleep(0.5)
            
            print('REL POS', x, y)
            
            if True:
                fmS['mirror_position'] = True  # APD
                psmS['collect_apd_counter'] = True
                psmS['collect_andor_ccd'] = False
                afmS['optimization_quantity'] = "hardware/apd_counter/count_rate"
                afmS['N_samples'] = 1
                time.sleep(0.5)
                self.start_nested_measure_and_wait(psm, nested_interrupt=False)
                
            if False:
                fmS['mirror_position'] = False            
                psmS['collect_apd_counter'] = False
                psmS['collect_andor_ccd'] = True
                afmS['optimization_quantity'] = "measure/andor_ccd_readout/count_rate"
                ccdS['continuous'] = False
                ccdS['activation'] = False
                afmS['N_samples'] = 10
                time.sleep(0.5)
                self.start_nested_measure_and_wait(psm, nested_interrupt=False)
                ccdS['continuous'] = True
                ccdS['activation'] = True
                
    def sweep_temperature_up_(self, start=77, stop=330, step=20, x0=0, y0=0):
        ''' 
        Quick and dirty measurement that kind of works well with
        Janis flow cryo. 
        Heats until T at heater is OVERSHOOT deg above set temperature
        collect_temperature is invoke when T at sample and T at heater are close
        Good results are achieved when flow to cryo is closed and
        '''
        
        Ts = list(range(start, stop, step))
        Ts = [77, 100, 200, 250, 300, 350, 400]
        Ts = [209, 258, 309, 359, 409]
        
        self.app.hardware.andor_ccd.settings['exposure_time'] = 10
        
        OVERSHOOT = step / 2  # heuristic value
        OVERSHOOT = 15
        
        tctrl = self.app.hardware['lakeshore331']
        TS = tctrl.settings

        print(self.name, 'sweep_temperature_up starting, set temperatures', Ts)
        for ii, T in enumerate(Ts):
            print(self.name, 'temp_up', ii, T)

            self.set_temperature_and_wait(T, 10 * 60)
            self.collect_power_scan(self)
            '''

            if T < 293:

                self.set_temperature_and_wait(T, timeout=10*60)
                print(self.name,'temp_up', ii, T)
                if self.interrupt_measurement_called:
                    break
                
                self.set_progress((ii + 1.0) * 100 / len(Ts))
                
                TS['setpoint_T'] = T + 100
                print(self.name,'temp_up', ii, T)
    
                if ((T - TS['T_A']) > 3):
                    
                    TS['heater_range'] = 'high (50W)'    
                    while TS['T_A'] < T + OVERSHOOT:
                        print('heating to', T, 'deltaT', TS['T_A'] - T - 15)
                        time.sleep(1)
                        if self.interrupt_measurement_called:
                            break
                        
                    TS['heater_range'] = 'off'
                    
                # while abs(TS['T_A'] - TS['T_B']) > 6 and TS['T_B']-3 == T:
                #    print(T, 'settling', TS['T_A'] - TS['T_B'])
                #    time.sleep(1)
                #    if self.interrupt_measurement_called:
                #        break
                
                
                t0 = time.time()
                sleep_t = 90
                while time.time() - t0 < sleep_t and (T - TS['T_A']) > 1:
                    time.sleep(1)
                    print('waiting T to settle dt, error_T', sleep_t - (time.time()-t0), T - TS['T_A'] < 1)
                    if self.interrupt_measurement_called:
                        break
    
                TS['heater_range'] = 'off'
                
                if self.interrupt_measurement_called:
                    break    
            else:
                self.set_temperature_and_wait(T, 10*60)
            '''
            
            # actual measurement here!
            # self.app.measurements.lakeshore_measure.set_history_start()
            # self.collect_multiple_power_scan(x0, y0)
            # self.app.measurements.lakeshore_measure.save_history()
            # TS['heater_range'] = 'off'
