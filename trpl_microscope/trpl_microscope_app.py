from __future__ import division, print_function
from ScopeFoundry import BaseMicroscopeApp
from ScopeFoundry.helper_funcs import sibling_path, load_qt_ui_file
import logging
from PyQt5.QtWidgets import QPushButton, QHBoxLayout

logging.basicConfig(level='WARNING')  # , filename='m3_log.txt')
# logging.getLogger('').setLevel(logging.WARNING)
logging.getLogger("ipykernel").setLevel(logging.WARNING)
logging.getLogger('PyQt4').setLevel(logging.WARNING)
logging.getLogger('PyQt5').setLevel(logging.WARNING)
logging.getLogger('LoggedQuantity').setLevel(logging.WARNING)
logging.getLogger('pyvisa').setLevel(logging.WARNING)


class TRPLMicroscopeApp(BaseMicroscopeApp):

    name = 'trpl_microscope'
    
    def setup(self):
        
        self.add_quickbar(load_qt_ui_file(sibling_path(__file__, 'trpl_quick_access.ui')))
        
        print("Adding Hardware Components")
        from ScopeFoundryHW.picoharp import PicoHarpHW
        self.add_hardware(PicoHarpHW(self))
        
        from ScopeFoundryHW.picoquant.hydraharp_hw import HydraHarpHW
        self.add_hardware(HydraHarpHW(self))

        from ScopeFoundryHW.picoquant.hydraharp_optimizer import HydraHarpOptimizerMeasure
        self.add_measurement(HydraHarpOptimizerMeasure(self))

        from ScopeFoundryHW.picoquant.hydraharp_hist_measure import HydraHarpHistogramMeasure
        self.add_measurement(HydraHarpHistogramMeasure(self))
        
        # from ScopeFoundryHW.apd_counter import APDCounterHW, APDOptimizerMeasure
        # self.add_hardware(APDCounterHW(self))
        # self.add_measurement(APDOptimizerMeasure(self))
        from ScopeFoundryHW.ni_daq.hw.ni_freq_counter_callback import NI_FreqCounterCallBackHW
        self.add_hardware(NI_FreqCounterCallBackHW(self, name='apd_counter'))
        from confocal_measure.apd_optimizer_cb import APDOptimizerCBMeasurement
        self.add_measurement_component(APDOptimizerCBMeasurement(self))

        from ScopeFoundryHW.andor_camera import AndorCCDHW, AndorCCDReadoutMeasure
        self.add_hardware(AndorCCDHW(self))
        self.add_measurement(AndorCCDReadoutMeasure)
        
        from ScopeFoundryHW.acton_spec import ActonSpectrometerHW
        self.add_hardware(ActonSpectrometerHW(self))

        # from ScopeFoundryHW.andor_spec.andor_spec_hw import AndorShamrockSpecHW
        # self.add_hardware(AndorShamrockSpecHW(self))
        
        from ScopeFoundryHW.flip_mirror_arduino import FlipMirrorHW
        rainbow = '''qlineargradient(spread:pad, x1:0, y1:0, x2:1, y2:0, stop:0 rgba(255, 0, 0, 100), 
                        stop:0.166 rgba(255, 255, 0, 100), stop:0.333 rgba(0, 255, 0, 100), stop:0.5 rgba(0, 255, 255, 100), 
                        stop:0.666 rgba(0, 0, 255, 100), stop:0.833 rgba(255, 0, 255, 100), stop:1 rgba(255, 0, 0, 100))'''
        self.add_hardware(FlipMirrorHW(self, colors=[rainbow, 'none']))
        
        from ScopeFoundryHW.thorlabs_powermeter import ThorlabsPowerMeterHW, PowerMeterOptimizerMeasure
        self.add_hardware(ThorlabsPowerMeterHW(self))
        self.add_measurement(PowerMeterOptimizerMeasure(self))
                
        # self.thorlabs_powermeter_analog_readout_hc = self.add_hardware_component(ThorlabsPowerMeterAnalogReadOut(self))

        from ScopeFoundryHW.mcl_stage.mcl_xyz_stage import MclXYZStageHW
        self.add_hardware(MclXYZStageHW(self))
        
        from ScopeFoundryHW.keithley_sourcemeter.keithley_sourcemeter_hc import KeithleySourceMeterComponent
        self.add_hardware(KeithleySourceMeterComponent(self))
        
        # self.srs_lockin_hc = self.add_hardware_component(SRSLockinComponent(self))
        
        # self.thorlabs_optical_chopper_hc = self.add_hardware_component(ThorlabsOpticalChopperComponent(self))        
        
        # from ScopeFoundryHW.powerwheel_arduino import PowerWheelArduinoHW
        # self.power_wheel = self.add_hardware_component(PowerWheelArduinoHW(self))
        # from ScopeFoundryHW.pololu_servo.single_servo_hw import PololuMaestroServoHW
        # self.add_hardware(PololuMaestroServoHW(self, name='power_wheel'))
        
        # from ScopeFoundryHW.oceanoptics_spec.oceanoptics_spec import OceanOpticsSpectrometerHW
        # self.add_hardware_component(OceanOpticsSpectrometerHW(self))
        
        # self.crystaltech_aotf_hc = self.add_hardware_component(CrystalTechAOTF(self))
        
        from ScopeFoundryHW.shutter_servo_arduino.shutter_servo_arduino_hc import ShutterServoHW
        self.add_hardware(ShutterServoHW(self, colors=['none', 'rgba(0,255,0,120)']))
        
        from ScopeFoundryHW.dli_powerswitch import DLIPowerSwitchHW
        self.add_hardware(DLIPowerSwitchHW(self))

        # from ScopeFoundryHW.quantum_composer import QuantumComposerHW
        # self.add_hardware(QuantumComposerHW(self))

        from ScopeFoundryHW.toupcam import ToupCamHW, ToupCamLiveMeasure
        self.add_hardware_component(ToupCamHW(self))
        # self.add_measurement(ToupCamLiveMeasure(self))
        
        from confocal_measure.toupcam_spot_optimizer import ToupCamSpotOptimizer
        self.add_measurement(ToupCamSpotOptimizer(self))     
                
        # from ScopeFoundryHW.powermate.powermate_hw import PowermateHW
        # self.add_hardware(PowermateHW(self))
        
        from ScopeFoundryHW.asi_stage import ASIStageHW, ASIStageControlMeasure
        self.add_hardware(ASIStageHW(self))  # sample stage
        # self.add_hardware(ASIStageHW(self, name='fiber_stage'))
        self.add_measurement(ASIStageControlMeasure(self))
        # self.add_measurement(ASIStageControlMeasure(self,name='Fiber_Stage_Control', hw_name='fiber_stage'))
        
        # from xbox_trpl_measure import XboxControllerTRPLMeasure
        # from ScopeFoundryHW.xbox_controller.xbox_controller_hw import XboxControllerHW
        # self.add_hardware(XboxControllerHW(self))
        # self.add_measurement(XboxControllerTRPLMeasure(self))

        from ScopeFoundryHW.crystaltech_aotf.crystaltech_aotf_hc import CrystalTechAOTF 
        self.add_hardware(CrystalTechAOTF(self))
        
        from ScopeFoundryHW.linkam_thermal_stage.linkam_temperature_controller import LinkamControllerHC
        self.add_hardware(LinkamControllerHC(self))
        
        from ScopeFoundryHW.thorlabs_elliptec.elliptec_hw import ThorlabsElliptecSingleHW
        self.add_hardware(ThorlabsElliptecSingleHW(self, name='polarizer'))
        # from ScopeFoundryHW.thorlabs_elliptec.elliptec_hw import ThorlabsElliptcMultiHW, ThorlabsElliptecSingleHW
        # self.add_hardware(ThorlabsElliptcMultiHW(self, motors=[(0, 'HWP'), (1,'QWP')]))
        # self.add_hardware(ThorlabsElliptecSingleHW(self, name='elliptec_analyzer'))
        
        from ScopeFoundryHW.thorlabs_pax1000.pax1000_hw import ThorlabsPAX1000_PolarimeterHW
        pax = self.add_hardware(ThorlabsPAX1000_PolarimeterHW(self))
        pax.settings['port'] = "USB::0x1313::0x8031::M00559829::INSTR"

        from trpl_microscope.polarization_calibration_measure import PolarizationCalibrationMeasure
        self.add_measurement(PolarizationCalibrationMeasure(self))
    
        from confocal_measure.calibration_sweep import CalibrationSweep
        self.add_measurement(CalibrationSweep(self,
                                              # spectrometer_hw_name='andor_spec', 
                                              spectrometer_hw_name='acton_spectrometer',
                                              camera_readout_measure_name='andor_ccd_readout'))
        
        from ScopeFoundryHW.dynamixel_servo import DynamixelXServosHW, DynamixelFilterWheelHW, DynamixelServoHW
        self.add_hardware(DynamixelXServosHW(self, devices=dict(output_select=31,
                                                                focus_knob=32,
                                                                reflector_wheel=33,
                                                                power_wheel=34,
                                                                )))                                                                
        reflector_wheel = self.add_hardware(DynamixelFilterWheelHW(self, name='reflector_wheel',
                                        named_positions={'empty':  (90, "lightgreen"),
                                                         'mirror': (180, 'lightgrey'),
                                                         'glass':  (270, "yellow"),
                                                         },
                                        positions_in_degrees=True))
        self.add_hardware(DynamixelServoHW(self, name='focus_knob',
                                            lq_kwargs={'spinbox_decimals':3, 'unit':'um'}))       
        self.add_hardware(DynamixelServoHW(self, name='power_wheel'))
        self.add_hardware(DynamixelFilterWheelHW(self, name='output_select',
                                                 named_positions={  # 'R':(33,'red'),
                                                                  # 'AUX':(1480,'red')
                                                                  'L':(3012, 'lime'),
                                                                  'EYE':(4548, 'oldlace')},
                                                 release_at_target=True))                                                        
        from ScopeFoundryHW.lakeshore_331 import Lakeshore331HW, LakeshoreMeasure
        self.add_hardware(Lakeshore331HW(self))
        
        from ScopeFoundryHW.rigol.rigol_DG1000Z_hw import RigolDG1000ZHW
        self.add_hardware(RigolDG1000ZHW(self))
        
        ########################## MEASUREMENTS
        print("Adding Measurement Components")
        
        # hardware specific measurements
        
        from ScopeFoundryHW.picoharp.picoharp_hist_measure import PicoHarpHistogramMeasure
        self.add_measurement(PicoHarpHistogramMeasure(self))

        # from ScopeFoundryHW.oceanoptics_spec.oo_spec_measure import  OOSpecLive
        # self.add_measurement(OOSpecLive(self))
        
        '''
        powermate_lq_choices = [
                    'hardware/asi_stage/x_target',
                    'hardware/asi_stage/y_target',
                    '',
                   ]
        from ScopeFoundryHW.powermate.powermate_measure import PowermateMeasure
        self.add_measurement(PowermateMeasure(self, n_devs=2, dev_lq_choices=powermate_lq_choices))
        '''

        # Combined Measurements
        from confocal_measure.power_scan import PowerScanMeasure
        self.add_measurement_component(PowerScanMeasure(self, 'hardware/shutter_servo/shutter_open'))        

        # Current Measurements
#        from ScopeFoundryHW.keithley_sourcemeter.photocurrent_iv import PhotocurrentIVMeasurement
#        self.add_measurement(PhotocurrentIVMeasurement(self))

        # Mapping Measurements        
        from confocal_measure.apd_mcl_2dslowscan import APD_MCL_2DSlowScan, APD_MCL_3DSlowScan, APD_MCL_2DCDScan
        apd_scan = self.add_measurement(APD_MCL_2DSlowScan(self))
        self.add_measurement(APD_MCL_3DSlowScan(self))
        self.add_measurement(APD_MCL_2DCDScan(self))
        
        # from confocal_measure import Picoharp_MCL_2DSlowScan
        # picoharp_scan = self.add_measurement(Picoharp_MCL_2DSlowScan(self))
        
        from confocal_measure.andor_hyperspec_scan import AndorHyperSpec2DScan
        andor_scan = self.add_measurement(AndorHyperSpec2DScan(self))

        from confocal_measure.mcl_trpl_2d_scan import MCL_TRPL2DScan
        mcl_trpl_2d_scan = self.add_measurement(MCL_TRPL2DScan(self))        
        
        # Polarization Sweep
        from trpl_microscope.polarization_sweep import PolariaztionSweepMeasure
        self.add_measurement(PolariaztionSweepMeasure(self))

        # connect mapping measurement settings        
        lq_names = ['h0', 'h1', 'v0', 'v1', 'Nh', 'Nv', 'h_axis', 'v_axis']
        
        for scan in [  # picoharp_scan, 
                     andor_scan, mcl_trpl_2d_scan]:
            for lq_name in lq_names:
                master_scan_lq = apd_scan.settings.get_lq(lq_name)
                scan.settings.get_lq(lq_name).connect_to_lq(master_scan_lq)     
        
        from trpl_microscope.step_and_glue_spec_measure import SpecStepAndGlue
        self.add_measurement(SpecStepAndGlue(self))
        
        from confocal_measure.apd_asi_2dslowscan import APD_ASI_2DSlowScan
        apd_asi = self.add_measurement(APD_ASI_2DSlowScan(self))
        
        from confocal_measure.asi_hyperspec_scan import AndorHyperSpecASIScan
        hyperspec_asi = self.add_measurement(AndorHyperSpecASIScan(self))
        
        from confocal_measure.asi_trpl_2d_scan import ASI_TRPL2DScan
        asi_trpl_2d_scan = self.add_measurement(ASI_TRPL2DScan(self))
        
        from confocal_measure.tiled_large_area_map import ASIMCLToupcamTiledLargeAreaMapMeasure
        self.add_measurement(ASIMCLToupcamTiledLargeAreaMapMeasure(self))
                
        from confocal_measure.ranged_optimization import RangedOptimization
        self.add_measurement(RangedOptimization(self, name='auto_focus',
                                                lq_kwargs={'spinbox_decimals':2,
                                                           'unit':'um'}))  

        from trpl_microscope.nested_measurement import NestedMeasure
        self.add_measurement(NestedMeasure(self))
        
        self.add_measurement(LakeshoreMeasure(self))
        
        from trpl_microscope.measurements.asi_quartz_plate_scan import ASIQuartzPlateScan
        self.add_measurement(ASIQuartzPlateScan(self))
        
        # connect mapping measurement settings        
        lq_names = ['h0', 'h1', 'v0', 'v1', 'Nh', 'Nv']
        
        for scan in [apd_asi, hyperspec_asi, asi_trpl_2d_scan]:
            for lq_name in lq_names:
                master_scan_lq = apd_asi.settings.get_lq(lq_name)
                scan.settings.get_lq(lq_name).connect_to_lq(master_scan_lq)         
        
        from ScopeFoundryHW.laser_quantum.pmd24HW import PMD24HW
        self.add_hardware(PMD24HW(self))        
        
        from ScopeFoundryHW.laser_quantum.laser_quantum_optimizer import LaserQuantumOptimizer
        self.add_measurement(LaserQuantumOptimizer(self))
        
        from confocal_measure.sequencer import SweepSequencer
        self.add_measurement(SweepSequencer(self))
        
        ####### Quickbar connections #################################
        
        Q = self.quickbar
        
        # 2D Scan Area
        apd_scan.settings.h0.connect_to_widget(Q.h0_doubleSpinBox)
        apd_scan.settings.h1.connect_to_widget(Q.h1_doubleSpinBox)
        apd_scan.settings.v0.connect_to_widget(Q.v0_doubleSpinBox)
        apd_scan.settings.v1.connect_to_widget(Q.v1_doubleSpinBox)
        apd_scan.settings.dh.connect_to_widget(Q.dh_doubleSpinBox)
        apd_scan.settings.dv.connect_to_widget(Q.dv_doubleSpinBox)
        apd_scan.settings.h_axis.connect_to_widget(Q.h_axis_comboBox)
        apd_scan.settings.v_axis.connect_to_widget(Q.v_axis_comboBox)
        
        # MadCity Labs
        mcl = self.hardware['mcl_xyz_stage']

        mcl.settings.x_position.connect_to_widget(Q.cx_doubleSpinBox)
        Q.x_set_lineEdit.returnPressed.connect(mcl.settings.x_target.update_value)
        Q.x_set_lineEdit.returnPressed.connect(lambda: Q.x_set_lineEdit.setText(""))

        mcl.settings.y_position.connect_to_widget(Q.cy_doubleSpinBox)
        Q.y_set_lineEdit.returnPressed.connect(mcl.settings.y_target.update_value)
        Q.y_set_lineEdit.returnPressed.connect(lambda: Q.y_set_lineEdit.setText(""))

        mcl.settings.z_position.connect_to_widget(Q.cz_doubleSpinBox)
        Q.z_set_lineEdit.returnPressed.connect(mcl.settings.z_target.update_value)
        Q.z_set_lineEdit.returnPressed.connect(lambda: Q.z_set_lineEdit.setText(""))

        mcl.settings.move_speed.connect_to_widget(Q.nanodrive_move_slow_doubleSpinBox)        
        
        # Power Wheel #(POLULO)
        # pw = self.hardware['power_wheel_arduino']
        # pw = self.hardware['power_wheel'] # POLULO
        # pw.settings.connected.connect_to_widget(Q.power_wheel_connected_checkBox)
        # pw.settings.position.connect_to_widget(Q.power_wheel_encoder_pos_doubleSpinBox)
        # pw.settings.jog_step.connect_to_widget(Q.powerwheel_move_steps_doubleSpinBox)
        # Q.powerwheel_move_fwd_pushButton.clicked.connect(pw.jog_fwd)
        # Q.powerwheel_move_bkwd_pushButton.clicked.connect(pw.jog_bkwd)
        # Q.power_wheel_encoder_pos_min_pushButton.clicked.connect(lambda: pw.settings.position.update_value(0))
        # Q.power_wheel_encoder_pos_half_pushButton.clicked.connect(lambda: pw.settings.position.update_value(140))
        # Q.power_wheel_encoder_pos_max_pushButton.clicked.connect(lambda: pw.settings.position.update_value(280))

        pw = self.hardware['power_wheel']
        pw.settings.connected.connect_to_widget(Q.power_wheel_connected_checkBox)
        pw.settings.position.connect_to_widget(Q.power_wheel_position_label)
        pw.settings.target_position.connect_to_widget(Q.power_wheel_target_position_doubleSpinBox)
        pw.settings.jog.connect_to_widget(Q.power_wheel_move_steps_doubleSpinBox)
        Q.power_wheel_move_fwd_pushButton.clicked.connect(pw.jog_fwd)
        Q.power_wheel_move_bkwd_pushButton.clicked.connect(pw.jog_bkwd)
        Q.power_wheel_position_min_pushButton.clicked.connect(lambda: pw.settings.target_position.update_value(0))
        Q.power_wheel_position_140_pushButton.clicked.connect(lambda: pw.settings.target_position.update_value(140))
        Q.power_wheel_position_280_pushButton.clicked.connect(lambda: pw.settings.target_position.update_value(280))
        Q.power_wheel_position_max_pushButton.clicked.connect(lambda: pw.settings.target_position.update_value(320))

        # Power Wheel
        fk = self.hardware['focus_knob']
        fk.settings.connected.connect_to_widget(Q.focus_knob_connected_checkBox)
        # fk.settings.position.connect_to_widget(Q.focus_knob_position_doubleSpinBox)
        fk.settings.position.connect_to_widget(Q.focus_knob_position_label)
        fk.settings.target_position.connect_to_widget(Q.focus_knob_target_position_doubleSpinBox)
        fk.settings.jog.connect_to_widget(Q.focus_knob_jog_doubleSpinBox)
        Q.focus_knob_move_fwd_pushButton.clicked.connect(fk.jog_fwd)
        Q.focus_knob_move_bkwd_pushButton.clicked.connect(fk.jog_bkwd)
        Q.focus_knob_zero_position_pushButton.clicked.connect(fk.zero_position)
        Q.focus_knob_min_pushButton.clicked.connect(fk.min_jog)

        # connect events
        apd = self.hardware['apd_counter']
        apd.settings.int_time.connect_to_widget(Q.apd_counter_int_doubleSpinBox)
        # apd.settings.count_rate.updated_text_value.connect(
        #                                   Q.apd_counter_output_lineEdit.setText)
        apd.settings.count_rate.connect_to_widget(Q.apd_counter_output_lineEdit)

        apd_opt = self.measurements['apd_optimizer']
        # apd.settings
        apd_opt.settings.activation.connect_to_widget(Q.apd_optimize_startstop_checkBox)
        Q.apd_optimize_show_ui_pushButton.clicked.connect(apd_opt.show_ui) 

        # self.measurement_state_changed[bool].connect(self.gui.ui.apd_optimize_startstop_checkBox.setChecked)
        
        # Acton Spectrometer
        acton_spec = self.hardware['acton_spectrometer']
        acton_spec.settings.connected.connect_to_widget(Q.andor_spec_connected_checkBox)
        acton_spec.settings.center_wl.connect_to_widget(Q.andor_spec_center_wl_doubleSpinBox)
        acton_spec.settings.grating_id.connect_to_widget(Q.andor_spec_grating_id_comboBox)     
        
        # Andor Spectrometer
        # aspec = self.hardware['andor_spec']
        # aspec.settings.connected.connect_to_widget(Q.andor_spec_connected_checkBox)
        # aspec.settings.center_wl.connect_to_widget(Q.andor_spec_center_wl_doubleSpinBox)
        # aspec.settings.grating_id.connect_to_widget(Q.andor_spec_grating_id_comboBox)       
        
        # Andor CCD
        andor = self.hardware['andor_ccd']
        andor.settings.connected.connect_to_widget(Q.andor_ccd_connected_checkBox)
        andor.settings.exposure_time.connect_to_widget(Q.andor_ccd_int_time_doubleSpinBox)
        andor.settings.em_gain.connect_to_widget(Q.andor_ccd_emgain_doubleSpinBox)
        andor.settings.temperature.connect_to_widget(Q.andor_ccd_temp_doubleSpinBox)
        andor.settings.ccd_status.connect_to_widget(Q.andor_ccd_status_label)
        andor.settings.shutter_open.connect_to_widget(Q.andor_ccd_shutter_open_checkBox)        
        andor.settings.output_amp.connect_to_widget(Q.andor_ccd_output_amp_comboBox)
        
        # Andor Readout
        aro = self.measurements['andor_ccd_readout']
        aro.settings.bg_subtract.connect_to_widget(Q.andor_ccd_bgsub_checkBox)
        Q.andor_ccd_acq_bg_pushButton.clicked.connect(aro.acquire_bg_start)
        aro.settings.explore_mode.connect_to_pushButton(Q.andor_run_explore_mode_pushButton,
                                                        colors=['yellow', 'orange'],
                                                        texts=['run explore', 'interrupt explore'])
        aro.settings.count_rate.connect_to_widget(Q.andor_count_rate_doubleSpinBox)   
        aro.settings.explore_mode_exposure_time.connect_to_widget(Q.andor_explore_mode_exposure_time_doubleSpinBox)  
        Q.andor_ccd_readout_show_ui_pushButton.clicked.connect(aro.show_ui) 
        
        # power meter
        pm = self.hardware['thorlabs_powermeter']
        pm.settings.connected.connect_to_widget(Q.power_meter_connected_checkBox)
        pm.settings.wavelength.connect_to_widget(Q.power_meter_wl_doubleSpinBox)
        pm.settings.power.connect_to_widget(Q.power_meter_power_label)
        pm.settings.average_count.connect_to_widget(Q.power_meter_average_count_doubleSpinBox)
        pm.settings.power_range.connect_to_widget(Q.power_meter_power_range_doubleSpinBox)
        pm.settings.auto_range.connect_to_widget(Q.power_meter_auto_range_checkBox)
        
        pm_opt = self.measurements['powermeter_optimizer']
        pm_opt.settings.activation.connect_to_widget(Q.power_meter_acquire_cont_checkBox)
        Q.power_meter_optimize_show_ui_pushButton.clicked.connect(pm_opt.show_ui) 
        
        shutter = self.hardware['shutter_servo']
        shutter.settings.shutter_open.connect_to_widget(Q.shutter_open_checkBox)
        shutter.settings.connected.connect_to_widget(Q.shutter_connected_checkBox)

        # Other
        reflector_wheel.settings.named_position.connect_to_widget(Q.reflector_named_position_comboBox)
        reflector_wheel.settings.connected.connect_to_widget(Q.reflector_connected_checkBox)
        
        self.hardware['flip_mirror'].settings.mirror_position.connect_to_widget(Q.flip_mirror_checkBox)
        self.hardware['flip_mirror'].settings.connected.connect_to_widget(Q.flip_mirror_connected_checkBox)
        
        self.hardware['output_select'].settings.named_position.connect_to_widget(Q.output_select_comboBox)
        self.hardware['output_select'].settings.connected.connect_to_widget(Q.output_select_connected_checkBox)

        self.hardware['polarizer'].settings.connected.connect_to_widget(Q.polarizer_connected_checkBox)
        self.hardware['polarizer'].settings.position.connect_to_widget(Q.polarizer_position_doubleSpinBox)
        
        # AOTF
        aotf_hw = self.hardware['CrystalTechAOTF_DDS']
        aotf_hw.settings.freq0.connect_to_widget(Q.atof_freq_doubleSpinBox)
        aotf_hw.settings.pwr0.connect_to_widget(Q.aotf_power_doubleSpinBox)
        aotf_hw.settings.modulation_enable.connect_to_widget(Q.aotf_mod_enable_checkBox)     
        
        # ASI Stage
        asi = self.hardware['asi_stage']

        asi.settings.connected.connect_to_widget(Q.asi_connected_checkBox)

        asi.settings.x_position.connect_to_widget(Q.asi_x_position_doubleSpinBox)
        asi.settings.x_target.connect_to_widget(Q.asi_x_target_position_lineEdit)
        Q.asi_x_target_position_lineEdit.returnPressed.connect(asi.settings.x_target.update_value)
        Q.asi_x_target_position_lineEdit.returnPressed.connect(lambda: Q.asi_x_target_position_lineEdit.setText(""))

        asi.settings.y_position.connect_to_widget(Q.asi_y_position_doubleSpinBox)
        asi.settings.y_target.connect_to_widget(Q.asi_y_target_position_lineEdit)
        Q.asi_y_target_position_lineEdit.returnPressed.connect(asi.settings.y_target.update_value)
        Q.asi_y_target_position_lineEdit.returnPressed.connect(lambda: Q.asi_y_target_position_lineEdit.setText(""))

        asi.settings.speed_xy.connect_to_widget(Q.asi_speed_xy_doubleSpinBox)         
        
        Q.asi_home_xy_pushButton.clicked.connect(asi.home_xy)
        Q.asi_halt_xy_pushButton.clicked.connect(asi.halt_xy)
        
        asi_control = self.measurements["ASI_Stage_Control"]
        Q.asi_x_up_pushButton.clicked.connect(asi_control.x_up)
        Q.asi_x_down_pushButton.clicked.connect(asi_control.x_down)
        Q.asi_y_up_pushButton.clicked.connect(asi_control.y_up)
        Q.asi_y_down_pushButton.clicked.connect(asi_control.y_down)
        asi_control.settings.jog_step_xy.connect_to_widget(Q.asi_jog_step_xy_doubleSpinBox)   
        
        # Waveform generator
        wg = self.hardware.rigol_waveform_generator        
        wg.settings.connected.connect_to_widget(Q.rigol_waveform_generator_connected_checkBox)
        wg.settings.waveform_1.connect_to_widget(Q.rigol_waveform_generator_waveform_1_comboBox)
        wg.settings.V_DC_1.connect_to_widget(Q.rigol_waveform_generator_V_DC_1_doubleSpinBox)
        wg.settings.output_1.connect_to_widget(Q.rigol_waveform_generator_output_1_checkBox)
              
        # DLI Power switch
        dliS = self.hardware['dli_powerswitch'].settings
        dliS.connected.connect_to_widget(Q.dli_connected_checkBox)
        for i in range(1, 9):
            dliS.get_lq(f'Outlet_{i}_Name').connect_to_widget(getattr(Q, f'outlet_{i}_label'))
            dliS.get_lq(f'Outlet_{i}').connect_to_widget(getattr(Q, f'outlet_{i}_checkBox'))
        
        # # quick views
        # temperature 
        L = QHBoxLayout()
        Q.layout().addLayout(L)
        btn = QPushButton('temperature dep. window positions')
        btn.clicked.connect(lambda:self.load_window_positions_json('temperature_dep_window_positions.json'))
        L.addWidget(btn)
        btn = QPushButton('temperature dep. settings')

        def load_temp_dep_set():
            self.settings_load_ini('temperature_dep.ini')
            self.measurements['sweep_sequencer'].load_file('temperature_dep_sequence.json')

        btn.clicked.connect(load_temp_dep_set)
        L.addWidget(btn)
        
        ################# Shared Settings for Map Measurements ########################
        
        ############
        # Color scheme
         
        # from qtpy.QtGui import QPalette, QColor
        # from qtpy.QtCore import Qt
        # Now use a palette to switch to dark colors:
        # palette = QPalette()
        # palette.setColor(QPalette.Window, QColor(53, 53, 53))
        # palette.setColor(QPalette.WindowText, Qt.white)
        # palette.setColor(QPalette.Base, QColor(25, 25, 25))
        # palette.setColor(QPalette.AlternateBase, QColor(53, 53, 53))
        # palette.setColor(QPalette.ToolTipBase, Qt.white)
        # palette.setColor(QPalette.ToolTipText, Qt.white)
        # palette.setColor(QPalette.Text, Qt.white)
        # palette.setColor(QPalette.Button, QColor(53, 53, 53))
        # palette.setColor(QPalette.ButtonText, Qt.white)
        # palette.setColor(QPalette.BrightText, Qt.red)
        # palette.setColor(QPalette.Link, QColor(42, 130, 218))
        # palette.setColor(QPalette.Highlight, QColor(42, 130, 218))
        # palette.setColor(QPalette.HighlightedText, Qt.black)
        # self.qtapp.setPalette(palette)
        # self.qtapp.setStyle('WindowsXP')
        
        ##########
        self.settings_load_ini('trpl_defaults.ini')


if __name__ == '__main__':
    import sys
    app = TRPLMicroscopeApp(sys.argv)
    sys.exit(app.exec_())
