'''
Created on May 12, 2021

@author: lab
'''

from ScopeFoundryHW.asi_stage.asi_stage_raster import ASIStage2DScan
import time


class ASIQuartzPlateScan(ASIStage2DScan):
    
    name = 'asi_quartz_plate_scan'
    
    def setup(self):
        ASIStage2DScan.setup(self)
        S = self.settings
        S.New('collect_power_scan', dtype=bool, initial=True)
        S.New('collect_andor_ccd_readout', dtype=bool, initial=True)        
        
    
    
    def pre_scan_setup(self):
        
        self.app.hardware['asi_stage'].settings['speed_xy'] = 5
        
        
        S = self.settings
        S['h0'],S['h1'] = -36,36
        S['v0'],S['v1'] = -18,36
        S['Nh'],S['Nv'] = 9,7
        
        self.rows = {6: 'H', 5: 'G', 4: 'F', 3: 'E', 2: 'D', 1: 'C', 0: 'B'}
        self.cols = {8: '3', 7: '4', 6: '5', 5: '6', 4: '7', 3: '8', 2: '9', 1: '10', 0: '11'}
        
                        
                
    def collect_pixel(self, pixel_num, k, j, i):  

        s = ''.join([self.rows[j], self.cols[i]])               
        self.app.settings['sample'] = s
        print(' ')
        print('*'*pixel_num,  s, '*'*(62-pixel_num))
               
        
        fmS = self.app.hardware['flip_mirror'].settings
        pwS = self.app.hardware['power_wheel'].settings
        ssS = self.app.hardware['shutter_servo'].settings

        
        if self.settings['collect_power_scan']:
            print('* POWER SCAN @', s)
            ps = self.app.measurements['power_scan']
            fmS['mirror_position'] = True # APD
            pwS['target_position'] = 180
            time.sleep(0.5)
            self.start_nested_measure_and_wait(ps, nested_interrupt=False)                      
            self.display_image_map[k,j,i] = ps.hydraharp_histograms.sum()

        if self.settings['collect_andor_ccd_readout']:        
            print(' ')                
            print('* ANDOR CCD READOUT @', s)
    
            sp = self.app.measurements['andor_ccd_readout'] 
            sp.settings['continuous'] = False
            sp.settings['save_h5'] = True
            ssS['shutter_open'] = True 
            fmS['mirror_position'] = False # Spec
            pwS['target_position'] = 280
            time.sleep(0.5)
            if hasattr(sp, 'spectrum'):
                self.display_image_map[k,j,i] = sp.get_spectrum().sum()

            self.start_nested_measure_and_wait(sp, nested_interrupt=False)               