from ScopeFoundry.measurement import Measurement
import time
import numpy as np
import pandas as pd
import csv
import pyqtgraph as pg
import datetime
import os

class PolarizationCalibrationMeasure(Measurement):
    
    name = "pol_cal"
    
    def setup(self):
        pass
    
    def setup_figure(self):
        self.first_display = True
        self.ui = self.graph_layout = pg.GraphicsLayoutWidget()
        self.graph_layout.clear()

    
    def run(self):
        
        self.first_display = True
        # half first (address 0)
        # quarter next (address 1)
        try:
            hwp_pos_lq = self.app.hardware['elliptec_motors'].settings.zero_position
            qwp_pos_lq = self.app.hardware['elliptec_motors'].settings.one_position
            
            hwp_pos_lq.update_value(81.5)
            qwp_pos_lq.update_value(70.5)
            
            polarimeter = self.app.hardware['polarimeter']
            
            print("hello", polarimeter.settings['azimuth'])
            
            #mean_stokes = polarimeter.history['Stokes'][0:100,:].mean(axis=0)
            
            #print("stokes:", mean_stokes, mean_stokes/mean_stokes[0])
            
            time.sleep(1.0)
            mean_tp = polarimeter.history['total_power'][0:100].mean()
            mean_az = polarimeter.history['azimuth'][0:100].mean()
            mean_el = polarimeter.history['ellipticity'][0:100].mean()
            mean_DOP = polarimeter.history['DOP'][0:100].mean()
    
    
            print(repr(mean_tp), mean_az, mean_el, mean_DOP)
    
            print("="*80)
            
            dummy = [['qwp_angle', 'hwp_angle', 'mean_tp', 'mean_az', 'mean_el', 'mean_DOP']]
    
            data = self.data = dict()
            for x in ['qwp_angle', 'hwp_angle', 'total_power', 'azimuth', 'ellipticity', 'DOP']:
                data[x] = list() 
                
            qwp_angles = np.arange(69.5,251.5,1)
            hwp_angles = np.arange(80.5,172.5,1)
    
            for ii, qwp_angle in enumerate(qwp_angles):
                qwp_pos_lq.update_value(qwp_angle)
                time.sleep(1.0)
                if self.interrupt_measurement_called:
                        break
    
                for hwp_angle in hwp_angles:
                    
                    if self.interrupt_measurement_called:
                        break
                    
                    hwp_pos_lq.update_value(hwp_angle)
                    #time.sleep(0.010)
                    time.sleep(0.10)
                    N = 9
                    mean_tp = polarimeter.history['total_power'][0:N].mean()
                    mean_az = polarimeter.history['azimuth'][0:N].mean()
                    mean_el = polarimeter.history['ellipticity'][0:N].mean()
                    mean_DOP = polarimeter.history['DOP'][0:N].mean()
                    
                    
                    self.current_tp_section = polarimeter.history['total_power'][0:N]
                    
                    data['qwp_angle'].append(qwp_angle)
                    data['hwp_angle'].append(hwp_angle)
                    data['total_power'].append(mean_tp)
                    data['azimuth'].append(mean_az)
                    data['ellipticity'].append(mean_el)
                    data['DOP'].append(mean_DOP)

                    
                    print(qwp_angle, hwp_angle, mean_tp, mean_az, mean_el, mean_DOP)
                    dummy.append([qwp_angle, hwp_angle, mean_tp, mean_az, mean_el, mean_DOP])
    
    
                self.set_progress((ii+1)/(len(qwp_angles)+1))

            
            print("="*80)
            
            my_df = pd.DataFrame(dummy)
            f = self.app.settings['data_fname_format'].format(
                app=self.app,
                measurement=self,
                timestamp=datetime.datetime.fromtimestamp(time.time()),
                ext='csv')
            fname = os.path.join(self.app.settings['save_dir'], f)        
            my_df.to_csv(fname, index=False, header=False)
        finally:
            pass
        
    def update_display(self):
        
        t0 = pg.ptime.time()
                
        if self.first_display:
            
            self.graph_layout.clear()

            self.plot_lines = dict()
            
            self.do_plot = self.graph_layout.addPlot(0,0)
            self.total_power_history_plotline = self.do_plot.plot()
            self.current_tp_section_plotline = self.do_plot.plot(pen='r')
            #self.pol_power_history_plotline = self.do_plot.plot()
            #self.do_plot.show()
            self.ao_plot = self.graph_layout.addPlot(1,0)
            self.azi_history_plotline = self.ao_plot.plot(pen='g')
            self.ell_history_plotline = self.ao_plot.plot(pen='r')
            
            self.set_angle_plot = self.graph_layout.addPlot(2,0)
            self.set_angle_plot.addLegend()
            self.qwp_data_plotline = self.set_angle_plot.plot(pen='g', name='QWP')
            self.hwp_data_plotline = self.set_angle_plot.plot(pen='r', name='HWP')
            
            self.ai_plot = self.graph_layout.addPlot(3,0)
            self.total_power_data_plotline = self.ai_plot.plot(name='total_power')
            
            self.angle_data_plot = self.graph_layout.addPlot(4,0)
            self.azi_data_plotline = self.angle_data_plot.plot(name='Azi', pen='g')
            self.ell_data_plotline = self.angle_data_plot.plot(name='Ell', pen='r')

            
            
#             for i in range(self.do_chan_count):
#                 self.plot_lines['do{}'.format(i)] = self.do_plot.plot()#autoDownsample=True, downsampleMethod='subsample', autoDownsampleFactor=1.0)
#             for i in range(self.ao_chan_count):
#                 self.plot_lines['ao{}'.format(i)] = self.ao_plot.plot()#autoDownsample=True, downsampleMethod='subsample', autoDownsampleFactor=1.0)
#             for i in range(self.di_chan_count):
#                 self.plot_lines['di{}'.format(i)] = self.di_plot.plot()
#             for i in range(self.ai_chan_count):
#                 self.plot_lines['ai{}'.format(i)] = self.ai_plot.plot()
                
            self.first_display = False
        
        polarimeter = self.app.hardware['polarimeter']
        #print(polarimeter.history['total_power'].shape)
        self.total_power_history_plotline.setData(polarimeter.history['total_power'])
        
        self.current_tp_section_plotline.setData(self.current_tp_section)
        
        #self.pol_power_history_plotline.setData(polarimeter.history['polarized_power'])
        self.azi_history_plotline.setData(polarimeter.history['azimuth'])
        self.ell_history_plotline.setData(polarimeter.history['ellipticity'])
        
        self.qwp_data_plotline.setData(self.data['qwp_angle'])
        self.hwp_data_plotline.setData(self.data['hwp_angle'])
        self.total_power_data_plotline.setData(self.data['total_power'])
        
        self.azi_data_plotline.setData(self.data['azimuth'])
        self.ell_data_plotline.setData(self.data['ellipticity'])


  