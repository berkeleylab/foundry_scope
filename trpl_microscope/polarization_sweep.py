from ScopeFoundry import Measurement, h5_io
import pyqtgraph as pg
from qtpy import QtWidgets
import numpy as np
import time
from scipy.interpolate.interpolate import interp1d

class PolariaztionSweepMeasure(Measurement):
    
    name = 'polarization_sweep'
    
    def setup(self):
        self.theta_range = self.settings.New_Range('theta', dtype=float, include_center_span=True, unit='deg', initials=[0,180,5])
        self.settings.New('adjust_input_power', dtype=bool, initial=False)
        self.settings.New('input_power_calibration', dtype=float, array=True, initial=[[0, 1.0],[180,1.0], [360, 1.0]])
        
    def setup_figure(self):
        
        self.ui = self.graph_layout = pg.GraphicsLayoutWidget()
        
        
        self.polar_plot = self.graph_layout.addPlot()
        self.spec_plot = self.graph_layout.addPlot()
        
        p = self.polar_plot 
        
        p.setAspectLocked()
        
        # Add polar grid lines
        p.addLine(x=0, pen=0.2)
        p.addLine(y=0, pen=0.2)
        for r in np.arange(0,1.1, 0.1):
            circle = QtWidgets.QGraphicsEllipseItem(-r, -r, r*2, r*2)
            circle.setPen(pg.mkPen(0.2))
            p.addItem(circle)
        circle.setPen(pg.mkPen(0.8))
                    
        # make polar data
        theta = np.linspace(0, 2*np.pi, 100)
        radius = np.random.normal(loc=10, size=100)
        
        # Transform to cartesian and plot
        x = radius * np.cos(theta)
        y = radius * np.sin(theta)
        self.polar_plotline = p.plot(x, y)

        self.polar_plotline_pm_power = p.plot(pen='r')


    def run(self):
        
        def norm(X):
            return X/np.max(X)

        
        # hardware
        polarizer = self.app.hardware['polarizer']
        apd = self.app.hardware['apd_counter']
        pol_pos = polarizer.settings.position
        pw_pos = self.app.hardware['power_wheel'].settings.position
        
        pm_power = self.app.hardware['thorlabs_powermeter'].settings.power
        
        # arrays
        self.theta_array = self.theta_range.array
        
        self.apd_count_rates = np.zeros_like(self.theta_array)
        self.pm_powers = np.zeros_like(self.theta_array)
        
    
        #
        initial_pol_pos = pol_pos.value
        initial_powerwheel_pos = pw_pos.value
        
        """calib_pol_angles = np.array(
           [  0.        ,   0.99441964,   1.99888393,   2.99330357,
                 3.99274554,   4.9921875 ,   5.99665179,   6.99358259,
                 7.99553571,   8.99748884,   9.9969308 ,  10.99386161,
                11.99581473,  12.99023438,  13.9921875 ,  14.99414062,
                15.99107143,  16.99553571,  18.00502232,  19.00195312,
                19.99135045,  20.99832589,  21.99274554,  22.99720982,
                23.99916295,  24.99107143,  25.99553571,  26.9874442 ,
                27.99190848,  28.99386161,  29.99330357,  30.9952567 ,
                31.99469866,  32.99414062,  33.99609375,  34.99051339,
                35.99497768,  36.9969308 ,  37.99386161,  38.99330357,
                39.9952567 ,  40.9921875 ,  41.99162946,  42.99609375,
                44.00055804,  44.99497768,  45.99190848,  46.98883929,
                47.99581473,  48.99023438,  49.99469866,  51.00418527,
                51.99609375,  52.99553571,  54.        ,  54.98688616,
                56.00641741,  56.99832589,  57.99776786,  58.99720982,
                59.99162946,  60.99609375,  62.0030692 ,  63.01506696,
                64.00446429,  65.00390625,  66.00334821,  67.00027902,
                67.99469866,  69.00669643,  70.00864955,  71.01060268,
                72.00502232,  73.00948661,  74.00641741,  74.99581473,
                76.00530134,  77.01729911,  78.00920759,  79.00362723,
                80.00055804,  81.00502232,  82.00697545,  82.99888393,
                84.00334821,  84.9952567 ,  85.99720982,  86.99916295,
                88.00111607,  89.00055804,  90.00753348,  90.99944196,
                92.00139509,  93.00837054,  94.01534598,  95.00976562,
                96.01674107,  97.0186942 ,  98.015625  ,  99.02260045,
                99.99944196, 101.02148438, 102.00334821, 103.0078125 ,
               104.00976562, 105.00418527, 106.01618304, 107.015625  ,
               108.01004464, 109.00697545, 110.00892857, 111.00334821,
               112.0078125 , 113.00223214, 114.00920759, 114.97851562,
               116.00809152, 117.01757812, 118.01702009, 119.00892857,
               119.99581473, 121.00027902, 122.00725446, 123.00418527,
               124.00111607, 125.0030692 , 126.00251116, 126.99944196,
               128.00390625, 129.00837054, 130.00530134, 131.00223214,
               132.00669643, 133.00111607, 134.00055804, 135.00251116,
               135.99441964, 136.99888393, 138.00585938, 139.00027902,
               139.99720982, 141.00669643, 142.00864955, 143.00558036,
               144.        , 144.99441964, 146.00390625, 147.00585938,
               147.9952567 , 148.99972098, 150.00418527, 151.00613839,
               152.00055804, 153.        , 154.00195312, 154.99637277,
               155.99581473, 156.9952567 , 158.00223214, 159.00167411,
               160.00111607, 161.0030692 , 161.99748884, 162.98939732,
               163.99386161, 164.99581473, 165.9952567 , 166.9921875 ,
               167.99916295, 168.99107143, 170.00055804, 171.00251116,
               171.99944196, 173.00390625, 173.99330357, 174.9952567 ,
               175.99469866, 176.99162946, 177.99358259, 178.99804688,
               179.99246652, 180.99190848, 181.99135045, 183.00334821,
               183.99274554, 184.99720982, 185.99414062, 186.99358259,
               187.98800223, 188.99497768, 189.9969308 , 190.99386161,
               191.99581473, 192.9952567 , 193.99972098, 194.99916295,
               195.99860491, 196.99051339, 197.9874442 , 198.99944196,
               200.00139509, 200.99832589, 201.99776786, 202.99720982,
               203.99916295, 204.99860491, 205.99302455, 206.98995536,
               207.99441964, 208.99135045, 209.99832589, 211.00027902,
               211.99469866, 212.99162946, 213.99107143, 214.99804688,
               215.99246652, 216.99944196, 217.99888393, 218.99330357,
               219.99274554, 220.99972098, 221.99916295, 222.99358259,
               223.99051339, 224.98995536, 225.99441964, 226.98883929,
               227.98828125, 228.99274554, 229.98716518, 230.98409598,
               231.98856027, 232.98549107, 233.98493304, 234.98688616,
               235.99135045, 236.98577009, 237.98270089, 238.98465402,
               239.98409598, 240.98353795, 241.98297991, 242.98493304,
               243.99441964, 244.9813058 , 245.98577009, 246.99274554,
               247.99469866, 248.99665179, 249.99358259, 250.99302455,
               251.99497768, 252.99190848, 253.99386161, 254.99330357,
               255.99274554, 256.99720982, 257.99162946, 258.99860491,
               259.98800223, 260.99246652, 261.98939732, 262.99386161,
               263.99581473, 264.99274554, 265.99469866, 266.99414062,
               267.99107143, 268.99553571, 269.99246652, 270.99944196,
               271.99637277, 272.99581473, 274.00279018, 275.00223214,
               275.99665179, 277.00362723, 278.0030692 , 279.01004464,
               280.00446429, 280.99637277, 282.00585938, 282.9952567 ,
               283.99720982, 284.99916295, 286.00362723, 287.0030692 ,
               287.99748884, 288.99190848, 289.99386161, 290.98828125,
               291.99023438, 292.99469866, 293.99665179, 294.98856027,
               295.99051339, 296.99246652, 297.99441964, 298.99637277,
               299.99832589, 300.99274554, 302.00223214, 303.00920759,
               303.99358259, 305.00558036, 306.00251116, 307.00697545,
               307.99386161, 309.00083705, 309.99776786, 310.99720982,
               312.00669643, 313.00111607, 313.99804688, 314.99497768,
               315.99944196, 316.99888393, 318.00083705, 318.99776786,
               320.0047433 , 321.00167411, 321.99609375, 323.00055804,
               324.00502232, 325.00195312, 325.99888393, 327.00083705,
               328.0078125 , 329.0047433 , 330.00167411, 331.00864955,
               332.01311384, 333.00502232, 334.00195312, 335.00139509,
               335.99581473, 336.9952567 , 338.0047433 , 339.00669643,
               339.99860491, 341.01311384, 341.99748884, 342.99944196,
               343.99637277, 344.99581473, 345.9952567 , 346.98967634,
               347.9891183 , 348.99358259, 349.99804688, 350.99748884,
               351.99441964, 352.98883929, 353.99079241, 354.99023438,
               355.9921875 , 356.9891183 , 357.99358259, 358.99302455])
        
        calib_pol_pm_powers = np.array(
            [0.98007369, 0.97628993, 0.9751786 , 0.97326723, 0.97159376,
               0.96814298, 0.9651895 , 0.96417328, 0.96126307, 0.9589971 ,
               0.95674417, 0.95100149, 0.94847179, 0.94525885, 0.94219295,
               0.93755731, 0.93327625, 0.92950981, 0.92439849, 0.92173905,
               0.91691747, 0.91303859, 0.9091208 , 0.90523757, 0.90081384,
               0.89632091, 0.89193607, 0.88876204, 0.88474478, 0.88307128,
               0.88087455, 0.87840106, 0.87838807, 0.87584971, 0.87350597,
               0.87231246, 0.87143029, 0.87183679, 0.8702238 , 0.87068221,
               0.87103681, 0.87158164, 0.87308216, 0.87683567, 0.87857836,
               0.88047237, 0.88104753, 0.88369831, 0.88608964, 0.88700206,
               0.89109283, 0.89545603, 0.89778251, 0.90194245, 0.90591648,
               0.91106239, 0.9153348 , 0.9184699 , 0.92364606, 0.92635742,
               0.93074223, 0.93436598, 0.93768271, 0.94102105, 0.94557451,
               0.94863612, 0.95296473, 0.95688687, 0.96168683, 0.96588571,
               0.96869645, 0.97259699, 0.97501861, 0.98028991, 0.98246071,
               0.98702281, 0.98756333, 0.99085846, 0.99356977, 0.99512652,
               0.99696003, 0.99707246, 0.99631139, 0.99707245, 0.99717192,
               0.99505735, 0.99464219, 0.99152005, 0.9919611 , 0.98854496,
               0.98582931, 0.98372769, 0.97937746, 0.97572776, 0.97242403,
               0.96887811, 0.96517217, 0.95989225, 0.95587497, 0.95209122,
               0.94849774, 0.94250862, 0.93693025, 0.93146871, 0.93093681,
               0.92742981, 0.92030773, 0.915071  , 0.90960511, 0.90587326,
               0.90108194, 0.89801601, 0.89305174, 0.8877934 , 0.88429073,
               0.87911024, 0.87589728, 0.87137843, 0.86810926, 0.86570064,
               0.86538929, 0.85916228, 0.85898934, 0.85761421, 0.85612233,
               0.85537855, 0.85256345, 0.8497959 , 0.84946293, 0.8496705 ,
               0.8507429 , 0.85455692, 0.85696989, 0.86001852, 0.86507793,
               0.86915573, 0.87270164, 0.87535243, 0.87690916, 0.88165293,
               0.88610693, 0.88932419, 0.8953047 , 0.89894141, 0.9014495 ,
               0.90602893, 0.91076399, 0.9141759 , 0.91837045, 0.92102555,
               0.9249347 , 0.92960926, 0.9329476 , 0.93381679, 0.93810647,
               0.94116373, 0.94543612, 0.94931072, 0.95210851, 0.95707712,
               0.95982737, 0.96561325, 0.96746406, 0.96951809, 0.97262725,
               0.97569316, 0.9762683 , 0.97794612, 0.97967153, 0.98046287,
               0.98183798, 0.9835331 , 0.98604122, 0.98706604, 0.98954387,
               0.99089737, 0.98781417, 0.98653418, 0.98760226, 0.98902927,
               0.98604985, 0.98539257, 0.98261203, 0.97809317, 0.9746813 ,
               0.97137754, 0.96550948, 0.96521545, 0.96238302, 0.95867709,
               0.95683496, 0.95349662, 0.95017988, 0.94502966, 0.94166969,
               0.93888917, 0.93522217, 0.93163733, 0.92899519, 0.92458875,
               0.91979746, 0.91705153, 0.91578452, 0.91232073, 0.90824298,
               0.90423003, 0.8985479 , 0.8941155 , 0.89157284, 0.88842475,
               0.88419128, 0.8819686 , 0.88049401, 0.88010051, 0.87840536,
               0.87726809, 0.87615675, 0.87614377, 0.87612647, 0.87552107,
               0.87502811, 0.87549513, 0.87568972, 0.87745403, 0.87952968,
               0.88135022, 0.88398804, 0.88520315, 0.88810909, 0.88952312,
               0.89267553, 0.89503224, 0.89744954, 0.90018679, 0.90224518,
               0.90544946, 0.91033158, 0.91408939, 0.91736289, 0.91823638,
               0.92291526, 0.92590334, 0.93019736, 0.93457787, 0.93877674,
               0.94489561, 0.94873992, 0.95353554, 0.95798956, 0.96323058,
               0.96589866, 0.97040459, 0.97522184, 0.97795047, 0.98180772,
               0.9828196 , 0.98691472, 0.98963469, 0.99126061, 0.99327138,
               0.99482381, 0.99556759, 0.99727138, 0.99916107, 0.99910486,
               0.99904864, 0.99990486, 1.        , 0.99682596, 0.99504004,
               0.99533841, 0.99264436, 0.99029629, 0.98723903, 0.98417742,
               0.97968448, 0.975451  , 0.97138616, 0.9664911 , 0.9631787 ,
               0.96022952, 0.9567182 , 0.95246311, 0.94724369, 0.94329995,
               0.93785997, 0.93142548, 0.92851952, 0.92470986, 0.9196461 ,
               0.91606992, 0.91284831, 0.90719648, 0.90286788, 0.89904952,
               0.895322  , 0.89037069, 0.88774585, 0.88456748, 0.88077077,
               0.87702163, 0.87575891, 0.87671893, 0.87715566, 0.87772646,
               0.87616541, 0.87512756, 0.87465624, 0.87462595, 0.87376107,
               0.87360542, 0.8740119 , 0.87466053, 0.87893726, 0.88108644,
               0.88395777, 0.8859772 , 0.88677287, 0.8888615 , 0.89071231,
               0.89553388, 0.89753602, 0.90228411, 0.90523758, 0.90920725,
               0.91284833, 0.91532614, 0.92134987, 0.92583847, 0.92971735,
               0.933873  , 0.9379465 , 0.93978432, 0.94391833, 0.94639181,
               0.95155932, 0.95362202, 0.95672687, 0.95677442, 0.96250841,
               0.96363706, 0.96638732, 0.9672781 , 0.9686273 , 0.96917648,
               0.96939269, 0.97150294, 0.9750921 , 0.97640237, 0.97998288,
               0.98245203, 0.98507255, 0.98554391, 0.98722607, 0.98675904,
               0.98638714, 0.98614931, 0.98451906, 0.98357634, 0.98382285])

        power_at_sample = np.array(
        [
            (10, 51e-6),
            (20, 66e-6),
            (30, 80e-6),
            (50, 144e-6),
            (60, 170e-6),
            (70, 222.8e-6),
            (80, 273.6e-6),
            (90,  353e-6),
            (100, 449e-6),
            (110, 0.607e-3),
            (120, 0.779e-3),
            (130, 1.03e-3),
            (150, 1.78e-3),
            (170, 3.47e-3),
            (200, 9.2e-3),
            (210, 13.6e-3),
            (220, 20.25e-3),
            (230, 29e-3),
            (240, 45.25e-3),
            (250, 69.5e-3),
            (260, 137e-3),
            (270,272e-3),
        ])
        
        
        print(calib_pol_angles.shape, calib_pol_pm_powers.shape)
        pol_calib_func = interp1d(calib_pol_angles, calib_pol_pm_powers)
        pw_reverse_func = interp1d(norm(power_at_sample[:,1]), power_at_sample[:,0])
        f = interp1d(power_at_sample[:,0], np.log(norm(power_at_sample[:,1])))
        pw_calib_func = lambda x: np.exp(f(x))
        """
    
    
    
        # spectra:
        
        self.andor_ccd_readout = self.app.measurements['andor_ccd_readout']
        self.andor_ccd_readout.interrupt_measurement_called = self.interrupt_measurement_called

        self.andor_ccd_readout.settings['continuous'] = False
        self.andor_ccd_readout.settings['save_h5'] = False
        
        
    
        # Set up H5
        
        try:
        
            for ii, theta in enumerate(self.theta_array):
                print(ii, theta)
                if self.interrupt_measurement_called:
                    break
            
                pol_pos.update_value(theta)
                self.theta_array[ii] = pol_pos.read_from_hardware()
    
                """
                new_pw_pos = pw_reverse_func(pw_calib_func(initial_powerwheel_pos)*
                                             1.2*pol_calib_func(initial_pol_pos)/pol_calib_func(theta))
                
                pw_pos.update_value(new_pw_pos)
                print("new pw pos:", new_pw_pos)
                time.sleep(0.050)
                """
                
                #time.sleep(0.1)
                
                # collect signal
                power = 0
                n = 10
                for i in range(n):
                    power += pm_power.read_from_hardware()
                self.pm_powers[ii] = power/n
                
                self.apd_count_rates[ii] = apd.settings['count_rate'] ##np.abs(self.theta_array[ii] - theta) 
                
            
                # Spectra:
                self.andor_ccd_readout.interrupt_measurement_called = self.interrupt_measurement_called
                self.andor_ccd_readout.settings['continuous'] = False
                self.andor_ccd_readout.settings['save_h5'] = False
                
                self.start_nested_measure_and_wait(self.andor_ccd_readout)
                if ii == 0:
                    self.log.info("pixel 0: creating data arrays")
                    spec_map_shape = self.theta_array.shape + self.andor_ccd_readout.spectra_data.shape
                    
                    self.spec_map = np.zeros(spec_map_shape, dtype=np.float)
                    self.wls = np.array(self.andor_ccd_readout.wls)

                spec = self.andor_ccd_readout.spectra_data
                self.spec_map[ii,:] = spec
                #if self.settings['save_h5']:
                #self.spec_map_h5[ii,:] = spec


            
        finally:
            # write data to h5 file on disk        
            self.t0 = time.time()
            self.h5_file = h5_io.h5_base_file(app=self.app,measurement=self)
            try:
                #self.h5_file.attrs['time_id'] = self.t0
                
                H = self.h5_meas_group  =  h5_io.h5_create_measurement_group(self, self.h5_file)    
#                 if self.settings['collect_apd_counter']:
                H['apd_count_rates'] = self.apd_count_rates
#                 if self.settings['collect_picoharp']:
#                     H['picoharp_elapsed_time'] = self.picoharp_elapsed_time
#                     H['picoharp_histograms'] = self.picoharp_histograms
#                     H['picoharp_time_array'] = self.picoharp_time_array
#                 if self.settings['collect_hydraharp']:
#                     H['hydraharp_elapsed_time'] = self.hydraharp_elapsed_time
#                     H['hydraharp_histograms'] = self.hydraharp_histograms
#                     H['hydraharp_time_array'] = self.hydraharp_time_array                
#                 if self.settings['collect_winspec_remote_client']:
#                     H['wls'] = self.spec_readout.wls
#                     H['spectra'] = np.squeeze(np.array(self.spectra))
#                     H['integrated_spectra'] = np.array(self.integrated_spectra)
#                 if self.settings['collect_andor_ccd']:
#                     H['wls'] = self.andor_readout.wls
#                     H['spectra'] = np.squeeze(np.array(self.spectra))
#                     H['integrated_spectra'] = np.array(self.integrated_spectra)
#                 if self.settings['collect_ascom_img']:
#                     H['ascom_img_stack'] = np.array(self.ascom_img_stack)
#                     H['ascom_img_integrated'] = np.array(self.ascom_img_integrated)
                    
                H['pm_powers'] = self.pm_powers
                #H['pm_powers_after'] = self.pm_powers_after
                #H['power_wheel_positions'] = self.power_wheel_positions
                H['angles'] = self.theta_array
                H['wls'] = self.wls
                H['spectra'] = self.spec_map
            finally:
                self.log.warning("data saved " + self.h5_file.filename)
                self.h5_file.close()                

            self.andor_ccd_readout.settings['save_h5'] = False

            
            
    def update_display(self):
        def norm(X):
            return X/np.max(X)
        
        theta = self.theta_array*np.pi/180.
        radius = self.apd_count_rates / np.max(self.apd_count_rates)
        
        kk0 = self.wls.searchsorted(420)
        kk1 = self.wls.searchsorted(460)
        
        radius = norm(self.spec_map[:,kk0:kk1].sum(axis=-1))
        
        # Transform to cartesian and plot
        x = radius * np.cos(theta)
        y = radius * np.sin(theta)
        self.polar_plotline.setData(x,y)
        
        radius = norm(self.pm_powers)
        x = radius * np.cos(theta)
        y = radius * np.sin(theta)
        self.polar_plotline_pm_power.setData(x,y)
        
        
        
        
        
        


