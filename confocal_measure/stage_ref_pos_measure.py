from ScopeFoundry.measurement import Measurement
import pyqtgraph as pg
from qtpy import QtWidgets
from ScopeFoundry import helper_funcs
from ScopeFoundry.helper_funcs import sibling_path, load_qt_ui_file
class StageReferencedPositionMeasure(Measurement):
    
    name = 'stage_ref'
    
    def setup(self):
        
        #self.settings.New('ref_pos_file', dtype='file')
        
        #self.settings.New('coord_sys', dtype=str, choices=('Stage') )
        S = self.settings
        
        xy_kwargs = dict(initial = 0.00001,
                          dtype=float,
                          unit='mm',
                          spinbox_decimals = 5,
                          spinbox_step=0.10000)
        
        
        S.New('circ_x0', **xy_kwargs)
        S.New('circ_y0', **xy_kwargs)
        S.New('circ_r', **xy_kwargs)

        S.New('rect_x0', **xy_kwargs)
        S.New('rect_y0', **xy_kwargs)
        S.New('rect_h', **xy_kwargs)
        S.New('rect_w', **xy_kwargs)

        S.New('circ_x_target', **xy_kwargs)
        S.New('circ_y_target',**xy_kwargs)

        S.New('circ_x_pos', ro=True, **xy_kwargs)
        S.New('circ_y_pos', ro=True, **xy_kwargs)
        
        S.New('rect_x_target', **xy_kwargs)
        S.New('rect_y_target', **xy_kwargs)

        S.New('rect_x_pos', ro=True,**xy_kwargs)
        S.New('rect_y_pos', ro=True,**xy_kwargs)
        
        
        self.stage = self.app.hardware['asi_stage']
        
        S.circ_x_target.connect_lq_math((self.stage.settings.x_target, S.circ_x0), 
                                        lambda x, x0: x-x0,
                                        reverse_func= lambda new_x, old: (new_x+old[1], old[1]))
        S.circ_y_target.connect_lq_math((self.stage.settings.y_target, S.circ_y0), 
                                        lambda y, y0: y-y0,
                                        reverse_func= lambda new_y, old: (new_y+old[1], old[1]))
        
        S.rect_x_target.connect_lq_math((self.stage.settings.x_target, S.rect_x0), 
                                        lambda x, x0: x-x0,
                                        reverse_func= lambda new_x, old: (new_x+old[1], old[1]))
        S.rect_y_target.connect_lq_math((self.stage.settings.y_target, S.rect_y0), 
                                        lambda y, y0: y-y0,
                                        reverse_func= lambda new_y, old: (new_y+old[1], old[1]))
        
        
        #self.stage.settings.x_position.add_listener(self.on_stage_pos_update)
        

        S.circ_x_pos.connect_lq_math((self.stage.settings.x_position, S.circ_x0), 
                                        lambda x, x0: x-x0)
        S.circ_y_pos.connect_lq_math((self.stage.settings.y_position, S.circ_y0), 
                                        lambda y, y0: y-y0)
        S.rect_x_pos.connect_lq_math((self.stage.settings.x_position, S.rect_x0), 
                                        lambda x, x0: x-x0)
        S.rect_y_pos.connect_lq_math((self.stage.settings.y_position, S.rect_y0), 
                                        lambda y, y0: y-y0)
        
        self.add_operation('Clear and Plot', self.clear_and_plot)
        
        for lq in self.settings.as_list():
            lq.add_listener(self.update_display)
        self.stage.settings.x_position.add_listener(self.update_display)
        self.stage.settings.y_position.add_listener(self.update_display)
        
            
    
    def setup_figure(self):
        S = self.settings
        stageS = self.stage.settings
        
        self.ui = ui=  load_qt_ui_file(sibling_path(__file__, 'stage_ref_pos.ui'))
        
        self.graph_layout = pg.GraphicsLayoutWidget()
        self.ui.plot_groupBox.layout().addWidget(self.graph_layout)
        
        # Connect Widgets
        stageS.x_position.connect_to_widget(ui.stage_x_pos_doubleSpinBox)
        stageS.y_position.connect_to_widget(ui.stage_y_pos_doubleSpinBox)
        stageS.x_target.connect_to_widget(ui.stage_x_target_doubleSpinBox)
        stageS.y_target.connect_to_widget(ui.stage_y_target_doubleSpinBox)
        stageS.connected.connect_to_widget(ui.stage_hw_connect_checkBox)


        S.circ_x_pos.connect_to_widget(ui.circ_x_pos_doubleSpinBox)
        S.circ_y_pos.connect_to_widget(ui.circ_y_pos_doubleSpinBox)
        S.circ_x_target.connect_to_widget(ui.circ_x_target_doubleSpinBox)
        S.circ_y_target.connect_to_widget(ui.circ_y_target_doubleSpinBox)

        S.rect_x_pos.connect_to_widget(ui.rect_x_pos_doubleSpinBox)
        S.rect_y_pos.connect_to_widget(ui.rect_y_pos_doubleSpinBox)
        S.rect_x_target.connect_to_widget(ui.rect_x_target_doubleSpinBox)
        S.rect_y_target.connect_to_widget(ui.rect_y_target_doubleSpinBox)
        
        
        self.clear_and_plot()
        
        
    def clear_and_plot(self):
        
        self.graph_layout.clear()

        self.xy_plot = self.graph_layout.addPlot(0,0)
        self.xy_plot.setAspectLocked(lock=True, ratio=1)
        self.xy_plot.setLabels(left=('asi y', 'mm'), bottom=('asi x', 'mm'))

        
        # Circle
        self.circ_pen = pg.mkPen((0, 255, 0, 100))
        self.circ_item = QtWidgets.QGraphicsEllipseItem(0, 0, 1, 1)  # x, y, width, height
        self.circ_item.setPen(self.circ_pen)
        self.circ_item.setBrush(pg.mkBrush(None))#pg.mkBrush((50, 50, 200)))
        self.xy_plot.addItem(self.circ_item)
        
        self.circ_cross = self.xy_plot.plot([-1,+1,0,0], [0,0,+1,-1], connect='pairs', pen=self.circ_pen)

        # Rect
        self.rect_pen = pg.mkPen((255, 0, 0, 100))
        self.rect_item = QtWidgets.QGraphicsRectItem(0,0,1,1)
        self.rect_item.setPen(self.rect_pen)
        self.rect_item.setBrush(pg.mkBrush((None)))
        self.xy_plot.addItem(self.rect_item)
        
        self.rect_cross = self.xy_plot.plot([-1,+1,0,0], [0,0,+1,-1], connect='pairs', pen=self.rect_pen)

        

        # Arrow
        self.current_stage_pos_arrow = pg.ArrowItem()
        self.current_stage_pos_arrow.setZValue(100)
        self.xy_plot.addItem(self.current_stage_pos_arrow)
        
        #self.stage = self.gui.hardware_components['dummy_xy_stage']
        #self.stage.x_position.updated_value.connect(self.update_arrow_pos, QtCore.Qt.UniqueConnection)
        #self.stage.y_position.updated_value.connect(self.update_arrow_pos, QtCore.Qt.UniqueConnection)


        self.update_display()
        
    # def on_stage_pos_update(self):
    #     S = self.settings   
    #
    #     x = self.stage.settings['x_position']
    #     y = self.stage.settings['y_position']
    #
    #     S['circ_x_pos'] = x - S['circ_x0']
    #     S['circ_y_pos'] = y - S['circ_y0']
    #
    #     S['rect_x_pos'] = x - S['rect_x0']
    #     S['rect_y_pos'] = y - S['rect_y0']
    #
    #     self.update_display()
        
    
    def update_display(self):
        S = self.settings

        x = self.stage.settings['x_position']
        y = self.stage.settings['y_position']
        
        
        r = S['circ_r']
        self.circ_item.setRect(S['circ_x0']-r, S['circ_y0']-r,2*r, 2*r)
        xC = S['circ_x0']
        xL = xC-r
        xR = xC+r
        yC = S['circ_y0']
        yT = yC+r
        yB = yC-r
        self.circ_cross.setData([xL,xR,xC,xC],[yC,yC,yT,yB])
        
        w = S['rect_w']
        h = S['rect_h']
        self.rect_item.setRect(S['rect_x0']-w/2, S['rect_y0']-h/2, w,h)
        
        xC = S['rect_x0']
        xL = xC-w/2
        xR = xC+w/2
        yC = S['rect_y0']
        yT = yC+h/2
        yB = yC-h/2
        self.rect_cross.setData([xL,xR,xC,xC],[yC,yC,yT,yB])

        
        self.current_stage_pos_arrow.setPos(x,y)

        