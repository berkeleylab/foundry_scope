#from confocal_measure.tiled_large_area_map import TiledLargeAreaMapMeasure
from ScopeFoundry.measurement import Measurement
import numpy as np
import pyqtgraph as pg
import time
from qtpy import QtCore, QtGui, QtWidgets
from qtpy.QtCore import Qt
from ScopeFoundry.helper_funcs import load_qt_ui_file, sibling_path


class BaseTiledLargeAreaMapMeasure(Measurement):
    
    name = "base_tiled_large_area_map"
    
    new_snap_signal = QtCore.Signal()
    
    def setup(self):
        
        #img_scale = self.settings.New("img_scale", dtype=float, unit='um', initial=50.)
        #img_scale.add_listener(self.on_new_img_scale)
        
        lq = self.settings.New("img_opacity", unit="%", dtype=int, initial=100, vmin=0, vmax=100)
        lq.add_listener(self.on_new_img_opacity)

        lq = self.settings.New("edge_fade", unit="%", dtype=int, initial=10, vmin=0, vmax=100)
        lq.add_listener(self.on_new_edge_fade)

        

        self.settings.New("center_x", dtype=float, unit='%', initial=50)
        self.settings.New("center_y", dtype=float, unit='%', initial=50)
        
        
        
        self.add_operation('Clear All', self.clear_snaps)
        self.add_operation('Snap', self.snap)
        
        self.snaps = []
        
        
    
    def setup_figure(self):
        #self.graph_layout = self.ui= pg.GraphicsLayoutWidget()
        
        self.ui = load_qt_ui_file(sibling_path(__file__, "tiled_large_area_map.ui"))
        
        #self.app.hardware['toupcam'].settings.connected.connect_to_widget(self.ui.camera_connect_checkBox)
        self.ui.clear_all_pushButton.clicked.connect(self.clear_snaps)
        self.settings.activation.connect_to_widget(self.ui.run_checkBox)
        self.ui.snap_pushButton.clicked.connect(self.snap)
        
        
        
        self.graph_layout = self.ui.graph_layout
        self.plot = self.graph_layout.addPlot()
        self.img_item = pg.ImageItem()
        self.plot.addItem(self.img_item)
        self.img_item.setZValue(1000)

        self.plot.setAspectLocked(lock=True, ratio=1)
        
        """self.table_view = QtWidgets.QTableView()
        
        self.table_view_model = SnapsQTabelModel(snaps=self.snaps)
        self.table_view.setModel(self.table_view_model)
        self.table_view.show()
        
        self.new_snap_signal.connect(self.table_view_model.on_update_snaps)
        """
        
        self.plot.scene().sigMouseClicked.connect(self.on_scene_clicked)
        
        self.graph_layout_eventProxy = EventProxy(self.graph_layout, self.graph_layout_event_filter)
        
        self.current_stage_pos_arrow = pg.ArrowItem()
        self.current_stage_pos_arrow.setZValue(1001)
        self.plot.addItem(self.current_stage_pos_arrow)
        
        # fstage = self.app.hardware['mcl_xyz_stage']
        # cstage = self.app.hardware['asi_stage']
        #
        # fstage.settings.x_position.updated_value.connect(self.update_arrow_pos, QtCore.Qt.UniqueConnection)
        # fstage.settings.y_position.updated_value.connect(self.update_arrow_pos, QtCore.Qt.UniqueConnection)
        # cstage.settings.x_position.updated_value.connect(self.update_arrow_pos, QtCore.Qt.UniqueConnection)
        # cstage.settings.y_position.updated_value.connect(self.update_arrow_pos, QtCore.Qt.UniqueConnection)


        self.fine_stage_border_plotline = self.plot.plot([0,1,1,0],[0,0,1,1],pen='r')

    def update_arrow_pos(self):
        x,y = self.get_current_stage_position()
        self.current_stage_pos_arrow.setPos(x,y)
        #
        # fstage = self.app.hardware['mcl_xyz_stage']
        # cstage = self.app.hardware['asi_stage']
        #
        # fx, fy = fstage.settings['x_position'] , fstage.settings['y_position']
        # cx, cy = 1e3*cstage.settings['x_position'] , 1e3*cstage.settings['y_position']
        #
        #
        # #print(x, cx+fx, y, cy+fy)
        #
        # self.current_stage_pos_arrow.setPos(cx+fx,cy+fy)
        self.update_fine_stage_border()
        
    def update_fine_stage_border(self):
        cx,cy = self.get_coarse_stage_position()
        f_xmin, f_xmax, f_ymin, f_ymax = self.get_fine_stage_limits() 
        x0 = cx + f_xmin
        # x1 = x0 + fstage.settings['x_max']
        x1 = cx + f_xmax
        y0 = cy + f_ymin
        # y1 = y0 + fstage.settings['y_max']
        y1 = cy + f_ymax
        
        self.fine_stage_border_plotline.setData(
             [x0, x1,x1,x0, x0], [y0, y0, y1, y1,y0]
             )
            
        self.fine_stage_border_plotline.setZValue(1002)
    
    
    def update_display(self):
        im_alpha = edge_fade_img(self.im, tukey_alpha=0.01*self.settings['edge_fade'])
        #print(alpha.shape, self.im.shape)
        self.img_item.setImage(im_alpha)
        self.img_rect = self.get_current_rect()
        self.img_item.setRect(self.img_rect)
        
    def get_current_stage_position(self):
        # fstage = self.app.hardware['mcl_xyz_stage']
        # cstage = self.app.hardware['asi_stage']
        #
        # x = 1e3*cstage.settings['x_position'] + fstage.settings['x_position']
        # y = 1e3*cstage.settings['y_position'] + fstage.settings['y_position']
        # return x,y
        fx, fy = self.get_fine_stage_position()
        cx, cy = self.get_coarse_stage_position()
        return cx+fx, cy+fy
    
    def get_fine_stage_position(self):
        return 0,0
    
    def get_coarse_stage_position(self):
        return 0,0
    
    def get_fine_stage_limits(self):
        #f_xmin, f_xmax, f_ymin, f_ymax
        return -1,+1,-1,+1


    
    def get_current_rect(self, x=None, y=None):
        if x is None:
            x,y = self.get_current_stage_position()
        scale = self.settings['img_scale']
        S = self.settings
        return pg.QtCore.QRectF(x-S['center_x']*scale/100,
                                y-S['center_y']*scale*self.im_aspect/100, 
                                scale,
                                scale*self.im_aspect)
        
        
    # def get_image(self):
        # cam = self.app.hardware['toupcam'].cam
        #
        # data = cam.get_image_data()
        # raw = data.view(np.uint8).reshape(data.shape + (-1,))
        # bgr = raw[..., :3]
        # return bgr[..., ::-1]
        
    def get_live_image(self):
        import skimage.data
        return skimage.data.chelsea()
    
    def get_image(self):
        import skimage.data
        return skimage.data.chelsea()


    def run(self):
        
        #tcam = self.app.hardware['toupcam']
        #tcam.settings['connected'] = True
        
        
        # fstage = self.app.hardware['mcl_xyz_stage']
        # cstage = self.app.hardware['asi_stage']
        #
        # fstage.settings['connected'] = True
        # cstage.settings['connected'] = True
        
        self.im = self.get_image()
#        self.im = np.flip(self.im.swapaxes(0,1),0)
        self.im_aspect = self.im.shape[1]/self.im.shape[0]
        
        from ScopeFoundry import h5_io

        try:
            self.h5_file = h5_io.h5_base_file(app=self.app, measurement=self)
            H = self.h5_meas_group  =  h5_io.h5_create_measurement_group(self, self.h5_file)
    
            self.snaps_h5 = H.create_dataset('snaps', (self.im.shape + (1,)), dtype=np.uint8, maxshape=(self.im.shape +(None,)))
            self.snaps_c_pos_h5 = H.create_dataset('snaps_coarse_pos', (2,1), dtype='float', maxshape=(2,None))
            self.snaps_f_pos_h5 = H.create_dataset('snaps_fine_pos', (2,1), dtype='float', maxshape=(2,None))
            self.snaps_pos_h5 = H.create_dataset('snaps_pos', (2,1), dtype='float', maxshape=(2,None))
    
            while not self.interrupt_measurement_called:
                self.im = self.get_live_image()
                #self.im = np.flip(self.im.swapaxes(0,1),0)
                self.im_aspect = self.im.shape[1]/self.im.shape[0]
                time.sleep(0.1)
        finally:
            self.h5_file.close()
            
    def snap(self):

        snap = dict()
        
        snap['img'] = self.im.copy()
        snap['img_item'] = pg.ImageItem(edge_fade_img(snap['img'], tukey_alpha=0.01*self.settings['edge_fade']))
        #snap['img_item_bg'] = pg.ImageItem(self.im)
        snap['img_rect'] = self.get_current_rect()
        snap['img_item'].setRect(snap['img_rect'])
        #snap['img_item_bg'].setRect(snap['img_rect'])
        
        #fstage = self.app.hardware['mcl_xyz_stage'] # Fine
        #cstage = self.app.hardware['asi_stage'] # Coarse
        
        fx, fy = self.get_fine_stage_position()
        cx, cy = self.get_coarse_stage_position()

        snap['fine_pos'] = (fx, fy) #(fstage.settings['x_position'], fstage.settings['y_position']) # FIXME
        snap['coarse_pos'] = (cx, cy) #(cstage.settings['x_position']*1e3, cstage.settings['y_position']*1e3) #FIXME
        
        fx, fy = snap['fine_pos']
        cx, cy = snap['coarse_pos']

        snap['pos'] = (cx+fx, cy+fy)
        
        #snap['img_item_bg'].setZValue(-1)
        #self.plot.addItem(snap['img_item_bg'])
        self.plot.addItem(snap['img_item'])
        
        self.snaps.append(snap)
        print ("SNAP")
                

        ## Write to H5
        self.snaps_h5.resize((self.im.shape +( len(self.snaps),)))
        self.snaps_h5[:,:,:,-1] = self.im
        print("shape", self.snaps_h5.shape)
        self.snaps_c_pos_h5.resize((2, len(self.snaps)))
        self.snaps_c_pos_h5[:,-1] = snap['coarse_pos']
        self.snaps_f_pos_h5.resize((2, len(self.snaps)))
        self.snaps_f_pos_h5[:,-1] = snap['fine_pos']
        self.snaps_pos_h5.resize((2, len(self.snaps)))
        self.snaps_pos_h5[:,-1] = snap['pos']
        
        # TODO update LQ's in H5
        
        self.new_snap_signal.emit()
    
    def clear_snaps(self):

        for snap in self.snaps:
            self.plot.removeItem(snap['img_item'])
            
        self.snaps = []
        
    def on_new_img_scale(self):
        for snap in self.snaps:
            x,y = snap['pos']
            snap['img_rect'] = self.get_current_rect(x,y)
            snap['img_item'].setRect(snap['img_rect'])

    def on_new_img_opacity(self):
        op = self.settings['img_opacity']*0.01
        if not hasattr(self, 'img_item'):
            return        
        self.img_item.setOpacity(op)
        for snap in self.snaps:
            snap['img_item'].setOpacity(op)
            
    def on_new_edge_fade(self):
        if not hasattr(self, 'im'):
            return
        im_alpha = edge_fade_img(self.im, tukey_alpha=0.01*self.settings['edge_fade'])
        self.img_item.setImage(im_alpha)
        for snap in self.snaps:
            im_alpha = edge_fade_img(snap['img'], tukey_alpha=0.01*self.settings['edge_fade'])
            snap['img_item'].setImage(im_alpha)
            
            
    def on_scene_clicked(self, event):
        p = self.plot
        viewbox = p.vb
        pos = event.scenePos()
        if not p.sceneBoundingRect().contains(pos):
            return
                
        pt = viewbox.mapSceneToView(pos)
        print("on_scene_clicked", pt.x(), pt.y())
        
        
        x = pt.x()
        y = pt.y()
        
        
        #fstage = self.app.hardware['mcl_xyz_stage']
        #cstage = self.app.hardware['asi_stage']
        
        #fx, fy = fstage.settings['x_position'] , fstage.settings['y_position'] # FIXME
        #cx, cy = 1e3*cstage.settings['x_position'] , 1e3*cstage.settings['y_position'] # FIXME

        fx, fy = self.get_fine_stage_position()
        cx, cy = self.get_coarse_stage_position()

        #x0,y0 = self.get_current_stage_position()
        x0 = cx + fx
        y0 = cy + fy
        
        dx = x - x0
        dy = y - y0
        
        print("dx, dy", dx,dy)
        
        #self.plot.plot([x0,x],[y0,y], pen='r')


        
        
        # Move coarse stage
        if  event.modifiers() == QtCore.Qt.ShiftModifier and event.double():            
            print('Shift+Click', 'double click')
                        
#             cstage.settings['x_target'] = (x-fx)/1000
#             cstage.settings['y_target'] = (y-fy)/1000
            #cstage.settings['x_target'] = cstage.settings['x_position'] + 1e-3*dx
            #cstage.settings['y_target'] = cstage.settings['y_position'] + 1e-3*dy
            self.move_coarse_stage_rel(dx, dy)
            
        # Move fine stage
        if  event.modifiers() == QtCore.Qt.ControlModifier and event.double():            
            print('Shift+Click', 'double click')
            
            self.move_fine_stage_rel(dx, dy)
            # fstage.settings['x_target'] = fstage.settings['x_position'] + dx
            # fstage.settings['y_target'] = fstage.settings['y_position'] + dy

    def graph_layout_event_filter(self, obj,event):
        #print(self.name, 'eventFilter', obj, event)
        try:
            if type(event) == QtGui.QKeyEvent:
                
                if event.key() == QtCore.Qt.Key_Space:
                    self.snap()
                    print(event.key(), repr(event.text()), event.isAutoRepeat())
                    #event.accept()
                    #return True
        finally:
            # standard event processing            
            return QtCore.QObject.eventFilter(self,obj, event)
            #return False

class EventProxy(QtCore.QObject):
    def __init__(self, qobj, callback):
        QtCore.QObject.__init__(self)
        self.callback = callback
        qobj.installEventFilter(self)
        
    def eventFilter(self, obj, ev):
        return self.callback(obj, ev)


class SnapsQTabelModel(QtCore.QAbstractTableModel):
    
    def __init__(self, snaps,*args, **kwargs):
        self.snaps  = snaps
        QtCore.QAbstractTableModel.__init__(self, *args, **kwargs)
        
    def rowCount(self, *args, **kwargs):
        return len(self.snaps)
    
    def columnCount(self, *args, **kwargs):
        return 5
    
    
    def on_update_snaps(self):
        self.layoutChanged.emit()
        
    def data(self, index, role=Qt.DisplayRole):
        print("table model data", index, role)
        if index.isValid():
            print("valid")
            if role == Qt.DisplayRole or role==Qt.EditRole:
                row = index.row()
                col = index.column()
                text = "{} {}".format(row,col)
                print(text, index)
                return text 
        else:
            print("no data", index)
            return None

def edge_fade_img(im, tukey_alpha=0.5):
    """Converts a Ny x Ny x 3 RGB uint image into an
    RGBA image with a Tukey window as the alpha channel
    for fading overlay tiles together.
    tukey_alpha = 0 -->
    tukey_alpha = 1 -->
    """
    Ny, Nx = im.shape[0:2]
    from scipy.signal import tukey
    alpha_x = tukey(Nx, alpha=tukey_alpha)
    alpha_y = tukey(Ny, alpha=tukey_alpha)
    alpha = 255*alpha_x.reshape(1,Nx)*alpha_y.reshape(Ny,1)
    im_alpha = np.dstack((im, alpha.astype(int)))
    return im_alpha

class TiledAreaMapGridMeasure(Measurement):
    
    name = 'tiled_map_grid'
    
    def setup(self):
        
        self.xr = self.settings.New_Range('x', include_center_span=True)
        self.yr = self.settings.New_Range('y', include_center_span=True)
        #self.xr.add_listener(self.on_new_grid)
        #self.yr.add_listener(self.on_new_grid)
        self.settings.New('show_grid', dtype=bool, initial=False)
        

        
    def setup_figure(self):
        #TiledAreaMapGridMeasure.setup_figure(self)
        area_map_measure = self.app.measurements[self.area_map_measure_name]
        self.grid_plotline = area_map_measure.plot.plot([1,2,3],[3,2,1])
        
    def on_new_grid(self):
        print("on_new_grid", self.xr.array, self.yr.array)
        X, Y = np.meshgrid(self.xr.array, self.yr.array)
        if hasattr(self, 'grid_plotline'):
            self.grid_plotline.setData(X.flat, Y.flat)
        
class TiledAreaMapGridSEMMeasure(TiledAreaMapGridMeasure):
    
    name = 'tiled_map_grid_sem'
    
    area_map_measure_name = 'tiled_map_sem'
    
    

class TiledLargeAreaMapSEMMeasure(BaseTiledLargeAreaMapMeasure):
    
    name = "tiled_map_sem"

    def setup_figure(self):
        BaseTiledLargeAreaMapMeasure.setup_figure(self)
        
        remcon = self.app.hardware['sem_remcon']
        remcon.settings.stage_x.updated_value.connect(self.update_arrow_pos, QtCore.Qt.UniqueConnection)
        remcon.settings.stage_y.updated_value.connect(self.update_arrow_pos, QtCore.Qt.UniqueConnection)
        remcon.settings.magnification.updated_value.connect(self.update_arrow_pos, QtCore.Qt.UniqueConnection)


    def get_fine_stage_limits(self):
        #f_xmin, f_xmax, f_ymin, f_ymax
        remcon = self.app.hardware['sem_remcon']

        f  = 0.5*remcon.settings['full_size']*1000

        return -f,+f,-f,+f

    def get_fine_stage_position(self):
        return 0,0
    
    def get_coarse_stage_position(self):
        remcon = self.app.hardware['sem_remcon']
        h,v = -remcon.settings['stage_y'], remcon.settings['stage_x']
        return h,v
    
    def get_current_rect(self, x=None, y=None):
        remcon = self.app.hardware['sem_remcon']        
        if x is None:
            x,y = self.get_current_stage_position()
        scale  = remcon.settings['full_size']*1000
        #scale = self.settings['img_scale']
        # scale for sem computed from remcom full_size
        S = self.settings
        return pg.QtCore.QRectF(x-S['center_x']*scale/100,
                                y-S['center_y']*scale*self.im_aspect/100, 
                                scale,
                                scale*self.im_aspect)


        
    def get_live_image(self):
        #import skimage.data
        #return skimage.data.chelsea()[:256,:256]
        M = self.app.measurements['sync_raster_scan']
        grey =  50*M.display_image_map[0].swapaxes(0,1)
        return np.stack([grey,grey,grey], axis=-1)
    
    def get_image(self):
        #import skimage.data
        #return skimage.data.chelsea()[:256,:256]
        M = self.app.measurements['sync_raster_scan']
        grey =  50*M.display_image_map[0].swapaxes(0,1)
        return np.stack([grey,grey,grey], axis=-1)
    
    
    def move_coarse_stage_rel(self, dx, dy):
        remcon = self.app.hardware['sem_remcon']        
        remcon.remcon.set_stage_delta(x=dy, y=-dx)
        # wait until move is complete
        self.wait_until_move_complete()