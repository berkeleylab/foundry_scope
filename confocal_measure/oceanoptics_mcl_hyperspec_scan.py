from ScopeFoundryHW.mcl_stage import MCLStage2DSlowScan
import numpy as np
import time

class OceanOpticsMCLHyperSpec2DScan(MCLStage2DSlowScan):
    
    name = "oo_mcl_hyperspec_scan"
    
    def scan_specific_setup(self):
        self.spec = self.app.measurements['oo_spec_live']
        MCLStage2DSlowScan.scan_specific_setup(self) 
        
    def setup_figure(self):
        MCLStage2DSlowScan.setup_figure(self)
        self.spec.roi.sigRegionChanged.connect(self.recompute_image_map)        

    def pre_scan_setup(self):
        self.settings.save_h5.change_readonly(True)
        if 'bg_subtract' in self.spec.settings:
            self.spec.settings['bg_subtract'] = False
        self.spec.settings['continuous'] = False
        self.spec.settings['save_h5'] = False
        time.sleep(0.01)
                
    def collect_pixel(self, pixel_num, k, j, i):
        #if self.settings['debug_mode']: print("collect_pixel", pixel_num, k,j,i)
        self.spec.interrupt_measurement_called = self.interrupt_measurement_called
        # self.start_nested_measure_and_wait(self.spec)
        self.spec.run()
        print('spectrometer run complete')
        if pixel_num == 0:
            self.log.info("pixel 0: creating data arrays")
            #if self.settings['debug_mode']: print("pixel 0: creating data arrays")
            self.time_array = np.zeros(self.scan_h_positions.shape)
            spec_map_shape = self.scan_shape + self.spec.spectrum.shape
            
            self.spec_map = np.zeros(spec_map_shape, dtype=np.float)
            if self.settings['save_h5']:
                self.spec_map_h5 = self.h5_meas_group.create_dataset(
                                      'spec_map', spec_map_shape, dtype=np.float)
            else:
                self.spec_map_h5 = np.zeros(spec_map_shape)
 
            self.wls = np.array(self.spec.wls)
            if self.settings['save_h5']:
                self.h5_meas_group['wls'] = self.wls

        # store in arrays
        t = self.time_array[pixel_num] = time.time()
        print('time', t)
        spec = np.array(self.spec.spectrum)
        print(f"collect_pixel, {pixel_num}, {k}, {j}, {i}, {spec.shape}")
        self.spec_map[k,j,i,:] = spec
        self.spec_map_h5[k,j,i,:] = spec
        s = self.display_image_map[k,j,i] = spec.sum()
        #if self.settings['debug_mode']: print('time', t, 'sum', s)

        ind_min = np.searchsorted(self.wls,self.spec.settings.roi_min.val)
        ind_max = np.searchsorted(self.wls,self.spec.settings.roi_max.val)
        self.display_image_map[k,j,i] = self.spec_map[k,j,i,ind_min:ind_max].sum()
        if self.spec.settings['baseline_subtract']:
            self.display_image_map[k,j,i] -= self.spec.settings.baseline_val.val*(ind_max-ind_min)
        if pixel_num == 0 and self.settings['save_h5']:
            self.h5_meas_group.create_dataset('dark_indices', data=self.spec.hw.get_dark_indices())
            
    def post_scan_cleanup(self):
        self.settings.save_h5.change_readonly(False)
        self.stage.other_observer = False
        self.ui.setWindowTitle(self.name)

    
    def recompute_image_map(self):
        if not hasattr(self, 'display_image_map'):
            self.display_image_map = np.zeros(self.scan_shape)
            
        ind_min = np.searchsorted(self.wls,self.spec.settings.roi_min.val)
        ind_max = np.searchsorted(self.wls,self.spec.settings.roi_max.val)
        if self.settings['debug']: print("Min %d Max %d" % (ind_min,ind_max))
        self.display_image_map = np.sum(self.spec_map[:,:,:,ind_min:ind_max],axis=-1)
        if self.spec.settings['baseline_subtract']:
            non_zero_index = np.nonzero(self.display_image_map)
            self.display_image_map[non_zero_index] -= self.spec.settings.baseline_val.val*(ind_max-ind_min)
            
    def interrupt(self):
        MCLStage2DSlowScan.interrupt(self)
        self.spec.interrupt()

    def update_display(self):
        #self.update_time()
        MCLStage2DSlowScan.update_display(self)
        self.spec.update_display()
