from ScopeFoundryHW.sync_stream_scan.sync_stream_scan import SyncStreamScan
import numpy as np
import pyqtgraph as pg
import time
#from .kde_interp_fft import KDE_Image
from ScopeFoundry import h5_io
from ScopeFoundryHW.sync_stream_scan.lj_scan import LJScan
import PyDAQmx as mx


class HyperspecCL_LJScan(LJScan):

    name = 'hyperspec_cl_lj_scan'
    
    def setup(self):
        LJScan.setup(self)
        
        self.settings.New("spec_time", dtype=float, unit='s', initial=0.005, si=True)
    

    def on_start_of_run(self):
        LJScan.on_start_of_run(self)
        
        #mx.DAQmxConnectTerms(b"/X-6368/port0/line0", b"/X-6368/PFI4", mx.DAQmx_Val_DoNotInvertPolarity )

    def do_stream_gen(self, t):
        
        spec_time = self.settings['spec_time']
        
        do_stream = np.zeros((self.N_do, self.do_chan_count), dtype=np.uint8)
        pulses = t % spec_time < 0.1*spec_time # np.zeros(len(t), dtype=np.uint32)
        do_stream[:,1] = pulses
        #do_stream[(t*self.do_rate).astype(int)] = 0b0001 
        #do_stream[(t*self.do_rate).astype(int)%2 == 0 ] |= 0b0001
        #print("do_stream_gen", do_stream[:15], t[:15])
        return do_stream
    
    
    def handle_new_data(self):
        LJScan.handle_new_data(self)
        
        # handle CL data