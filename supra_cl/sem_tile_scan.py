from ScopeFoundry import Measurement
import pyqtgraph as pg
from qtpy import QtCore
import numpy as np

class SEMTileScan(Measurement):
    
    name = 'sem_tile_scan'
    
    def setup(self):
        
        pass
    
    def setup_figure(self):
        
        self.ui = self.graph_layout = pg.GraphicsLayoutWidget()
        
        self.plot = self.graph_layout.addPlot()

        self.current_stage_pos_arrow = pg.ArrowItem()
        self.current_stage_pos_arrow.setZValue(1001)
        self.plot.addItem(self.current_stage_pos_arrow)

        remcon = self.app.hardware['sem_remcon']
        remcon.settings.stage_x.updated_value.connect(self.update_arrow_pos, QtCore.Qt.UniqueConnection)
        remcon.settings.stage_y.updated_value.connect(self.update_arrow_pos, QtCore.Qt.UniqueConnection)
        remcon.settings.magnification.updated_value.connect(self.update_arrow_pos, QtCore.Qt.UniqueConnection)
        
        
        def circ_coords(cx, cy, r):
            angles = np.linspace(0,2*np.pi*100)
            x = r*np.sin(angles)
            y = r*np.cos(angles)
            return x, y
        self.sample_holder_outline = self.plot.plot(*circ_coords(56.5, 56.5, 20.0))
        self.scan_area_outline = self.plot.plot()
        

    def update_arrow_pos(self):
        # x,y = self.get_current_stage_position()
        #
        # fstage = self.app.hardware['mcl_xyz_stage']
        # cstage = self.app.hardware['asi_stage']
        #
        # fx, fy = fstage.settings['x_position'] , fstage.settings['y_position']
        # cx, cy = 1e3*cstage.settings['x_position'] , 1e3*cstage.settings['y_position']
        #
        # x0 = cx
        # x1 = x0 + fstage.settings['x_max']
        # y0 = cy
        # y1 = y0 + fstage.settings['y_max']
        #
        # self.fine_stage_border_plotline.setData(
            # [x0, x1,x1,x0, x0], [y0, y0, y1, y1,y0]
            # )
            #
        # self.fine_stage_border_plotline.setZValue(1002)
        #
        # #print(x, cx+fx, y, cy+fy)
        
        #self.current_stage_pos_arrow.setPos(cx+fx,cy+fy)
        remcon = self.app.hardware['sem_remcon']
        h,v = -remcon.settings['stage_y'], remcon.settings['stage_x']
        self.current_stage_pos_arrow.setPos(h, v)

        import numpy as np
                
        def circ_coords(cx, cy, r):
            angles = np.linspace(0,2*np.pi,100)
            x = r*np.sin(angles) + cx
            y = r*np.cos(angles) + cy
            return x, y

        self.sample_holder_outline.setData(*circ_coords(-65, 65, 28.25))
        
        self.plot.setAspectLocked(1)
        
        if not hasattr(self, 'scan_area_outline'):
            self.scan_area_outline = self.plot.plot()
        
        print("ASdf")
        f  = remcon.settings['full_size']*1000
        x0 = h - f/2.
        x1 = h + f/2.
        y0 = v - f/2.
        y1 = v + f/2.
        print(h,v,f)
        print([x0, x1, x1, x0, x0], [y0, y0,y1,y1,y0])
        self.scan_area_outline.setData([x0, x1, x1, x0, x0], [y0, y0,y1,y1,y0])
        