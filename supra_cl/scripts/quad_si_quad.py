import os
import time


save_dir = app.settings['save_dir']
Msrs = app.measurements['sync_raster_scan']
Mq   = app.measurements['cl_quad_view']
Msi  = app.measurements['hyperspec_cl']
Msiq = app.measurements['hyperspec_cl_quad_view']

moose = 'asdf'


def start_measurement_and_wait(M):
    M.settings['activation'] = True
    time.sleep(0.1)
    while M.is_measuring():
        app.qtapp.processEvents()


with open(os.path.join(save_dir, f"experiment_{int(time.time())}.txt"), 'w') as exp_file:

    Mq.settings['n_pixels'] = 256
    Msrs.settings['adc_rate'] = 2e6
    Msrs.settings['adc_oversample'] = 25
    Msrs.settings['continuous_scan'] = False
    Msrs.settings['save_h5'] = True
    Msrs.settings['description'] = """\
    Hello this is a test
    
    weee2e!
    """
    
    
    Msiq.settings['n_pixels'] = 8
    Msi.settings['adc_rate'] = 2e3
    Msi.settings['adc_oversample'] = 1000
    Msi.settings['continuous_scan'] = False
    Msi.settings['save_h5'] = True

    
    Mq.settings['beam_blank_after'] = True
    Msiq.settings['beam_blank_after'] = True
    
    
            
    start_measurement_and_wait(Mq)
    
    exp_file.write(Msrs.h5_filename)
    exp_file.write("\n")
    
    
    start_measurement_and_wait(Msiq)
    
    exp_file.write(Msi.h5_filename)
    exp_file.write("\n")
    